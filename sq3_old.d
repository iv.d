/* Invisible Vector Library
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// sqlite3 helpers
module iv.sq3_old /*is aliced*/;
//pragma(lib, "sqlite3");

//version = sq3_debug_stmtlist;

import iv.alice;

//public import etc.c.sqlite3;
public import iv.c.sqlite3;
import std.traits;
import std.range.primitives;

private import std.internal.cstring : tempCString;

version(none) {
public {
  //enum SQLITE_DETERMINISTIC = 0x800;
  enum SQLITE_DIRECTONLY = 0x000080000;
  enum SQLITE_SUBTYPE    = 0x000100000;
  enum SQLITE_INNOCUOUS  = 0x000200000;

  enum SQLITE_PREPARE_PERSISTENT = 0x01U;
  enum SQLITE_PREPARE_NORMALIZE  = 0x02U;
  enum SQLITE_PREPARE_NO_VTAB    = 0x04U;

extern(C) {
  int sqlite3_prepare_v3(
    sqlite3 *db,            /** Database handle */
    const(char)*zSql,       /** SQL statement, UTF-8 encoded */
    int nByte,              /** Maximum length of zSql in bytes. */
    uint prepFlags,         /* Zero or more SQLITE_PREPARE_ flags */
    sqlite3_stmt **ppStmt,  /** OUT: Statement handle */
    const(char*)*pzTail     /** OUT: Pointer to unused portion of zSql */
  );
}
}
}


////////////////////////////////////////////////////////////////////////////////
mixin(NewExceptionClass!("SQLiteException", "Exception"));

class SQLiteErr : SQLiteException {
  int code;

  this (string msg, string file=__FILE__, usize line=__LINE__, Throwable next=null) @trusted nothrow {
    code = SQLITE_ERROR;
    super("SQLite ERROR: "~msg, file, line, next);
  }

  this (sqlite3* db, int rc, string file=__FILE__, usize line=__LINE__, Throwable next=null) @trusted nothrow {
    //import core.stdc.stdio : stderr, fprintf;
    //fprintf(stderr, "SQLITE ERROR: %s\n", sqlite3_errstr(rc));
    code = rc;
    if (rc == SQLITE_OK) {
      super("SQLite ERROR: no error!", file, line, next);
    } else {
      import std.exception : assumeUnique;
      import std.string : fromStringz;
      if (db) {
        super(sqlite3_errstr(sqlite3_extended_errcode(db)).fromStringz.assumeUnique, file, line, next);
      } else {
        super(sqlite3_errstr(rc).fromStringz.assumeUnique, file, line, next);
      }
    }
  }
}


public void sq3check (sqlite3* db, int rc, string file=__FILE__, usize line=__LINE__) {
  //pragma(inline, true);
  if (rc != SQLITE_OK) throw new SQLiteErr(db, rc, file, line);
}


////////////////////////////////////////////////////////////////////////////////
/*
//k8: nope, don't do this, because users may want to call `sqlite3_config()` before it
shared static this () {
  if (sqlite3_initialize() != SQLITE_OK) throw new Error("can't initialize SQLite");
}

// and this is not required at all
shared static ~this () {
  sqlite3_shutdown();
}
*/


// use this in `to` to avoid copying
public alias SQLStringc = const(char)[];
// use this in `to` to avoid copying
public alias SQ3Blob = const(char)[];
// use this in `to` to avoid copying
public alias SQ3Text = const(char)[];


////////////////////////////////////////////////////////////////////////////////
// WARNING! don't forget to finalize ALL prepared statements!
struct Database {
private:
  static struct DBInfo {
    sqlite3* db = null;
    uint rc = 0;
    usize onCloseSize = 0;
    char *onClose = null; // 0-terminated
    DBStatement.Data *stmthead = null;
    DBStatement.Data *stmttail = null;
  }

  DBInfo* dbi = null;

private:
  // caller should perform all necessary checks
  void clearStatements () nothrow @trusted {
    while (dbi.stmthead !is null) {
      version(sq3_debug_stmtlist) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "clearStatements(%p): p=%p\n", dbi, dbi.stmthead); }
      DBStatement.Data *dd = dbi.stmthead;
      dbi.stmthead = dd.next;
      dd.owner = null;
      dd.prev = null;
      dd.next = null;
      dd.stepIndex = 0;
      if (dd.st !is null) {
        sqlite3_reset(dd.st);
        sqlite3_clear_bindings(dd.st);
        sqlite3_finalize(dd.st);
        dd.st = null;
      }
    }
    dbi.stmttail = null;
  }

public:
  enum Mode {
    ReadOnly,
    ReadWrite,
    ReadWriteCreate,
  }

public:
  // `null` schema means "open as R/O"
  // non-null, but empty schema means "open as r/w"
  // non-empty scheme means "create if absent"
  this (const(char)[] name, Mode mode, const(char)[] pragmas=null, const(char)[] schema=null) { openEx(name, mode, pragmas, schema); }
  this (const(char)[] name, const(char)[] schema) { openEx(name, (schema !is null ? Mode.ReadWriteCreate : Mode.ReadOnly), null, schema); }
  ~this () nothrow @trusted { close(); }

  this (this) nothrow @trusted @nogc { if (dbi !is null) ++dbi.rc; }

  @property bool isOpen () const pure nothrow @safe @nogc { return (dbi !is null && dbi.db !is null); }

  void close () nothrow @trusted {
    if (dbi !is null) {
      if (--dbi.rc == 0) {
        import core.stdc.stdlib : free;
        if (dbi.db !is null) {
          clearStatements();
          if (dbi.onClose !is null) {
            auto rc = sqlite3_exec(dbi.db, dbi.onClose, null, null, null);
            if (rc != SQLITE_OK) {
              import core.stdc.stdio : stderr, fprintf;
              fprintf(stderr, "SQLITE ERROR ON CLOSE: %s\n", sqlite3_errstr(sqlite3_extended_errcode(dbi.db)));
            }
            version(none) {
              import core.stdc.stdio : stderr, fprintf;
              fprintf(stderr, "exec:===\n%s\n===\n", dbi.onClose);
            }
            free(dbi.onClose);
          }
          sqlite3_close_v2(dbi.db);
        }
        free(dbi);
      }
      dbi = null;
    }
  }

  void appendOnClose (const(char)[] stmts) {
    if (!isOpen) throw new SQLiteException("database is not opened");
    while (stmts.length && stmts[0] <= 32) stmts = stmts[1..$];
    while (stmts.length && stmts[$-1] <= 32) stmts = stmts[0..$-1];
    if (stmts.length == 0) return;
    import core.stdc.stdlib : realloc;
    //FIXME: overflow. don't do it.
    usize nsz = dbi.onCloseSize+stmts.length;
    if (nsz+1 <= dbi.onCloseSize) throw new SQLiteException("out of memory for OnClose");
    char *np = cast(char *)realloc(dbi.onClose, nsz+1);
    if (np is null) throw new SQLiteException("out of memory for OnClose");
    dbi.onClose = np;
    np[dbi.onCloseSize..dbi.onCloseSize+stmts.length] = stmts[];
    dbi.onCloseSize += stmts.length;
    np[dbi.onCloseSize] = 0;
  }

  void setOnClose (const(char)[] stmts) {
    if (!isOpen) throw new SQLiteException("database is not opened");
    if (dbi.onClose !is null) {
      import core.stdc.stdlib : free;
      free(dbi.onClose);
      dbi.onClose = null;
      dbi.onCloseSize = 0;
    }
    appendOnClose(stmts);
  }

  // `null` schema means "open as R/O"
  // non-null, but empty schema means "open as r/w"
  // non-empty scheme means "create is absend"
  void openEx (const(char)[] name, Mode mode, const(char)[] pragmas=null, const(char)[] schema=null) {
    close();
    import core.stdc.stdlib : malloc, free;
    import std.internal.cstring;
    immutable bool allowWrite = (mode == Mode.ReadWrite || mode == Mode.ReadWriteCreate);
    bool allowCreate = (mode == Mode.ReadWriteCreate);
    if (allowCreate) {
      while (schema.length && schema[$-1] <= ' ') schema = schema[0..$-1];
      if (schema.length == 0) allowCreate = false;
    }
    dbi = cast(DBInfo *)malloc(DBInfo.sizeof);
    if (!dbi) throw new Error("out of memory");
    *dbi = DBInfo.init;
    dbi.rc = 1;
    immutable int rc = sqlite3_open_v2(name.tempCString, &dbi.db, (allowWrite ? SQLITE_OPEN_READWRITE : SQLITE_OPEN_READONLY)|(allowCreate ? SQLITE_OPEN_CREATE : 0), null);
    if (rc != SQLITE_OK) {
      free(dbi);
      dbi = null;
      sq3check(null, rc);
    }
    scope(failure) { close(); }
    execute(pragmas);
    if (allowCreate && schema.length) execute(schema);
  }

  // `null` schema means "open as R/O"
  void open (const(char)[] name, const(char)[] schema=null) {
    Mode mode = Mode.ReadOnly;
    if (schema != null) {
      while (schema.length && schema[$-1] <= ' ') schema = schema[0..$-1];
      mode = (schema.length ? Mode.ReadWriteCreate : Mode.ReadWrite);
    }
    return openEx(name, mode, null, schema);
  }

  ulong lastRowId () nothrow @trusted { return (isOpen ? sqlite3_last_insert_rowid(dbi.db) : 0); }

  void setBusyTimeout (int msecs) nothrow @trusted { if (isOpen) sqlite3_busy_timeout(dbi.db, msecs); }

  // execute one or more SQL statements
  // SQLite will take care of splitting
  void execute (const(char)[] ops) {
    if (!isOpen) throw new SQLiteException("database is not opened");
    import std.internal.cstring;
    //char* errmsg;
    immutable int rc = sqlite3_exec(dbi.db, ops.tempCString, null, null, null/*&errmsg*/);
    if (rc != SQLITE_OK) {
      //import core.stdc.stdio : stderr, fprintf;
      //fprintf(stderr, "SQLITE ERROR: %s\n", errmsg);
      //sqlite3_free(errmsg);
      sq3check(dbi.db, rc);
    }
  }

  // create prepared SQL statement
  DBStatement statement (const(char)[] stmtstr, bool persistent=false) {
    if (!isOpen) throw new SQLiteException("database is not opened");
    return DBStatement(dbi, stmtstr, persistent);
  }

  DBStatement persistentStatement (const(char)[] stmtstr) {
    return statement(stmtstr, persistent:true);
  }

  sqlite3* getHandle () nothrow @nogc @trusted { return (isOpen ? dbi.db : null); }

  extern(C) {
    alias UserFn = void function (sqlite3_context *ctx, int argc, sqlite3_value **argv);
  }

  void createFunction (const(char)[] name, int argc, UserFn xFunc, bool deterministic=true, int moreflags=0) {
    import std.internal.cstring : tempCString;
    if (!isOpen) throw new SQLiteException("database is not opened");
    immutable int rc = sqlite3_create_function(dbi.db, name.tempCString, argc, SQLITE_UTF8|(deterministic ? SQLITE_DETERMINISTIC : 0)|moreflags, null, xFunc, null, null);
    sq3check(dbi.db, rc);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public static bool isUTF8ValidSQ3 (const(char)[] str) pure nothrow @trusted @nogc {
  usize len = str.length;
  immutable(ubyte)* p = cast(immutable(ubyte)*)str.ptr;
  while (len--) {
    immutable ubyte b = *p++;
    if (b == 0) return false;
    if (b < 128) continue;
    ubyte blen =
      (b&0xe0) == 0xc0 ? 1 :
      (b&0xf0) == 0xe0 ? 2 :
      (b&0xf8) == 0xe8 ? 3 :
      0; // no overlongs
    if (!blen) return false;
    if (len < blen) return false;
    len -= blen;
    while (blen--) {
      immutable ubyte b1 = *p++;
      if ((b1&0xc0) != 0x80) return false;
    }
  }
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
struct DBFieldIndex {
  uint idx;
}

struct DBFieldType {
  enum {
    Unknown,
    Integer,
    Float,
    Text,
    Blob,
    Null,
  }
  uint idx;

  string toString () const pure nothrow @trusted @nogc {
    switch (idx) {
      case Unknown: return "Unknown";
      case Integer: return "Integer";
      case Float: return "Float";
      case Text: return "Text";
      case Blob: return "Blob";
      case Null: return "Null";
      default: break;
    }
    return "Invalid";
  }
}

struct DBStatement {
private:
  enum FieldIndexMixin = `
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    char[257] fldname = 0;
    if (name.length > 255) throw new SQLiteException("field name too long");
    if (name[0] == ':' || name[0] == '?' || name[0] == '@') {
      fldname[0..name.length] = name[];
    } else {
      fldname[0] = ':';
      fldname[1..name.length+1] = name[];
    }
    immutable idx = sqlite3_bind_parameter_index(data.st, fldname.ptr);
    if (idx < 1) throw new SQLiteException("invalid field name: '"~name.idup~"'");
  `;

public:
  this (this) nothrow @trusted @nogc { this.incref(data); }
  ~this () nothrow @trusted { this.decref(data); data = null; }

  private this (Database.DBInfo *dbi, const(char)[] stmtstr, bool persistent=false) {
    if (dbi is null || dbi.db is null) throw new SQLiteException("database is not opened");
    if (stmtstr.length > int.max/4) throw new SQLiteException("statement too big");
    import core.stdc.stdlib : malloc;
    data = cast(Data*)malloc(Data.sizeof);
    if (data is null) {
      import core.exception : onOutOfMemoryErrorNoGC;
      onOutOfMemoryErrorNoGC();
    }
    *data = Data.init;
    data.refcount = 1;
    // register in the owner list
    version(sq3_debug_stmtlist) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "new stmt(%p): p=%p\n", dbi, data); }
    data.owner = dbi;
    data.prev = dbi.stmttail;
    if (dbi.stmttail !is null) dbi.stmttail.next = data; else dbi.stmthead = data;
    dbi.stmttail = data;
    // done registering
    scope(failure) { data.st = null; DBStatement.decref(data); }
    const(char)* e;
    if (persistent) {
      sq3check(dbi.db, sqlite3_prepare_v3(dbi.db, stmtstr.ptr, cast(int)stmtstr.length, SQLITE_PREPARE_PERSISTENT, &data.st, &e));
    } else {
      sq3check(dbi.db, sqlite3_prepare_v2(dbi.db, stmtstr.ptr, cast(int)stmtstr.length, &data.st, &e));
    }
    // check for extra code
    if (e !is null) {
      usize left = stmtstr.length-cast(usize)(e-stmtstr.ptr);
      //{ import core.stdc.stdio : printf; printf("%u: <%.*s>\n", cast(uint)left, cast(uint)left, e); }
      while (left--) {
        char ch = *e++;
        //{ import core.stdc.stdio : printf; printf("left=%u: ch=%u\n", cast(uint)left, cast(uint)ch); }
        if (ch <= 32 || ch == ';') continue;
        if (ch == '-') {
          if (left < 1 || *e != '-') throw new SQLiteErr("extra code in SQL statement (--)");
          while (left && *e != '\n') { --left; ++e; }
          continue;
        }
        if (ch == '/') {
          if (left < 1 || *e != '*') throw new SQLiteErr("extra code in SQL statement (mlc)");
          --left;
          ++e;
          while (left > 1) {
            if (*e == '*' && e[1] == '/') {
              left -= 2;
              e += 2;
              break;
            }
            --left;
            ++e;
          }
        }
        throw new SQLiteErr("extra code in SQL statement (text): "~e[0..left].idup);
      }
    }
  }

  @property bool valid () nothrow @trusted @nogc { return (data !is null && data.st !is null); }

  void close () nothrow @trusted { if (data !is null) this.decref(data); data = null; }

  @property auto range () {
    if (!valid) throw new SQLiteException("cannot get range from invalid statement");
    //if (st is null) throw new SQLiteException("statement is not prepared");
    if (data.stepIndex != 0) throw new SQLiteException("can't get range from busy statement");
    return DBRowRange(this);
  }

  void reset () nothrow @trusted {
    //if (data.stepIndex != 0) throw new SQLiteException("can't reset busy statement");
    if (valid) {
      data.stepIndex = 0;
      sqlite3_reset(data.st);
      sqlite3_clear_bindings(data.st);
    }
  }

  void doAll () {
    if (!valid) throw new SQLiteException("cannot execute invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't doAll on busy statement");
    scope(exit) reset();
    for (;;) {
      auto rc = sqlite3_step(data.st);
      if (rc == SQLITE_DONE) break;
      if (rc != SQLITE_ROW) sq3check(data.owner.db, rc);
    }
  }

  ref DBStatement bind(T) (uint idx, T value) if ((isNarrowString!T && is(ElementEncodingType!T : char)) || isIntegral!T) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1 || idx > sqlite3_bind_parameter_count(data.st)) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    int rc;
    static if (is(T == typeof(null))) {
      rc = sqlite3_bind_null(data.st, idx);
    } else static if (isNarrowString!T) {
      if (value.length > cast(usize)int.max) throw new SQLiteException("value too big");
      static if (is(ElementEncodingType!T == immutable(char))) {
        version(none) {
          if (isUTF8ValidSQ3(value[])) {
            rc = sqlite3_bind_text(data.st, idx, value.ptr, cast(int)value.length, /*SQLITE_STATIC*/SQLITE_TRANSIENT);
          } else {
            rc = sqlite3_bind_blob(data.st, idx, value.ptr, cast(int)value.length, /*SQLITE_STATIC*/SQLITE_TRANSIENT);
          }
        } else {
          rc = sqlite3_bind_text(data.st, idx, value.ptr, cast(int)value.length, /*SQLITE_STATIC*/SQLITE_TRANSIENT);
        }
      } else {
        version(none) {
          if (isUTF8ValidSQ3(value[])) {
            rc = sqlite3_bind_text(data.st, idx, value.ptr, cast(int)value.length, SQLITE_TRANSIENT);
          } else {
            rc = sqlite3_bind_blob(data.st, idx, value.ptr, cast(int)value.length, SQLITE_TRANSIENT);
          }
        } else {
            rc = sqlite3_bind_text(data.st, idx, value.ptr, cast(int)value.length, SQLITE_TRANSIENT);
        }
      }
    } else static if (isIntegral!T) {
      static if (isSigned!T) {
        // signed ints
        static if (T.sizeof <= 4) {
          rc = sqlite3_bind_int(data.st, idx, cast(int)value);
        } else {
          rc = sqlite3_bind_int64(data.st, idx, cast(long)value);
        }
      } else {
        // unsigned ints
        static if (T.sizeof < 4) {
          rc = sqlite3_bind_int(data.st, idx, cast(int)cast(uint)value);
        } else {
          rc = sqlite3_bind_int64(data.st, idx, cast(ulong)value);
        }
      }
    } else {
      static assert(0, "WTF?!");
    }
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bind(T) (const(char)[] name, T value) if ((isNarrowString!T && is(ElementEncodingType!T : char)) || isIntegral!T) {
    mixin(FieldIndexMixin);
    return bind!T(idx, value);
  }


  ref DBStatement bindText (uint idx, const(void)[] text, bool transient=true, bool allowNull=false) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    int rc;
    if (text is null) {
      if (allowNull) {
        rc = sqlite3_bind_null(data.st, idx);
      } else {
        rc = sqlite3_bind_text(data.st, idx, "".ptr, 0, SQLITE_STATIC);
      }
    } else {
      rc = sqlite3_bind_text(data.st, idx, cast(const(char)*)text.ptr, cast(int)text.length, (transient ? SQLITE_TRANSIENT : SQLITE_STATIC));
    }
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindText (const(char)[] name, const(void)[] text, bool transient=true, bool allowNull=false) {
    mixin(FieldIndexMixin);
    return bindText(cast(uint)idx, text, transient, allowNull);
  }


  ref DBStatement bindBlob (uint idx, const(void)[] blob, bool transient=true, bool allowNull=false) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    int rc;
    if (blob is null) {
      if (allowNull) {
        rc = sqlite3_bind_null(data.st, idx);
      } else {
        rc = sqlite3_bind_blob(data.st, idx, "".ptr, 0, SQLITE_STATIC);
      }
    } else {
      rc = sqlite3_bind_blob(data.st, idx, blob.ptr, cast(int)blob.length, (transient ? SQLITE_TRANSIENT : SQLITE_STATIC));
    }
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindBlob (const(char)[] name, const(void)[] blob, bool transient=true, bool allowNull=false) {
    mixin(FieldIndexMixin);
    return bindBlob(cast(uint)idx, blob, transient, allowNull);
  }


  ref DBStatement bindNull (uint idx) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    immutable int rc = sqlite3_bind_null(data.st, idx);
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindNull (const(char)[] name) {
    mixin(FieldIndexMixin);
    return bindNull(cast(uint)idx);
  }


  ref DBStatement bindInt(T) (uint idx, T v) if (isIntegral!T) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    static if (isSigned!T) {
      immutable int rc = sqlite3_bind_int64(data.st, idx, cast(long)v);
    } else {
      immutable int rc = sqlite3_bind_int64(data.st, idx, cast(ulong)v);
    }
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindInt(T) (const(char)[] name, T v) if (isIntegral!T) {
    mixin(FieldIndexMixin);
    return bindInt(cast(uint)idx, v);
  }


  ref DBStatement bindFloat(T) (uint idx, T v) if (is(T == float) || is(T == double)) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    rc = sqlite3_bind_double(data.st, idx, cast(double)v);
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindFloat(T) (const(char)[] name, T v) if (is(T == float) || is(T == double)) {
    mixin(FieldIndexMixin);
    return bindFloat(cast(uint)idx, v);
  }

  int colcount () nothrow @trusted {
    if (!valid) return 0;
    return sqlite3_column_count(data.st);
  }

  const(char)[] colname (int idx) nothrow @trusted {
    if (!valid || idx < 0) return null;
    if (idx >= sqlite3_column_count(data.st)) return null;
    const(char)* cname = sqlite3_column_name(data.st, idx);
    if (cname is null) return null;
    usize ep = 0;
    while (cname[ep]) ++ep;
    return cname[0..ep];
  }

private:
  struct DBRow {
    private this (DBStatement.Data* adata) nothrow @trusted @nogc {
      data____ = adata;
      DBStatement.incref(data____);
      ++data____.rowcount;
    }

    this (this) nothrow @trusted @nogc { DBStatement.incref(data____); ++data____.rowcount; }

    ~this () nothrow @trusted {
      DBStatement.decrowref(data____);
      DBStatement.decref(data____);
    }

    bool valid_ () pure nothrow @trusted @nogc {
      pragma(inline, true);
      return (data____.stepIndex > 0 && data____.st !is null);
    }

    int colcount_ () nothrow @trusted {
      if (data____.st is null) return 0;
      return sqlite3_column_count(data____.st);
    }

    const(char)[] colname_ (int idx) nothrow @trusted {
      if (idx < 0 || data____.st is null) return null;
      if (idx >= sqlite3_column_count(data____.st)) return null;
      const(char)* cname = sqlite3_column_name(data____.st, idx);
      if (cname is null) return null;
      usize ep = 0;
      while (cname[ep]) ++ep;
      return cname[0..ep];
    }

    int fieldIndex____ (const(char)[] name) {
      if (name.length > 0) {
        foreach (immutable int idx; 0..sqlite3_data_count(data____.st)) {
          import core.stdc.string : memcmp, strlen;
          auto n = sqlite3_column_name(data____.st, idx);
          if (n !is null) {
            auto len = strlen(n);
            if (len == name.length && memcmp(n, name.ptr, len) == 0) return idx;
          }
        }
      }
      throw new SQLiteException("invalid field name: '"~name.idup~"'");
    }

    T to(T) (uint idx)
      if ((isNarrowString!T && is(ElementEncodingType!T : char)) || isIntegral!T || is(T : DBFieldIndex) || is(T : DBFieldType) ||
          is(T == float) || is(T == double))
    {
      if (!valid_) throw new SQLiteException("can't get row field of completed statement");
      if (idx >= sqlite3_data_count(data____.st)) throw new SQLiteException("invalid result index");
      static if (is(T : DBFieldIndex)) {
        return DBFieldIndex(idx);
      } else static if (is(T : DBFieldType)) {
        switch (sqlite3_column_type(data____.st, idx)) {
          case SQLITE_INTEGER: return DBFieldType(DBFieldType.Integer);
          case SQLITE_FLOAT: return DBFieldType(DBFieldType.Float);
          case SQLITE3_TEXT: return DBFieldType(DBFieldType.Text);
          case SQLITE_BLOB: return DBFieldType(DBFieldType.Blob);
          case SQLITE_NULL: return DBFieldType(DBFieldType.Null);
          default: break;
        }
        return DBFieldType(DBFieldType.Unknown);
      } else static if (isIntegral!T) {
        auto res = sqlite3_column_int64(data____.st, idx);
        if (res < T.min || res > T.max) throw new SQLiteException("integral overflow");
        return cast(T)res;
      } else static if (is(T == double)) {
        auto res = sqlite3_column_double(data____.st, idx);
        return cast(double)res;
      } else static if (is(T == float)) {
        auto res = sqlite3_column_double(data____.st, idx);
        return cast(float)res;
      } else {
        auto len = sqlite3_column_bytes(data____.st, idx);
        if (len < 0) throw new SQLiteException("invalid result");
        const(char)* res = cast(const(char)*)sqlite3_column_blob(data____.st, idx);
        if (len == 0) res = ""; else if (res is null) throw new SQLiteException("invalid result");
        static if (is(ElementEncodingType!T == const(char))) {
          return res[0..len];
        } else static if (is(ElementEncodingType!T == immutable(char))) {
          return res[0..len].idup;
        } else {
          return res[0..len].dup;
        }
      }
    }
    T to(T) (const(char)[] name) { return this.to!T(fieldIndex____(name)); }

    const(ubyte)[] blob_ (const(char)[] name) {
      immutable int idx = fieldIndex____(name);
      auto len = sqlite3_column_bytes(data____.st, idx);
      if (len < 0) throw new SQLiteException("invalid result");
      const(ubyte)* res = cast(const(ubyte)*)sqlite3_column_blob(data____.st, idx);
      return res[0..len];
    }

    template opIndex() {
      T opIndexImpl(T) (uint idx) if ((isNarrowString!T && is(ElementEncodingType!T : char)) || isIntegral!T || is(T : DBFieldIndex) || is(T : DBFieldType)) { return this.to!T(idx); }
      T opIndexImpl(T) (const(char)[] name) if ((isNarrowString!T && is(ElementEncodingType!T : char)) || isIntegral!T || is(T : DBFieldIndex) || is(T : DBFieldType)) { return this.to!T(name); }
      alias opIndex = opIndexImpl;
    }

    template opDispatch(string name) {
      T opDispatchImpl(T=const(char)[]) () if ((isNarrowString!T && is(ElementEncodingType!T : char)) || isIntegral!T || is(T : DBFieldIndex) || is(T : DBFieldType)) { return this.to!T(name); }
      alias opDispatch = opDispatchImpl;
    }

    auto index_ () pure const nothrow @nogc { return (data____.stepIndex > 0 ? data____.stepIndex-1 : 0); }

    private DBStatement.Data* data____;
  } // end of DBRow

private:
  struct DBRowRange {
    private this (ref DBStatement astat) {
      data = astat.data;
      DBStatement.incref(data);
      ++data.rowcount;
      assert(data.stepIndex == 0);
      data.stepIndex = 1;
      popFront();
    }

    this (this) nothrow @trusted @nogc { DBStatement.incref(data); ++data.rowcount; }

    ~this () nothrow @trusted {
      DBStatement.decrowref(data);
      DBStatement.decref(data);
    }

    @property bool empty () const pure nothrow @nogc { return (data.stepIndex == 0); }

    @property auto front () {
      if (data.stepIndex == 0) throw new SQLiteException("can't get front element of completed statement");
      return DBRow(data);
    }

    void popFront () {
      if (data.stepIndex == 0) throw new SQLiteException("can't pop element of completed statement");
      auto rc = sqlite3_step(data.st);
      if (rc == SQLITE_DONE) {
        data.stepIndex = 0;
        return;
      }
      if (rc != SQLITE_ROW) {
        data.stepIndex = 0;
        sq3check(data.owner.db, rc);
      }
      ++data.stepIndex;
    }

    auto index_ () pure const nothrow @nogc { return (data.stepIndex > 0 ? data.stepIndex-1 : 0); }

    private DBStatement.Data* data;
  } // end of DBRowRange

private:
  static void incref (Data* data) nothrow @nogc @trusted {
    if (data !is null) ++data.refcount;
  }

  static void decref (Data* data) nothrow @trusted {
    if (data !is null) {
      if (--data.refcount == 0) {
        import core.stdc.stdlib : free;
        if (data.st !is null) {
          sqlite3_reset(data.st);
          sqlite3_clear_bindings(data.st);
          sqlite3_finalize(data.st);
        }
        // unregister from the owner list
        if (data.owner) {
          version(sq3_debug_stmtlist) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "removing stmt(%p): p=%p\n", data.owner, data); }
          if (data.prev) data.prev.next = data.next; else data.owner.stmthead = data.next;
          if (data.next) data.next.prev = data.prev; else data.owner.stmttail = data.prev;
          data.owner = null;
        }
        free(data);
        data = null;
      }
    }
  }

  static void decrowref (Data* data) nothrow @trusted {
    if (data !is null) {
      if (--data.rowcount == 0) {
        data.stepIndex = 0;
        if (data.st !is null) {
          sqlite3_reset(data.st);
          sqlite3_clear_bindings(data.st);
        }
      }
    }
  }

private:
  static struct Data {
    uint refcount = 0;
    uint rowcount = 0; // number of row structs using this statement
    uint stepIndex = 0;
    sqlite3_stmt* st = null;
    Data* prev = null;
    Data* next = null;
    Database.DBInfo* owner = null;
  }
  Data* data;
}
