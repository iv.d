// RIPEMD-160, based on the algorithm description
// public domain
// this is around 2 times faster than `std.digest.ripemd`
module iv.ripemd160;

public enum RIPEMD160_BITS = 160;

// Buffer size for RIPEMD-160 hash, in bytes.
public enum RIPEMD160_BYTES = (RIPEMD160_BITS>>3);

// RIPEMD-160 context.
public struct RIPEMD160_Ctx {
  uint[RIPEMD160_BYTES>>2] wkbuf = [0x67452301U, 0xefcdab89U, 0x98badcfeU, 0x10325476U, 0xc3d2e1f0U];
  ubyte[64] chunkd = 0;
  uint chunkpos = 0;
  uint total = 0;
  uint totalhi = 0;
}


private:
enum RIPEMD160_ROL(string x, string n) = `(((`~x~`)<<(`~n~`))|((`~x~`)>>(32-(`~n~`))))`;

template RIPEMD160_PRIM(string bop, string x, string y, string z) {
       static if (bop == "F") enum RIPEMD160_PRIM = `((`~x~`)^(`~y~`)^(`~z~`))`;
  else static if (bop == "G") enum RIPEMD160_PRIM = `(((`~x~`)&(`~y~`))|(~(`~x~`)&(`~z~`)))`;
  else static if (bop == "H") enum RIPEMD160_PRIM = `(((`~x~`)|~(`~y~`))^(`~z~`))`;
  else static if (bop == "I") enum RIPEMD160_PRIM = `(((`~x~`)&(`~z~`))|((`~y~`)&~(`~z~`)))`;
  else static if (bop == "J") enum RIPEMD160_PRIM = `((`~x~`)^((`~y~`)|~(`~z~`)))`;
  else static assert(0, "invalid bop");
}

enum RIPEMD160_BOP(string bop, string addconst, string a, string b, string c, string d, string e, string x, string s) =
  `(`~a~`) += `~RIPEMD160_PRIM!(bop, b, c, d)~`+(`~x~`)+`~addconst~`;`~
  `(`~a~`) = `~RIPEMD160_ROL!(a, s)~`+(`~e~`);`~
  `(`~c~`) = `~RIPEMD160_ROL!(c, "10")~`;`;

enum RIPEMD160_FF(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("F", "0U", a, b, c, d, e, x, s);
enum RIPEMD160_GG(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("G", "0x5a827999U", a, b, c, d, e, x, s);
enum RIPEMD160_HH(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("H", "0x6ed9eba1U", a, b, c, d, e, x, s);
enum RIPEMD160_II(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("I", "0x8f1bbcdcU", a, b, c, d, e, x, s);
enum RIPEMD160_JJ(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("J", "0xa953fd4eU", a, b, c, d, e, x, s);
enum RIPEMD160_FFF(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("F", "0U", a, b, c, d, e, x, s);
enum RIPEMD160_GGG(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("G", "0x7a6d76e9U", a, b, c, d, e, x, s);
enum RIPEMD160_HHH(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("H", "0x6d703ef3U", a, b, c, d, e, x, s);
enum RIPEMD160_III(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("I", "0x5c4dd124U", a, b, c, d, e, x, s);
enum RIPEMD160_JJJ(string a, string b, string c, string d, string e, string x, string s) = RIPEMD160_BOP!("J", "0x50a28be6U", a, b, c, d, e, x, s);


static void ripemd160_compress (uint *wkbuf/*[RIPEMD160_BYTES>>2]*/, const uint* X) nothrow @trusted @nogc {
  uint aa = wkbuf[0];
  uint bb = wkbuf[1];
  uint cc = wkbuf[2];
  uint dd = wkbuf[3];
  uint ee = wkbuf[4];
  uint aaa = wkbuf[0];
  uint bbb = wkbuf[1];
  uint ccc = wkbuf[2];
  uint ddd = wkbuf[3];
  uint eee = wkbuf[4];

  // round 1
  mixin(RIPEMD160_FF!("aa", "bb", "cc", "dd", "ee", "X[ 0]", "11"));
  mixin(RIPEMD160_FF!("ee", "aa", "bb", "cc", "dd", "X[ 1]", "14"));
  mixin(RIPEMD160_FF!("dd", "ee", "aa", "bb", "cc", "X[ 2]", "15"));
  mixin(RIPEMD160_FF!("cc", "dd", "ee", "aa", "bb", "X[ 3]", "12"));
  mixin(RIPEMD160_FF!("bb", "cc", "dd", "ee", "aa", "X[ 4]", " 5"));
  mixin(RIPEMD160_FF!("aa", "bb", "cc", "dd", "ee", "X[ 5]", " 8"));
  mixin(RIPEMD160_FF!("ee", "aa", "bb", "cc", "dd", "X[ 6]", " 7"));
  mixin(RIPEMD160_FF!("dd", "ee", "aa", "bb", "cc", "X[ 7]", " 9"));
  mixin(RIPEMD160_FF!("cc", "dd", "ee", "aa", "bb", "X[ 8]", "11"));
  mixin(RIPEMD160_FF!("bb", "cc", "dd", "ee", "aa", "X[ 9]", "13"));
  mixin(RIPEMD160_FF!("aa", "bb", "cc", "dd", "ee", "X[10]", "14"));
  mixin(RIPEMD160_FF!("ee", "aa", "bb", "cc", "dd", "X[11]", "15"));
  mixin(RIPEMD160_FF!("dd", "ee", "aa", "bb", "cc", "X[12]", " 6"));
  mixin(RIPEMD160_FF!("cc", "dd", "ee", "aa", "bb", "X[13]", " 7"));
  mixin(RIPEMD160_FF!("bb", "cc", "dd", "ee", "aa", "X[14]", " 9"));
  mixin(RIPEMD160_FF!("aa", "bb", "cc", "dd", "ee", "X[15]", " 8"));

  // round 2
  mixin(RIPEMD160_GG!("ee", "aa", "bb", "cc", "dd", "X[ 7]", " 7"));
  mixin(RIPEMD160_GG!("dd", "ee", "aa", "bb", "cc", "X[ 4]", " 6"));
  mixin(RIPEMD160_GG!("cc", "dd", "ee", "aa", "bb", "X[13]", " 8"));
  mixin(RIPEMD160_GG!("bb", "cc", "dd", "ee", "aa", "X[ 1]", "13"));
  mixin(RIPEMD160_GG!("aa", "bb", "cc", "dd", "ee", "X[10]", "11"));
  mixin(RIPEMD160_GG!("ee", "aa", "bb", "cc", "dd", "X[ 6]", " 9"));
  mixin(RIPEMD160_GG!("dd", "ee", "aa", "bb", "cc", "X[15]", " 7"));
  mixin(RIPEMD160_GG!("cc", "dd", "ee", "aa", "bb", "X[ 3]", "15"));
  mixin(RIPEMD160_GG!("bb", "cc", "dd", "ee", "aa", "X[12]", " 7"));
  mixin(RIPEMD160_GG!("aa", "bb", "cc", "dd", "ee", "X[ 0]", "12"));
  mixin(RIPEMD160_GG!("ee", "aa", "bb", "cc", "dd", "X[ 9]", "15"));
  mixin(RIPEMD160_GG!("dd", "ee", "aa", "bb", "cc", "X[ 5]", " 9"));
  mixin(RIPEMD160_GG!("cc", "dd", "ee", "aa", "bb", "X[ 2]", "11"));
  mixin(RIPEMD160_GG!("bb", "cc", "dd", "ee", "aa", "X[14]", " 7"));
  mixin(RIPEMD160_GG!("aa", "bb", "cc", "dd", "ee", "X[11]", "13"));
  mixin(RIPEMD160_GG!("ee", "aa", "bb", "cc", "dd", "X[ 8]", "12"));

  // round 3
  mixin(RIPEMD160_HH!("dd", "ee", "aa", "bb", "cc", "X[ 3]", "11"));
  mixin(RIPEMD160_HH!("cc", "dd", "ee", "aa", "bb", "X[10]", "13"));
  mixin(RIPEMD160_HH!("bb", "cc", "dd", "ee", "aa", "X[14]", " 6"));
  mixin(RIPEMD160_HH!("aa", "bb", "cc", "dd", "ee", "X[ 4]", " 7"));
  mixin(RIPEMD160_HH!("ee", "aa", "bb", "cc", "dd", "X[ 9]", "14"));
  mixin(RIPEMD160_HH!("dd", "ee", "aa", "bb", "cc", "X[15]", " 9"));
  mixin(RIPEMD160_HH!("cc", "dd", "ee", "aa", "bb", "X[ 8]", "13"));
  mixin(RIPEMD160_HH!("bb", "cc", "dd", "ee", "aa", "X[ 1]", "15"));
  mixin(RIPEMD160_HH!("aa", "bb", "cc", "dd", "ee", "X[ 2]", "14"));
  mixin(RIPEMD160_HH!("ee", "aa", "bb", "cc", "dd", "X[ 7]", " 8"));
  mixin(RIPEMD160_HH!("dd", "ee", "aa", "bb", "cc", "X[ 0]", "13"));
  mixin(RIPEMD160_HH!("cc", "dd", "ee", "aa", "bb", "X[ 6]", " 6"));
  mixin(RIPEMD160_HH!("bb", "cc", "dd", "ee", "aa", "X[13]", " 5"));
  mixin(RIPEMD160_HH!("aa", "bb", "cc", "dd", "ee", "X[11]", "12"));
  mixin(RIPEMD160_HH!("ee", "aa", "bb", "cc", "dd", "X[ 5]", " 7"));
  mixin(RIPEMD160_HH!("dd", "ee", "aa", "bb", "cc", "X[12]", " 5"));

  // round 4
  mixin(RIPEMD160_II!("cc", "dd", "ee", "aa", "bb", "X[ 1]", "11"));
  mixin(RIPEMD160_II!("bb", "cc", "dd", "ee", "aa", "X[ 9]", "12"));
  mixin(RIPEMD160_II!("aa", "bb", "cc", "dd", "ee", "X[11]", "14"));
  mixin(RIPEMD160_II!("ee", "aa", "bb", "cc", "dd", "X[10]", "15"));
  mixin(RIPEMD160_II!("dd", "ee", "aa", "bb", "cc", "X[ 0]", "14"));
  mixin(RIPEMD160_II!("cc", "dd", "ee", "aa", "bb", "X[ 8]", "15"));
  mixin(RIPEMD160_II!("bb", "cc", "dd", "ee", "aa", "X[12]", " 9"));
  mixin(RIPEMD160_II!("aa", "bb", "cc", "dd", "ee", "X[ 4]", " 8"));
  mixin(RIPEMD160_II!("ee", "aa", "bb", "cc", "dd", "X[13]", " 9"));
  mixin(RIPEMD160_II!("dd", "ee", "aa", "bb", "cc", "X[ 3]", "14"));
  mixin(RIPEMD160_II!("cc", "dd", "ee", "aa", "bb", "X[ 7]", " 5"));
  mixin(RIPEMD160_II!("bb", "cc", "dd", "ee", "aa", "X[15]", " 6"));
  mixin(RIPEMD160_II!("aa", "bb", "cc", "dd", "ee", "X[14]", " 8"));
  mixin(RIPEMD160_II!("ee", "aa", "bb", "cc", "dd", "X[ 5]", " 6"));
  mixin(RIPEMD160_II!("dd", "ee", "aa", "bb", "cc", "X[ 6]", " 5"));
  mixin(RIPEMD160_II!("cc", "dd", "ee", "aa", "bb", "X[ 2]", "12"));

  // round 5
  mixin(RIPEMD160_JJ!("bb", "cc", "dd", "ee", "aa", "X[ 4]", " 9"));
  mixin(RIPEMD160_JJ!("aa", "bb", "cc", "dd", "ee", "X[ 0]", "15"));
  mixin(RIPEMD160_JJ!("ee", "aa", "bb", "cc", "dd", "X[ 5]", " 5"));
  mixin(RIPEMD160_JJ!("dd", "ee", "aa", "bb", "cc", "X[ 9]", "11"));
  mixin(RIPEMD160_JJ!("cc", "dd", "ee", "aa", "bb", "X[ 7]", " 6"));
  mixin(RIPEMD160_JJ!("bb", "cc", "dd", "ee", "aa", "X[12]", " 8"));
  mixin(RIPEMD160_JJ!("aa", "bb", "cc", "dd", "ee", "X[ 2]", "13"));
  mixin(RIPEMD160_JJ!("ee", "aa", "bb", "cc", "dd", "X[10]", "12"));
  mixin(RIPEMD160_JJ!("dd", "ee", "aa", "bb", "cc", "X[14]", " 5"));
  mixin(RIPEMD160_JJ!("cc", "dd", "ee", "aa", "bb", "X[ 1]", "12"));
  mixin(RIPEMD160_JJ!("bb", "cc", "dd", "ee", "aa", "X[ 3]", "13"));
  mixin(RIPEMD160_JJ!("aa", "bb", "cc", "dd", "ee", "X[ 8]", "14"));
  mixin(RIPEMD160_JJ!("ee", "aa", "bb", "cc", "dd", "X[11]", "11"));
  mixin(RIPEMD160_JJ!("dd", "ee", "aa", "bb", "cc", "X[ 6]", " 8"));
  mixin(RIPEMD160_JJ!("cc", "dd", "ee", "aa", "bb", "X[15]", " 5"));
  mixin(RIPEMD160_JJ!("bb", "cc", "dd", "ee", "aa", "X[13]", " 6"));

  // parallel round 1
  mixin(RIPEMD160_JJJ!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 5]", " 8"));
  mixin(RIPEMD160_JJJ!("eee", "aaa", "bbb", "ccc", "ddd", "X[14]", " 9"));
  mixin(RIPEMD160_JJJ!("ddd", "eee", "aaa", "bbb", "ccc", "X[ 7]", " 9"));
  mixin(RIPEMD160_JJJ!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 0]", "11"));
  mixin(RIPEMD160_JJJ!("bbb", "ccc", "ddd", "eee", "aaa", "X[ 9]", "13"));
  mixin(RIPEMD160_JJJ!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 2]", "15"));
  mixin(RIPEMD160_JJJ!("eee", "aaa", "bbb", "ccc", "ddd", "X[11]", "15"));
  mixin(RIPEMD160_JJJ!("ddd", "eee", "aaa", "bbb", "ccc", "X[ 4]", " 5"));
  mixin(RIPEMD160_JJJ!("ccc", "ddd", "eee", "aaa", "bbb", "X[13]", " 7"));
  mixin(RIPEMD160_JJJ!("bbb", "ccc", "ddd", "eee", "aaa", "X[ 6]", " 7"));
  mixin(RIPEMD160_JJJ!("aaa", "bbb", "ccc", "ddd", "eee", "X[15]", " 8"));
  mixin(RIPEMD160_JJJ!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 8]", "11"));
  mixin(RIPEMD160_JJJ!("ddd", "eee", "aaa", "bbb", "ccc", "X[ 1]", "14"));
  mixin(RIPEMD160_JJJ!("ccc", "ddd", "eee", "aaa", "bbb", "X[10]", "14"));
  mixin(RIPEMD160_JJJ!("bbb", "ccc", "ddd", "eee", "aaa", "X[ 3]", "12"));
  mixin(RIPEMD160_JJJ!("aaa", "bbb", "ccc", "ddd", "eee", "X[12]", " 6"));

  // parallel round 2
  mixin(RIPEMD160_III!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 6]", " 9"));
  mixin(RIPEMD160_III!("ddd", "eee", "aaa", "bbb", "ccc", "X[11]", "13"));
  mixin(RIPEMD160_III!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 3]", "15"));
  mixin(RIPEMD160_III!("bbb", "ccc", "ddd", "eee", "aaa", "X[ 7]", " 7"));
  mixin(RIPEMD160_III!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 0]", "12"));
  mixin(RIPEMD160_III!("eee", "aaa", "bbb", "ccc", "ddd", "X[13]", " 8"));
  mixin(RIPEMD160_III!("ddd", "eee", "aaa", "bbb", "ccc", "X[ 5]", " 9"));
  mixin(RIPEMD160_III!("ccc", "ddd", "eee", "aaa", "bbb", "X[10]", "11"));
  mixin(RIPEMD160_III!("bbb", "ccc", "ddd", "eee", "aaa", "X[14]", " 7"));
  mixin(RIPEMD160_III!("aaa", "bbb", "ccc", "ddd", "eee", "X[15]", " 7"));
  mixin(RIPEMD160_III!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 8]", "12"));
  mixin(RIPEMD160_III!("ddd", "eee", "aaa", "bbb", "ccc", "X[12]", " 7"));
  mixin(RIPEMD160_III!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 4]", " 6"));
  mixin(RIPEMD160_III!("bbb", "ccc", "ddd", "eee", "aaa", "X[ 9]", "15"));
  mixin(RIPEMD160_III!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 1]", "13"));
  mixin(RIPEMD160_III!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 2]", "11"));

  // parallel round 3
  mixin(RIPEMD160_HHH!("ddd", "eee", "aaa", "bbb", "ccc", "X[15]", " 9"));
  mixin(RIPEMD160_HHH!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 5]", " 7"));
  mixin(RIPEMD160_HHH!("bbb", "ccc", "ddd", "eee", "aaa", "X[ 1]", "15"));
  mixin(RIPEMD160_HHH!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 3]", "11"));
  mixin(RIPEMD160_HHH!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 7]", " 8"));
  mixin(RIPEMD160_HHH!("ddd", "eee", "aaa", "bbb", "ccc", "X[14]", " 6"));
  mixin(RIPEMD160_HHH!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 6]", " 6"));
  mixin(RIPEMD160_HHH!("bbb", "ccc", "ddd", "eee", "aaa", "X[ 9]", "14"));
  mixin(RIPEMD160_HHH!("aaa", "bbb", "ccc", "ddd", "eee", "X[11]", "12"));
  mixin(RIPEMD160_HHH!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 8]", "13"));
  mixin(RIPEMD160_HHH!("ddd", "eee", "aaa", "bbb", "ccc", "X[12]", " 5"));
  mixin(RIPEMD160_HHH!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 2]", "14"));
  mixin(RIPEMD160_HHH!("bbb", "ccc", "ddd", "eee", "aaa", "X[10]", "13"));
  mixin(RIPEMD160_HHH!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 0]", "13"));
  mixin(RIPEMD160_HHH!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 4]", " 7"));
  mixin(RIPEMD160_HHH!("ddd", "eee", "aaa", "bbb", "ccc", "X[13]", " 5"));

  // parallel round 4
  mixin(RIPEMD160_GGG!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 8]", "15"));
  mixin(RIPEMD160_GGG!("bbb", "ccc", "ddd", "eee", "aaa", "X[ 6]", " 5"));
  mixin(RIPEMD160_GGG!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 4]", " 8"));
  mixin(RIPEMD160_GGG!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 1]", "11"));
  mixin(RIPEMD160_GGG!("ddd", "eee", "aaa", "bbb", "ccc", "X[ 3]", "14"));
  mixin(RIPEMD160_GGG!("ccc", "ddd", "eee", "aaa", "bbb", "X[11]", "14"));
  mixin(RIPEMD160_GGG!("bbb", "ccc", "ddd", "eee", "aaa", "X[15]", " 6"));
  mixin(RIPEMD160_GGG!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 0]", "14"));
  mixin(RIPEMD160_GGG!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 5]", " 6"));
  mixin(RIPEMD160_GGG!("ddd", "eee", "aaa", "bbb", "ccc", "X[12]", " 9"));
  mixin(RIPEMD160_GGG!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 2]", "12"));
  mixin(RIPEMD160_GGG!("bbb", "ccc", "ddd", "eee", "aaa", "X[13]", " 9"));
  mixin(RIPEMD160_GGG!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 9]", "12"));
  mixin(RIPEMD160_GGG!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 7]", " 5"));
  mixin(RIPEMD160_GGG!("ddd", "eee", "aaa", "bbb", "ccc", "X[10]", "15"));
  mixin(RIPEMD160_GGG!("ccc", "ddd", "eee", "aaa", "bbb", "X[14]", " 8"));

  // parallel round 5
  mixin(RIPEMD160_FFF!("bbb", "ccc", "ddd", "eee", "aaa", "X[12] ", " 8"));
  mixin(RIPEMD160_FFF!("aaa", "bbb", "ccc", "ddd", "eee", "X[15] ", " 5"));
  mixin(RIPEMD160_FFF!("eee", "aaa", "bbb", "ccc", "ddd", "X[10] ", "12"));
  mixin(RIPEMD160_FFF!("ddd", "eee", "aaa", "bbb", "ccc", "X[ 4] ", " 9"));
  mixin(RIPEMD160_FFF!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 1] ", "12"));
  mixin(RIPEMD160_FFF!("bbb", "ccc", "ddd", "eee", "aaa", "X[ 5] ", " 5"));
  mixin(RIPEMD160_FFF!("aaa", "bbb", "ccc", "ddd", "eee", "X[ 8] ", "14"));
  mixin(RIPEMD160_FFF!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 7] ", " 6"));
  mixin(RIPEMD160_FFF!("ddd", "eee", "aaa", "bbb", "ccc", "X[ 6] ", " 8"));
  mixin(RIPEMD160_FFF!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 2] ", "13"));
  mixin(RIPEMD160_FFF!("bbb", "ccc", "ddd", "eee", "aaa", "X[13] ", " 6"));
  mixin(RIPEMD160_FFF!("aaa", "bbb", "ccc", "ddd", "eee", "X[14] ", " 5"));
  mixin(RIPEMD160_FFF!("eee", "aaa", "bbb", "ccc", "ddd", "X[ 0] ", "15"));
  mixin(RIPEMD160_FFF!("ddd", "eee", "aaa", "bbb", "ccc", "X[ 3] ", "13"));
  mixin(RIPEMD160_FFF!("ccc", "ddd", "eee", "aaa", "bbb", "X[ 9] ", "11"));
  mixin(RIPEMD160_FFF!("bbb", "ccc", "ddd", "eee", "aaa", "X[11] ", "11"));

  // combine results
  ddd += cc+wkbuf[1]; // final result for wkbuf[0]
  wkbuf[1] = wkbuf[2]+dd+eee;
  wkbuf[2] = wkbuf[3]+ee+aaa;
  wkbuf[3] = wkbuf[4]+aa+bbb;
  wkbuf[4] = wkbuf[0]+bb+ccc;
  wkbuf[0] = ddd;
}


// ////////////////////////////////////////////////////////////////////////// //
public void ripemd160_init (ref RIPEMD160_Ctx ctx) nothrow @trusted @nogc {
  import core.stdc.string : memset;
  memset(cast(void*)&ctx, 0, RIPEMD160_Ctx.sizeof);
  ctx.wkbuf.ptr[0] = 0x67452301U;
  ctx.wkbuf.ptr[1] = 0xefcdab89U;
  ctx.wkbuf.ptr[2] = 0x98badcfeU;
  ctx.wkbuf.ptr[3] = 0x10325476U;
  ctx.wkbuf.ptr[4] = 0xc3d2e1f0U;
}


// ////////////////////////////////////////////////////////////////////////// //
public void ripemd160_putbyte (ref RIPEMD160_Ctx ctx, in ubyte b) nothrow @trusted @nogc {
  pragma(inline, true);
  version(BigEndian) {
    (cast(uint*)ctx.chunkd.ptr)[ctx.chunkpos>>2] |= (cast(uint)b)<<((ctx.chunkpos&0x03U)<<3);
  } else {
    ctx.chunkd.ptr[ctx.chunkpos] = b;
  }
  if (++ctx.chunkpos == 64U) {
    ripemd160_compress(ctx.wkbuf.ptr, cast(const uint*)ctx.chunkd.ptr);
    version(BigEndian) {
      import core.stdc.string : memset;
      memset(ctx.chunkd.ptr, 0, ctx.chunkd.sizeof);
    }
    ctx.chunkpos = 0U;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void ripemd160_put (ref RIPEMD160_Ctx ctx, const(void)[] data) nothrow @trusted @nogc {
  if (data.length == 0) return;
  static if ((void*).sizeof <= 4) {
    if (ctx.total+data.length <= ctx.total) ++ctx.totalhi;
    ctx.total += data.length;
  } else {
    if (ctx.total+cast(uint)data.length <= ctx.total) ++ctx.totalhi;
    ctx.total += cast(uint)data.length;
    ctx.totalhi += cast(uint)(data.length>>32);
  }
  const(ubyte)* b = cast(const(ubyte)*)data.ptr;
  usize datasize = data.length;
  version(BigEndian) {
    while (datasize--) ripemd160_putbyte(ctx, *b++);
  } else {
    // process full chunks, if possible
    if (ctx.chunkpos == 0 && datasize >= cast(usize)64) {
      do {
        ripemd160_compress(ctx.wkbuf.ptr, cast(const uint*)b);
        b += 64;
      } while ((datasize -= cast(usize)64) >= cast(usize)64);
      if (datasize == 0) return;
    }
    // we can use `memcpy()` here
    import core.stdc.string : memcpy, memset;
    uint left = 64U-ctx.chunkpos;
    if (cast(usize)left > datasize) left = cast(uint)datasize;
    memcpy(ctx.chunkd.ptr+ctx.chunkpos, b, cast(usize)left);
    if ((ctx.chunkpos += left) == 64U) {
      ripemd160_compress(ctx.wkbuf.ptr, cast(const uint*)ctx.chunkd.ptr);
      b += left;
      datasize -= cast(usize)left;
      while (datasize >= cast(usize)64) {
        ripemd160_compress(ctx.wkbuf.ptr, cast(const uint*)b);
        b += 64;
        datasize -= 64;
      }
      if ((ctx.chunkpos = cast(uint)datasize) != 0) memcpy(ctx.chunkd.ptr, b, datasize);
    } else {
      // this is invariant
      version(none) {
        datasize -= cast(usize)left;
        if (datasize) assert(0);
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public ubyte[RIPEMD160_BYTES] ripemd160_finish (const ref RIPEMD160_Ctx ctx) nothrow @trusted @nogc {
  import core.stdc.string : memcpy, memset;

  RIPEMD160_Ctx rctx = void;
  memcpy(cast(void*)&rctx, cast(void*)&ctx, RIPEMD160_Ctx.sizeof);

  // padding with `1` bit
  ripemd160_putbyte(ref rctx, 0x80);
  // length in bits goes into two last dwords
  version(BigEndian) {
    if (64U-rctx.chunkpos < 8U) {
      while (rctx.chunkpos) ripemd160_putbyte(&rctx, 0);
      ripemd160_compress(rctx.wkbuf.ptr, cast(const uint*)rctx.chunkd.ptr);
      memset(rctx.chunkd.ptr, 0, rctx.chunkd.sizeof);
    }
    // chunk is guaranteed to be properly cleared here
    immutable uint t0 = ctx.total<<3;
    rctx.chunkd.ptr[56U] = t0&0xffU;
    rctx.chunkd.ptr[57U] = (t0>>8)&0xffU;
    rctx.chunkd.ptr[58U] = (t0>>16)&0xffU;
    rctx.chunkd.ptr[59U] = (t0>>24)&0xffU;
    immutable uint t1 = (ctx.total>>29)|(ctx.totalhi<<3);
    rctx.chunkd.ptr[60U] = t1&0xffU;
    rctx.chunkd.ptr[61U] = (t1>>8)&0xffU;
    rctx.chunkd.ptr[62U] = (t1>>16)&0xffU;
    rctx.chunkd.ptr[63U] = (t1>>24)&0xffU;
  } else {
    uint left = 64U-rctx.chunkpos;
    if (left < 8U) {
      if (left) memset(rctx.chunkd.ptr+64U-left, 0, left);
      ripemd160_compress(rctx.wkbuf.ptr, cast(const uint*)rctx.chunkd.ptr);
      left = 64U;
    }
    left -= 8U;
    if (left) memset(rctx.chunkd.ptr+64U-8U-left, 0, left);
    (cast(uint*)rctx.chunkd.ptr)[14] = ctx.total<<3;
    (cast(uint*)rctx.chunkd.ptr)[15] = (ctx.total>>29)|(ctx.totalhi<<3);
  }
  ripemd160_compress(rctx.wkbuf.ptr, cast(const uint*)rctx.chunkd);

  ubyte[RIPEMD160_BYTES] hash = void;
  memcpy(hash.ptr, cast(void*)rctx.wkbuf.ptr, RIPEMD160_BYTES);
  return hash;
}


public int ripemd160_canput (const ref RIPEMD160_Ctx ctx, in usize datasize) pure nothrow @trusted @nogc {
  static if ((void*).sizeof <= 4) {
    return (ctx.totalhi < 0x1fffffffU+(ctx.total+datasize > ctx.total));
  } else {
    // totally untested
    immutable uint lo = cast(uint)datasize;
    immutable uint hi = cast(uint)(datasize>>32);
    if (ctx.totalhi+hi >= 0x20000000U) return 0;
    return ((hi+ctx.totalhi) < 0x1fffffffU+(ctx.total+lo > ctx.total));
  }
}


version(unittest_ripemd160)
unittest {
  static void selftest_str (const(void)[] str, string res) {
    import core.stdc.stdio : snprintf;
    import core.stdc.string : memcmp;
    assert(res.length == RIPEMD160_BYTES*2);

    RIPEMD160_Ctx ctx;
    ubyte[RIPEMD160_BYTES] hash;
    char[RIPEMD160_BYTES*2+2] hhex;

    ripemd160_init(ref ctx);
    ripemd160_put(ref ctx, str[]);
    hash = ripemd160_finish(ref ctx);
    foreach (immutable c; 0..RIPEMD160_BYTES) snprintf(hhex.ptr+c*2, 3, "%02x", hash[c]);

    if (memcmp(hhex.ptr, res.ptr, RIPEMD160_BYTES*2) != 0) {
      import core.stdc.stdio : stderr, fprintf;
      fprintf(stderr, "FAILURE!\n");
      fprintf(stderr, "string  : %.*s\n", cast(uint)str.length, str.ptr);
      fprintf(stderr, "result  : %s\n", hhex.ptr);
      fprintf(stderr, "expected: %.*s\n", cast(uint)res.length, res.ptr);
      assert(0);
    }
  }


  static void selftest (void) {
    selftest_str("", "9c1185a5c5e9fc54612808977ee8f548b2258d31");
    selftest_str("abcdefghijklmnopqrstuvwxyz", "f71c27109c692c1b56bbdceb5b9d2865b3708dbc");

    selftest_str("a", "0bdc9d2d256b3ee9daae347be6f4dc835a467ffe");
    selftest_str("abc", "8eb208f7e05d987a9b044a8e98c6b087f15a0bfc");
    selftest_str("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq", "12a053384a9c0c88e405a06c27dcf49ada62eb2b");
    selftest_str("message digest", "5d0689ef49d2fae572b881b123a85ffa21595f36");
    selftest_str("abcdefghijklmnopqrstuvwxyz", "f71c27109c692c1b56bbdceb5b9d2865b3708dbc");
    selftest_str("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", "b0e20b6e3116640286ed3a87a5713079b21f5189");
    selftest_str("1234567890123456789012345678901234567890"~"1234567890123456789012345678901234567890", "9b752e45573d4b39f4dbd3323cab82bf63326bfb");

    selftest_str(
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"~
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"~
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"~
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"~
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"~
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"~
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"~
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
      , "160dd118d84b2ceff71261ab78fb9239e257c8d3");

    // 160 zero bits
    char[160/8] zero = 0;
    selftest_str(zero[], "5c00bd4aca04a9057c09b20b05f723f2e23deb65");

    selftest_str("Alice", "b417ed99b95ce21448b7d789c50009e4f088e3a7");
    selftest_str("Miriel", "c95659db5eb45fe56023c073e96ca597347860d9");
  }

  selftest();
}
