/* Invisible Vector Library
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// loosely based on opticron and Adam D. Ruppe work
module iv.sslsocket /*is aliced*/;

import iv.alice;
public import std.socket;
import iv.gnutls;


// ///////////////////////////////////////////////////////////////////////// //
shared static this () { gnutls_global_init(); }
shared static ~this () { gnutls_global_deinit(); }


// ///////////////////////////////////////////////////////////////////////// //
/// deprecated!
class SSLClientSocket : Socket {
  gnutls_certificate_credentials_t xcred;
  gnutls_session_t session;
  private bool sslInitialized = false;
  bool manualHandshake = false; // for non-blocking sockets this should be `true`
  bool isblocking = false;
  string certBaseName;

  // take care of pre-connection TLS stuff
  //FIXME: possible memory leak on exception? (sholdn't be, as `close()` will free the things)
  private void sslInit (string acertbasename, const(char)[] hostname=null) {
    if (sslInitialized) return;

    // x509 stuff
    gnutls_certificate_allocate_credentials(&xcred);

    // sets the trusted certificate authority file (no need for us, as we aren't checking any certificate)
    //gnutls_certificate_set_x509_trust_file(xcred, CAFILE, GNUTLS_X509_FMT_PEM);
    if (acertbasename.length) {
      certBaseName = acertbasename;
      import std.internal.cstring : tempCString;
      string cfname = certBaseName~".cer";
      string kfname = certBaseName~".key";
      int ret = gnutls_certificate_set_x509_key_file(xcred, cfname.tempCString, kfname.tempCString, GNUTLS_X509_FMT_PEM);
      if (ret < 0) {
        import std.string : fromStringz;
        import std.conv : to;
        string errstr = gnutls_strerror(ret).fromStringz.idup;
        gnutls_certificate_free_credentials(xcred);
        throw new Exception("TLS Error ("~errstr~"): cannot load certificate, err="~ret.to!string);
      }
    }

    // initialize TLS session
    gnutls_init(&session, GNUTLS_CLIENT|(certBaseName.length ? GNUTLS_FORCE_CLIENT_CERT : 0));

    // use default priorities
    /*
    const(char)* err;
    auto ret = gnutls_priority_set_direct(session, "PERFORMANCE", &err);
    if (ret < 0) {
      import std.string : fromStringz;
      import std.conv : to;
      if (ret == GNUTLS_E_INVALID_REQUEST) throw new Exception("Syntax error at: "~err.fromStringz.idup);
      string errstr = gnutls_strerror(ret).fromStringz.idup;
      gnutls_deinit(session);
      gnutls_certificate_free_credentials(xcred);
      throw new Exception("TLS Error ("~errstr~"): returned with "~ret.to!string);
    }
    */
    auto ret = gnutls_set_default_priority(session);
    if (ret < 0) {
      import std.string : fromStringz;
      import std.conv : to;
      //if (ret == GNUTLS_E_INVALID_REQUEST) throw new Exception("Syntax error at: "~err.fromStringz.idup);
      string errstr = gnutls_strerror(ret).fromStringz.idup;
      gnutls_deinit(session);
      gnutls_certificate_free_credentials(xcred);
      throw new Exception("TLS Error ("~errstr~"): returned with "~ret.to!string);
    }
    gnutls_session_enable_compatibility_mode(session);

    if (hostname.length) {
      version(none) {
        import core.stdc.stdio : stderr, fprintf;
        fprintf(stderr, "TLS: setting host name to '%.*s'\n", cast(uint)hostname.length, hostname.ptr);
      }
      int xres = gnutls_server_name_set(session, GNUTLS_NAME_DNS, hostname.ptr, cast(uint)hostname.length);
      if (xres < 0) {
        import std.string : fromStringz;
        import std.conv : to;
        string errstr = gnutls_strerror(xres).fromStringz.idup;
        gnutls_deinit(session);
        gnutls_certificate_free_credentials(xcred);
        throw new Exception("TLS Error ("~errstr~"): returned with "~xres.to!string);
      }
    }

    // put the x509 credentials to the current session
    gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred);
    sslInitialized = true;
  }

  // this is required for new TLS (fuck)
  // call this before connecting
  public void sslhostname (const(char)[] hname) @trusted {
    if (hname.length == 0) return;
    //import std.internal.cstring : tempCString;
    int res = gnutls_server_name_set(session, GNUTLS_NAME_DNS, hname.ptr/*tempCString*/, hname.length);
    if (res < 0) {
      import std.string : fromStringz;
      import std.conv : to;
      string errstr = gnutls_strerror(res).fromStringz.idup;
      //gnutls_deinit(session);
      //gnutls_certificate_free_credentials(xcred);
      throw new Exception("TLS Error ("~errstr~"): returned with "~res.to!string);
    }
  }

  public void sslHandshake () {
    if (!sslInitialized) throw new Exception("trying to handshake on uninitialised SSL session");
    // lob the socket handle off to gnutls
    gnutls_transport_set_ptr(session, cast(gnutls_transport_ptr_t)handle);
    gnutls_handshake_set_timeout(session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);
    // perform the TLS handshake
    for (;;) {
      auto ret = gnutls_handshake(session);
      if (ret < 0 && !gnutls_error_is_fatal(ret)) continue;
      if (ret < 0) {
        import std.string : fromStringz;
        throw new Exception("Handshake failed: "~gnutls_strerror(ret).fromStringz.idup);
      }
      break;
    }
  }

  public string getSessionInfo () {
    if (!sslInitialized) return null;
    char* desc = gnutls_session_get_desc(session);
    if (desc is null) return null;
    import core.stdc.string : strlen;
    usize len = strlen(desc);
    string res;
    if (len != 0) res = desc[0..len].idup;
    gnutls_free(desc);
    return res;
  }

  override @property void blocking (bool byes) @trusted {
    super.blocking(byes);
    isblocking = byes;
  }

  override void connect (Address to) @trusted {
    super.connect(to);
    if (!manualHandshake) sslHandshake();
  }

  // close the encrypted connection
  override void close () @trusted {
    if (sslInitialized) {
      sslInitialized = false;
      //{ import core.stdc.stdio : printf; printf("deiniting\n"); }
      //!!!gnutls_bye(session, GNUTLS_SHUT_RDWR);
      gnutls_deinit(session);
      gnutls_certificate_free_credentials(xcred);
    }
    super.close();
  }

  override ptrdiff_t send (const(void)[] buf, SocketFlags flags) @trusted {
    if (buf.length == 0) return 0;
    for (;;) {
      auto res = gnutls_record_send(session, buf.ptr, buf.length);
      if (res >= 0 || !isblocking) return res;
      if (res == GNUTLS_E_INTERRUPTED || res == GNUTLS_E_AGAIN) continue;
      //if (gnutls_error_is_fatal(res)) return res;
      return res;
    }
  }

  override ptrdiff_t send (const(void)[] buf) {
    import core.sys.posix.sys.socket;
    static if (is(typeof(MSG_NOSIGNAL))) {
      return send(buf, cast(SocketFlags)MSG_NOSIGNAL);
    } else {
      return send(buf, SocketFlags.NOSIGNAL);
    }
  }

  override ptrdiff_t receive (void[] buf, SocketFlags flags) @trusted {
    if (buf.length == 0) return 0;
    for (;;) {
      auto res = gnutls_record_recv(session, buf.ptr, buf.length);
      if (res >= 0 || !isblocking) return res;
      if (res == GNUTLS_E_INTERRUPTED || res == GNUTLS_E_AGAIN) continue;
      //if (gnutls_error_is_fatal(res)) return res;
      return res;
    }
  }

  override ptrdiff_t receive (void[] buf) { return receive(buf, SocketFlags.NONE); }

  this (AddressFamily af, const(char)[] hostname, SocketType type=SocketType.STREAM, string certbasename=null) {
    sslInit(certbasename, hostname);
    super(af, type);
  }

  this (AddressFamily af, SocketType type=SocketType.STREAM, string certbasename=null) {
    sslInit(certbasename);
    super(af, type);
  }

  this (socket_t sock, AddressFamily af, string certbasename=null) {
    sslInit(certbasename);
    super(sock, af);
  }
}


// ///////////////////////////////////////////////////////////////////////// //
// this can be used as both client and server socket
// don't forget to set certificate file (and key file, if you have both) for server!
// `connect()` will do client mode, `accept()` will do server mode (and will return `SSLSocket` instance)
class SSLSocket : Socket {
  gnutls_certificate_credentials_t xcred;
  gnutls_session_t session;
  private bool sslInitialized = false;
  bool manualHandshake = false; // for non-blocking sockets this should be `true`
  bool isblocking = false;
  private bool thisIsServer = false;
  // server
  private string certfilez; // "cert.pem"
  private string keyfilez; // "key.pem"

  // both key and cert can be in one file
  void setKeyCertFile (const(char)[] certname, const(char)[] keyname=null) {
    if (certname.length == 0) { certname = keyname; keyname = null; }
    if (certname.length == 0) {
      certfilez = keyfilez = "";
    } else {
      auto buf = new char[](certname.length+1);
      buf[] = 0;
      buf[0..certname.length] = certname;
      certfilez = cast(string)buf;
      if (keyname.length != 0) {
        buf = new char[](keyname.length+1);
        buf[] = 0;
        buf[0..keyname.length] = keyname;
      }
      keyfilez = cast(string)buf;
    }
  }

  // take care of pre-connection TLS stuff
  //FIXME: possible memory leak on exception? (sholdn't be, as `close()` will free the things)
  private void sslInit () {
    if (sslInitialized) return;
    sslInitialized = true;

    // x509 stuff
    gnutls_certificate_allocate_credentials(&xcred);

    // sets the trusted certificate authority file (no need for us, as we aren't checking any certificate)
    //gnutls_certificate_set_x509_trust_file(xcred, CAFILE, GNUTLS_X509_FMT_PEM);

    if (thisIsServer) {
      // server
      if (certfilez.length < 1) throw new SocketException("TLS Error: certificate file not set");
      if (keyfilez.length < 1) throw new SocketException("TLS Error: key file not set");
      auto res = gnutls_certificate_set_x509_key_file(xcred, certfilez.ptr, keyfilez.ptr, GNUTLS_X509_FMT_PEM);
      if (res < 0) {
        import std.conv : to;
        throw new SocketException("TLS Error: returned with "~res.to!string);
      }
      gnutls_init(&session, GNUTLS_SERVER);
      gnutls_certificate_server_set_request(session, GNUTLS_CERT_IGNORE);
      gnutls_handshake_set_timeout(session, /*GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT*/2300);
    } else {
      // client
      // initialize TLS session
      gnutls_init(&session, GNUTLS_CLIENT);
    }

    // use default priorities
    /*
    const(char)* err;
    auto ret = gnutls_priority_set_direct(session, "PERFORMANCE", &err);
    if (ret < 0) {
      import std.string : fromStringz;
      import std.conv : to;
      if (ret == GNUTLS_E_INVALID_REQUEST) throw new SocketException("Syntax error at: "~err.fromStringz.idup);
      throw new SocketException("TLS Error: returned with "~ret.to!string);
    }
    */
    auto ret = gnutls_set_default_priority(session);
    if (ret < 0) {
      import std.string : fromStringz;
      import std.conv : to;
      //if (ret == GNUTLS_E_INVALID_REQUEST) throw new Exception("Syntax error at: "~err.fromStringz.idup);
      string errstr = gnutls_strerror(ret).fromStringz.idup;
      gnutls_deinit(session);
      gnutls_certificate_free_credentials(xcred);
      throw new Exception("TLS Error ("~errstr~"): returned with "~ret.to!string);
    }
    gnutls_session_enable_compatibility_mode(session);

    // put the x509 credentials to the current session
    gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred);
  }

  // this is required for new TLS (fuck)
  // call this before connecting
  public void sslhostname (const(char)[] hname) @trusted {
    import std.internal.cstring : tempCString;
    int res = gnutls_server_name_set(session, GNUTLS_NAME_DNS, hname.tempCString, hname.length);
    if (res < 0) {
      import std.string : fromStringz;
      import std.conv : to;
      string errstr = gnutls_strerror(res).fromStringz.idup;
      //gnutls_deinit(session);
      //gnutls_certificate_free_credentials(xcred);
      throw new Exception("TLS Error ("~errstr~"): returned with "~res.to!string);
    }
  }

  public void sslHandshake () {
    sslInit();
    // lob the socket handle off to gnutls
    gnutls_transport_set_ptr(session, cast(gnutls_transport_ptr_t)handle);
    // perform the TLS handshake
    for (;;) {
      auto ret = gnutls_handshake(session);
      if (ret < 0 && !gnutls_error_is_fatal(ret)) continue;
      if (ret < 0) {
        import std.string : fromStringz;
        throw new Exception("Handshake failed: "~gnutls_strerror(ret).fromStringz.idup);
      }
      break;
    }
  }

  override @property void blocking (bool byes) @trusted {
    super.blocking(byes);
    manualHandshake = !byes;
    isblocking = byes;
  }

  override void connect (Address to) @trusted {
    if (sslInitialized && thisIsServer) throw new SocketException("wtf?!");
    thisIsServer = false;
    sslInit();
    super.connect(to);
    if (!manualHandshake) sslHandshake();
  }

  protected override Socket accepting () pure nothrow {
    return new SSLSocket();
  }

  override Socket accept () @trusted {
    auto sk = super.accept();
    if (auto ssk = cast(SSLSocket)sk) {
      ssk.keyfilez = keyfilez;
      ssk.certfilez = certfilez;
      ssk.manualHandshake = manualHandshake;
      ssk.thisIsServer = true;
      ssk.sslInit();
      if (!ssk.manualHandshake) ssk.sslHandshake();
    } else {
      throw new SocketAcceptException("failed to create ssl socket");
    }
    return sk;
  }

  // close the encrypted connection
  override void close () @trusted {
    scope(exit) sslInitialized = false;
    if (sslInitialized) {
      //{ import core.stdc.stdio : printf; printf("deiniting\n"); }
      gnutls_bye(session, GNUTLS_SHUT_RDWR);
      gnutls_deinit(session);
      gnutls_certificate_free_credentials(xcred);
    }
    super.close();
  }

  override ptrdiff_t send (const(void)[] buf, SocketFlags flags) @trusted {
    if (session is null || !sslInitialized) throw new SocketException("not initialized");
    //return gnutls_record_send(session, buf.ptr, buf.length);
    if (buf.length == 0) return 0;
    for (;;) {
      auto res = gnutls_record_send(session, buf.ptr, buf.length);
      if (res >= 0 || !isblocking) return res;
      if (res == GNUTLS_E_INTERRUPTED || res == GNUTLS_E_AGAIN) continue;
      //if (gnutls_error_is_fatal(res)) return res;
      return res;
    }
  }

  override ptrdiff_t send (const(void)[] buf) {
    import core.sys.posix.sys.socket;
    static if (is(typeof(MSG_NOSIGNAL))) {
      return send(buf, cast(SocketFlags)MSG_NOSIGNAL);
    } else {
      return send(buf, SocketFlags.NOSIGNAL);
    }
  }

  override ptrdiff_t receive (void[] buf, SocketFlags flags) @trusted {
    if (session is null || !sslInitialized) throw new SocketException("not initialized");
    //return gnutls_record_recv(session, buf.ptr, buf.length);
    if (buf.length == 0) return 0;
    for (;;) {
      auto res = gnutls_record_recv(session, buf.ptr, buf.length);
      if (res >= 0 || !isblocking) return res;
      if (res == GNUTLS_E_INTERRUPTED || res == GNUTLS_E_AGAIN) continue;
      //if (gnutls_error_is_fatal(res)) return res;
      return res;
    }
  }

  override ptrdiff_t receive (void[] buf) { return receive(buf, SocketFlags.NONE); }

  private this () pure nothrow @safe {}

  this (AddressFamily af, SocketType type=SocketType.STREAM) {
    super(af, type);
  }

  this (socket_t sock, AddressFamily af) {
    super(sock, af);
  }
}
