import std.stdio;

import iv.flexlay2;


// ////////////////////////////////////////////////////////////////////////// //
///
enum FuiDir {
  Horiz, ///
  Vert, ///
}


// ////////////////////////////////////////////////////////////////////////// //
/// point
public align(1) struct FuiPoint {
align(1):
  int x, y;

nothrow @safe @nogc:
  bool inside() (in auto ref FuiRect rc) pure const { pragma(inline, true); return (x >= rc.pos.x && y >= rc.pos.y && x < rc.pos.x+rc.size.w && y < rc.pos.y+rc.size.h); }
  bool inside() (in auto ref FuiSize sz) pure const { pragma(inline, true); return (x >= 0 && y >= 0 && x < sz.w && y < sz.h); }

  void opOpAssign(string op) (in int v) if (op == "+" || op == "-" || op == "*" || op == "/") {
    pragma(inline, true);
    mixin("x"~op~"=v; y"~op~"=v;");
  }

  void opOpAssign(string op) (in auto ref FuiPoint pt) if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("x"~op~"=pt.x; y"~op~"=pt.y;");
  }

  FuiPoint opBinary(string op) (in auto ref FuiPoint pt) if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("return FuiPoint(x"~op~"pt.x, y"~op~"pt.y);");
  }

  int opIndex (in FuiDir dir) pure const { pragma(inline, true); return (dir == FuiDir.Horiz ? x : y); }

  void opIndexAssign (in int v, in FuiDir dir) { pragma(inline, true); if (dir == FuiDir.Horiz) x = v; else y = v; }

  void opIndexOpAssign(string op) (in int v, in FuiDir dir) if (op == "+" || op == "-") {
    pragma(inline, true);
    if (dir == FuiDir.Horiz) mixin("x "~op~"= v;"); else mixin("y "~op~"= v;");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
/// size
public align(1) struct FuiSize {
align(1):
  int w, h;

nothrow @safe @nogc:
  @property bool valid () pure const { pragma(inline, true); return (w >= 0 && h >= 0); }
  @property bool empty () pure const { pragma(inline, true); return (w < 1 && h < 1); }

  bool inside() (in auto ref FuiPoint pt) pure const { pragma(inline, true); return (x >= 0 && y >= 0 && x < w && y < h); }

  void sanitize () { pragma(inline, true); if (w < 0) w = 0; if (h < 0) h = 0; }

  void opOpAssign(string op) (in int v) if (op == "+" || op == "-" || op == "*" || op == "/") {
    pragma(inline, true);
    mixin("w"~op~"=v; h"~op~"=v;");
  }

  void opOpAssign(string op) (in auto ref FuiSize sz) if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("w"~op~"=sz.w; h"~op~"=sz.h;");
  }

  FuiSize opBinary(string op) (in auto ref FuiSize sz) if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("return FuiSize(w"~op~"sz.w, h"~op~"sz.h);");
  }

  int opIndex (in FuiDir dir) pure const { pragma(inline, true); return (dir == FuiDir.Horiz ? w : h); }

  void opIndexAssign (in int v, in FuiDir dir) { pragma(inline, true); if (dir == FuiDir.Horiz) w = v; else h = v; }

  //FIXME: overflow!
  void opIndexOpAssign(string op) (in int v, in FuiDir dir) if (op == "+" || op == "-") {
    pragma(inline, true);
    if (dir == FuiDir.Horiz) {
      mixin("w "~op~"= v;");
      if (w < 0) w = 0;
    } else {
      mixin("h "~op~"= v;");
      if (h < 0) h = 0;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
/// UI box (container)
class FuiBox {
  FuiBox parent;
  FuiBox firstChild;
  FuiBox nextSibling;

  FuiSize minSize; /// minimum size
  FuiSize maxSize = FuiSize(int.max, int.max); /// maximum size
  FuiSize prefSize; /// preferred (initial) size

  FuiSize size; /// box size
  FuiSize finalSize; /// final size (can be bigger than `size)`
  FuiPoint finalPos; /// final position (for the final size); relative to the parent origin

  FuiDir childDir = FuiDir.Horiz; /// children orientation
  int flex; /// <=0: not flexible

  this () {}
  this (FuiBox aparent) { if (aparent !is null) aparent.appendChild(this); }

  void appendChild (FuiBox box) {
    assert(box !is this);
    if (box is null) return;
    assert(box.parent is null);
    box.parent = this;
    FuiBox lc = firstChild;
    if (lc is null) {
      firstChild = box;
    } else {
      while (lc.nextSibling) lc = lc.nextSibling;
      lc.nextSibling = box;
    }
  }

public:
  void dumpLayout (const(char)[] foname=null) {
    import core.stdc.stdio : stderr, fopen, fclose, fprintf, fputc;
    import std.internal.cstring;

    auto fo = (!foname.length ? stderr : fopen(foname.tempCString, "w"));
    if (fo is null) return;
    scope(exit) if (foname.length) fclose(fo);

    void ind (in int indent) { foreach (immutable _; 0..indent) fputc(' ', fo); }

    void dumpBox (FuiBox id, in int indent) {
      if (id is null) return;
      ind(indent);
      fo.fprintf("Ctl#%08x: rpos:(%3d,%3d); rsize:(%3d,%3d); size:(%3d,%3d); pref:(%3d,%3d); flex:%02d; min:(%3d,%3d); max:(%3d,%3d)\n",
        cast(uint)cast(void*)id,
        id.finalPos.x, id.finalPos.y, id.finalSize.w, id.finalSize.h, id.size.w, id.size.h,
        id.prefSize.w, id.prefSize.h, id.flex, id.minSize.w, id.minSize.h, id.maxSize.w, id.maxSize.h);
      for (id = id.firstChild; id !is null; id = id.nextSibling) dumpBox(id, indent+2);
    }

    fprintf(fo, "==== boxes ====\n");
    dumpBox(this, 0);
  }
}


void main () {
  alias BoxIdT = FuiBox;

  FuiFlexLayouter!BoxIdT lay;

  lay.isValidBoxId = delegate bool (BoxIdT id) { return (id !is null); };

  lay.firstChild = delegate BoxIdT (BoxIdT id) { return id.firstChild; };
  lay.nextSibling = delegate BoxIdT (BoxIdT id) { return id.nextSibling; };

  lay.getMinSize = delegate int (BoxIdT id, in bool horiz) { return (horiz ? id.minSize.w : id.minSize.h); };
  lay.getMaxSize = delegate int (BoxIdT id, in bool horiz) { return (horiz ? id.maxSize.w : id.maxSize.h); };
  lay.getPrefSize = delegate int (BoxIdT id, in bool horiz) { return (horiz ? id.prefSize.w : id.prefSize.h); };

  lay.isHorizBox = delegate bool (BoxIdT id) { return (id.childDir == FuiDir.Horiz); };

  lay.getFlex = delegate int (BoxIdT id) { return id.flex; };

  lay.getSize = delegate int (BoxIdT id, in bool horiz) { return (horiz ? id.size.w : id.size.h); };
  lay.setSize = delegate void (BoxIdT id, in bool horiz, int val) { if (horiz) id.size.w = val; else id.size.h = val; };

  lay.getFinalSize = delegate int (BoxIdT id, in bool horiz) { return (horiz ? id.finalSize.w : id.finalSize.h); };
  lay.setFinalSize = delegate void (BoxIdT id, in bool horiz, int val) { if (horiz) id.finalSize.w = val; else id.finalSize.h = val; };

  lay.setFinalPos = delegate void (BoxIdT id, in bool horiz, int val) { if (horiz) id.finalPos.x = val; else id.finalPos.y = val; };


  FuiBox root = null;
  FuiBox ctr = null;

  FuiBox newSpring (int flex) {
    FuiBox box = new FuiBox(ctr);
    box.flex = flex;
    if (root is null) root = box;
    return box;
  }

  FuiBox newBox (in FuiSize psz) {
    FuiBox box = new FuiBox(ctr);
    box.prefSize = psz;
    if (root is null) root = box;
    return box;
  }

  FuiBox newContainer (in FuiDir dir, in FuiSize psz=FuiSize(int.min, int.min)) {
    FuiBox box = new FuiBox(ctr);
    box.childDir = dir;
    if (psz.w >= 0) box.prefSize.w = psz.w;
    if (psz.h >= 0) box.prefSize.h = psz.h;
    ctr = box;
    if (root is null) root = box;
    return box;
  }

  void closeContainer () {
    assert(ctr !is null);
    ctr = ctr.parent;
  }

  newContainer(FuiDir.Vert);

    newContainer(FuiDir.Horiz, FuiSize(96, 16));
      newSpring(1);
      newBox(FuiSize(8, 17));
      newBox(FuiSize(2, 0));
      newBox(FuiSize(12, 17));
      newSpring(1);
    closeContainer();

    newContainer(FuiDir.Horiz);
      newSpring(1);
      newBox(FuiSize(12, 6));
    closeContainer();

    // table-like container (input form, for example)
    // first row
    newContainer(FuiDir.Horiz, FuiSize(0, 10));
      newBox(FuiSize(24, 6)); // input text
      newBox(FuiSize(12, 6)).flex = 1; // input field
    closeContainer();
    // second row
    newContainer(FuiDir.Horiz, FuiSize(0, 10));
      newBox(FuiSize(24, 6)); // input text
      newBox(FuiSize(12, 6)).flex = 1; // input field
      newBox(FuiSize(8, 6)); // small button (ellipsis, for example)
    closeContainer();

    // container with the maximum size, so widgets will be shrinked
    newContainer(FuiDir.Horiz, FuiSize(42, 10));
      ctr.maxSize = ctr.prefSize; // don't expand
      newBox(FuiSize(32, 7));
      newBox(FuiSize(22, 7));
      //newSpring(1);
    closeContainer();

  closeContainer();
  assert(ctr is null);
  assert(root !is null);

  root.dumpLayout();
  lay.layout(root);
  root.dumpLayout();
}
