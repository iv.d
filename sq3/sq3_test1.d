/* Invisible Vector Library
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module sq3_test /*is aliced*/;

import iv.alice;
import iv.sq3;
import iv.strex;
import iv.vfs;
import iv.vfs.io;


static immutable schema =
  "PRAGMA auto_vacuum = NONE;\n"~
  "PRAGMA journal_mode = MEMORY; /*MEMORY;*/ /*PERSIST;*/ /*WAL;*/ /*MEMORY;*/\n"~
  "PRAGMA synchronous = OFF;\n"~
  "PRAGMA encoding = \"UTF-8\";\n"~
  "\n"~
  "CREATE TABLE IF NOT EXISTS test(\n"~
  "    id INTEGER PRIMARY KEY\n"~
  "  , text TEXT\n"~
  ");\n"~
  "\n"~
  "CREATE UNIQUE INDEX IF NOT EXISTS testindex ON test(text);\n"~
  "";




////////////////////////////////////////////////////////////////////////////////
void main () {
  try { import std.file : remove; remove("test.db"); } catch (Exception) {}

  writeln("opening db...");
  auto db = Database("test.db", schema);
  writeln("starting the transaction");
  db.execute("begin transaction;");
  writeln("creating the statement");
  auto stmt = db.statement("INSERT INTO test (id,text) VALUES(NULL,:text);");
  writeln("binding");
  stmt.bind(":text", n"���!");
  writeln("executing");
  stmt.doAll();
  writeln("binding");
  stmt.bind(":text", n"�����!");
  writeln("executing");
  stmt.doAll();
  writeln("binding");
  stmt.bind(":text", n"���-���!");
  writeln("executing");
  stmt.doAll();
  writeln("commiting the transaction");
  db.execute("commit transaction;");

  auto stx = db.statement("SELECT text AS text FROM test WHERE :a=:a;");
  auto sty = stx;
  foreach (auto row; sty.bindConstText(":a", "boo!").range) {
    import std.conv : to;
    writeln("idx=", row.index_, "; type=", row.text!DBFieldType, "; val=<", row.text!SQ3Text, ">");
    /*
    writeln("char[]");
    char[] cc = row.text!(char[]);
    writeln("SQ3Text");
    SQ3Text ct = row.text!SQ3Text;
    writeln("string");
    string cs = row.text!string;
    */
  }

  writeln("closing the db");
  db.close();
}
