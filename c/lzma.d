/* converted by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.c.lzma;
pragma(lib, "clzma");

extern (C) __gshared nothrow:

/* LZMA_PROB32 can increase the speed on some CPUs,
   but memory usage for CLzmaDec::probs will be doubled in that case */
//#define LZMA_PROB32

/* use smaller (by ~3KB), but slightly slower (prolly) decompression code? */
//#define LZMA_SIZE_OPT

/* use slightly smaller (around 300 bytes) code with branching in compressor? */
//#define LZMA_ENC_USE_BRANCH


// ////////////////////////////////////////////////////////////////////////// //
static void *lzmaMMAlloc (ISzAllocPtr p, usize size) {
  import core.stdc.stdlib : malloc;
  size += !size;
  return malloc(size);
}

/* address can be 0 */
static void lzmaMMFree (ISzAllocPtr p, void* address) {
  import core.stdc.stdlib : free;
  if (address !is null) free(address);
}

__gshared ISzAlloc lzmaDefAllocator = ISzAlloc(&lzmaMMAlloc, &lzmaMMFree);


// ////////////////////////////////////////////////////////////////////////// //
enum {
  SZ_OK = 0,

  SZ_ERROR_DATA = 1,
  SZ_ERROR_MEM = 2,
  // SZ_ERROR_CRC = 3,
  SZ_ERROR_UNSUPPORTED = 4,
  SZ_ERROR_PARAM = 5,
  SZ_ERROR_INPUT_EOF = 6,
  SZ_ERROR_OUTPUT_EOF = 7,
  SZ_ERROR_READ = 8,
  SZ_ERROR_WRITE = 9,
  SZ_ERROR_PROGRESS = 10,
  SZ_ERROR_FAIL = 11,
}


alias SRes = int;
alias WRes = int;


/* The following interfaces use first parameter as pointer to structure */

struct ISeqInStream {
  SRes function (/*const*/ISeqInStream* p, void* buf, usize* size) Read;
    /* if (input(*size) != 0 && output(*size) == 0) means end_of_stream.
       (output(*size) < input(*size)) is allowed */
};
//#define ISeqInStream_Read(p, buf, size) (p)->Read(p, buf, size)


struct ISeqOutStream {
  usize function (/*const*/ISeqOutStream* p, const(void)* buf, usize size) nothrow Write;
    /* Returns: result - the number of actually written bytes.
       (result < size) means error */
};
//#define ISeqOutStream_Write(p, buf, size) (p)->Write(p, buf, size)


struct ICompressProgress {
  SRes function (/*const*/ICompressProgress* p, ulong inSize, ulong outSize) nothrow Progress;
    /* Returns: result. (result != SZ_OK) means break.
       Value (ulong)(long)-1 for size means unknown value. */
};
//#define ICompressProgress_Progress(p, inSize, outSize) (p)->Progress(p, inSize, outSize)


alias ISzAllocPtr = ISzAlloc*;
struct ISzAlloc {
  void *function (ISzAllocPtr p, usize size) nothrow Alloc;
  void function (ISzAllocPtr p, void *address) nothrow Free; /* address can be 0 */
};

//#define ISzAlloc_Alloc(p, size) (p)->Alloc(p, size)
//#define ISzAlloc_Free(p, a) (p)->Free(p, a)

/* deprecated */
//#define IAlloc_Alloc(p, size) ISzAlloc_Alloc(p, size)
//#define IAlloc_Free(p, a) ISzAlloc_Free(p, a)


// ////////////////////////////////////////////////////////////////////////// //
enum LZMA_PROPS_SIZE = 5;

struct CLzmaEncProps {
  int level;        /* 0 <= level <= 9 */
  uint dictSize;    /* (1 << 12) <= dictSize <= (1 << 27) for 32-bit version
                       (1 << 12) <= dictSize <= (3 << 29) for 64-bit version
                       default = (1 << 24) */
  int lc;           /* 0 <= lc <= 8, default = 3 */
  int lp;           /* 0 <= lp <= 4, default = 0 */
  int pb;           /* 0 <= pb <= 4, default = 2 */
  int algo;         /* 0 - fast, 1 - normal, default = 1 */
  int fb;           /* 5 <= fb <= 273, default = 32 */
  int btMode;       /* 0 - hashChain Mode, 1 - binTree mode - normal, default = 1 */
  int numHashBytes; /* 2, 3 or 4, default = 4 */
  uint mc;          /* 1 <= mc <= (1 << 30), default = 32 */
  uint writeEndMark;/* 0 - do not write EOPM, 1 - write EOPM, default = 0 */

  ulong reduceSize; /* estimated size of data that will be compressed. default = (ulong)(long)-1.
                       Encoder uses this value to reduce dictionary size */
}

void LzmaEncProps_Init (CLzmaEncProps *p) @nogc;
void LzmaEncProps_Normalize (CLzmaEncProps *p) @nogc;
uint LzmaEncProps_GetDictSize (const(CLzmaEncProps)* props2) @nogc;


/* ---------- CLzmaEncHandle Interface ---------- */

/* LzmaEnc* functions can return the following exit codes:
SRes:
  SZ_OK           - OK
  SZ_ERROR_MEM    - Memory allocation error
  SZ_ERROR_PARAM  - Incorrect paramater in props
  SZ_ERROR_WRITE  - ISeqOutStream write callback error
  SZ_ERROR_OUTPUT_EOF - output buffer overflow - version with (ubyte *) output
  SZ_ERROR_PROGRESS - some break from progress callback
*/

alias CLzmaEncHandle = void*;

CLzmaEncHandle LzmaEnc_Create (ISzAllocPtr alloc);
void LzmaEnc_Destroy (CLzmaEncHandle p, ISzAllocPtr alloc, ISzAllocPtr allocBig);

SRes LzmaEnc_SetProps (CLzmaEncHandle p, const(CLzmaEncProps)* props) @nogc;
void LzmaEnc_SetDataSize (CLzmaEncHandle p, ulong expectedDataSiize) @nogc;
SRes LzmaEnc_WriteProperties (CLzmaEncHandle p, ubyte* properties, usize* size) @nogc;
uint LzmaEnc_IsWriteEndMark (CLzmaEncHandle p) @nogc;

SRes LzmaEnc_Encode (CLzmaEncHandle p, ISeqOutStream* outStream, ISeqInStream* inStream,
                     ICompressProgress* progress, ISzAllocPtr alloc, ISzAllocPtr allocBig);

SRes LzmaEnc_MemEncode (CLzmaEncHandle p, void* dest, usize* destLen, const(void)* src, usize srcLen,
                        int writeEndMark, ICompressProgress* progress, ISzAllocPtr alloc, ISzAllocPtr allocBig);


/* ---------- One Call Interface ---------- */

SRes LzmaEncode (void* dest, usize* destLen, const(void)* src, usize srcLen,
                 const(CLzmaEncProps)* props, ubyte* propsEncoded, usize* propsSize,
                 int writeEndMark, ICompressProgress* progress, ISzAllocPtr alloc, ISzAllocPtr allocBig);


// ////////////////////////////////////////////////////////////////////////// //
/*
typedef
#ifdef LZMA_PROB32
  uint
#else
  uint16_t
#endif
  CLzmaProb;
*/
alias CLzmaProb = ushort;


/* ---------- LZMA Properties ---------- */

struct CLzmaProps {
  ubyte lc;
  ubyte lp;
  ubyte pb;
  ubyte _pad_;
  uint dicSize;
}

/* LzmaProps_Decode - decodes properties
Returns:
  SZ_OK
  SZ_ERROR_UNSUPPORTED - Unsupported properties
*/

SRes LzmaProps_Decode (CLzmaProps* p, const(void)* data, uint size) @nogc;


/* ---------- LZMA Decoder state ---------- */

/* LZMA_REQUIRED_INPUT_MAX = number of required input bytes for worst case.
   Num bits = log2((2^11 / 31) ^ 22) + 26 < 134 + 26 = 160; */

enum LZMA_REQUIRED_INPUT_MAX = 20;

struct CLzmaDec {
  /* Don't change this structure. ASM code can use it. */
  CLzmaProps prop;
  CLzmaProb* probs;
  CLzmaProb* probs_1664;
  ubyte* dic;
  usize dicBufSize;
  usize dicPos;
  const(ubyte)* buf;
  uint range;
  uint code;
  uint processedPos;
  uint checkDicSize;
  uint[4] reps;
  uint state;
  uint remainLen;

  uint numProbs;
  uint tempBufSize;
  ubyte[LZMA_REQUIRED_INPUT_MAX] tempBuf;
}

//#define LzmaDec_Construct(p) { (p)->dic = NULL; (p)->probs = NULL; }
void LzmaDec_Construct (CLzmaDec* p) @nogc { pragma(inline, true); if (p) { p.dic = null; p.probs = null; } }

void LzmaDec_Init (CLzmaDec* p) @nogc;

/* There are two types of LZMA streams:
     - Stream with end mark. That end mark adds about 6 bytes to compressed size.
     - Stream without end mark. You must know exact uncompressed size to decompress such stream. */

alias ELzmaFinishMode = int;
enum {
  LZMA_FINISH_ANY, /* finish at any point */
  LZMA_FINISH_END, /* block must be finished at the end */
}

/* ELzmaFinishMode has meaning only if the decoding reaches output limit !!!

   You must use LZMA_FINISH_END, when you know that current output buffer
   covers last bytes of block. In other cases you must use LZMA_FINISH_ANY.

   If LZMA decoder sees end marker before reaching output limit, it returns SZ_OK,
   and output value of destLen will be less than output buffer size limit.
   You can check status result also.

   You can use multiple checks to test data integrity after full decompression:
     1) Check Result and "status" variable.
     2) Check that output(destLen) = uncompressedSize, if you know real uncompressedSize.
     3) Check that output(srcLen) = compressedSize, if you know real compressedSize.
        You must use correct finish mode in that case. */

alias ELzmaStatus = int;
enum {
  LZMA_STATUS_NOT_SPECIFIED,               /* use main error code instead */
  LZMA_STATUS_FINISHED_WITH_MARK,          /* stream was finished with end mark. */
  LZMA_STATUS_NOT_FINISHED,                /* stream was not finished */
  LZMA_STATUS_NEEDS_MORE_INPUT,            /* you must provide more input bytes */
  LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK, /* there is probability that stream was finished without end mark */
}

/* ELzmaStatus is used only as output value for function call */


/* ---------- Interfaces ---------- */

/* There are 3 levels of interfaces:
     1) Dictionary Interface
     2) Buffer Interface
     3) One Call Interface
   You can select any of these interfaces, but don't mix functions from different
   groups for same object. */


/* There are two variants to allocate state for Dictionary Interface:
     1) LzmaDec_Allocate / LzmaDec_Free
     2) LzmaDec_AllocateProbs / LzmaDec_FreeProbs
   You can use variant 2, if you set dictionary buffer manually.
   For Buffer Interface you must always use variant 1.

LzmaDec_Allocate* can return:
  SZ_OK
  SZ_ERROR_MEM         - Memory allocation error
  SZ_ERROR_UNSUPPORTED - Unsupported properties
*/

SRes LzmaDec_AllocateProbs (CLzmaDec* p, const(void)* props, uint propsSize, ISzAllocPtr alloc);
void LzmaDec_FreeProbs (CLzmaDec* p, ISzAllocPtr alloc);

SRes LzmaDec_Allocate (CLzmaDec* p, const(void)* props, uint propsSize, ISzAllocPtr alloc);
void LzmaDec_Free (CLzmaDec* p, ISzAllocPtr alloc);

/* ---------- Dictionary Interface ---------- */

/* You can use it, if you want to eliminate the overhead for data copying from
   dictionary to some other external buffer.
   You must work with CLzmaDec variables directly in this interface.

   STEPS:
     LzmaDec_Construct()
     LzmaDec_Allocate()
     for (each new stream)
     {
       LzmaDec_Init()
       while (it needs more decompression)
       {
         LzmaDec_DecodeToDic()
         use data from CLzmaDec::dic and update CLzmaDec::dicPos
       }
     }
     LzmaDec_Free()
*/

/* LzmaDec_DecodeToDic

   The decoding to internal dictionary buffer (CLzmaDec::dic).
   You must manually update CLzmaDec::dicPos, if it reaches CLzmaDec::dicBufSize !!!

finishMode:
  It has meaning only if the decoding reaches output limit (dicLimit).
  LZMA_FINISH_ANY - Decode just dicLimit bytes.
  LZMA_FINISH_END - Stream must be finished after dicLimit.

Returns:
  SZ_OK
    status:
      LZMA_STATUS_FINISHED_WITH_MARK
      LZMA_STATUS_NOT_FINISHED
      LZMA_STATUS_NEEDS_MORE_INPUT
      LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK
  SZ_ERROR_DATA - Data error
  SZ_ERROR_FAIL - Some unexpected error: internal error of code, memory corruption or hardware failure
*/

SRes LzmaDec_DecodeToDic (CLzmaDec* p, usize dicLimit,
                          const(void)* src, usize* srcLen,
                          ELzmaFinishMode finishMode, ELzmaStatus* status) @nogc;


/* ---------- Buffer Interface ---------- */

/* It's zlib-like interface.
   See LzmaDec_DecodeToDic description for information about STEPS and return results,
   but you must use LzmaDec_DecodeToBuf instead of LzmaDec_DecodeToDic and you don't need
   to work with CLzmaDec variables manually.

finishMode:
  It has meaning only if the decoding reaches output limit (*destLen).
  LZMA_FINISH_ANY - Decode just destLen bytes.
  LZMA_FINISH_END - Stream must be finished after (*destLen).
*/

SRes LzmaDec_DecodeToBuf (CLzmaDec* p, void* dest, usize* destLen,
                          const(void)* src, usize* srcLen,
                          ELzmaFinishMode finishMode, ELzmaStatus* status) @nogc;


/* ---------- One Call Interface ---------- */

/* LzmaDecode

finishMode:
  It has meaning only if the decoding reaches output limit (*destLen).
  LZMA_FINISH_ANY - Decode just destLen bytes.
  LZMA_FINISH_END - Stream must be finished after (*destLen).

Returns:
  SZ_OK
    status:
      LZMA_STATUS_FINISHED_WITH_MARK
      LZMA_STATUS_NOT_FINISHED
      LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK
  SZ_ERROR_DATA - Data error
  SZ_ERROR_MEM  - Memory allocation error
  SZ_ERROR_UNSUPPORTED - Unsupported properties
  SZ_ERROR_INPUT_EOF - It needs more bytes in input buffer (src).
  SZ_ERROR_FAIL - Some unexpected error: internal error of code, memory corruption or hardware failure
*/

SRes LzmaDecode (void* dest, usize* destLen, const(void)* src, usize* srcLen,
                 const(void)* propData, uint propSize,
                 ELzmaFinishMode finishMode, ELzmaStatus* status, ISzAllocPtr alloc);
