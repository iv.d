module lzmatest;

import iv.c.lzma;
import iv.strex;
import iv.vfs;
import iv.vfs.io;


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  if (args.length != 4) {
    writeln("usage: fuckme <c|x> infile outfile");
    return;
  }

  if (args[1] == "c") {
    auto fi = VFile(args[2]);
    usize insize = cast(usize)fi.size;
    ubyte *inbuf = cast(ubyte*)lzmaDefAllocator.Alloc(&lzmaDefAllocator, insize);
    fi.rawReadExact(inbuf[0..insize]);
    fi.close();

    ubyte *outbuf = cast(ubyte*)lzmaDefAllocator.Alloc(&lzmaDefAllocator, insize);

    CLzmaEncProps props;
    LzmaEncProps_Init(&props);
    props.level = 9;
    //props.dictSize = 1;
    //while (props.dictSize < insize) props.dictSize <<= 1;
    //props.dictSize = 1<<27; //128MB
    props.dictSize = 1<<22; //4MB
    props.reduceSize = insize;

    ubyte[LZMA_PROPS_SIZE+8] header;
    uint headerSize = cast(uint)header.sizeof;

    writeln("compressing...");
    usize destLen = insize;
    SRes res = LzmaEncode(outbuf, &destLen, inbuf, insize, &props, header.ptr, &headerSize, 0/*writeEndMark*/, null, &lzmaDefAllocator, &lzmaDefAllocator);
    lzmaDefAllocator.Free(&lzmaDefAllocator, inbuf);

    switch (res) {
      case SZ_OK: break;
      case SZ_ERROR_MEM: stderr.writeln("FUCK: memory"); return;
      case SZ_ERROR_PARAM: stderr.writeln("FUCK: param"); return;
      case SZ_ERROR_OUTPUT_EOF: stderr.writeln("FUCK: compressed is bigger"); return;
      default: stderr.writeln("FUCK: something else"); return;
    }

    writeln("compressed ", intWithCommas(insize), " to ", intWithCommas(destLen), "; ratio: ", destLen*100U/insize, "%");

    auto fo = VFile(args[3], "w");
    fo.writeNum!ubyte(cast(ubyte)usize.sizeof);
    fo.writeNum!ubyte(cast(ubyte)headerSize);
    fo.writeNum!usize(insize);
    fo.writeNum!usize(destLen);
    fo.rawWriteExact(header[0..headerSize]);
    fo.rawWriteExact(outbuf[0..destLen]);
    fo.close();

    lzmaDefAllocator.Free(&lzmaDefAllocator, outbuf);
  } else if (args[1] == "x") {
    usize unsize = 0;
    usize pksize = 0;
    ubyte[LZMA_PROPS_SIZE+8] header;
    uint headerSize = 0;

    auto fi = VFile(args[2]);
    if (fi.readNum!ubyte != cast(ubyte)usize.sizeof) throw new Exception("invalid arch");
    headerSize = fi.readNum!ubyte;
    if (!headerSize || headerSize > cast(uint)header.sizeof) throw new Exception("invalid header size");
    unsize = fi.readNum!usize;
    pksize = fi.readNum!usize;
    fi.rawReadExact(header[0..headerSize]);
    ubyte *inbuf = cast(ubyte*)lzmaDefAllocator.Alloc(&lzmaDefAllocator, pksize);
    fi.rawReadExact(inbuf[0..pksize]);
    fi.close();

    ubyte *outbuf = cast(ubyte*)lzmaDefAllocator.Alloc(&lzmaDefAllocator, unsize);
    usize srcLen = pksize;
    usize destLen = unsize;
    ELzmaStatus status = 0;
    writeln("decompressing...");
    int res = LzmaDecode(outbuf, &destLen, inbuf, &srcLen, header.ptr, headerSize, /*LZMA_FINISH_END*/LZMA_FINISH_ANY, &status, &lzmaDefAllocator);
    lzmaDefAllocator.Free(&lzmaDefAllocator, inbuf);

    switch (res) {
      case SZ_OK:
        switch (status) {
          case LZMA_STATUS_FINISHED_WITH_MARK: break;
          case LZMA_STATUS_NOT_FINISHED: stderr.writeln("FUCK: unfinished"); return;
          case LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK: break; //stderr.writeln("FUCK: finished w/o mark"); return;
          default: stderr.writeln("FUCK: fucked status"); return;
        }
        break;
      case SZ_ERROR_DATA: stderr.writeln("FUCK: data error"); return;
      case SZ_ERROR_MEM: stderr.writeln("FUCK: memory"); return;
      case SZ_ERROR_UNSUPPORTED: stderr.writeln("FUCK: unsupported properties"); return;
      case SZ_ERROR_INPUT_EOF: stderr.writeln("FUCK: out of compressed data"); return;
      default: stderr.writeln("FUCK: something else"); return;
    }

    auto fo = VFile(args[3], "w");
    fo.rawWriteExact(outbuf[0..destLen]);
    fo.close();

    lzmaDefAllocator.Free(&lzmaDefAllocator, outbuf);
  }
}
