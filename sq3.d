/* Invisible Vector Library
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// sqlite3 helpers
module iv.sq3 is aliced;

//version = sq3_debug_stmtlist;

import iv.alice;

public import iv.c.sqlite3;
//private import std.traits;
private import std.range.primitives;

private import std.internal.cstring : tempCString;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared logToStderr = true;


// ////////////////////////////////////////////////////////////////////////// //
// use this in `to` to avoid copying
public alias SQ3Blob = const(char)[];
// use this in `to` to avoid copying
public alias SQ3Text = const(char)[];


// ////////////////////////////////////////////////////////////////////////// //
struct DBFieldIndex {
  uint idx;
}

struct DBFieldType {
  enum {
    Unknown,
    Integer,
    Float,
    Text,
    Blob,
    Null,
  }
  uint idx;

  string toString () const pure nothrow @trusted @nogc {
    switch (idx) {
      case Unknown: return "Unknown";
      case Integer: return "Integer";
      case Float: return "Float";
      case Text: return "Text";
      case Blob: return "Blob";
      case Null: return "Null";
      default: break;
    }
    return "Invalid";
  }
}


////////////////////////////////////////////////////////////////////////////////
mixin(NewExceptionClass!("SQLiteException", "Exception"));


class SQLiteErr : SQLiteException {
  int code;

  this (string msg, string file=__FILE__, usize line=__LINE__, Throwable next=null) @trusted nothrow {
    code = SQLITE_ERROR;
    super("SQLite ERROR: "~msg, file, line, next);
  }

  this (sqlite3* db, int rc, string file=__FILE__, usize line=__LINE__, Throwable next=null) @trusted nothrow {
    //import core.stdc.stdio : stderr, fprintf;
    //fprintf(stderr, "SQLITE ERROR: %s\n", sqlite3_errstr(rc));
    code = rc;
    if (rc == SQLITE_OK) {
      super("SQLite ERROR: no error!", file, line, next);
    } else {
      import std.exception : assumeUnique;
      import std.string : fromStringz;
      if (db) {
        super(sqlite3_errstr(sqlite3_extended_errcode(db)).fromStringz.assumeUnique, file, line, next);
      } else {
        super(sqlite3_errstr(rc).fromStringz.assumeUnique, file, line, next);
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public static bool isUTF8ValidSQ3 (const(char)[] str) pure nothrow @trusted @nogc {
  usize len = str.length;
  immutable(ubyte)* p = cast(immutable(ubyte)*)str.ptr;
  while (len--) {
    immutable ubyte b = *p++;
    if (b == 0) return false;
    if (b < 128) continue;
    ubyte blen =
      (b&0xe0) == 0xc0 ? 1 :
      (b&0xf0) == 0xe0 ? 2 :
      (b&0xf8) == 0xe8 ? 3 :
      0; // no overlongs
    if (!blen) return false;
    if (len < blen) return false;
    len -= blen;
    while (blen--) {
      immutable ubyte b1 = *p++;
      if ((b1&0xc0) != 0x80) return false;
    }
  }
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
public void sq3check() (sqlite3* db, int rc, string file=__FILE__, usize line=__LINE__) {
  pragma(inline, true);
  if (rc != SQLITE_OK) throw new SQLiteErr(db, rc, file, line);
}

extern(C) {
  static usize isSqlError (const(char)* zMsg) {
    usize pos = 0;
    while (zMsg[pos] != 0 && zMsg[pos] != '\n') {
      if (zMsg[pos + 0] == ' ' && zMsg[pos + 1] == 'i' &&
          zMsg[pos + 2] == 'n' && zMsg[pos + 3] == ' ' &&
          zMsg[pos + 4] == '"')
      {
        return pos + 5;
      }
      pos += 1;
    }
    return 0;
  }
  //SQLITE ERROR LOG (1): no such table: packagesz in "INSERT INTO packages

  static void sq3PrintStderrErr (const(char)* pfx, int iErrCode, const(char)* zMsg) {
    import core.stdc.stdio : stderr, fprintf, fflush, fputc;
    fflush(null); /* flush all output */
    fprintf(stderr, "\r%s (%d): ", pfx, iErrCode);
    bool wasNL = false;
    usize expos = isSqlError(zMsg);
    if (expos) {
      fprintf(stderr, "%.*s\n", cast(uint)expos, zMsg);
      zMsg += expos;
      wasNL = true;
    }
    while (zMsg[0] != 0) {
      usize end = 0;
      while (zMsg[end] != 0 && zMsg[end] != '\n') end += 1;
      if (end == 0) {
        zMsg += 1;
      } else {
        usize ee = end;
        while (ee != 0 && zMsg[ee - 1] <= 32) ee -= 1;
        if (ee != 0) {
          bool seenBad = false;
          usize pos = 0;
          while (pos != ee && !seenBad) {
            seenBad = zMsg[pos] == 127 || (zMsg[pos] < 32 && zMsg[pos] != 9);
            pos += 1;
          }
          seenBad = true;
          if (wasNL) fprintf(stderr, "... ");
          if (seenBad) {
            pos = 0;
            while (pos != ee) {
              char ch = zMsg[pos];
              if (ch == 127 || (ch < 32 && ch != 9)) {
                fprintf(stderr, "\\x%02x", cast(uint)ch);
              } else {
                fputc(ch, stderr);
              }
              pos += 1;
            }
            fputc('\n', stderr);
          } else {
            fprintf(stderr, "%.*s\n", cast(uint)ee, zMsg);
          }
        }
        zMsg += end;
        if (zMsg[0] == '\n') zMsg += 1;
        wasNL = true;
      }
    }
    if (!wasNL) fprintf(stderr, "wtf?!\n");
  }

  static void sq3ErrorLogCallback (void* pArg, int iErrCode, const(char)* zMsg) {
    if (!logToStderr) return;
    switch (iErrCode) {
      case SQLITE_NOTICE:
        sq3PrintStderrErr("SQLITE NOTICE", iErrCode, zMsg);
        break;
      case SQLITE_WARNING:
        sq3PrintStderrErr("SQLITE WARNING", iErrCode, zMsg);
        break;
      case SQLITE_NOTICE_RECOVER_WAL:
        sq3PrintStderrErr("SQLITE WAL RECOVER", iErrCode, zMsg);
        break;
      case SQLITE_NOTICE_RECOVER_ROLLBACK:
        sq3PrintStderrErr("SQLITE ROLLBACK RECOVER", iErrCode, zMsg);
        break;
      case SQLITE_WARNING_AUTOINDEX:
        sq3PrintStderrErr("SQLITE AUTOINDEX WARNING", iErrCode, zMsg);
        break;
      default:
        sq3PrintStderrErr("SQLITE ERROR LOG", iErrCode, zMsg);
        break;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
// WARNING! don't forget to finalize ALL prepared statements!
struct Database {
private:
  static struct DBInfo {
    sqlite3* db = null;
    uint rc = 0;
    usize onCloseSize = 0;
    char *onClose = null; // 0-terminated
    DBStatement.StmtData *stmthead = null;
    DBStatement.StmtData *stmttail = null;
  }

  usize udbi = 0;

  static __gshared errCBInited = false;

private:
  @property inout(DBInfo)* dbi () inout pure nothrow @trusted @nogc { pragma(inline, true); return cast(inout(DBInfo)*)udbi; }
  @property void dbi (DBInfo *adbi) nothrow @trusted @nogc { pragma(inline, true); udbi = cast(usize)adbi; }

private:
  // caller should perform all necessary checks
  void clearStatements () nothrow @trusted @nogc {
    for (;;) {
      DBStatement.StmtData *dd = dbi.stmthead;
      if (dd is null) break;
      version(sq3_debug_stmtlist) {
        import core.stdc.stdio : stderr, fprintf;
        fprintf(stderr, "clearStatements(%p): p=%p\n", dbi, dbi.stmthead);
      }
      dbi.stmthead = dd.next;
      dd.owner = null;
      dd.prev = null;
      dd.next = null;
      dd.stepIndex = 0;
      if (dd.st !is null) {
        sqlite3_reset(dd.st);
        sqlite3_clear_bindings(dd.st);
        sqlite3_finalize(dd.st);
        dd.st = null;
      }
    }
    dbi.stmttail = null;
  }

public:
  enum Mode {
    ReadOnly,
    ReadWrite,
    ReadWriteCreate,
  }

  alias getHandle this;

public:
  @disable this (usize);

  /// `null` schema means "open as R/O"
  /// non-null, but empty schema means "open as r/w"
  /// non-empty scheme means "create if absent"
  this (const(char)[] name, Mode mode, const(char)[] pragmas=null, const(char)[] schema=null) { openEx(name, mode, pragmas, schema); }
  this (const(char)[] name, const(char)[] schema) { openEx(name, (schema !is null ? Mode.ReadWriteCreate : Mode.ReadOnly), null, schema); }

  ~this () nothrow @trusted { pragma(inline, true); if (udbi) { if (--dbi.rc == 0) { ++dbi.rc; close(); } } }

  this (this) nothrow @trusted @nogc { pragma(inline, true); if (udbi) ++dbi.rc; }

  @property bool isOpen () const pure nothrow @trusted @nogc { return (udbi && (cast(const(DBInfo)*)udbi).db !is null); }

  void close () nothrow @trusted {
    if (!udbi) return;
    if (--dbi.rc == 0) {
      import core.stdc.stdlib : free;
      if (dbi.db !is null) {
        clearStatements();
        if (dbi.onClose !is null) {
          auto rc = sqlite3_exec(dbi.db, dbi.onClose, null, null, null);
          if (rc != SQLITE_OK) {
            import core.stdc.stdio : stderr, fprintf;
            fprintf(stderr, "SQLITE ERROR ON CLOSE: %s\n", sqlite3_errstr(sqlite3_extended_errcode(dbi.db)));
          }
          version(none) {
            import core.stdc.stdio : stderr, fprintf;
            fprintf(stderr, "exec:===\n%s\n===\n", dbi.onClose);
          }
          free(dbi.onClose);
        }
        sqlite3_close_v2(dbi.db);
      }
      free(dbi);
    }
    udbi = 0;
  }

  void appendOnClose (const(char)[] stmts) {
    if (!isOpen) throw new SQLiteException("database is not opened");
    while (stmts.length && stmts[0] <= 32) stmts = stmts[1..$];
    while (stmts.length && stmts[$-1] <= 32) stmts = stmts[0..$-1];
    if (stmts.length == 0) return;
    import core.stdc.stdlib : realloc;
    //FIXME: overflow. don't do it.
    usize nsz = dbi.onCloseSize+stmts.length;
    if (nsz+1 <= dbi.onCloseSize) throw new SQLiteException("out of memory for OnClose");
    char *np = cast(char *)realloc(dbi.onClose, nsz+1);
    if (np is null) throw new SQLiteException("out of memory for OnClose");
    dbi.onClose = np;
    np[dbi.onCloseSize..dbi.onCloseSize+stmts.length] = stmts[];
    dbi.onCloseSize += stmts.length;
    np[dbi.onCloseSize] = 0;
  }

  void setOnClose (const(char)[] stmts) {
    if (!isOpen) throw new SQLiteException("database is not opened");
    if (dbi.onClose !is null) {
      import core.stdc.stdlib : free;
      free(dbi.onClose);
      dbi.onClose = null;
      dbi.onCloseSize = 0;
    }
    appendOnClose(stmts);
  }

  // `null` schema means "open as R/O"
  // non-null, but empty schema means "open as r/w"
  // non-empty scheme means "create if absent"
  void openEx (const(char)[] name, Mode mode, const(char)[] pragmas=null, const(char)[] schema=null) {
    close();
    import core.stdc.stdlib : calloc, free;
    import std.internal.cstring;
    immutable bool allowWrite = (mode == Mode.ReadWrite || mode == Mode.ReadWriteCreate);
    bool allowCreate = (mode == Mode.ReadWriteCreate);
    if (!errCBInited) {
      // we are interested in all errors
      sqlite3_config(SQLITE_CONFIG_LOG, &sq3ErrorLogCallback, null);
      errCBInited = true;
      if (sqlite3_initialize() != SQLITE_OK) {
        throw new Error("cannot initialise SQLite");
      }
    }
    if (allowCreate) {
      while (schema.length && schema[$-1] <= ' ') schema = schema[0..$-1];
      if (schema.length == 0) allowCreate = false;
    }
    dbi = cast(DBInfo *)calloc(1, DBInfo.sizeof);
    if (dbi is null) throw new Error("out of memory");
    //*dbi = DBInfo.init;
    dbi.rc = 1;
    immutable int rc = sqlite3_open_v2(name.tempCString, &dbi.db, (allowWrite ? SQLITE_OPEN_READWRITE : SQLITE_OPEN_READONLY)|(allowCreate ? SQLITE_OPEN_CREATE : 0), null);
    if (rc != SQLITE_OK) {
      free(dbi);
      dbi = null;
      sq3check(null, rc);
    }
    scope(failure) { close(); }
    if (dbi) sqlite3_extended_result_codes(dbi.db, 1);
    if (pragmas.length) {
      execute(pragmas);
      execute(`PRAGMA trusted_schema = ON;`);
    }
    if (allowCreate && schema.length) execute(schema);
  }

  // `null` schema means "open as R/O"
  void open (const(char)[] name, const(char)[] schema=null) {
    Mode mode = Mode.ReadOnly;
    if (schema != null) {
      while (schema.length && schema[$-1] <= ' ') schema = schema[0..$-1];
      mode = (schema.length ? Mode.ReadWriteCreate : Mode.ReadWrite);
    }
    return openEx(name, mode, null, schema);
  }

  ulong lastRowId () nothrow @trusted @nogc { return (isOpen ? sqlite3_last_insert_rowid(dbi.db) : 0); }

  void setBusyTimeout (int msecs) nothrow @trusted @nogc { if (isOpen) sqlite3_busy_timeout(dbi.db, msecs); }
  void setMaxBusyTimeout () nothrow @trusted @nogc { if (isOpen) sqlite3_busy_timeout(dbi.db, 0x1fffffff); }

  // will change if any change to the database occured, either with this connection, or with any other connection
  uint getDataVersion () nothrow @trusted @nogc {
    pragma(inline, true);
    if (!isOpen) return 0;
    uint res;
    if (sqlite3_file_control(dbi.db, "main", SQLITE_FCNTL_DATA_VERSION, &res) != SQLITE_OK) res = 0;
    return res;
  }

  // will change if any change to the database occured, either with this connection, or with any other connection
  uint getTempDataVersion () nothrow @trusted @nogc {
    pragma(inline, true);
    if (!isOpen) return 0;
    uint res;
    if (sqlite3_file_control(dbi.db, "temp", SQLITE_FCNTL_DATA_VERSION, &res) != SQLITE_OK) res = 0;
    return res;
  }

  // will change if any change to the database occured, either with this connection, or with any other connection
  uint getDBDataVersion (const(char)[] dbname) nothrow @trusted @nogc {
    import core.stdc.stdlib : malloc, free;
    if (!isOpen || dbname.length == 0) return 0;
    char* ts = cast(char*)malloc(dbname.length+1);
    if (ts is null) return 0;
    scope(exit) free(ts);
    ts[0..dbname.length] = dbname[];
    ts[dbname.length] = 0;
    uint res;
    if (sqlite3_file_control(dbi.db, ts, SQLITE_FCNTL_DATA_VERSION, &res) != SQLITE_OK) res = 0;
    return res;
  }

  // execute one or more SQL statements
  // SQLite will take care of splitting
  void execute (const(char)[] ops) {
    if (!isOpen) throw new SQLiteException("database is not opened");
    sq3check(dbi.db, sqlite3_exec(dbi.db, ops.tempCString, null, null, null));
  }

  // create prepared SQL statement
  DBStatement statement (const(char)[] stmtstr, bool persistent=false) {
    if (!isOpen) throw new SQLiteException("database is not opened");
    return DBStatement(dbi, stmtstr, persistent);
  }

  DBStatement persistentStatement (const(char)[] stmtstr) {
    return statement(stmtstr, persistent:true);
  }

  @property sqlite3* getHandle () nothrow @trusted @nogc { return (isOpen ? dbi.db : null); }

  extern(C) {
    alias UserFn = void function (sqlite3_context *ctx, int argc, sqlite3_value **argv);
  }

  void createFunction (const(char)[] name, int argc, UserFn xFunc, bool deterministic=true, int moreflags=0) {
    import std.internal.cstring : tempCString;
    if (!isOpen) throw new SQLiteException("database is not opened");
    immutable int rc = sqlite3_create_function(dbi.db, name.tempCString, argc, SQLITE_UTF8|(deterministic ? SQLITE_DETERMINISTIC : 0)|moreflags, null, xFunc, null, null);
    sq3check(dbi.db, rc);
  }

  private static void performLoopedExecOn(string stmt, int seconds) (sqlite3 *db) {
    if (db is null) throw new SQLiteException("database is not opened");
    static if (seconds > 0) {
      //int tries = 30000; // ~300 seconds
      int tries = seconds*100;
    }
    for (;;) {
      immutable rc = sqlite3_exec(db, stmt, null, null, null);
      if (rc == SQLITE_OK) break;
      if (rc != SQLITE_BUSY) sq3check(db, rc);
      static if (seconds > 0) {
        if (--tries == 0) sq3check(db, rc);
      }
      sqlite3_sleep(10);
    }
  }

  static void beginTransactionOn(int seconds=-1) (sqlite3 *db) { performLoopedExecOn!(`BEGIN IMMEDIATE TRANSACTION;`, seconds)(db); }
  static void commitTransactionOn(int seconds=-1) (sqlite3 *db) { performLoopedExecOn!(`COMMIT TRANSACTION;`, seconds)(db); }
  static void rollbackTransactionOn(int seconds=-1) (sqlite3 *db) { performLoopedExecOn!(`ROLLBACK TRANSACTION;`, seconds)(db); }

  void beginTransaction(int seconds=-1) () { if (!isOpen) throw new SQLiteException("database is not opened"); beginTransactionOn!seconds(dbi.db); }
  void commitTransaction(int seconds=-1) () { if (!isOpen) throw new SQLiteException("database is not opened"); commitTransactionOn!seconds(dbi.db); }
  void rollbackTransaction(int seconds=-1) () { if (!isOpen) throw new SQLiteException("database is not opened"); rollbackTransactionOn!seconds(dbi.db); }

  void transacted(int seconds=-1) (scope void delegate () dg) {
    if (dg is null) return;
    if (!isOpen) throw new SQLiteException("database is not opened");
    beginTransaction!seconds();
    scope(success) commitTransaction!seconds();
    scope(failure) rollbackTransaction!seconds();
    dg();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct DBStatement {
private:
  enum FieldIndexMixin = `
    if (!valid) throw new SQLiteException("cannot bind field \""~name.idup~"\" in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (name.length == 0) throw new SQLiteException("empty field name");
    if (name.length > 63) throw new SQLiteException("field name too long");
    char[64] fldname = void;
    if (name.ptr[0] == ':' || name.ptr[0] == '?' || name.ptr[0] == '@' || name.ptr[0] == '$') {
      fldname[0..name.length] = name[];
      fldname[name.length] = 0;
    } else {
      fldname[0] = ':';
      fldname[1..name.length+1] = name[];
      fldname[name.length+1] = 0;
    }
    immutable idx = sqlite3_bind_parameter_index(data.st, fldname.ptr);
    if (idx < 1) throw new SQLiteException("invalid field name: \""~name.idup~"\"");
  `;

private:
  static struct StmtData {
    uint refcount = 0;
    uint rowcount = 0; // number of row structs using this statement
    uint stepIndex = 0;
    sqlite3_stmt* st = null;
    StmtData* prev = null;
    StmtData* next = null;
    Database.DBInfo* owner = null;
  }

  usize datau = 0;

private:
  @property inout(StmtData)* data () inout pure nothrow @trusted @nogc { pragma(inline, true); return cast(inout(StmtData)*)datau; }
  @property void data (StmtData *adbi) nothrow @trusted @nogc { pragma(inline, true); datau = cast(usize)adbi; }

private:
  static void clearStmt (StmtData* dta) nothrow @trusted {
    assert(dta);
    assert(dta.rowcount == 0);
    dta.stepIndex = 0;
    if (dta.st !is null) {
      sqlite3_reset(dta.st);
      sqlite3_clear_bindings(dta.st);
    }
  }

  static void killData (StmtData* dta) nothrow @trusted {
    assert(dta);
    assert(dta.refcount == 0);
    import core.stdc.stdlib : free;
    if (dta.st !is null) {
      sqlite3_reset(dta.st);
      sqlite3_clear_bindings(dta.st);
      sqlite3_finalize(dta.st);
      dta.st = null;
    }
    // unregister from the owner list
    if (dta.owner) {
      version(sq3_debug_stmtlist) {
        import core.stdc.stdio : stderr, fprintf;
        fprintf(stderr, "removing stmt(%p): p=%p\n", dta.owner, dta);
      }
      if (dta.prev) dta.prev.next = dta.next; else dta.owner.stmthead = dta.next;
      if (dta.next) dta.next.prev = dta.prev; else dta.owner.stmttail = dta.prev;
      dta.owner = null;
    }
    free(dta);
  }

private:
  static void incrowref (const usize datau) nothrow @nogc @trusted {
    pragma(inline, true);
    if (datau) {
      ++(cast(StmtData*)datau).refcount;
      ++(cast(StmtData*)datau).rowcount;
    }
  }

  static void decref (ref usize udata) nothrow @trusted {
    pragma(inline, true);
    if (udata) {
      if (--(cast(StmtData*)udata).refcount == 0) killData(cast(StmtData*)udata);
      udata = 0;
    }
  }

  static void decrowref (ref usize udata) nothrow @trusted {
    pragma(inline, true);
    if (udata) {
      if (--(cast(StmtData*)udata).rowcount == 0) clearStmt(cast(StmtData*)udata);
      decref(ref udata);
    }
  }

private:
  // skips blanks and comments
  static const(char)[] skipSQLBlanks (const(char)[] s) nothrow @trusted @nogc {
    while (s.length) {
      char ch = s.ptr[0];
      if (ch <= 32 || ch == ';') { s = s[1..$]; continue; }
      switch (ch) {
        case '-': // single-line comment
          if (s.length == 1 || s.ptr[1] != '-') return s;
          while (s.length && s.ptr[0] != '\n') s = s[1..$];
          break;
        case '/': // multi-line comment
          if (s.length == 1 || s.ptr[1] != '*') return s;
          s = s[2..$];
          while (s.length) {
            ch = s.ptr[0];
            s = s[1..$];
            if (ch == '*' && s.length && s.ptr[1] == '/') {
              s = s[1..$];
              break;
            }
          }
          break;
        default:
          return s;
      }
    }
    return s;
  }

public:
  @disable this (usize);
  this (this) nothrow @trusted @nogc { pragma(inline, true); if (datau) ++(cast(StmtData*)datau).refcount; }
  ~this () nothrow @trusted { pragma(inline, true); decref(ref datau); }

  private this (Database.DBInfo *dbi, const(char)[] stmtstr, bool persistent=false) {
    if (dbi is null || dbi.db is null) throw new SQLiteException("database is not opened");
    auto anchor = stmtstr; // for GC
    stmtstr = skipSQLBlanks(stmtstr);
    if (stmtstr.length > int.max/4) throw new SQLiteException("SQL statement too big");
    if (stmtstr.length == 0) throw new SQLiteException("empty SQL statement");
    version(none) {
      import core.stdc.stdio : stderr, fprintf;
      fprintf(stderr, "stmt:===\n%.*s\n===\n", cast(uint)stmtstr.length, stmtstr.ptr);
    }
    import core.stdc.stdlib : calloc;
    data = cast(StmtData*)calloc(1, StmtData.sizeof);
    if (data is null) { import core.exception : onOutOfMemoryErrorNoGC; onOutOfMemoryErrorNoGC(); }
    //assert(datau);
    //*data = StmtData.init;
    data.refcount = 1;
    // register in the owner list
    version(sq3_debug_stmtlist) {
      import core.stdc.stdio : stderr, fprintf;
      fprintf(stderr, "new stmt(%p): p=%p\n", dbi, data);
    }
    data.owner = dbi;
    data.prev = dbi.stmttail;
    if (dbi.stmttail !is null) dbi.stmttail.next = data; else dbi.stmthead = data;
    dbi.stmttail = data;
    // done registering
    scope(failure) { data.st = null; decref(datau); }
    const(char)* e;
    if (persistent) {
      sq3check(dbi.db, sqlite3_prepare_v3(dbi.db, stmtstr.ptr, cast(int)stmtstr.length, SQLITE_PREPARE_PERSISTENT, &data.st, &e));
    } else {
      sq3check(dbi.db, sqlite3_prepare_v2(dbi.db, stmtstr.ptr, cast(int)stmtstr.length, &data.st, &e));
    }
    // check for extra code
    if (e !is null) {
      usize left = stmtstr.length-cast(usize)(e-stmtstr.ptr);
      stmtstr = skipSQLBlanks(e[0..left]);
      if (stmtstr.length) throw new SQLiteErr("extra code in SQL statement (text): "~stmtstr.idup);
    }
  }

  @property bool valid () const pure nothrow @trusted @nogc { return (datau && data.st !is null); }
  @property bool busy () const pure nothrow @trusted @nogc { return (datau && data.st !is null && data.stepIndex); }

  void close () nothrow @trusted { pragma(inline, true); decref(datau); }

  @property auto range () {
    if (!valid) throw new SQLiteException("cannot get range from invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't get range from busy statement");
    if (data.st is null) throw new SQLiteException("can't get range of empty statement");
    return DBRowRange(this);
  }

  void reset () nothrow @trusted {
    //if (data.stepIndex != 0) throw new SQLiteException("can't reset busy statement");
    if (valid) {
      data.stepIndex = 0;
      sqlite3_reset(data.st);
      sqlite3_clear_bindings(data.st);
    }
  }

  void doAll (void delegate (sqlite3_stmt* stmt) dg=null) {
    if (!valid) throw new SQLiteException("cannot execute invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't doAll on busy statement");
    scope(exit) reset();
    for (;;) {
      immutable rc = sqlite3_step(data.st);
      if (rc == SQLITE_DONE) break;
      if (rc != SQLITE_ROW) sq3check(data.owner.db, rc);
      if (dg !is null) dg(data.st);
    }
  }

  void beginTransaction () {
    if (!valid) throw new SQLiteException("cannot execute invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't doAll on busy statement");
    Database.beginTransactionOn(data.owner.db);
  }

  void commitTransaction () {
    if (!valid) throw new SQLiteException("cannot execute invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't doAll on busy statement");
    Database.commitTransactionOn(data.owner.db);
  }

  void rollbackTransaction () {
    if (!valid) throw new SQLiteException("cannot execute invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't doAll on busy statement");
    Database.rollbackTransactionOn(data.owner.db);
  }

  void doTransacted (void delegate (sqlite3_stmt* stmt) dg=null) {
    if (!valid) throw new SQLiteException("cannot execute invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't doAll on busy statement");
    scope(exit) reset();
    Database.beginTransactionOn(data.owner.db);
    scope(success) Database.commitTransactionOn(data.owner.db);
    scope(failure) Database.rollbackTransactionOn(data.owner.db);
    for (;;) {
      immutable rc = sqlite3_step(data.st);
      if (rc == SQLITE_DONE) break;
      if (rc != SQLITE_ROW) sq3check(data.owner.db, rc);
      if (dg !is null) dg(data.st);
    }
  }

  ref DBStatement bind(T) (uint idx, T value)
    if (is(T:const(char)[]) ||
        is(T:const(byte)[]) ||
        is(T:const(ubyte)[]) ||
        __traits(isIntegral, T) ||
        (__traits(isFloating, T) && T.sizeof <= 8))
  {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1 || idx > sqlite3_bind_parameter_count(data.st)) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    int rc = void;
    static if (is(T == typeof(null))) {
      rc = sqlite3_bind_null(data.st, idx);
    } else static if (is(T:const(char)[])) {
      if (value.length >= cast(usize)int.max) throw new SQLiteException("value too big");
      rc = sqlite3_bind_text(data.st, idx, value.ptr, cast(int)value.length, SQLITE_TRANSIENT);
    } else static if (is(T:const(byte)[]) || is(T:const(ubyte)[])) {
      if (value.length >= cast(usize)int.max) throw new SQLiteException("value too big");
      rc = sqlite3_bind_blob(data.st, idx, value.ptr, cast(int)value.length, SQLITE_TRANSIENT);
    } else static if (__traits(isIntegral, T)) {
      static if (__traits(isUnsigned, T)) {
        // unsigned ints
        static if (T.sizeof < 4) {
          rc = sqlite3_bind_int(data.st, idx, cast(int)cast(uint)value);
        } else {
          rc = sqlite3_bind_int64(data.st, idx, cast(ulong)value);
        }
      } else {
        // signed ints
        static if (T.sizeof <= 4) {
          rc = sqlite3_bind_int(data.st, idx, cast(int)value);
        } else {
          rc = sqlite3_bind_int64(data.st, idx, cast(long)value);
        }
      }
    } else static if (__traits(isFloating, T) && T.sizeof <= 8) {
      rc = sqlite3_bind_double(data.st, idx, cast(double)value);
    } else {
      static assert(0, "WTF?!");
    }
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bind(T) (const(char)[] name, T value)
    if (is(T:const(char)[]) ||
        is(T:const(byte)[]) ||
        is(T:const(ubyte)[]) ||
        __traits(isIntegral, T) ||
        (__traits(isFloating, T) && T.sizeof <= 8))
  {
    mixin(FieldIndexMixin);
    return bind!T(idx, value);
  }


  ref DBStatement bindText (uint idx, const(void)[] text, bool transient=true, bool allowNull=false) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    int rc;
    if (text is null) {
      if (allowNull) {
        rc = sqlite3_bind_null(data.st, idx);
      } else {
        rc = sqlite3_bind_text(data.st, idx, "".ptr, 0, SQLITE_STATIC);
      }
    } else {
      rc = sqlite3_bind_text(data.st, idx, cast(const(char)*)text.ptr, cast(int)text.length, (transient ? SQLITE_TRANSIENT : SQLITE_STATIC));
    }
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindConstText (uint idx, const(void)[] text, bool allowNull=false) {
    return bindText(idx, text, false, allowNull);
  }

  ref DBStatement bindText (const(char)[] name, const(void)[] text, bool transient=true, bool allowNull=false) {
    mixin(FieldIndexMixin);
    return bindText(cast(uint)idx, text, transient, allowNull);
  }

  ref DBStatement bindConstText (const(char)[] name, const(void)[] text, bool allowNull=false) {
    mixin(FieldIndexMixin);
    return bindConstText(cast(uint)idx, text, allowNull);
  }


  ref DBStatement bindBlob (uint idx, const(void)[] blob, bool transient=true, bool allowNull=false) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    int rc;
    if (blob is null) {
      if (allowNull) {
        rc = sqlite3_bind_null(data.st, idx);
      } else {
        rc = sqlite3_bind_blob(data.st, idx, "".ptr, 0, SQLITE_STATIC);
      }
    } else {
      rc = sqlite3_bind_blob(data.st, idx, blob.ptr, cast(int)blob.length, (transient ? SQLITE_TRANSIENT : SQLITE_STATIC));
    }
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindConstBlob (uint idx, const(void)[] blob, bool allowNull=false) {
    return bindBlob(idx, blob, false, allowNull);
  }

  ref DBStatement bindBlob (const(char)[] name, const(void)[] blob, bool transient=true, bool allowNull=false) {
    mixin(FieldIndexMixin);
    return bindBlob(cast(uint)idx, blob, transient, allowNull);
  }

  ref DBStatement bindConstBlob (const(char)[] name, const(void)[] blob, bool allowNull=false) {
    mixin(FieldIndexMixin);
    return bindConstBlob(cast(uint)idx, blob, allowNull);
  }


  ref DBStatement bindNull (uint idx) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    immutable int rc = sqlite3_bind_null(data.st, idx);
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindNull (const(char)[] name) {
    mixin(FieldIndexMixin);
    return bindNull(cast(uint)idx);
  }


  ref DBStatement bindInt(T) (uint idx, T v) if (__traits(isIntegral, T)) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    int rc = void;
    static if (__traits(isUnsigned, T)) {
      // unsigned ints
      static if (T.sizeof < 4) {
        rc = sqlite3_bind_int(data.st, idx, cast(int)cast(uint)value);
      } else {
        rc = sqlite3_bind_int64(data.st, idx, cast(ulong)value);
      }
    } else {
      // signed ints
      static if (T.sizeof <= 4) {
        rc = sqlite3_bind_int(data.st, idx, cast(int)value);
      } else {
        rc = sqlite3_bind_int64(data.st, idx, cast(long)value);
      }
    }
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindInt(T) (const(char)[] name, T v) if (__traits(isIntegral, T)) {
    mixin(FieldIndexMixin);
    return bindInt(cast(uint)idx, v);
  }


  ref DBStatement bindFloat(T) (uint idx, T v) if (__traits(isFloating, T) && T.sizeof <= 8) {
    if (!valid) throw new SQLiteException("cannot bind in invalid statement");
    if (data.stepIndex != 0) throw new SQLiteException("can't bind on busy statement");
    if (idx < 1) {
      import std.conv : to;
      throw new SQLiteException("invalid field index: "~to!string(idx));
    }
    rc = sqlite3_bind_double(data.st, idx, cast(double)v);
    sq3check(data.owner.db, rc);
    return this;
  }

  ref DBStatement bindFloat(T) (const(char)[] name, T v) if (__traits(isFloating, T) && T.sizeof <= 8) {
    mixin(FieldIndexMixin);
    return bindFloat(cast(uint)idx, v);
  }

  int colcount () nothrow @trusted @nogc {
    if (!valid) return 0;
    return sqlite3_column_count(data.st);
  }

  const(char)[] colname (int idx) nothrow @trusted @nogc {
    import core.stdc.string : strlen;
    if (!valid || idx < 0) return null;
    if (idx >= sqlite3_column_count(data.st)) return null;
    const(char)* cname = sqlite3_column_name(data.st, idx);
    if (cname is null) return null;
    return cname[0..strlen(cname)];
  }

private:
  struct DBRow {
  private:
    /*DBStatement.StmtData* */usize data_____u;

    @property inout(DBStatement.StmtData)* data____ () inout pure nothrow @trusted @nogc { pragma(inline, true); return cast(inout(DBStatement.StmtData)*)data_____u; }

  public:
    @disable this (usize);

    private this (DBStatement.StmtData* adata) nothrow @trusted @nogc {
      pragma(inline, true);
      data_____u = cast(usize)adata;
      DBStatement.incrowref(cast(usize)adata);
    }

    this (this) nothrow @trusted @nogc { pragma(inline, true); DBStatement.incrowref(data_____u); }
    ~this () nothrow @trusted { pragma(inline, true); DBStatement.decrowref(data_____u); }

    sqlite3_stmt* getStatementHandle () { return (data_____u ? data____.st : null); }

    bool valid_ () pure nothrow @trusted @nogc {
      pragma(inline, true);
      return (data_____u && data____.stepIndex > 0 && data____.st !is null);
    }

    int colcount_ () nothrow @trusted @nogc {
      if (!data_____u || data____.st is null) return 0;
      return sqlite3_column_count(data____.st);
    }

    const(char)[] colname_ (int idx) nothrow @trusted {
      import core.stdc.string : strlen;
      if (idx < 0 || !data_____u || data____.st is null) return null;
      if (idx >= sqlite3_column_count(data____.st)) return null;
      const(char)* cname = sqlite3_column_name(data____.st, idx);
      if (cname is null) return null;
      return cname[0..strlen(cname)];
    }

    int fieldIndex____ (const(char)[] name) {
      if (name.length > 0 && data_____u && data____.st !is null) {
        foreach (immutable int idx; 0..sqlite3_data_count(data____.st)) {
          import core.stdc.string : memcmp, strlen;
          auto n = sqlite3_column_name(data____.st, idx);
          if (n !is null) {
            immutable len = strlen(n);
            if (len == name.length && memcmp(n, name.ptr, len) == 0) return idx;
          }
        }
      }
      throw new SQLiteException("invalid field name: '"~name.idup~"'");
    }

    T to(T) (uint idx)
      if (is(T:const(char)[]) ||
          is(T:const(byte)[]) ||
          is(T:const(ubyte)[]) ||
          __traits(isIntegral, T) ||
          (__traits(isFloating, T)) ||
          is(T:DBFieldIndex) || is(T:DBFieldType))
    {
      if (!valid_) throw new SQLiteException("can't get row field of completed statement");
      if (idx >= sqlite3_data_count(data____.st)) throw new SQLiteException("invalid result index");
      static if (is(T:DBFieldIndex)) {
        return DBFieldIndex(idx);
      } else static if (is(T:DBFieldType)) {
        switch (sqlite3_column_type(data____.st, idx)) {
          case SQLITE_INTEGER: return DBFieldType(DBFieldType.Integer);
          case SQLITE_FLOAT: return DBFieldType(DBFieldType.Float);
          case SQLITE3_TEXT: return DBFieldType(DBFieldType.Text);
          case SQLITE_BLOB: return DBFieldType(DBFieldType.Blob);
          case SQLITE_NULL: return DBFieldType(DBFieldType.Null);
          default: break;
        }
        return DBFieldType(DBFieldType.Unknown);
      } else static if (__traits(isIntegral, T)) {
        auto res = sqlite3_column_int64(data____.st, idx);
        if (res < T.min || res > T.max) throw new SQLiteException("integral overflow");
        return cast(T)res;
      } else static if (__traits(isFloating, T)) {
        return cast(T)sqlite3_column_double(data____.st, idx);
      } else {
        auto len = sqlite3_column_bytes(data____.st, idx);
        if (len < 0) throw new SQLiteException("invalid result");
        const(char)* res;
        if (len == 0) {
          res = "";
        } else {
          res = cast(const(char)*)sqlite3_column_blob(data____.st, idx);
          if (res is null) throw new SQLiteException("invalid result");
        }
        static if (is(T:char[]) || is(T:byte[]) || is(T:ubyte[])) {
          //{ import core.stdc.stdio : printf; printf("***DUP***\n"); }
          return res[0..len].dup;
        } else static if (is(T:immutable(char)[]) || is(T:immutable(byte)[]) || is(T:immutable(ubyte)[])) {
          //{ import core.stdc.stdio : printf; printf("***I-DUP***\n"); }
          return res[0..len].idup;
        } else {
          //{ import core.stdc.stdio : printf; printf("***NO-DUP***\n"); }
          return res[0..len];
        }
      }
    }
    T to(T) (const(char)[] name) { return this.to!T(fieldIndex____(name)); }

    const(ubyte)[] blob_ (const(char)[] name) {
      immutable int idx = fieldIndex____(name);
      auto len = sqlite3_column_bytes(data____.st, idx);
      if (len < 0) throw new SQLiteException("invalid result");
      const(ubyte)* res = cast(const(ubyte)*)sqlite3_column_blob(data____.st, idx);
      if (len == 0 && res is null) return cast(const(ubyte)[])"";
      return res[0..len];
    }

    template opIndex() {
      T opIndexImpl(T) (uint idx)
        if (is(T:const(char)[]) ||
            is(T:const(byte)[]) ||
            is(T:const(ubyte)[]) ||
            __traits(isIntegral, T) ||
            (__traits(isFloating, T)) ||
            is(T:DBFieldIndex) || is(T:DBFieldType))
      { pragma(inline, true); return this.to!T(idx); }
      T opIndexImpl(T) (const(char)[] name)
        if (is(T:const(char)[]) ||
            is(T:const(byte)[]) ||
            is(T:const(ubyte)[]) ||
            __traits(isIntegral, T) ||
            (__traits(isFloating, T)) ||
            is(T:DBFieldIndex) || is(T:DBFieldType))
      { pragma(inline, true); return this.to!T(name); }
      alias opIndex = opIndexImpl;
    }

    template opDispatch(string name) {
      T opDispatchImpl(T=const(char)[]) ()
        if (is(T:const(char)[]) ||
            is(T:const(byte)[]) ||
            is(T:const(ubyte)[]) ||
            __traits(isIntegral, T) ||
            (__traits(isFloating, T)) ||
            is(T:DBFieldIndex) || is(T:DBFieldType))
      { pragma(inline, true); return this.to!T(name); }
      alias opDispatch = opDispatchImpl;
    }

    auto index_ () pure const nothrow @nogc { return (data____.stepIndex > 0 ? data____.stepIndex-1 : 0); }
  } // end of DBRow

private:
  struct DBRowRange {
  private:
    /*DBStatement.StmtData* */usize datau;

    @property inout(DBStatement.StmtData)* data () inout pure nothrow @trusted @nogc { pragma(inline, true); return cast(inout(DBStatement.StmtData)*)datau; }

  public:
    @disable this (usize);

    private this (ref DBStatement astat) {
      datau = astat.datau;
      DBStatement.incrowref(datau);
      assert(data.stepIndex == 0);
      data.stepIndex = 1;
      // perform first step
      popFront!false();
    }

    this (this) nothrow @trusted @nogc { pragma(inline, true); DBStatement.incrowref(datau); }
    ~this () nothrow @trusted { pragma(inline, true); DBStatement.decrowref(datau); }

    @property bool empty () const pure nothrow @trusted @nogc { pragma(inline, true); return (data.stepIndex == 0); }

    @property auto front () {
      if (data.stepIndex == 0) throw new SQLiteException("can't get front element of completed statement");
      return DBRow(data);
    }

    void popFront(bool incrow=true) () {
      if (data.stepIndex == 0) throw new SQLiteException("can't pop element of completed statement");
      auto rc = sqlite3_step(data.st);
      if (rc == SQLITE_DONE) {
        data.stepIndex = 0;
        return;
      }
      if (rc != SQLITE_ROW) {
        data.stepIndex = 0;
        sq3check(data.owner.db, rc);
      }
      static if (incrow) ++data.stepIndex;
    }

    auto index_ () pure const nothrow @trusted @nogc { pragma(inline, true); return (data.stepIndex > 0 ? data.stepIndex-1 : 0); }
  } // end of DBRowRange
}


shared static this () {
  if (sqlite3_libversion_number() < SQLITE_VERSION_NUMBER) {
    //import core.stdc.string : strlen;
    //immutable(char)* lver = sqlite3_libversion();
    //auto len = strlen(lver);
    //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "VER(%u): <%s> : <%s>\n", cast(uint)len, lver, sqlite3_version); }
    throw new Exception("expected SQLite at least v"~SQLITE_VERSION);
  }
}
