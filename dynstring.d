/*
 * simple malloced refcounted dynamic strings
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.dynstring;

private import iv.atomic;
private import iv.strex;
private import iv.utfutil;

//version = dynstring_debug;
//version = dynstring_debug_clear;
//version = dynstring_more_asserts;


// ////////////////////////////////////////////////////////////////////////// //
alias dynstr = dynstring;

public struct dynstring {
private:
  static struct Data {
    uint rc = void;
    uint used = void;
    uint alloted = void;
    uint dummy = void;
    // string data follows

    inout(char)* ptr () inout pure nothrow @trusted @nogc { pragma(inline, true); return (cast(inout(char)*)&this)+Data.sizeof; }
  }
  static assert(Data.sizeof == 16, "invalid `dynstring.Data` size");

private:
  usize udata = 0;
  uint ofs = 0, len = 0; // for slices

nothrow @trusted @nogc:
private:
  inout(Data)* datap () inout pure { pragma(inline, true); return cast(Data*)udata; }

  void outofmem () const {
    import core.exception : onOutOfMemoryErrorNoGC;
    version(dynstring_test) assert(0, "out of memory");
    onOutOfMemoryErrorNoGC();
  }

  void incRef () const {
    pragma(inline, true);
    if (udata) {
      atomicFetchAdd((cast(Data*)udata).rc, 1);
      version(dynstring_debug) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "incRef: %08x; rc=%u; used=%u; alloted=%u; len=%u; ofs=%u\n", cast(uint)udata, datap.rc, datap.used, datap.alloted, len, ofs); }
    }
  }

  void decRef () {
    pragma(inline, true);
    if (udata) {
      version(dynstring_debug) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "decRef: %08x; rc=%u; used=%u; alloted=%u; len=%u; ofs=%u\n", cast(uint)udata, datap.rc, datap.used, datap.alloted, len, ofs); }
      if (atomicFetchSub((cast(Data*)udata).rc, 1) == 1) {
        version(dynstring_debug) {
          import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "freeData: %08x; used=%u; alloted=%u\n", cast(uint)udata, (cast(Data*)udata).used, (cast(Data*)udata).alloted);
        }
        import core.stdc.stdlib : free;
        version(dynstring_debug_clear) import core.stdc.string : memset;
        version(dynstring_debug_clear) assert((cast(Data*)udata).used <= (cast(Data*)udata).alloted);
        version(dynstring_debug_clear) memset((cast(Data*)udata), 0, Data.sizeof);
        free((cast(Data*)udata));
      }
      udata = 0;
      ofs = len = 0;
    }
  }

private:
  bool isMyData (const(void)* ptr) const pure {
    pragma(inline, true);
    if (ptr is null || !udata) return false;
    immutable usize cc = cast(usize)ptr;
    return (cc >= udata+Data.sizeof && cc < udata+Data.sizeof+datap.alloted);
  }

  static uint alignSize(bool overalloc) (uint sz) pure {
    pragma(inline, true);
    static if (overalloc) {
           if (sz <= 256) return (sz|0x1fU)+1U;
      else if (sz <= 1024) return (sz|0x3fU)+1U;
      else if (sz <= 8192) return (sz|0x1ffU)+1U;
      else if (sz <= 16384) return (sz|0x3ffU)+1U;
      else if (sz <= 32768) return (sz|0xfffU)+1U;
      else return (sz|0x1fffU)+1U;
    } else {
      return (sz|0x1fU)+1U;
    }
  }

  // make sure that we have a room for at least `sz` chars
  void ensureRoomFor(bool overalloc) (uint sz) {
    if (sz == 0) return;
    if (sz > 0x3fff_ffffU) outofmem();
    // if empty, allocate new storage and data
    if (!udata) {
      import core.stdc.stdlib : malloc;
      version(dynstring_more_asserts) assert(len == 0);
      sz = alignSize!overalloc(sz);
      Data* xdp = cast(Data*)malloc(Data.sizeof+sz);
      if (xdp is null) outofmem();
      xdp.rc = 1;
      xdp.used = 0;
      xdp.alloted = sz;
      xdp.dummy = 0;
      udata = cast(usize)xdp;
      ofs = len = 0; // just in case
      version(dynstring_debug) {
        import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "NEWSTR000: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u; sz=%u\n",
          cast(uint)udata, (udata ? datap.rc : 0), ofs, len, (udata ? datap.used : 0), (udata ? datap.alloted : 0), sz);
      }
      return;
    }
    // if shared, allocate completely new data and storage
    Data* dp = datap;
    if (atomicLoad(dp.rc) != 1) {
      version(dynstring_debug) {
        import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "NEWSTR100: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u; sz=%u\n",
          cast(uint)udata, (udata ? datap.rc : 0), ofs, len, (udata ? datap.used : 0), (udata ? datap.alloted : 0), sz);
      }
      import core.stdc.stdlib : malloc;
      if (len > 0x3fff_ffffU || uint.max-len <= sz || len+sz > 0x3fff_ffffU) outofmem();
      sz = alignSize!overalloc(len+sz);
      version(dynstring_more_asserts) assert(sz > len);
      Data* dpnew = cast(Data*)malloc(Data.sizeof+sz);
      if (dpnew is null) outofmem();
      dpnew.rc = 1;
      dpnew.used = len;
      dpnew.alloted = sz;
      dpnew.dummy = 0;
      if (len) dpnew.ptr[0..len] = dp.ptr[ofs..ofs+len];
      decRef();
      udata = cast(usize)dpnew;
      ofs = 0; // always reset by `decRef()`, but meh...
      len = dpnew.used; // it was reset by `decRef()`, so restore it
      version(dynstring_debug) {
        import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "NEWSTR101: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u; sz=%u\n",
          cast(uint)udata, (udata ? datap.rc : 0), ofs, len, (udata ? datap.used : 0), (udata ? datap.alloted : 0), sz);
      }
      return;
    }
    // if unshared slice, normalise storage
    if (ofs) {
      version(dynstring_debug) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "strOFS: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u\n", cast(uint)dp, dp.rc, ofs, len, dp.used, dp.alloted); }
      import core.stdc.string : memmove;
      if (len) memmove(dp.ptr, dp.ptr+ofs, len);
      ofs = 0;
    }
    if (len != dp.used) {
      version(dynstring_debug) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "strLEN: udata=0x%08x (0x%08x); rc=%u; ofs=%u; len=%u; used=%u; alloted=%u\n", cast(uint)dp, udata, dp.rc, ofs, len, dp.used, dp.alloted); }
      version(dynstring_more_asserts) assert(len <= dp.used);
      dp.used = len;
    }
    // not shared, not slice, expand storage
    version(dynstring_more_asserts) assert(ofs == 0);
    version(dynstring_more_asserts) assert(len == dp.used);
    if (len > 0x3fff_ffffU || uint.max-len <= sz || len+sz > 0x3fff_ffffU) outofmem();
    sz = alignSize!overalloc(len+sz);
    version(dynstring_more_asserts) assert(sz > len);
    if (sz > dp.alloted) {
      import core.stdc.stdlib : realloc;
      Data* np = cast(Data*)realloc(dp, Data.sizeof+sz);
      if (np is null) outofmem();
      np.alloted = sz;
      udata = cast(usize)np;
      version(dynstring_debug) {
        import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "NEWSTR200: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u; sz=%u\n",
          cast(uint)udata, (udata ? datap.rc : 0), ofs, len, (udata ? datap.used : 0), (udata ? datap.alloted : 0), sz);
      }
    }
  }

  enum MixinEnsureUniqueBuf(string bname) = `
    dynstring tmpstr;
    if (isMyData(`~bname~`.ptr)) {
      version(dynstring_debug) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "MYDATA!: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u; ptr=0x%08x\n",
        cast(uint)udata, datap.rc, ofs, len, datap.used, datap.alloted, cast(uint)`~bname~`.ptr); }
      tmpstr.set(`~bname~`);
      `~bname~` = cast(typeof(`~bname~`))tmpstr.getData();
      version(dynstring_more_asserts) assert(!isMyData(`~bname~`.ptr));
    }
  `;

public:
  alias getData this;

public:
  this() (in auto ref dynstring s) { pragma(inline, true); if (s.len) { udata = s.udata; ofs = s.ofs; len = s.len; incRef(); } }

  this (const(char)[] s) {
    if (s.length) {
      ensureRoomFor!false(s.length);
      datap.ptr[0..s.length] = s[];
      len = datap.used = cast(uint)s.length;
      //ofs = 0;
    }
  }

  this (in char ch) { ensureRoomFor!false(1); datap.ptr[0] = ch; len = datap.used = 1; ofs = 0; }

  this (this) { pragma(inline, true); if (udata) incRef(); }
  ~this () { pragma(inline, true); if (udata) decRef(); }

  void clear () { pragma(inline, true); decRef(); }

  void makeUnique(bool compress=false) () {
    if (!udata) { ofs = len = 0; return; }
    if (!len) { decRef(); return; }
    Data* dp = datap;
    // non-shared?
    if (atomicLoad(dp.rc) == 1) {
      // normalise storage
      if (ofs) {
        import core.stdc.string : memmove;
        if (len) memmove(dp.ptr, dp.ptr+ofs, len);
        ofs = 0;
      }
      if (len != dp.used) { version(dynstring_more_asserts) assert(len <= dp.used); dp.used = len; }
      static if (compress) {
        if ((dp.used|0x1fU)+1U < dp.alloted) {
          // realloc
          import core.stdc.stdlib : realloc;
          immutable uint sz = (dp.used|0x1fU)+1U;
          Data* np = cast(Data*)realloc(dp, Data.sizeof+sz);
          if (np !is null) {
            np.alloted = sz;
            udata = cast(usize)np;
            version(dynstring_debug) {
              import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "UNIQUERE: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u\n",
                cast(uint)udata, (udata ? datap.rc : 0), ofs, len, (udata ? datap.used : 0), (udata ? datap.alloted : 0));
            }
          }
        }
      }
      return;
    }
    // allocate new storage
    dynstring tmpstr;
    tmpstr.set(getData());
    decRef();
    udata = tmpstr.udata;
    ofs = tmpstr.ofs;
    len = tmpstr.len;
    tmpstr.udata = 0;
    tmpstr.ofs = tmpstr.len = 0;
  }

  //WARNING! MAKE SURE THAT YOU KNOW WHAT YOU'RE DOING!
  char *makeUniquePointer () { pragma(inline, true); makeUnique(); return (len ? datap.ptr : null); }

  bool isSlice () const pure { pragma(inline, true); return !!ofs; }
  bool isShared () const pure { pragma(inline, true); return (udata && atomicLoad(datap.rc) != 1); }
  uint capacity () const pure { pragma(inline, true); return (udata ? datap.alloted : 0); }

  void reserve(bool overalloc=false) (uint newsz) {
    pragma(inline, true);
    if (newsz && newsz < 0x3fff_ffffU && newsz > len) ensureRoomFor!overalloc(newsz-len);
  }

  void append(bool overalloc=true) (const(void)[] buf) {
    if (buf.length == 0) return;
    if (buf.length > cast(usize)0x3fff_ffff) outofmem();
    //if (udata && cast(usize)0x3000_0000-datap.used < buf.length) outofmem();
    version(dynstring_debug) {
      import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "APPEND000: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u; ptr=0x%08x; buflen=%u\n",
        cast(uint)udata, (udata ? datap.rc : 0), ofs, len, (udata ? datap.used : 0), (udata ? datap.alloted : 0), cast(uint)buf.ptr, cast(uint)buf.length);
    }
    mixin(MixinEnsureUniqueBuf!"buf");
    version(dynstring_debug) {
      import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "APPEND001: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u; ptr=0x%08x; buflen=%u\n",
        cast(uint)udata, (udata ? datap.rc : 0), ofs, len, (udata ? datap.used : 0), (udata ? datap.alloted : 0), cast(uint)buf.ptr, cast(uint)buf.length);
    }
    ensureRoomFor!overalloc(buf.length);
    version(dynstring_debug) {
      import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "APPEND002: udata=0x%08x; rc=%u; ofs=%u; len=%u; used=%u; alloted=%u; ptr=0x%08x; buflen=%u\n",
        cast(uint)udata, (udata ? datap.rc : 0), ofs, len, (udata ? datap.used : 0), (udata ? datap.alloted : 0), cast(uint)buf.ptr, cast(uint)buf.length);
    }
    version(dynstring_more_asserts) assert(ofs == 0);
    Data* data = datap;
    version(dynstring_more_asserts) assert(data !is null && data.used+buf.length <= data.alloted);
    version(dynstring_more_asserts) assert(len == datap.used);
    data.ptr[len..len+buf.length] = cast(const(char)[])buf;
    data.used += cast(uint)buf.length;
    len += cast(uint)buf.length;
    version(dynstring_more_asserts) assert(len <= data.used);
  }
  void append(bool overalloc=true) (in char ch) { pragma(inline, true); append!overalloc((&ch)[0..1]); }

  void set (const(void)[] buf) {
    if (buf.length > cast(usize)0x3fff_ffff) outofmem();
    if (buf.length == 0) { decRef(); return; }
    if (udata) {
      if (isMyData(buf.ptr)) {
        // do not copy data if it is not necessary
        if (ofs == 0 && buf.ptr == datap.ptr && buf.length <= datap.used) {
          if (len != cast(uint)buf.length) {
            len = cast(uint)buf.length;
            if (atomicLoad(datap.rc) == 1) datap.used = len;
          }
          return;
        }
        dynstring tmpstr;
        tmpstr.set(buf);
        decRef();
        udata = tmpstr.udata;
        ofs = tmpstr.ofs;
        len = tmpstr.len;
        tmpstr.udata = 0;
        tmpstr.ofs = tmpstr.len = 0;
        return;
      }
      decRef();
    }
    version(dynstring_more_asserts) assert(udata == 0 && ofs == 0 && len == 0);
    ensureRoomFor!false(buf.length);
    datap.ptr[0..buf.length] = cast(const(char)[])buf;
    len = datap.used = cast(uint)buf.length;
  }
  void set (in char ch) { pragma(inline, true); set((&ch)[0..1]); }

  uint length () const pure { pragma(inline, true); return len; }

  void length (in uint newlen) {
    pragma(inline, true);
    if (newlen < len) {
      len = newlen;
    } else if (newlen != len) {
      ensureRoomFor!false(newlen-len);
      version(dynstring_more_asserts) assert(len < newlen);
      version(dynstring_more_asserts) assert(ofs == 0);
      version(dynstring_more_asserts) assert(newlen <= datap.alloted);
      datap.ptr[len..newlen] = 0;
      len = datap.used = newlen;
    }
  }

  const(char)[] getData () const pure { pragma(inline, true); return (len ? datap.ptr[ofs..ofs+len] : null); }
  const(ubyte)[] getBytes () const pure { pragma(inline, true); return cast(const(ubyte)[])(len ? datap.ptr[ofs..ofs+len] : null); }

  void opAssign() (const(char)[] s) { pragma(inline, true); set(s); }
  void opAssign() (in char ch) { pragma(inline, true); set(ch); }

  void opAssign() (in auto ref dynstring s) {
    pragma(inline, true);
    if (s.udata != udata) {
      if (s.len) {
        s.incRef();
        decRef();
        udata = s.udata;
        ofs = s.ofs;
        len = s.len;
      } else {
        decRef();
      }
    } else {
      // same data, so RC is already valid
      ofs = s.ofs;
      len = s.len;
    }
  }

  void opOpAssign(string op:"~") (const(char)[] s) { pragma(inline, true); if (s.length) append(s); }
  void opOpAssign(string op:"~") (in char ch) { pragma(inline, true); append(ch); }
  void opOpAssign(string op:"~") (in auto ref dynstring s) { pragma(inline, true); if (s.len) append(s.getData()); }

  const(char)[] opCast(T=const(char)[]) () const pure { pragma(inline, true); return getData(); }
  const(ubyte)[] opCast(T=const(ubyte)[]) () const pure { pragma(inline, true); return getBytes(); }

  bool opCast(T=bool) () const pure { pragma(inline, true); return (len != 0); }

  bool opEqual (const(char)[] other) const pure { pragma(inline, true); return (other.length == len && other[] == getBytes()[]); }
  bool opEqual() (in auto ref dynstring other) const pure { pragma(inline, true); return (other.len == len && other.getBytes()[] == getBytes()[]); }

  int opCmp (const(char)[] other) const pure {
    if (len == 0) return (other.length ? -1 : 0);
    if (other.length == 0) return 1;
    if (len == other.length && datap.ptr+ofs == other.ptr) return 0;
    import core.stdc.string : memcmp;
    immutable int cres = memcmp(datap.ptr+ofs, other.ptr, (len <= other.length ? len : other.length));
    if (cres != 0) return (cres < 0 ? -1 : +1);
    return (len < other.length ? -1 : len > other.length ? +1 : 0);
  }

  int opCmp() (in auto ref dynstring other) const { pragma(inline, true); return opCmp(other.getData); }

  dynstring opBinary(string op:"~") (in auto ref dynstring other) const {
    pragma(inline, true);
    dynstring res;
    if (!len) {
      res.set(other.getData);
    } else if (!other.len) {
      res.set(getData);
    } else {
      if (uint.max-len <= other.len) outofmem();
      res.ensureRoomFor!false(len+other.len);
      res.datap.ptr[0..len] = getData();
      res.datap.ptr[len..len+other.len] = other.getData();
      version(dynstring_more_asserts) assert(res.ofs == 0);
      res.datap.used = res.len = len+other.len;
    }
    return res;
  }

  dynstring opBinary(string op:"~") (const(char)[] other) const {
    pragma(inline, true);
    dynstring res;
    if (!len) {
      res.set(other);
    } else if (!other.length) {
      res.set(getData);
    } else {
      if (uint.max-len <= other.length) outofmem();
      res.ensureRoomFor!false(len+cast(uint)other.length);
      res.datap.ptr[0..len] = getData();
      res.datap.ptr[len..len+other.length] = other;
      version(dynstring_more_asserts) assert(res.ofs == 0);
      res.datap.used = res.len = len+cast(uint)other.length;
    }
    return res;
  }

  char opIndex (in uint pos) const pure {
    pragma(inline, true);
    //if (pos >= len) assert(0, "dynstring index out of bounds"); return datap.ptr[ofs+pos];
    return (pos < len ? datap.ptr[ofs+pos] : 0);
  }

  void opIndexAssign (in char ch, in uint pos) {
    pragma(inline, true);
    if (pos >= len) assert(0, "dynstring index out of bounds");
    makeUnique();
    version(dynstring_more_asserts) assert(pos < len);
    datap.ptr[ofs+pos] = ch;
  }

  dynstring opSlice (in uint lo, in uint hi) const {
    /*
    if (lo > hi) assert(0, "dynstring slice index out of bounds (0)");
    if (lo > len || hi > len) assert(0, "dynstring slice index out of bounds (1)");
    */
    dynstring res;
    if (lo > hi || lo > len || hi > len) return res;
    if (lo < hi) {
      incRef();
      res.udata = udata;
      res.ofs = ofs+lo;
      res.len = hi-lo;
    }
    return res;
  }

  uint opDollar () const pure { pragma(inline, true); return len; }

  void appendQEncoded (const(void)[] ss) {
    static bool isSpecial (immutable char ch) pure nothrow @safe @nogc {
      return
        ch < ' ' ||
        ch >= 127 ||
        ch == '\'' ||
        ch == '`' ||
        ch == '"' ||
        ch == '\\' ||
        ch == '@';
    }

    if (ss.length == 0) return;
    const(char)[] s = cast(const(char)[])ss;

    static immutable string hexd = "0123456789abcdef";

    bool needWork = (s[0] == '=' || s[0] == '?');
    if (!needWork) foreach (char ch; s) if (isSpecial(ch)) { needWork = true; break; }

    if (!needWork) {
      append(s);
    } else {
      mixin(MixinEnsureUniqueBuf!"ss");
      append("=?UTF-8?Q?"); // quoted printable
      foreach (char ch; s) {
        if (ch <= ' ') ch = '_';
        if (!isSpecial(ch) && ch != '=' && ch != '?') {
          append(ch);
        } else {
          append("=");
          append(hexd[(cast(ubyte)ch)>>4]);
          append(hexd[(cast(ubyte)ch)&0x0f]);
        }
      }
      append("?=");
    }
  }

  void appendB64Encoded (const(void)[] buf, uint maxlinelen=76) {
    static immutable string b64alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    ubyte[3] bts = void;
    uint btspos = 0;
    uint linelen = 0;

    void putB64Char (immutable char ch) nothrow @trusted @nogc {
      if (maxlinelen && linelen >= maxlinelen) { append("\r\n"); linelen = 0; }
      append(ch);
      ++linelen;
    }

    void encodeChunk () nothrow @trusted @nogc {
      if (btspos == 0) return;
      putB64Char(b64alphabet.ptr[(bts.ptr[0]&0xfc)>>2]);
      if (btspos == 1) {
        putB64Char(b64alphabet.ptr[(bts.ptr[0]&0x03)<<4]);
        /*static if (padding)*/ { putB64Char('='); putB64Char('='); }
      } else {
        // 2 or more
        putB64Char(b64alphabet.ptr[((bts.ptr[0]&0x03)<<4)|((bts.ptr[1]&0xf0)>>4)]);
        if (btspos == 2) {
          putB64Char(b64alphabet.ptr[(bts.ptr[1]&0x0f)<<2]);
          /*static if (padding)*/ putB64Char('=');
        } else {
          // 3 bytes
          putB64Char(b64alphabet.ptr[((bts.ptr[1]&0x0f)<<2)|((bts.ptr[2]&0xc0)>>6)]);
          putB64Char(b64alphabet.ptr[bts.ptr[2]&0x3f]);
        }
      }
      btspos = 0;
    }

    mixin(MixinEnsureUniqueBuf!"buf");

    foreach (immutable ubyte ib; (cast(const(ubyte)[])buf)[]) {
      bts.ptr[btspos++] = ib;
      if (btspos == 3) encodeChunk();
    }
    if (btspos != 0) encodeChunk();

    if (maxlinelen) append("\r\n");
  }

  void appendNum(T) (in T v) if (__traits(isIntegral, T)) {
    import core.stdc.stdio : snprintf;
    static if (T.sizeof <= 4) {
      char[32] buf = void;
      static if (__traits(isUnsigned, T)) {
        auto len = snprintf(buf.ptr, buf.length, "%u", cast(uint)v);
      } else {
        auto len = snprintf(buf.ptr, buf.length, "%d", cast(int)v);
      }
      append(buf[0..len]);
    } else {
      char[128] buf = void;
      static if (__traits(isUnsigned, T)) {
        auto len = snprintf(buf.ptr, buf.length, "%llu", cast(ulong)v);
      } else {
        auto len = snprintf(buf.ptr, buf.length, "%lld", cast(long)v);
      }
      append(buf[0..len]);
    }
  }

  void removeASCIICtrls (in char repch=' ') {
    static bool isCtrl (in char ch) pure nothrow @safe @nogc {
      pragma(inline, true);
      return (ch < 32 && ch != '\t' && ch != '\n');
    }

    bool needWork = false;
    foreach (immutable char ch; getData) if (isCtrl(ch)) { needWork = true; break; }
    if (!needWork) return;

    makeUnique();
    char[] buf = cast(char[])datap.ptr[0..datap.used];
    foreach (ref char ch; buf) if (isCtrl(ch)) ch = repch;
  }

  // only for ASCII
  void lowerInPlace () {
    bool needWork = false;
    foreach (immutable char ch; getData) if (ch >= 'A' && ch <= 'Z') { needWork = true; break; }
    if (!needWork) return;

    makeUnique();
    char[] buf = cast(char[])datap.ptr[0..datap.used];
    foreach (ref char ch; buf) if (ch >= 'A' && ch <= 'Z') ch += 32;
  }

  // only for ASCII
  void upperInPlace () {
    bool needWork = false;
    foreach (immutable char ch; getData) if (ch >= 'a' && ch <= 'z') { needWork = true; break; }
    if (!needWork) return;

    makeUnique();
    char[] buf = cast(char[])datap.ptr[0..datap.used];
    foreach (ref char ch; buf) if (ch >= 'a' && ch <= 'z') ch -= 32;
  }

  dynstring xstripright () const {
    dynstring res;
    if (len == 0) return res;
    if (opIndex(len-1) > ' ') { res = this; return res; }
    const(char)[] dt = getData().xstripright;
    res.set(dt);
    return res;
  }

  dynstring xstripleft () const {
    dynstring res;
    if (len == 0) return res;
    if (opIndex(0) > ' ') { res = this; return res; }
    const(char)[] dt = getData().xstripleft;
    res.set(dt);
    return res;
  }

  dynstring xstrip () const {
    dynstring res;
    if (len == 0) return res;
    if (opIndex(0) > ' ' && opIndex(len-1) > ' ') { res = this; return res; }
    const(char)[] dt = getData().xstrip;
    res.set(dt);
    return res;
  }

  // this can never return `int.min` from conversion!
  int toInt (int defval=int.min) const pure {
    const(char)[] dt = getData().xstrip;
    if (dt.length == 0) return defval;
    bool neg = false;
         if (dt[0] == '+') dt = dt[1..$];
    else if (dt[0] == '-') { neg = true; dt = dt[1..$]; }
    bool wasDigit = false;
    int n = 0;
    foreach (immutable char ch; dt) {
      if (ch == '_' || ch == ' ' || ch == ',') continue;
      immutable int dg = digitInBase(ch, 10);
      if (dg < 0) return defval;
      wasDigit = true;
      n *= 10;
      if (n < 0) return defval;
      n += dg;
      if (n < 0) return defval;
    }
    if (!wasDigit) return defval;
    if (neg) n = -n;
    return n;
  }

public:
  private static struct LineIteratorImpl {
    dynstring s;
    usize curridx;

  nothrow @trusted @nogc:
    this (dynstring src) { s = src; }
    this (in ref LineIteratorImpl src) { s = src.s; curridx = src.curridx; }

    @property bool empty () const pure { return (curridx >= s.length); }

    @property auto save () { return LineIteratorImpl(s); }

    @property const(char)[] front () const pure {
      if (empty) return null;
      const(char)[] ss = s.getData[curridx..$];
      auto ep = ss.indexOf('\n');
      if (ep >= 0) ss = ss[0..ep];
      return ss;
    }

    void popFront () {
      if (empty) return;
      const(char)[] ss = s.getData;
      auto ep = ss.indexOf('\n', curridx);
      if (ep < 0) { curridx = s.length; return; }
      curridx = ep+1;
    }
  }

  auto byLine () { return LineIteratorImpl(this); }

public:
  private static struct UniCharIteratorImpl {
    dynstring s;
    usize curridx;

  nothrow @trusted @nogc:
    this (dynstring src) { s = src; }
    this (in ref UniCharIteratorImpl src) { s = src.s; curridx = src.curridx; }

    @property bool empty () const pure { return (curridx >= s.length); }

    @property auto save () { return UniCharIteratorImpl(s); }

    @property dchar front () const {
      if (empty) return Utf8DecoderFast.replacement;
      Utf8DecoderFast dc;
      usize pos = curridx;
      while (pos < s.length) if (dc.decodeSafe(s[pos++])) return dc.codepoint;
      return Utf8DecoderFast.replacement;
    }

    void popFront () {
      if (empty) return;
      Utf8DecoderFast dc;
      while (curridx < s.length) if (dc.decodeSafe(s[curridx++])) break;
    }
  }

  auto byUniChar () { return UniCharIteratorImpl(this); }
}


version(dynstring_test) {
import iv.dynstring;
import iv.vfs.io;

void main () {
  dynstring tmp;
  tmp = "abc";
  writefln("tmp: udata=0x%08x; (%u) ofs=%u; len=%u; <%s>", tmp.udata, (tmp.udata ? tmp.datap.rc : 0), tmp.ofs, tmp.len, tmp.getData);
  tmp ~= "d";
  writefln("tmp: udata=0x%08x; (%u) ofs=%u; len=%u; <%s>", tmp.udata, (tmp.udata ? tmp.datap.rc : 0), tmp.ofs, tmp.len, tmp.getData);
  tmp = tmp[1..3];
  writefln("tmp: udata=0x%08x; (%u) ofs=%u; len=%u; <%s>", tmp.udata, (tmp.udata ? tmp.datap.rc : 0), tmp.ofs, tmp.len, tmp.getData);
  tmp[1] = '!';
  writefln("tmp: udata=0x%08x; (%u) ofs=%u; len=%u; <%s>", tmp.udata, (tmp.udata ? tmp.datap.rc : 0), tmp.ofs, tmp.len, tmp.getData);
  tmp.makeUnique();
  writefln("tmp: udata=0x%08x; (%u) ofs=%u; len=%u; <%s>", tmp.udata, (tmp.udata ? tmp.datap.rc : 0), tmp.ofs, tmp.len, tmp.getData);

  tmp = "abcdefgh";
  writefln("tmp: udata=0x%08x; (%u) ofs=%u; len=%u; <%s>", tmp.udata, (tmp.udata ? tmp.datap.rc : 0), tmp.ofs, tmp.len, tmp.getData);
  tmp = tmp[1..$-1];
  writefln("tmp: udata=0x%08x; (%u) ofs=%u; len=%u; <%s>", tmp.udata, (tmp.udata ? tmp.datap.rc : 0), tmp.ofs, tmp.len, tmp.getData);
  tmp = tmp[1..$-1];
  writefln("tmp: udata=0x%08x; (%u) ofs=%u; len=%u; <%s>", tmp.udata, (tmp.udata ? tmp.datap.rc : 0), tmp.ofs, tmp.len, tmp.getData);
  writeln("tmp[2]='", tmp[2], "'");

  tmp = " goo  ";
  tmp = tmp.xstripright;
  writeln("tmp: <", tmp.getData, ">");

  tmp = " goo  ";
  tmp = tmp.xstripleft;
  writeln("tmp: <", tmp.getData, ">");

  tmp = " goo  ";
  tmp = tmp.xstrip;
  writeln("tmp: <", tmp.getData, ">");

  tmp = ".Алиса.";
  foreach (dchar dch; tmp.byUniChar) writefln("0x%04x", cast(uint)dch);
}
}
