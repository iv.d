/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.atomic;

//version = iv_atomic_use_core_atomic;

version(DigitalMars) {
  version(X86) {
    version = iv_atomic_use_asm;
  } else {
    version = iv_atomic_use_core_atomic;
  }
} else {
  version = iv_atomic_use_core_atomic;
}


version(iv_atomic_use_core_atomic) {
  T atomicLoad(T) (in ref T val) nothrow @trusted @nogc
  if (T.sizeof <= 8 && __traits(isIntegral, T))
  {
    static import core.atomic;
    pragma(inline, true);
    return core.atomic.atomicLoad(*cast(shared T*)&val);
  }

  void atomicStore(T) (in ref T val, in T newval) nothrow @trusted @nogc
  if (T.sizeof <= 8 && __traits(isIntegral, T))
  {
    static import core.atomic;
    pragma(inline, true);
    core.atomic.atomicStore(*cast(shared T*)&val, newval);
  }

  // returns old value
  T atomicFetchAdd(T) (in ref T val, in size_t delta) nothrow @trusted @nogc
  if (T.sizeof <= 8 && __traits(isIntegral, T))
  {
    static import core.atomic;
    pragma(inline, true);
    return core.atomic.atomicFetchAdd(*cast(shared T*)&val, delta);
  }

  // returns old value
  T atomicFetchSub(T) (in ref T val, in size_t delta) nothrow @trusted @nogc
  if (T.sizeof <= 8 && __traits(isIntegral, T))
  {
    static import core.atomic;
    pragma(inline, true);
    return core.atomic.atomicFetchSub(*cast(shared T*)&val, delta);
  }
} else {
  uint atomicGetTId () pure nothrow @trusted @nogc {
    asm pure nothrow @trusted @nogc {
      naked;
      mov   EAX,224;
      int   0x80;
      ret;
    }
  }

  // returns previous value of `val`
  T atomicCompareSet(T) (ref T val, in T ifThis, in T writeThis) nothrow @trusted @nogc
  if (T.sizeof <= 4 && __traits(isIntegral, T))
  {
    static if (T.sizeof == byte.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov     DL,AL; // writeThis
        mov     AL,[ESP+4]; // ifThis
        mov     ECX,[ESP+8]; // val
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],DL; // compare with AL, store DL if equal, load into AL if not equal
        // AL is always the original [ECX] value here
      done:
        ret     4*2;
      }
    } else static if (T.sizeof == short.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov     DX,AX; // writeThis
        mov     AX,[ESP+4]; // ifThis
        mov     ECX,[ESP+8]; // val
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],DX; // compare with AX, store DX if equal, load into AX if not equal
        // AX is always the original [ECX] value here
        ret     4*2;
      }
    } else static if (T.sizeof == int.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov     EDX,EAX; // writeThis
        mov     EAX,[ESP+4]; // ifThis
        mov     ECX,[ESP+8]; // val
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],EDX; // compare with EAX, store EDX if equal, load into EAX if not equal
        // EAX is always the original [ECX] value here
      done:
        ret     4*2;
      }
    } else {
      static assert(false, "Invalid template type specified.");
    }
  }

  T atomicLoad(T) (in ref T val) nothrow @trusted @nogc
  if (T.sizeof <= 8 && __traits(isIntegral, T))
  {
    static if (T.sizeof == byte.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   ECX,EAX;
        xor   EDX,EDX;
        xor   EAX,EAX;
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],DL;
        ret;
      }
    } else static if (T.sizeof == short.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   ECX,EAX;
        xor   EDX,EDX;
        xor   EAX,EAX;
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],DX;
        ret;
      }
    } else static if (T.sizeof == int.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   ECX,EAX;
        xor   EDX,EDX;
        xor   EAX,EAX;
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],EDX;
        ret;
      }
    } else static if (T.sizeof == long.sizeof) {
      asm nothrow @nogc @trusted {
        push  EDI;
        push  EBX;
        xor   EBX,EBX;
        xor   ECX,ECX;
        xor   EAX,EAX;
        xor   EDX,EDX;
        mov   EDI,val;
        lock; // lock always needed to make this op atomic
        cmpxchg8b [EDI];
        pop   EBX;
        pop   EDI;
      }
    } else {
      static assert(false, "Invalid template type specified.");
    }
  }

  void atomicStore(T) (in ref T val, in T newval) nothrow @trusted @nogc
  if (T.sizeof <= 8 && __traits(isIntegral, T))
  {
    static if (T.sizeof == byte.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   EDX,SS:[ESP+4];
        lock;
        xchg  [EDX],AL;
        ret   4;
      }
    } else static if (T.sizeof == short.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   EDX,SS:[ESP+4];
        lock;
        xchg  [EDX],AX;
        ret   4;
      }
    } else static if (T.sizeof == int.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   EDX,SS:[ESP+4];
        lock;
        xchg  [EDX],EAX;
        ret   4;
      }
    } else static if (T.sizeof == long.sizeof) {
      asm nothrow @nogc @trusted {
        push  EDI;
        push  EBX;
        lea   EDI,newval;
        mov   EBX,[EDI];
        mov   ECX,4[EDI];
        mov   EDI,val;
        mov   EAX,[EDI];
        mov   EDX,4[EDI];
      L1: lock; // lock always needed to make this op atomic
        cmpxchg8b [EDI];
        jne   L1;
        pop   EBX;
        pop   EDI;
      }
    } else {
      static assert(false, "Invalid template type specified.");
    }
  }

  // returns old value
  T atomicFetchAdd(T) (ref T val, in T mod) nothrow @nogc @safe
  if (T.sizeof <= 4 && __traits(isIntegral, T))
  {
    static if (T.sizeof == byte.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   EDX,SS:[ESP+4];
        lock;
        xadd  [EDX],AL;
      }
      static if (__traits(isUnsigned, T)) {
        asm nothrow @nogc @trusted {
          movzx  EAX,AL;
        }
      } else {
        asm nothrow @nogc @trusted {
          movsx  EAX,AL;
        }
      }
      asm nothrow @nogc @trusted {
        ret   4;
      }
    } else static if (T.sizeof == short.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   EDX,SS:[ESP+4];
        lock;
        xadd  [EDX],AX;
      }
      static if (__traits(isUnsigned, T)) {
        asm nothrow @nogc @trusted {
          movzx  EAX,AX;
        }
      } else {
        asm nothrow @nogc @trusted {
          movsx  EAX,AX;
        }
      }
      asm nothrow @nogc @trusted {
        ret   4;
      }
    } else static if (T.sizeof == int.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   EDX,SS:[ESP+4];
        lock;
        xadd  [EDX],EAX;
        ret   4;
      }
    } else {
      static assert(false, "Invalid template type specified.");
    }
  }

  T atomicFetchSub(T) (ref T val, in T mod) nothrow @nogc @safe
  if (T.sizeof <= 4 && __traits(isIntegral, T))
  {
    static if (T.sizeof == byte.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   EDX,SS:[ESP+4];
        neg   AL;
        lock;
        xadd  [EDX],AL;
      }
      static if (__traits(isUnsigned, T)) {
        asm nothrow @nogc @trusted {
          movzx  EAX,AL;
        }
      } else {
        asm nothrow @nogc @trusted {
          movsx  EAX,AL;
        }
      }
      asm nothrow @nogc @trusted {
        ret   4;
      }
    } else static if (T.sizeof == short.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   EDX,SS:[ESP+4];
        neg   AX;
        lock;
        xadd  [EDX],AX;
      }
      static if (__traits(isUnsigned, T)) {
        asm nothrow @nogc @trusted {
          movzx  EAX,AX;
        }
      } else {
        asm nothrow @nogc @trusted {
          movsx  EAX,AX;
        }
      }
      asm nothrow @nogc @trusted {
        ret   4;
      }
    } else static if (T.sizeof == int.sizeof) {
      asm nothrow @nogc @trusted {
        naked;
        mov   EDX,SS:[ESP+4];
        neg   EAX;
        lock;
        xadd  [EDX],EAX;
        ret   4;
      }
    } else {
      static assert(false, "Invalid template type specified.");
    }
  }

  // non-recursive atomic spinlock
  public align(4) struct AtomicSpinLock {
  align(4):
    uint counter = 0;

    @disable this() (in auto ref AtomicSpinLock other);
    @disable this (this);
    @disable void opAssign() (in auto ref AtomicSpinLock other) {}

    void lock () nothrow @trusted @nogc {
      asm nothrow @trusted @nogc {
        naked;
        mov     ECX,EAX; // `this`
        mov     EDX,1; // writeThis
      spinloop:
        xor     EAX,EAX; // ifThis
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],EDX; // compare with EAX, store EDX if equal, load into EAX if not equal
        // EAX is always the original [ECX] value here
        jnz     spinloop;
        ret;
      }
    }

    void unlock () nothrow @trusted @nogc {
      asm nothrow @trusted @nogc {
        naked;
        // no need to perform atomic operation here
        mov     dword ptr [EAX],0;
        ret;
      }
    }

    bool isLocked () const nothrow @trusted @nogc {
      asm nothrow @trusted @nogc {
        naked;
        mov     ECX,EAX;
        xor     EDX,EDX;
        xor     EAX,EAX;
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],EDX;
        setnz   AL;
        movzx   EAX,AL;
        ret;
      }
    }
  }

  // recursive atomic spinlock
  public align(4) struct AtomicSpinLockRecursive {
  align(4):
    // low 32 bits: tid
    // higt 32 bits: counter
    ulong tidcounter = 0;

    @disable this() (in auto ref AtomicSpinLockRecursive other);
    @disable this (this);
    @disable void opAssign() (in auto ref AtomicSpinLockRecursive other) {}

    void lock () nothrow @trusted @nogc {
      /*
      immutable uint tid = atomicGetTId();
      for (;;) {
        // if zero: set our tid
        // returns old tid
        uint oldv = atomicCompareSet(currtid, 0, tid);
        if (oldv == 0 || oldv == tid) {
          // either no lock (and then the counter is zero), or our lock
          atomicFetchAdd(counter, 1);
          return;
        }
      }
      */
      asm nothrow @trusted @nogc {
        naked;
        mov     ECX,EAX; // save `this`

        mov     EAX,224; // gettid
        int     0x80;
        mov     EDX,EAX; // writeThis (tid)

     spinlock:
        xor     EAX,EAX; // ifThis
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],EDX; // compare with EAX, store EDX if equal, load into EAX if not equal
        // EAX is always the original [ECX] value here
        jz      gotlock;
        // recursive?
        cmp     EAX,EDX;
        jnz     spinlock;

      gotlock:
        // there is no need to perform atomic increment here
        inc     dword ptr [ECX+4]; // increment counter
        ret;
      }
    }

    void unlock () nothrow @trusted @nogc {
      /*
      immutable uint tid = atomicGetTId();
      // sanity check
      if (atomicLoad(currtid) != tid) assert(0, "atomicSpinLockRecursive: unbalanced lock/unlock!");
      if (atomicFetchSub(counter, 1) == 1) {
        // last recursive lock is freed, release global lock
        atomicStore(currtid, 0);
      }
      */
      asm nothrow @trusted @nogc {
        naked;
        mov     ECX,EAX; // save `this`
        mov     EAX,224; // gettid (ifThis)
        int     0x80;
        mov     EDX,EAX; // writeThis (tid)
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],EDX; // compare with EAX, store EDX if equal, load into EAX if not equal
        // EAX is always the original [ECX] value here
        jnz     failure; // should not happen
        // there is no need to perform atomic decrement here
        dec     dword ptr [ECX+4]; // decrement counter
        jnz     done;
        // released last recursive lock, clear `currtid`
        xor     EAX,EAX;
        lock;
        xchg    [ECX],EAX;
      done:
        ret;
      failure:
        nop;
      }
      assert(0, "atomicSpinLockRecursive: unbalanced lock/unlock!");
    }

    bool isLocked () const nothrow @trusted @nogc {
      /*
      pragma(inline, true);
      return (atomicLoad(currtid) != 0);
      */
      asm nothrow @trusted @nogc {
        naked;
        mov     ECX,EAX; // save `this`
        xor     EDX,EDX; // writeThis
        xor     EAX,EAX; // ifThis
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],EDX; // compare with EAX, store EDX if equal, load into EAX if not equal
        // EAX is always the original [ECX] value here
        setnz   AL;
        movzx   EAX,AL;
        ret;
      }
    }

    // is locked by current thread?
    bool isLockedByMe () const nothrow @trusted @nogc {
      /*
      pragma(inline, true);
      return (atomicLoad(currtid) == atomicGetTId());
      */
      asm nothrow @trusted @nogc {
        naked;
        mov     ECX,EAX; // save `this`
        xor     EDX,EDX; // writeThis
        xor     EAX,EAX; // ifThis
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],EDX; // compare with EAX, store EDX if equal, load into EAX if not equal
        // EAX is always the original [ECX] value here
        jz      done;
        mov     EDX,EAX; // save loaded value
        mov     EAX,224; // gettid
        int     0x80;
        cmp     EAX,EDX;
        setz    AL;
        movzx   EAX,AL;
      done:
        ret;
      }
    }

    // is locked by current thread, or free?
    bool isLockedByMeOrFree () const nothrow @trusted @nogc {
      /*
      pragma(inline, true);
      if (uint oldv = atomicLoad(currtid)) return (oldv == atomicGetTId());
      else return true;
      */
      asm nothrow @trusted @nogc {
        naked;
        mov     ECX,EAX; // save `this`
        xor     EDX,EDX; // writeThis
        xor     EAX,EAX; // ifThis
        lock; // lock always needed to make this op atomic
        cmpxchg [ECX],EDX; // compare with EAX, store EDX if equal, load into EAX if not equal
        // EAX is always the original [ECX] value here
        mov     EDX,EAX; // save loaded value
        jz      done;
        mov     EAX,224; // gettid
        int     0x80;
        cmp     EAX,EDX;
      done:
        setz    AL;
        movzx   EAX,AL;
        ret;
      }
    }
  }
}


version(none) {
import iv.vfs.io;

void testStore () {
  {
    byte tb = 10;
    atomicStore(tb, 42);
    writeln("byte: ", atomicLoad(tb));
    assert(tb == 42);
    typeof(tb) vb = atomicFetchAdd(tb, 10);
    writeln("byte: ", atomicLoad(tb), "; vb=", vb);
    assert(tb == 52);
    assert(vb == 42);
    vb = atomicFetchSub(tb, 40);
    writeln("byte: ", atomicLoad(tb), "; vb=", vb);
    assert(tb == 12);
    assert(vb == 52);
  }
  {
    ubyte tub = 10;
    atomicStore(tub, 42);
    writeln("ubyte: ", atomicLoad(tub));
    assert(tub == 42);
    typeof(tub) vub = atomicFetchAdd(tub, 100);
    writeln("byte: ", atomicLoad(tub), "; vub=", vub);
    assert(tub == 142);
    assert(vub == 42);
    vub = atomicFetchSub(tub, 40);
    writeln("byte: ", atomicLoad(tub), "; vub=", vub);
    assert(tub == 102);
    assert(vub == 142);
  }
  {
    int ti = 10;
    atomicStore(ti, 42);
    writeln("int: ", atomicLoad(ti));
    assert(ti == 42);
    typeof(ti) vi = atomicFetchAdd(ti, 1000);
    writeln("byte: ", atomicLoad(ti), "; vi=", vi);
    assert(ti == 1042);
    assert(vi == 42);
    vi = atomicFetchSub(ti, 500);
    writeln("byte: ", atomicLoad(ti), "; vi=", vi);
    assert(ti == 542);
    assert(vi == 1042);
  }
  {
    uint tui = 10;
    atomicStore(tui, 42);
    writeln("uint: ", atomicLoad(tui));
    assert(tui == 42);
    typeof(tui) vui = atomicFetchAdd(tui, 1000);
    writeln("byte: ", atomicLoad(tui), "; vui=", vui);
    assert(tui == 1042);
    assert(vui == 42);
    vui = atomicFetchSub(tui, 500);
    writeln("byte: ", atomicLoad(tui), "; vui=", vui);
    assert(tui == 542);
    assert(vui == 1042);
  }
  {
    long tl = 10;
    atomicStore(tl, 42);
    writeln("long: ", atomicLoad(tl));
    assert(tl == 42);
  }
  {
    ulong tul = 10;
    atomicStore(tul, 42);
    writeln("ulong: ", atomicLoad(tul));
    assert(tul == 42);
  }
}

void main () {
  testStore();
}
}
