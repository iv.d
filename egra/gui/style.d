/*
 * Simple Framebuffer GUI
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.style;

import iv.egra.gfx.base;

import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.xcolornames;

//version = egra_style_dynstr_debug;
//version = egra_style_debug_search;


// ////////////////////////////////////////////////////////////////////////// //
static immutable defaultStyleText = `
// inactive window
SubWindow {
  frame: #ddd;
  title-back: #ddd;
  title-text: black;
  shadow-color: rgba(0, 0, 0, 127);
  shadow-size: 8;
  shadow-dash: 0;
  back: rgb(0, 0, 180);

  drag-overlay-back: rgba(255, 127, 0, 79);
  drag-overlay-dash: 0; /* boolean */

  /+
  drag-overlay-back: #070;
  drag-overlay-dash: 1; // boolean
  //shadow-color: rgba(0, 0, 0, 42);
  +/

  // for widgets
  text: gray79;
  hotline: gray79;

  text-cursor-0: transparent;
  text-cursor-1: transparent;
  text-cursor-dot: transparent;

  text-cursor-blink-time: 800; /* in milliseconds */
  text-cursor-dot-time: 100; /* dot crawl time, in milliseconds */
}

SubWindow.minimised {
  icon-size-x: 24;
  icon-size-y: 24;
  icon-margin-x: 4;
  icon-margin-y: 4;
}

SubWindow:focused {
  frame: white;
  title-back: white;

  // for widgets
  text: white;

  text-cursor-0: grey67;
  text-cursor-1: white;
  text-cursor-dot: transparent;
}


YesNoWindow {
  frame: #ddd;
  title-back: #ddd;
  title-text: black;

  back: rgb(0xbf, 0xcf, 0xef);
  text: rgb(0x16, 0x2d, 0x59);
  hotline: rgb(0x16, 0x2d, 0x59);
}

YesNoWindow:focused {
  frame: rgb(0x40, 0x70, 0xcf);
  title-back: rgb(0x73, 0x96, 0xdc);
  title-text: rgb(0xff, 0xff, 0xff);
}


ProgressBarWidget {
  rect: gray20;
  back: rgb(17, 17, 0);
  text: white;
  stripe: rgb(52, 52, 38);

  back-hishade: rgb(52, 52, 38);
  stripe-hishade: rgb(82, 82, 70);

  back-full: rgb(159, 71, 0);
  stripe-full: rgb(173, 98, 38);

  back-full-hishade: rgb(173, 98, 38);
  stripe-full-hishade: rgb(203, 128, 70);
}


ButtonWidget {
  back: grey67;
  text: black;
  hotline: black;
  // gradient button
  grad0-back: grey50;
  grad1-back: grey77;
  grad2-back: grey56;
  grad-midp: 30; /* percents */
}

ButtonWidget:focused {
  back: grey98;
  text: #006;
  hotline: #006;
  // gradient button
  grad0-back: grey70;
  grad1-back: white;
  grad2-back: grey76;
  //grad-midp: 20; /* percents */
}

ButtonWidget:disabled {
  back: grey55;
  text: grey12;
  hotline: transparent;
  // gradient button
  grad0-back: grey32;
  grad1-back: grey42;
  grad2-back: grey36;
  //grad-midp: 20; /* percents */
}


ButtonExWidget {
  back: rgb(0x73, 0x96, 0xdc);
  text: grey94;
  hotline: grey94;
  rect: rgb(0x40, 0x70, 0xcf);
  shadowline: rgb(0x83, 0xa6, 0xec);
}

ButtonExWidget:focused {
  back: rgb(0x93, 0xb6, 0xfc);
  text: white;
  hotline: white;
  shadowline: rgb(0xa3, 0xc6, 0xff);
}


CheckboxWidget {
  //text: grey75;
  back: transparent;
  mark: #0d0;
}

CheckboxWidget:focused {
  //text: white;
  back: #004;
  mark: #0f0;
}

CheckboxWidget:disabled {
  text: grey40;
  back: transparent;
  mark: grey40;
}


SimpleListBoxWidget {
  back: #004;
  text: #ff0;
  cursor-back: #044;
  cursor-text: #ddd;
}

SimpleListBoxWidget:focused {
  cursor-back: #066;
  cursor-text: white;
}


EditorWidget {
  back: #007;

  status-back: gray80;
  status-text: black;

  text: rgb(220, 220, 0);
  quote0-text: rgb(128, 128, 0);
  quote1-text: rgb(0, 128, 128);
  wrap-mark-text: rgb(0, 90, 220);

  attach-file-text: rgb(0x6e, 0x00, 0xff);
  attach-bad-text: red;

  // marked block
  mark-back: rgb(0, 160, 160);
  mark-text: white;
}


LineEditWidget {
  back: black;
  text: rgb(220, 220, 0);
}
`;


// ////////////////////////////////////////////////////////////////////////// //
struct EgraCIString {
public:
  static uint joaatHashPart (const(void)[] buf, uint hash=0) pure nothrow @trusted @nogc {
    pragma(inline, true);
    foreach (immutable ubyte b; cast(const(ubyte)[])buf) {
      //hash += (uint8_t)locase1251(*s++);
      hash += b|0x20; // this converts ASCII capitals to locase (and destroys other, but who cares)
      hash += hash<<10;
      hash ^= hash>>6;
    }
    return hash;
  }

  static uint joaatHashFinish (uint hash) pure nothrow @trusted @nogc {
    pragma(inline, true);
    // final mix
    hash += hash<<3;
    hash ^= hash>>11;
    hash += hash<<15;
    return hash;
  }

  // ascii only
  static bool strEquCI (const(char)[] s0, const(char)[] s1) pure nothrow @trusted @nogc {
    if (s0.length != s1.length) return false;
    if (s0.ptr == s1.ptr) return true;
    foreach (immutable idx, char c0; s0) {
      // try the easiest case first
      if (c0 == s1.ptr[idx]) continue;
      c0 |= 0x20; // convert to ascii lowercase
      if (c0 < 'a' || c0 > 'z') return false; // it wasn't a letter, no need to check the second char
      // c0 is guaranteed to be a lowercase ascii here
      if (c0 != (s1.ptr[idx]|0x20)) return false; // c1 will become a lowercase ascii only if it was uppercase/lowercase ascii
    }
    return true;
  }

private:
  uint hashCurr; // current hash
  dynstring xstr;

nothrow @trusted @nogc:
public:
  alias getData this;

public:
  this() (in auto ref dynstring s) { pragma(inline, true); xstr = s; hashCurr = joaatHashPart(xstr.getData); }
  this (const(char)[] s) { pragma(inline, true); xstr = s; hashCurr = joaatHashPart(xstr.getData); }
  this (in char ch) { pragma(inline, true); xstr = ch; hashCurr = joaatHashPart(xstr.getData); }

  ~this () { pragma(inline, true); xstr.clear(); hashCurr = 0; }

  void clear () { pragma(inline, true); xstr.clear(); hashCurr = 0; }

  @property dynstring str () const { pragma(inline, true); return dynstring(xstr); }
  @property void str() (in auto ref dynstring s) { pragma(inline, true); xstr = s; hashCurr = joaatHashPart(xstr.getData); }
  @property void str (const(char)[] s) { pragma(inline, true); xstr = s; hashCurr = joaatHashPart(xstr.getData); }

  @property uint length () const pure { pragma(inline, true); return xstr.length; }

  @property const(char)[] getData () const pure { pragma(inline, true); return xstr.getData; }

  void opAssign() (const(char)[] s) { pragma(inline, true); xstr = s; hashCurr = joaatHashPart(xstr.getData); }
  void opAssign() (in char ch) { pragma(inline, true); xstr = ch; hashCurr = joaatHashPart(xstr.getData); }
  void opAssign() (in auto ref dynstring s) { pragma(inline, true); xstr = s.xstr; hashCurr = joaatHashPart(xstr.getData); }
  void opAssign() (in auto ref EgraCIString s) { pragma(inline, true); xstr = s.xstr; hashCurr = s.hashCurr; }

  void opOpAssign(string op:"~") (const(char)[] s) { pragma(inline, true); if (s.length) { xstr ~= s; hashCurr = joaatHashPart(xstr.getData); } }
  void opOpAssign(string op:"~") (in char ch) { pragma(inline, true); if (s.length) { xstr ~= s; hashCurr = joaatHashPart(xstr.getData); } }
  void opOpAssign(string op:"~") (in auto ref dynstring s) { pragma(inline, true); if (s.length) { xstr ~= s; hashCurr = joaatHashPart(xstr.getData); } }
  void opOpAssign(string op:"~") (in auto ref EgraCIString s) { pragma(inline, true); if (s.xstr.length) { xstr ~= s.xstr; hashCurr = joaatHashPart(xstr.getData); } }

  usize toHash () pure const @safe nothrow @nogc { pragma(inline, true); return joaatHashFinish(hashCurr); }

  // case-insensitive
  bool opEquals() (const(char)[] other) pure const @safe nothrow @nogc {
    pragma(inline, true);
    if (xstr.length != other.length) return false;
    return strEquCI(xstr.getData, other);
  }

  // case-insensitive
  bool opEquals() (in auto ref EgraCIString other) pure const @safe nothrow @nogc {
    pragma(inline, true);
    if (hashCurr != other.hashCurr) return false;
    if (xstr.length != other.xstr.length) return false;
    return strEquCI(xstr.getData, other.xstr.getData);
  }

  // case-insensitive
  bool opEquals() (in auto ref dynstring other) pure const @safe nothrow @nogc {
    pragma(inline, true);
    if (xstr.length != other.length) return false;
    return strEquCI(xstr.getData, other.getData);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct EgraSimpleParser {
private:
  const(char)[] text;
  const(char)[] str; // text left

public:
  this (const(char)[] atext) nothrow @safe @nogc { pragma(inline, true); setText(atext); }

  int getCurrentLine () pure const nothrow @safe @nogc {
    int res = 0;
    foreach (immutable char ch; text[0..$-str.length]) if (ch == '\n') ++res;
    return res;
  }

  void error (string msg) const {
    import std.conv : to;
    version(none) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "===\n%.*s\n===\n", cast(uint)str.length, str.ptr); }
    throw new Exception("parse error around line "~getCurrentLine.to!string~": "~msg);
  }

  void setText (const(char)[] atext) nothrow @safe @nogc { pragma(inline, true); text = atext; str = atext; }

  bool isEOT () {
    skipBlanks();
    return (str.length == 0);
  }

  void skipBlanks () {
    while (str.length) {
      if (str[0] <= ' ') { str = str.xstripleft; continue; }
      if (str.length < 2 || str[0] != '/') break;
      // single-line comment?
      if (str[1] == '/') {
        str = str[2..$];
        while (str.length && str[0] != '\n') str = str[1..$];
        continue;
      }
      // multiline comment?
      if (str[1] == '*') {
        bool endFound = false;
        auto svs = str;
        str = str[2..$];
        while (str.length) {
          if (str.length > 1 && str[0] == '*' && str[1] == '/') {
            endFound = true;
            str = str[2..$];
            break;
          }
          str = str[1..$];
        }
        if (!endFound) { str = svs; error("unfinished comment"); }
        continue;
      }
      // multiline nested comment?
      if (str[1] == '+') {
        bool endFound = false;
        auto svs = str;
        int level = 0;
        while (str.length) {
          if (str.length > 1) {
            if (str[0] == '/' && str[1] == '+') { str = str[2..$]; ++level; continue; }
            if (str[0] == '+' && str[1] == '/') { str = str[2..$]; if (--level == 0) { endFound = true; break;} continue; }
          }
          str = str[1..$];
        }
        if (!endFound) { str = svs; error("unfinished comment"); }
        continue;
      }
      break;
    }
  }

  bool checkNoEat (const(char)[] tk) {
    assert(tk.length);
    skipBlanks();
    return (str.length >= tk.length && str[0..tk.length] == tk);
  }

  bool checkDigitNoEat () {
    skipBlanks();
    return (str.length > 0 && isdigit(str[0]));
  }

  bool checkNoEat (in char ch) {
    skipBlanks();
    return (str.length > 0 && str[0] == ch);
  }

  bool check (const(char)[] tk) {
    if (!checkNoEat(tk)) return false;
    str = str[tk.length..$];
    skipBlanks();
    return true;
  }

  bool check (in char ch) {
    if (!checkNoEat(ch)) return false;
    str = str[1..$];
    skipBlanks();
    return true;
  }

  void expect (const(char)[] tk) {
    skipBlanks();
    auto svs = str;
    if (!check(tk)) { str = svs; error("`"~tk.idup~"` expected"); }
  }

  void expect (in char ch) {
    skipBlanks();
    auto svs = str;
    if (!check(ch)) { str = svs; error("`"~ch~"` expected"); }
  }

  const(char)[] expectId () {
    skipBlanks();
    if (str.length == 0) error("identifier expected");
    if (!isalpha(str[0]) && str[0] != '_' && str[0] != '-') error("identifier expected");
    usize pos = 1;
    while (pos < str.length) {
      if (!isalnum(str[pos]) && str[pos] != '_' && str[pos] != '-') break;
      ++pos;
    }
    const(char)[] res = str[0..pos];
    str = str[pos..$];
    skipBlanks();
    return res;
  }

  const(char)[] expectSelector () {
    static bool isSelChar (in char ch) pure nothrow @safe @nogc {
      pragma(inline, true);
      return (isalnum(ch) || ch == '_' || ch == '-' || ch == '#' || ch == '.' || ch == ':' || ch == '>' || ch == '+');
    }
    skipBlanks();
    if (str.length == 0) error("selector expected");
    if (!isSelChar(str[0])) error("selector expected");
    usize pos = 1;
    while (pos < str.length && isSelChar(str[pos])) ++pos;
    const(char)[] res = str[0..pos];
    str = str[pos..$];
    skipBlanks();
    return res;
  }

  uint parseColor () {
    skipBlanks();
    if (str.length == 0) error("color expected");

    // html-like color?
    if (check('#')) return parseHtmlColor();

    auto svs = str;
    auto id = expectId();

    if (id.strEquCI("transparent")) return gxTransparent;

    // `rgb()` or `rgba()`?
    bool allowAlpha;
         if (id.strEquCI("rgba")) allowAlpha = true;
    else if (id.strEquCI("rgb")) allowAlpha = false;
    else {
      auto xc = xFindColorByName(id);
      if (xc is null) { str = svs; error("invalid color definition"); }
      return gxrgb(xc.r, xc.g, xc.b);
    }

    skipBlanks();
    if (!check('(')) { str = svs; error("invalid color definition"); }
    immutable uint clr = parseColorRGB(allowAlpha);
    if (!check(')')) { str = svs; error("invalid color definition"); }
    return clr;
  }

  // open quote already eaten
  dynstring parseString (in char qch) {
    auto epos = str.indexOf('"');
    if (epos < 0) error("invalid string");
    auto svs = str;
    dynstring res;
    res.reserve(epos);
    usize pos = 0;
    while (pos < str.length) {
      immutable char ch = str.ptr[pos++];
      if (ch == 0) { str = svs; error("invalid string"); }
      if (ch == qch) {
        str = str[pos..$];
        skipBlanks();
        return res;
      }
      if (ch != '\\') {
        if (ch == '\n') { str = svs; error("unterminated string"); }
        res ~= ch;
        continue;
      }
      if (pos >= str.length) { str = svs; error("unterminated string"); }
      switch (str.ptr[pos++]) {
        case 't': res ~= '\t'; break;
        case 'n': res ~= '\n'; break;
        case 'r': res ~= '\r'; break;
        case '\n': break;
        case '\r': if (pos < str.length && str.ptr[pos] == '\n') ++pos; break;
        case '"': case '\'': case '\\': res ~= str.ptr[pos-1]; break;
        default: str = svs; error("invalid string escape");
      }
    }
    str = svs;
    error("unterminated string");
    assert(0);
  }

  int parseInt () {
    skipBlanks();
    if (str.length == 0) error("number expected");
    auto svs = str;
    bool neg = false;
         if (check('+')) {}
    else if (check('-')) neg = true;
    if (str.length == 0 || !isdigit(str[0])) { str = svs; error("number expected"); }
    int base = 0;
    // check bases
    if (str.length > 1 && str[0] == '0') {
      switch (str[1]) {
        case 'x': case 'X': base = 16; break;
        case 'b': case 'B': base = 2; break;
        case 'o': case 'O': base = 8; break;
        case 'd': case 'D': base = 10; break;
        default: break;
      }
      if (base) {
        str = str[2..$];
        while (str.length && str[0] == '_') str = str[1..$];
      }
    }
         if (!base) base = 10;
    else if (str.length == 0 || digitInBase(str[0], base) < 0) { str = svs; error("number expected"); }
    immutable long vmax = (neg ? -cast(long)int.min : cast(long)int.max);
    long n = 0;
    while (str.length) {
      if (str[0] != '_') {
        immutable int dg = digitInBase(str[0], base);
        if (dg < 0) break;
        n = n*base+dg;
        if (n > vmax) { str = svs; error("integer overflow"); }
      }
      str = str[1..$];
    }
    if (str.length && isalpha(str[0])) { str = svs; error("number expected"); }
    skipBlanks();
    return cast(int)n;
  }

private:
  // '#' skipped
  uint parseHtmlColor () {
    auto svs = str;
    skipBlanks();
    ubyte[3] rgb = 0;
    // first 3 digits
    foreach (immutable n; 0..3) {
      while (str.length && str[0] == '_') str = str[1..$];
      if (str.length == 0) { str = svs; error("invalid color"); }
      immutable int dg = digitInBase(str[0], 16);
      if (dg < 0) { str = svs; error("invalid color"); }
      rgb[n] = cast(ubyte)dg;
      str = str[1..$];
    }
    while (str.length && str[0] == '_') str = str[1..$];
    // second 3 digits?
    if (str.length && digitInBase(str[0], 16) >= 0) {
      foreach (immutable n; 0..3) {
        while (str.length && str[0] == '_') str = str[1..$];
        if (str.length == 0) { str = svs; error("invalid color"); }
        immutable int dg = digitInBase(str[0], 16);
        if (dg < 0) { str = svs; error("invalid color"); }
        rgb[n] = cast(ubyte)(rgb[n]*16+dg);
        str = str[1..$];
      }
      while (str.length && str[0] == '_') str = str[1..$];
    } else {
      foreach (immutable n; 0..3) rgb[n] = cast(ubyte)(rgb[n]*16+rgb[n]);
    }
    skipBlanks();
    return gxrgb(rgb[0], rgb[1], rgb[2]);
  }

  // "(" skipped
  uint parseColorRGB (bool allowAlpha) {
    auto svs = str;
    ubyte[4] rgba = 0;
    foreach (immutable n; 0..3+(allowAlpha ? 1 : 0)) {
      if (n && !check(',')) { str = svs; error("invalid color"); }
      skipBlanks();
      if (str.length == 0 || !isdigit(str[0])) { str = svs; error("invalid color"); }
      uint val = 0;
      uint base = 10;
      if (str[0] == '0' && str.length >= 2 && (str[1] == 'x' || str[1] == 'X')) {
        str = str[2..$];
        if (str.length == 0 || digitInBase(str[0], 16) < 0) { str = svs; error("invalid color"); }
        base = 16;
      }
      while (str.length) {
        if (str[0] != '_') {
          immutable int dg = digitInBase(str[0], cast(int)base);
          if (dg < 0) break;
          val = val*base+cast(uint)dg;
          if (val > 255) { str = svs; error("invalid color"); }
        }
        str = str[1..$];
      }
      while (str.length && str[0] == '_') str = str[1..$];
      rgba[n] = cast(ubyte)val;
    }
    skipBlanks();
    if (allowAlpha) return gxrgba(rgba[0], rgba[1], rgba[2], rgba[3]);
    return gxrgb(rgba[0], rgba[1], rgba[2]);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
/*
  style store is a simple list of selectors and properties.
  it also holds cache of path:prop, to avoid slow lookups.

  style searching is working like this:

  loop over all styles from the last one, check if the last path
  element of each style is for us, or for one of our superclasses.
  if it matches, and distance from us to superclass is lower than
  the current one, remember this value. if the distance is zero
  (exact match), stop searching, and use found value.

  there are some modifiers:
    :focused -- for focused widgets
    :disabled -- for disabled widgets

  if we asked to find something with one of modifiers, and there is
  only "unmodified" style available, use the unmodified one. technically
  it is implemented by two searches: with, and without a modifier.

  special class for subwindows:
    .minimised
*/
class WidgetStyle {
public:
  static struct Value {
    enum Type {
      Empty,
      Str,
      Int,
      Color,
    }
    Type type = Type.Empty;
    dynstring sval;
    union {
      uint color;
      int ival;
    };

    this (const(char)[] str) @safe nothrow @nogc { pragma(inline, true); sval = str; type = Type.Str; }
    this() (in auto ref dynstring str) @safe nothrow @nogc { pragma(inline, true); sval = str; type = Type.Str; }
    this (in int val) @safe nothrow @nogc { pragma(inline, true); ival = val; type = Type.Int; }
    this (in uint val) @safe nothrow @nogc { pragma(inline, true); color = val; type = Type.Color; }
    this() (in auto ref Value v) @safe nothrow @nogc { pragma(inline, true); type = v.type; sval = v.sval; color = v.color; }

    ~this () nothrow @safe @nogc { pragma(inline, true); sval.clear(); color = 0; type = Type.Empty; }
    void clear () nothrow @safe @nogc { pragma(inline, true); sval.clear(); color = 0; type = Type.Empty; }

    @property bool isEmpty () pure const @safe nothrow { pragma(inline, true); return (type == Type.Empty); }
    @property bool isString () pure const @safe nothrow { pragma(inline, true); return (type == Type.Str); }
    @property bool isColor () pure const @safe nothrow { pragma(inline, true); return (type == Type.Color); }
    @property bool isInteger () pure const @safe nothrow { pragma(inline, true); return (type == Type.Int); }

    static Value Empty () @safe nothrow @nogc { pragma(inline, true); return Value(); }
    static Value String (const(char)[] str) @safe nothrow @nogc { pragma(inline, true); return Value(str); }
    static Value String() (in auto ref dynstring str) @safe nothrow @nogc { pragma(inline, true); return Value(str); }
    static Value Color (in uint clr) @safe nothrow @nogc { pragma(inline, true); return Value(clr); }
    static Value Integer (in int val) @safe nothrow @nogc { pragma(inline, true); return Value(val); }

    void opAssign() (in auto ref Value v) { pragma(inline, true); type = v.type; sval = v.sval; color = v.color; }
  }

  static struct Item {
    dynstring sel; // selector
    EgraCIString prop; // property name
    Value value; // property value

    this() (in auto ref Item it) @trusted nothrow @nogc {
      pragma(inline, true);
      sel = it.sel; prop = it.prop; value = it.value;
      version(egra_style_dynstr_debug) {
        import core.stdc.stdio : stderr, fprintf;
        fprintf(stderr, "Item:0x%08x: constructed from 0x%08x! sel=[%.*s]; prop=[%.*s]\n",
          cast(uint)cast(void*)&this, cast(uint)cast(void*)&it,
          cast(uint)sel.length, sel.getData.ptr,
          cast(uint)prop.length, prop.getData.ptr);
      }
    }

    version(egra_style_dynstr_debug) {
      this (this) nothrow @trusted @nogc {
        import core.stdc.stdio : stderr, fprintf;
        fprintf(stderr, "Item:0x%08x: copied! sel=[%.*s]; prop=[%.*s]\n", cast(uint)cast(void*)&this,
          cast(uint)sel.length, sel.getData.ptr,
          cast(uint)prop.length, prop.getData.ptr);
      }
    }

     ~this () nothrow @trusted @nogc {
      pragma(inline, true);
      version(egra_style_dynstr_debug) {
        import core.stdc.stdio : stderr, fprintf;
        fprintf(stderr, "Item:0x%08x: DESTROYING! sel=[%.*s]; prop=[%.*s]\n", cast(uint)cast(void*)&this,
          cast(uint)sel.length, sel.getData.ptr,
          cast(uint)prop.length, prop.getData.ptr);
      }
      sel.clear();
      prop.clear();
      value.clear();
    }

    void clear () nothrow @safe @nogc { pragma(inline, true); sel.clear(); prop.clear(); value.clear(); }

    void opAssign() (in auto ref Item it) nothrow @trusted @nofc { pragma(inline, true); sel = it.sel; prop = it.prop; value = it.value; }
  }

protected:
  Item[] style;

  // "path" here is the path to a styled class, not a selector
  // modifier is appended to the path
  // prop name is appended last
  Value[EgraCIString] styleCache;

protected:
  final void clearStyleCache () @trusted nothrow {
    pragma(inline, true);
    if (styleCache.length) {
      foreach (ref kv; styleCache.byKeyValue) { kv.key.clear(); kv.value.clear(); }
      styleCache.clear();
      //styleCache = null;
    }
  }

  final const(Value)* findCachedValue (EgraStyledClass obj, const(char)[] prop) @trusted nothrow {
    pragma(inline, true);
    if (obj is null) {
      return null;
    } else {
      EgraCIString pp = obj.getFullPath();
      auto mod = obj.getCurrentMod();
      if (mod.length) { pp ~= "\x00"; pp ~= mod; }
      if (prop.length) { pp ~= "\x00"; pp ~= prop; }
      return (pp in styleCache);
    }
  }

  final void cacheValue (in ref Value val, EgraStyledClass obj, const(char)[] prop) @trusted nothrow {
    pragma(inline, true);
    if (obj is null) return;
    EgraCIString pp = obj.getFullPath();
    auto mod = obj.getCurrentMod();
    if (mod.length) { pp ~= "\x00"; pp ~= mod; }
    if (prop.length) { pp ~= "\x00"; pp ~= prop; }
    styleCache[pp] = Value(val);
  }

protected:
  final void addColorItem() (in auto ref Item ci) @trusted nothrow {
    if (ci.sel.length && ci.sel.length) {
      clearStyleCache();
      version(egra_style_dynstr_debug) {
        import core.stdc.stdio : stderr, fprintf;
        fprintf(stderr, "addColorItem:000: style.length=%u; style.capacity=%u; stype.ptr=0x%08x\n",
          cast(uint)style.length, cast(uint)style.capacity, cast(uint)style.ptr);
        conwriteln("ADDING: ci.sel:", ci.sel.getData, "; ci.prop:", ci.prop.getData);
      }
      style ~= Item(ci);
      version(egra_style_dynstr_debug) {
        import core.stdc.stdio : stderr, fprintf;
        fprintf(stderr, "addColorItem:001: style.length=%u; style.capacity=%u; stype.ptr=0x%08x\n",
          cast(uint)style.length, cast(uint)style.capacity, cast(uint)style.ptr);
        conwriteln("ADDED(", style.length, "): ci.sel:", style[$-1].sel.getData, "; ci.prop:", style[$-1].prop.getData);
      }
    }
  }

public:
  void parseStyle (const(char)[] str) {
    auto par = EgraSimpleParser(str);
    while (!par.isEOT) {
      if (par.check('!')) {
        auto cmd = par.expectId();
        if (cmd.strEquCI("clear-style")) {
          clear();
        } else {
          par.error("invalid command: '"~cmd.idup~"'");
        }
        par.expect(';');
        continue;
      }
      auto sel = par.expectSelector();
      par.expect("{");
      while (!par.check("}")) {
        auto prop = par.expectId();
        par.expect(":");
        Item ci;
        ci.sel = sel;
        ci.prop = prop;
        if (par.check('"')) {
          // string
          ci.value = Value.String(par.parseString('"'));
        } else if (par.check('\'')) {
          // string
          ci.value = Value.String(par.parseString('\''));
        } else if (par.checkDigitNoEat() || par.checkNoEat('+') || par.checkNoEat('-')) {
          // number
          ci.value = Value.Integer(par.parseInt());
        } else {
          // color
          ci.value = Value.Color(par.parseColor());
        }
        par.expect(';');
        addColorItem(ci);
      }
    }

    version(none) {
      conwriteln("items: ", style.length);
      conwriteln("0:ADDED: ci.sel:", style[0].sel.getData, "; ci.prop:", style[0].prop.getData);
      conwriteln("$-1:ADDED: ci.sel:", style[$-1].sel.getData, "; ci.prop:", style[$-1].prop.getData);
      foreach (const ref Item it; style; reversed) {
        conwriteln("*** it.sel:", it.sel.getData, "; it.prop:", it.prop.getData);
      }
    }
  }

public:
  this () {}

  void cloneFrom (WidgetStyle st) {
    if (st is null || st is this) return;
    //Item[] style;
    clearStyleCache();
    style.length -= style.length;
    style.reserve(st.style.length);
    foreach (const ref Item it; st.style) addColorItem(it);
  }

  void clear () {
    clearStyleCache();
    style.length -= style.length;
  }

  protected static struct BaseInfo {
    TypeInfo_Class defaultParent = void;
    TypeInfo_Class ctsrc = void;
  }

  protected const(Value)* findValueIntr (EgraStyledClass obj, const(char)[] prop) @trusted nothrow {
    if (obj is null || style.length == 0 || prop.length == 0) return null;

    version(egra_style_debug_search) conwriteln("*** SEARCHING:", typeid(obj).name, "; mod=", obj.getCurrentMod(), "; prop:", prop, "****");
    TypeInfo_Class cioverride = typeid(obj);
    const(Value)* resval = null;
    while (cioverride !is null) {
      const(Value)* resmod = null;
      const(Value)* resnomod = null;
      foreach (const ref Item it; style; reversed) {
        if (it.prop != prop) continue;
        version(egra_style_debug_search) conwriteln("  OBJ:", typeid(obj).name, "; ci:", classShortName(cioverride), "; prop:", prop, "; it.sel:", it.sel.getData, "; it.prop:", it.prop.getData);
        bool modhit, modseen;
        if (obj.isMySelector(it.sel, classShortName(cioverride), &modhit, &modseen, asQuery:false)) {
          if (modseen) {
            // last selector had mod
            if (!modhit || obj.getCurrentMod().length == 0) continue; // object has no mod, cannot apply
          } else {
            // last selector had no mod
            //if (modhit && obj.getCurrentMod().length != 0) modhit = false;
          }
          debug conwriteln("   FOUND! modseen=", modseen, "; modhit=", modhit, "; objmod=", obj.getCurrentMod, "; sel=", it.sel);
          if (modhit) { resmod = &it.value; break; }
          else if (resnomod is null) resnomod = &it.value;
        }
      }
      if (resmod !is null) {
        version(egra_style_debug_search) conwriteln("    FOUND MOD!");
        resval = resmod;
        break;
      }
      if (resnomod !is null) {
        version(egra_style_debug_search) conwriteln("    FOUND NOMOD!");
        resval = resnomod;
        break;
      }
      cioverride = cioverride.base;
      if (cioverride is typeid(EgraStyledClass)) {
        EgraStyledClass tl = obj.getTopLevel();
        if (tl is null || tl is obj) break;
        obj = tl;
        cioverride = typeid(obj);
      }
    }

    return resval;
  }

  final const(Value)* findValue (EgraStyledClass obj, const(char)[] prop) @trusted nothrow {
    if (obj is null || style.length == 0 || prop.length == 0) return null;

    if (auto fv = findCachedValue(obj, prop)) return (fv.isEmpty ? null : fv);

    if (auto fv = findValueIntr(obj, prop)) {
      cacheValue(*fv, obj, prop);
      return fv;
    }

    Value val = Value.Empty();
    cacheValue(val, obj, prop);
    return null;
  }

  final uint findColor (EgraStyledClass obj, const(char)[] prop, bool* foundp=null) @trusted nothrow {
    if (auto val = findValue(obj, prop)) {
      if (val.isColor) {
        if (foundp) *foundp = true;
        return val.color;
      }
    }

    if (foundp) *foundp = false;
    return gxUnknown;
  }

  // returns `null` if not found
  final dynstring findString (EgraStyledClass obj, const(char)[] prop, bool* foundp=null) @trusted nothrow {
    if (auto val = findValue(obj, prop)) {
      if (val.isString) {
        if (foundp) *foundp = true;
        return val.sval;
      }
    }

    if (foundp) *foundp = false;
    return dynstring();
  }

  // returns 0 if not found
  final int findInt (EgraStyledClass obj, const(char)[] prop, in int defval=0, bool* foundp=null) @trusted nothrow {
    if (auto val = findValue(obj, prop)) {
      if (val.isInteger) {
        if (foundp) *foundp = true;
        return val.ival;
      }
    }

    if (foundp) *foundp = false;
    return defval;
  }

static:
  static string classShortName (in TypeInfo_Class ct) pure nothrow @trusted @nogc {
    pragma(inline, true);
    if (ct is null) return null;
    string name = ct.name;
    auto dpos = name.lastIndexOf('.');
    return (dpos < 0 ? name : name[dpos+1..$]);
  }
}


abstract class EgraStyledClass {
protected:
  // cached path to this object, w/o property name
  EgraCIString mCachedPath;
  WidgetStyle mStyleSheet;
  // for styles
  dynstring mId;
  dynstring mStyleClass;
  bool mStyleCloned;

  // call when parent was changed
  final void invalidatePathCache () nothrow @trusted @nogc { pragma(inline, true); mCachedPath.clear(); }

public:
  bool isMyId (const(char)[] str) nothrow @trusted @nogc { return (str.length == 0 || str.strEquCI(mId.getData)); }
  bool isMyStyleClass (const(char)[] str) nothrow @trusted @nogc { return (str.length == 0 || str.strEquCI(mStyleClass.getData)); }
  bool isMyModifier (const(char)[] str) nothrow @trusted @nogc { return (str.length == 0 || str.strEquCI(getCurrentMod)); }

  EgraStyledClass getParent () nothrow @trusted @nogc { return null; }
  EgraStyledClass getFirstChild () nothrow @trusted @nogc { return null; }
  EgraStyledClass getNextSibling () nothrow @trusted @nogc { return null; }

  EgraStyledClass getTopLevel () nothrow @trusted @nogc {
    EgraStyledClass w = getParent();
    if (w is null) return null; // we are the top
    for (;;) {
      EgraStyledClass p = w.getParent();
      if (p is null) return w;
      w = p;
    }
  }

  // empty `str` should return `true`
  bool isMyClassName (const(char)[] str) nothrow @trusted @nogc {
    return (str.length == 0 || str.strEquCI(classShortName(typeid(this))));
  }

  // for styling
  EgraCIString getFullPath () nothrow @trusted @nogc {
    if (mCachedPath.length == 0) {
      mCachedPath.clear(); // just in case
      for (EgraStyledClass w = this; w !is null; w = w.getParent()) {
        mCachedPath ~= typeid(w).name;
        mCachedPath ~= "\x00"; // delimiter
        mCachedPath ~= mId;
        mCachedPath ~= "\x00"; // delimiter
        mCachedPath ~= mStyleClass;
        mCachedPath ~= "\x00"; // delimiter
      }
    }
    return mCachedPath;
  }

  string getCurrentMod () nothrow @trusted @nogc { return ""; }

  final WidgetStyle getStyle () nothrow @trusted @nogc {
    for (EgraStyledClass w = this; w !is null; w = w.getParent()) {
      if (w.mStyleSheet !is null) return w.mStyleSheet;
    }
    return defaultColorStyle;
  }

  final @property dynstring id () const nothrow @trusted @nogc { pragma(inline, true); return dynstring(mId); }
  @property void id (const(char)[] v) nothrow @trusted @nogc {
    if (v.length != mId.length || !strEquCI(v, mId.getData)) invalidatePathCache();
    if (v != mId.getData) mId = v;
  }

  final @property dynstring styleClass () const nothrow @trusted @nogc { pragma(inline, true); return dynstring(mStyleClass); }
  @property void styleClass (const(char)[] v) nothrow @trusted @nogc {
    if (v.length != mStyleClass.length || !strEquCI(v, mStyleClass.getData)) invalidatePathCache();
    if (v != mStyleClass.getData) mStyleClass = v;
  }

public:
  void widgetChanged () nothrow {}

  void setStyle (WidgetStyle stl) {
    if (stl !is mStyleSheet) {
      mStyleSheet = stl;
      mStyleCloned = false;
      widgetChanged();
    }
  }

  // this clones the style
  void appendStyle (const(char)[] str) {
    str = str.xstrip;
    if (str.length == 0) return;
    if (mStyleSheet is null) {
      mStyleSheet = new WidgetStyle;
      mStyleSheet.cloneFrom(defaultColorStyle);
      mStyleCloned = true;
    } else if (!mStyleCloned) {
      WidgetStyle ws = new WidgetStyle;
      ws.cloneFrom(mStyleSheet);
      mStyleSheet = ws;
      mStyleCloned = true;
    }
    mStyleSheet.parseStyle(str);
    widgetChanged();
  }

public:
  //this () {}
  ~this () nothrow @trusted @nogc { pragma(inline, true); mCachedPath.clear(); }

public:
  static template isGoodSelectorDelegate(DG) {
    import std.traits;
    enum isGoodSelectorDelegate =
      (is(ReturnType!DG == void) || is(ReturnType!DG == EgraStyledClass) || is(ReturnType!DG == bool)) &&
      is(typeof((inout int=0) { DG dg = void; EgraStyledClass w; dg(w); }));
  }

  final EgraStyledClass forEachSelector(DG) (const(char)[] sel, scope DG dg) if (isGoodSelectorDelegate!DG) {
    import std.traits;
    for (EgraStyledClass w = getFirstChild(); w !is null; w = w.getNextSibling()) {
      if (EgraStyledClass res = w.forEachSelector(sel, dg)) return res;
    }
    bool modhit;
    if (!isMySelector(sel, &modhit)) return null;
    if (!modhit) return null;
    static if (is(ReturnType!DG == void)) {
      dg(this);
      return null;
    } else static if (is(ReturnType!DG == bool)) {
      if (dg(this)) return this;
      return null;
    } else static if (is(ReturnType!DG == EgraStyledClass)) {
      if (EgraStyledClass res = dg(this)) return res;
      return null;
    } else {
      static assert(0, "wtf?!");
    }
  }

  final T querySelectorInternal(T:EgraStyledClass) (const(char)[] sel) {
    import std.traits;
    for (EgraStyledClass w = getFirstChild(); w !is null; w = w.getNextSibling()) {
      if (EgraStyledClass res = w.querySelector!T(sel)) return cast(T)res;
    }
    bool modhit;
    if (isMySelector(sel, &modhit)) {
      if (modhit) return cast(T)this;
    }
    return null;
  }

  final T querySelector(T:EgraStyledClass=EgraStyledClass) (const(char)[] sel) { pragma(inline, true); return querySelectorInternal!T(sel); }

  static template isGoodIteratorDelegate(DG, T) {
    import std.traits;
    enum isGoodIteratorDelegate =
      (is(ReturnType!DG == int)) &&
      is(typeof((inout int=0) { DG dg = void; /*EgraStyledClass*/T w; int res = dg(w); }));
  }

  static struct Iter(T) {
    EgraStyledClass c;
    dynstring sel;

    this (EgraStyledClass cc, const(char)[] asel) nothrow @safe @nogc {
      pragma(inline, true);
      asel = asel.xstrip;
      if (asel.length) { c = cc; sel = asel; }
    }

    int opApply(DG) (scope DG dg) if (isGoodIteratorDelegate!(DG, T)) {
      int res = 0;
      if (c is null || dg is null) return 0;
      c.forEachSelector(sel.getData, (EgraStyledClass w) {
        if (auto cw = cast(T)w) {
          res = dg(cw);
        }
        return (res != 0);
      });
      return res;
    }
  }

  final auto querySelectorAll(T:EgraStyledClass=EgraStyledClass) (const(char)[] sel) nothrow @safe @nogc { pragma(inline, true); return Iter!T(this, sel); }

public:
  static string classShortName (in TypeInfo_Class ct) pure nothrow @trusted @nogc {
    pragma(inline, true);
    if (ct is null) return null;
    string name = ct.name;
    auto dpos = name.lastIndexOf('.');
    return (dpos < 0 ? name : name[dpos+1..$]);
  }

  // `from`, or any superclass
  static bool isChildOf (in TypeInfo_Class from, const(char)[] cls) pure nothrow @trusted @nogc {
    cls = cls.xstrip;
    if (cls.length == 0) return false;
    // sorry for this cast
    for (TypeInfo_Class ti = cast(TypeInfo_Class)from; ti !is null; ti = ti.base) {
      if (cls.strEquCI(classShortName(ti))) {
        version(none) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "%.*s: isGoodMyClass: cls=<%.*s>  TRUE\n",
          cast(uint)ti.name.length, ti.name.ptr, cast(uint)cls.length, cls.ptr); }
        return true;
      }
      version(none) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "%.*s: isGoodMyClass: cls=<%.*s>  FALSE\n",
        cast(uint)ti.name.length, ti.name.ptr, cast(uint)cls.length, cls.ptr); }
    }
    return false;
  }

  static bool isIdChar (in char ch) pure nothrow @trusted @nogc {
    pragma(inline, true);
    return
      (ch >= '0' && ch <= '9') ||
      (ch >= 'A' && ch <= 'Z') ||
      (ch >= 'a' && ch <= 'z') ||
      ch == '_' || ch == '-';
  }

  // leading and trailing spaces should be stripped
  // also, there should be no spaces inside the string
  final bool checkOneSelector (const(char)[] sel, const(char)[] cnoverride, out bool modhit, out bool modseen, in bool asQuery) nothrow @trusted @nogc {
    modhit = true;
    modseen = false;
    //sel = sel.xstrip;
    version(none) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "%.*s: checkOneSelector: s=<%.*s>\n",
      cast(uint)typeid(this).name.length, typeid(this).name.ptr, cast(uint)sel.length, sel.ptr); }
    if (sel.length == 0) return false;
    usize epos = 0;
    while (epos < sel.length && isIdChar(sel.ptr[epos])) ++epos;
    const(char)[] cls = sel[0..epos];
    if (cls.length != 0) {
      if (cnoverride.length) {
        if (!cnoverride.strEquCI(cls)) return false;
      } else {
        if (!isMyClassName(cls)) return false;
      }
    }
    sel = sel[epos..$];
    // check id and style class
    while (sel.length) {
      immutable char ch = sel.ptr[0];
      if (ch != '.' && ch != '#' && ch != ':') return false;
      epos = 1;
      while (epos < sel.length && isIdChar(sel.ptr[epos])) ++epos;
      const(char)[] nm = sel[1..epos];
      sel = sel[epos..$];
      final switch (ch) {
        case '.': if (!isMyStyleClass(nm)) return false; break;
        case '#': if (!isMyId(nm)) return false; break;
        case ':': // all modifiers must match
          modseen = true;
          if (!isMyModifier(nm)) {
            debug conwriteln("CHECKONE(", typeid(this).name, "): mod=<", nm, ">: NOT MATCHED! (", getCurrentMod, ")");
            modhit = false;
          } else {
            debug conwriteln("CHECKONE(", typeid(this).name, "): mod=<", nm, ">: MATCHED! (", getCurrentMod, ")");
          }
          break;
      }
    }
    if (!modseen && !asQuery && getCurrentMod().length) modhit = false;
    return true;
  }

  final bool isMySelector (const(char)[] sel, const(char)[] clnameoverride,
                           bool* modhit=null, bool* modseen=null,
                           in bool asQuery=true) nothrow @trusted @nogc
  {
    bool tmpmh, tmpms;
    if (modhit) *modhit = false;
    if (modseen) *modseen = false;
    sel = sel.xstrip;
    if (sel.length == 0) return false;
    // check object class name
    usize epos = sel.length;
    while (epos > 0) {
      immutable char ch = sel[epos-1];
      if (ch <= ' ' || ch == '>') break;
      --epos;
    }
    if (!checkOneSelector(sel[epos..$], clnameoverride, tmpmh, tmpms, asQuery)) {
      return false;
    }
    if (modhit) *modhit = tmpmh;
    if (modseen) *modseen = tmpms;
    sel = sel[0..epos].xstripright;
    if (sel.length == 0) return true;
    immutable bool oneParent = (sel[$-1] == '>');
    if (oneParent) {
      sel = sel[0..$-1].xstripright;
      if (sel.length == 0) return true;
      if (sel[$-1] == '>') return false;
    }
    version(none) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "%.*s: isMySelector: oneParent=%d\n",
      cast(uint)typeid(this).name.length, typeid(this).name.ptr, cast(int)oneParent); }
    // sorry for this cast
    for (EgraStyledClass w = (cast(EgraStyledClass)this).getParent(); w !is null; w = w.getParent()) {
      version(none) { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "%.*s: isMySelector: parent=<%.*s>; oneParent=%d\n",
        cast(uint)typeid(this).name.length, typeid(this).name.ptr, cast(uint)typeid(w).name.length, typeid(w).name.ptr, cast(int)oneParent); }
      if (w.isMySelector(sel, null, null, null, asQuery:asQuery)) return true;
      if (oneParent) return false;
    }
    return false;
  }

  final bool isMySelector (const(char)[] sel, bool* modhit=null) nothrow @trusted @nogc {
    pragma(inline, true);
    return isMySelector(sel, null, modhit);
  }

public:
  // returns gxUnknown if not found
  final uint getColor (const(char)[] prop, bool* foundp=null) @trusted nothrow {
    pragma(inline, true);
    return getStyle().findColor(this, prop, foundp);
  }

  // returns empty string if not found
  final dynstring getString (const(char)[] prop, bool* foundp=null) @trusted nothrow {
    pragma(inline, true);
    return getStyle().findString(this, prop, foundp);
  }

  final int getInt (const(char)[] prop, in int defval=0, bool* foundp=null) @trusted nothrow {
    pragma(inline, true);
    return getStyle().findInt(this, prop, defval, foundp);
  }
}


__gshared WidgetStyle defaultColorStyle;

shared static this () {
  defaultColorStyle = new WidgetStyle;
  defaultColorStyle.parseStyle(defaultStyleText);
}
