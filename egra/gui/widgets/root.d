/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.widgets.root /*is aliced*/;
private:

import arsd.simpledisplay;

import iv.egra.gfx;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.utfutil;

import iv.egra.gui.subwindows;
import iv.egra.gui.widgets.base;


// ////////////////////////////////////////////////////////////////////////// //
// this is root widget that is used for subwindow client area
public class RootWidget : Widget {
  SubWindow owner;
  uint mButtons; // used for grab

  override EgraStyledClass getParent () nothrow @trusted @nogc { return owner; }

  @disable this ();

  this (SubWindow aowner) {
    childDir = GxDir.Vert;
    super(null);
    owner = aowner;
    if (aowner !is null) aowner.setRoot(this);
  }

  override bool isOwnerFocused () nothrow @safe @nogc {
    if (owner is null) return false;
    return owner.active;
  }

  override bool isOwnerInWindowList () nothrow @safe @nogc {
    if (owner is null) return false;
    return owner.inWindowList;
  }

  // can be called by the owner
  // this also resets pressed mouse buttons
  override void releaseGrab () {
    mButtons = 0;
  }

  override GxPoint getGlobalOffset () nothrow @safe {
    if (owner is null) return super.getGlobalOffset();
    return GxPoint(owner.x0+owner.clientOffsetX, owner.y0+owner.clientOffsetY)+rect.pos;
  }

  void checkGrab () {
    if (mButtons && !isOwnerFocused) releaseGrab();
  }

  bool hasGrab () {
    checkGrab();
    return (mButtons != 0);
  }

  // this is called by the owning window when the window is added to the window list
  // it is used to set focus to the first properly focusable child if nothing was focused yet
  void fixFocus () {
    Widget fc = focusedWidget;
    if (fc is null || fc is this || !fc.canAcceptFocus()) moveFocusForward();
    //fc = focusedWidget;
    //if (fc is null) conwriteln("FUCK!"); else conwriteln("FOCUSED: ", typeid(fc).name);
  }

  // move focus backward (if it is not obvious yet)
  void moveFocusBackward () {
    Widget pf = null;
    Widget fc = focusedWidget;
    foreach (Widget w; allVisualDepth) {
      if (w is fc) break;
      if (w.canAcceptFocus()) pf = w;
    }

    if (pf is null) {
      // find last
      foreach (Widget w; allVisualDepth) {
        if (w.canAcceptFocus()) pf = w;
      }
    }

    if (pf !is null) pf.focus();
  }

  // move focus forward (if it is not obvious yet)
  void moveFocusForward () {
    Widget fc = focusedWidget;
    bool seenFC = (fc is null);
    Widget nf = null;
    foreach (Widget w; allVisualDepth) {
      if (!seenFC) {
        seenFC = (w is fc);
      } else {
        if (w.canAcceptFocus()) { nf = w; break; }
      }
    }

    if (nf is null) {
      // find first
      foreach (Widget w; allVisualDepth) {
        if (w.canAcceptFocus()) { nf = w; break; }
      }
    }

    if (nf !is null) nf.focus();
  }


  override bool onKeyBubble (Widget dest, KeyEvent event) {
    if (event.pressed) {
      if (event == "Tab" || event == "C-Tab") { moveFocusForward(); return true; }
      if (event == "S-Tab" || event == "C-S-Tab") { moveFocusBackward(); return true; }

      Widget def;
      immutable bool isDefAccept = (event == "Enter");
      immutable bool isDefCancel = (event == "Escape");

      // check hotkeys
      Widget hk = null;
      foreach (Widget w; allVisualDepth) {
        if (w.isMyHotkey(event) && w.hotkeyActivated()) {
          def = null;
          hk = w;
          break;
        }
        if (def is null) {
          if (isDefAccept || isDefCancel) {
            if ((isDefAccept && w.deftype == Default.Accept) ||
                (isDefCancel && w.deftype == Default.Cancel))
            {
              if (w.canAcceptFocus()) def = w;
            }
          }
        }
      }

      if (hk !is null) return true;
      if (def !is null && def.hotkeyActivated()) return true;
    }

    return super.onKeyBubble(dest, event);
  }

  // dispatch keyboard event
  bool dispatchKey (KeyEvent event) {
    checkGrab();
    return dispatchTo(focusedWidget, delegate bool (Widget curr, Widget dest, EventPhase phase) {
      if (curr.isNonVisual) return false;
      final switch (phase) {
        case EventPhase.Sink: return curr.onKeySink(dest, event);
        case EventPhase.Mine: return curr.onKey(event);
        case EventPhase.Bubble: return curr.onKeyBubble(dest, event);
      }
      assert(0); // just in case
    });
  }

  // this is quite complicated...
  // find proper destination for mouse event according to current grab
  // also, updates the current grab
  protected Widget getMouseDestination (in MouseEvent event) {
    checkGrab();

    // still has a grab?
    if (mButtons) {
      // if some mouse buttons are still down, route everything to the focused widget
      // also, release a grab here if we can (grab flag is not used anywhere else)
      if (event.type == MouseEventType.buttonReleased) mButtons &= ~cast(uint)event.button;
      return focusedWidget;
    }

    Widget dest = childAt(event.x, event.y);
    assert(dest !is null);

    // if mouse button is pressed, and there were no pressed buttons before,
    // find the child, and check if it can grab events
    if (event.type == MouseEventType.buttonPressed) {
      if (dest !is focusedWidget) dest.focus();
      if (dest is focusedWidget) {
        // this activates the grab
        mButtons = cast(uint)event.button;
      } else {
        releaseGrab();
      }
    } else {
      // release grab, if necessary (it shouldn't be necessary here, but just in case...)
      if (mButtons && event.type == MouseEventType.buttonReleased) {
        mButtons &= ~cast(uint)event.button;
      }
    }

    // route to the proper child
    return dest;
  }

  // dispatch mouse event
  // mouse event coords should be relative to our rect
  bool dispatchMouse (MouseEvent event) {
    Widget dest = getMouseDestination(event);
    assert(dest !is null);
    // convert event to global
    immutable GxRect grect = globalRect;
    event.x += grect.pos.x;
    event.y += grect.pos.y;
    return dispatchTo(dest, delegate bool (Widget curr, Widget dest, EventPhase phase) {
      if (curr.isNonVisual) return false;
      // fix coordinates
      immutable GxRect wrect = curr.globalRect;
      MouseEvent ev = event;
      ev.x -= wrect.pos.x;
      ev.y -= wrect.pos.y;
      final switch (phase) {
        case EventPhase.Sink: return curr.onMouseSink(dest, ev);
        case EventPhase.Mine: return curr.onMouse(ev);
        case EventPhase.Bubble: return curr.onMouseBubble(dest, ev);
      }
      assert(0); // just in case
    });
  }

  // dispatch char event
  bool dispatchChar (dchar ch) {
    checkGrab();
    return dispatchTo(focusedWidget, delegate bool (Widget curr, Widget dest, EventPhase phase) {
      if (curr.isNonVisual) return false;
      final switch (phase) {
        case EventPhase.Sink: return curr.onCharSink(dest, ch);
        case EventPhase.Mine: return curr.onChar(ch);
        case EventPhase.Bubble: return curr.onCharBubble(dest, ch);
      }
      assert(0); // just in case
    });
  }
}
