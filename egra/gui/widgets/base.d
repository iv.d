/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.widgets.base /*is aliced*/;
private:

import core.time;

import arsd.simpledisplay;

import iv.egra.gfx;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.utfutil;

import iv.egra.gui.subwindows;
import iv.egra.gui.widgets.buttons : RadioWidget;


// ////////////////////////////////////////////////////////////////////////// //
// this is used as parent if parent is null (but not for root widgets)
public Widget creatorCurrentParent = null;


// ////////////////////////////////////////////////////////////////////////// //
public enum WidgetStringPropertyMixin(string propname, string fieldName) = `
  @property dynstring `~propname~` () const nothrow @safe @nogc { return `~fieldName~`; }
  @property void `~propname~`(T:const(char)[]) (T v) nothrow @trusted {
    static if (is(T == typeof(null))) {
      `~fieldName~`.clear();
    } else {
      `~fieldName~` = v;
    }
  }
`;


// ////////////////////////////////////////////////////////////////////////// //
// base widget class
// all widget are inherited from this one
// it contains all basic widget logic
public abstract class Widget : EgraStyledClass {
public:
  static template isGoodEnterBoxDelegate(DG) {
    import std.traits;
    enum isGoodEnterBoxDelegate =
      is(ReturnType!DG == void) &&
      (is(typeof((inout int=0) { DG dg = void; Widget w; dg(w); })) ||
       is(typeof((inout int=0) { DG dg = void; dg(); })));
  }

  static template isEnterBoxDelegateWithArgs(DG) {
    enum isEnterBoxDelegateWithArgs =
      isGoodEnterBoxDelegate!DG &&
      is(typeof((inout int=0) { DG dg = void; Widget w; dg(w); }));
  }

protected:
  // widgets w/o tabstop cannot be focused
  bool tabStop;
  // set this to `true` to skip painting; useful for spring and spacer widgets
  // such widgets will not receive any events too (including sink/bubble)
  // note that there is no reason to add children to such widgets, because
  // non-visual widgets cannot be painted, and cannot be focused
  bool nonVisual;
  // is this widget disabled?
  // do not change directly!
  bool mDisabled;

public:
  Widget parent;
  Widget firstChild;
  Widget nextSibling;

  // widget coords are relative to its parent
  // DO NOT MODIFY DIRECTLY!
  GxRect rect;

  // this is for "default accept" and "default cancel" widgets
  // uneaten "enter" will activate the first focusable "accept" widget
  // uneated "escape" will activate the first focusable "cancel" widget
  enum Default {
    None,
    Accept,
    Cancel,
  }

  // this can be freely changed (for now)
  Default deftype = Default.None;


  // this is for flexbox layouter //
  enum BoxHAlign {
    Expand,
    Left,
    Center,
    Right,
  }

  enum BoxVAlign {
    Expand,
    Top,
    Center,
    Bottom,
  }

  GxSize minSize; // minimum size
  GxSize maxSize = GxSize(int.max, int.max); // maximum size
  GxSize prefSize = GxSize(int.min, int.min); // preferred (initial) size; will be taken from widget size

  GxSize boxsize; /// box size
  GxSize finalSize; /// final size (can be bigger than `size)`
  GxPoint finalPos; /// final position (for the final size); relative to the parent origin

  GxDir childDir = GxDir.Horiz; /// children orientation
  int flex; /// <=0: not flexible

  BoxHAlign boxHAlign = BoxHAlign.Expand;
  BoxVAlign boxVAlign = BoxVAlign.Expand;

  // widgets with the same id will start with the equal preferred sizes (max of all)
  string hsizeId;
  string vsizeId;

  // working fields for layouter; do not use, do not modify
  Widget hsizeIdNext;
  Widget vsizeIdNext;

public:
  // layouter will call this before doing its work
  // this can be used to setup layouter vars
  // it is called in "depth first" order
  void preLayout () {
    if (prefSize.w == int.min) prefSize = rect.size; else rect.size = prefSize;
  }

  // layouter will call this after doing its work
  // this can be used to setup widget size from layouter vars
  // it is called in "depth first" order
  void postLayout () {
    rect.pos = finalPos;
    rect.size = boxsize;
    final switch (boxHAlign) with (BoxHAlign) {
      case Expand: rect.size.w = finalSize.w; break;
      case Left: break;
      case Center: rect.pos.x = finalPos.x+(finalSize.w-boxsize.w)/2; break;
      case Right: rect.pos.x = finalPos.x+finalSize.w-boxsize.w; break;
    }
    final switch (boxVAlign) with (BoxVAlign) {
      case Expand: rect.size.h = finalSize.h; break;
      case Top: break;
      case Center: rect.pos.y = finalPos.y+(finalSize.h-boxsize.h)/2; break;
      case Bottom: rect.pos.y = finalPos.y+finalSize.h-boxsize.h; break;
    }
  }

// this is from "styled class", to support styling and selector queries
public:
  override bool isMyModifier (const(char)[] str) nothrow @trusted @nogc {
    //if (nonVisual) return strEquCI("nonvisual");
    if (str.length == 0) return true;
    if (mDisabled && str.length) return str.strEquCI("disabled");
    //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "%.*s: MOD: <%.*s>; focused=%d\n", cast(uint)typeid(this).name.length, typeid(this).name.ptr, cast(uint)str.length, str.ptr, cast(int)isFocused); }
    if (strEquCI(str, "focused")) return isFocusedForStyle;
    if (strEquCI(str, "unfocused")) return !isFocusedForStyle;
    return false;
  }

  override EgraStyledClass getFirstChild () nothrow @trusted @nogc { return firstChild; }
  override EgraStyledClass getNextSibling () nothrow @trusted @nogc { return nextSibling; }
  override EgraStyledClass getParent () nothrow @trusted @nogc { return parent; }

  override string getCurrentMod () nothrow @trusted @nogc {
    return
      mDisabled ? "disabled" :
      isFocusedForStyle ? "focused" :
      "";
  }

  // call this when the widget need to be repainted
  // currently it notifies its parent window that some widget was changed
  // the parent window will take care of the rest
  // if you won't call this, repainting can be deferred for indefinite time
  override void widgetChanged () nothrow {
    EgraStyledClass w = getTopLevel();
    if (w is null) return;
    SubWindow win = cast(SubWindow)w;
    if (win is null) return;
    win.widgetChanged();
  }

  final T querySelector(T:EgraStyledClass=Widget) (const(char)[] sel) { pragma(inline, true); return querySelectorInternal!T(sel); }
  final auto querySelectorAll(T:EgraStyledClass=Widget) (const(char)[] sel) nothrow @safe @nogc { pragma(inline, true); return Iter!T(this, sel); }

protected:
  // active child; used to determine focus
  // this is NOT reset when focus changed
  // (i.e. if parent will change focus to some other child, this won't be reset)
  // do not change directly!
  Widget mActive;
  // hotkey, in emacs format
  dynstring mHotkey;

public:
  // called by keyboard event handler
  // return `true` if `event` represents our hotkey
  bool isMyHotkey (KeyEvent event) {
    return
      (mHotkey.length && event == mHotkey) ||
      (onCheckHotkey !is null && onCheckHotkey(this, event));
  }

  // will be called if `isMyHotkey()` returned `true`
  // perform hotkey action
  // return `true` if some action was done
  bool hotkeyActivated () {
    if (!canAcceptFocus) return false;
    focus();
    doAction();
    return true;
  }

  final dynstring getHotkey () const nothrow @safe @nogc { return dynstring(mHotkey); }

  // subclasses can override this, to take some action on hotkey change
  void setHotkey (const(char)[] v) { mHotkey = v; }

  // this is called by `isMyHotkey()`
  // can be used to assign additional hotkeys without subclassing
  bool delegate (Widget self, KeyEvent event) onCheckHotkey;

// widget action (it is usually performed on hotkey press or on click)
public:
  void delegate (Widget self) onAction;

  // this will be performed if `onAction` is not set
  protected void doDefaultAction () {}

  // use this method to perform "onclick action"
  // you'd better override `doDefaultAction()` to change the default
  void doAction () {
    if (onAction !is null) onAction(this); else doDefaultAction();
    widgetChanged();
  }

public:
  // extracts "&n" hotkey and hotkey coordsfrom title text
  // returns `true` if hotkey was found
  // doesn't modify `ref` vars if not found (hence the `ref`, and not `out`)
  // hotkey will be prepended with "M-"
  static bool extractHotKey (ref dynstring text, ref dynstring hotch, ref int hotxpos, ref int hotxlen) nothrow @safe {
    if (text.length < 2) return false;
    auto umpos = text.indexOf('&');
    if (umpos < 0 || umpos >= text.length-1) return false;
    char ch = text[umpos+1];
    if (ch > 32 && ch < 128) {
      hotch = text[umpos+1..umpos+2];
      text = text[0..umpos]~text[umpos+1..$];
    } else if (ch >= 128) {
      hotch = text[umpos+1..$];
      while (hotch.utflen > 1) hotch = hotch.utfchop;
      text = text[0..umpos]~text[umpos+1..$];
    } else {
      return false;
    }
    hotxlen = gxTextWidthUtf(hotch);
    hotxpos = gxTextWidthUtf(text[0..umpos+1])-hotxlen;
    hotch = "M-"~hotch;
    return true;
  }

public:
  // add child widget; use this instead of direct modifications
  void addChild (Widget w) {
    if (w is null) return;
    assert (w !is this);
    assert(w.parent is null);
    if (cast(RootWidget)w) throw new Exception("root widget cannot be child of anything");
    if (nonVisual) throw new Exception("non-visual widget cannot have children");
    w.parent = this;
    Widget lc = firstChild;
    if (lc is null) {
      firstChild = w;
    } else {
      while (lc.nextSibling) lc = lc.nextSibling;
      lc.nextSibling = w;
    }
    w.invalidatePathCache();
    w.widgetChanged();
    widgetChanged();
  }

  // get topmost widget (note that window is a completely different class, it is not a widget)
  // usually, window contains `RootWidget` containes, which in turn contains other widgets
  final Widget rootWidget () nothrow @safe @nogc {
    Widget res = this;
    while (res.parent) res = res.parent;
    return res;
  }

// widget enumerators (depth-first)
public:
  static template isFEDGoodDelegate(DG) {
    import std.traits;
    enum isFEDGoodDelegate =
      (is(ReturnType!DG == void) || is(ReturnType!DG == Widget)) &&
      (is(typeof((inout int=0) { DG dg = void; Widget w; Widget res = dg(w); })) ||
       is(typeof((inout int=0) { DG dg = void; Widget w; dg(w); })));
  }

  static template isFEDResDelegate(DG) {
    import std.traits;
    enum isFEDResDelegate = is(ReturnType!DG == Widget);
  }

  // Widget delegate (Widget w)
  final Widget forEachDepth(DG) (scope DG dg) if (isFEDGoodDelegate!DG) {
    for (Widget w = firstChild; w !is null; w = w.nextSibling) {
      Widget res = w.forEachDepth(dg);
      if (res !is null) return res;
    }
    static if (isFEDResDelegate!DG) {
      return dg(this);
    } else {
      dg(this);
      return null;
    }
  }

  // Widget delegate (Widget w)
  final Widget forEachVisualDepth(DG) (scope DG dg) if (isFEDGoodDelegate!DG) {
    if (nonVisual) return null;
    for (Widget w = firstChild; w !is null; w = w.nextSibling) {
      if (!w.nonVisual) {
        Widget res = w.forEachVisualDepth(dg);
        if (res !is null) return res;
      }
    }
    static if (isFEDResDelegate!DG) {
      return dg(this);
    } else {
      dg(this);
      return null;
    }
  }

  // `foreach()` support starts here
  static template isGoodIteratorDelegate(DG) {
    import std.traits;
    enum isGoodIteratorDelegate =
      (is(ReturnType!DG == int)) &&
      is(typeof((inout int=0) { DG dg = void; Widget w; int res = dg(w); }));
  }

  static struct IterChild {
    Widget w;
    bool onlyVisual;

    this (Widget ww, in bool onlyvis) pure nothrow @safe @nogc {
      pragma(inline, true);
      w = ww;
      onlyVisual = onlyvis;
    }

    int opApply(DG) (scope DG dg) if (isGoodIteratorDelegate!DG) {
      int res = 0;
      if (w is null || dg is null) return 0;
      if (onlyVisual) {
        w.forEachVisualDepth((Widget w) {
          res = dg(w);
          return (res ? w : null);
        });
      } else {
        w.forEachDepth((Widget w) {
          res = dg(w);
          return (res ? w : null);
        });
      }
      return res;
    }
  }

  final auto allDepth () pure nothrow @safe @nogc { pragma(inline, true); return IterChild(this, false); }
  final auto allVisualDepth () pure nothrow @safe @nogc { pragma(inline, true); return IterChild(this, true); }
  final auto allDepth (in bool onlyvis) pure nothrow @safe @nogc { pragma(inline, true); return IterChild(this, onlyvis); }

  final RadioWidget getSelectedRadio (const(char)[] groupname) {
    foreach (Widget w; rootWidget.allVisualDepth) {
      RadioWidget rw = cast(RadioWidget)w;
      if (rw is null) continue;
      if (rw.rgroup != groupname) continue;
      if (rw.checked) return rw;
    }
    return null;
  }

// `foreach()` support for children (no depth walking)
public:
  static struct IterChildOnly {
    Widget w;
    bool onlyVisual;

    this (Widget ww, in bool onlyvis) pure nothrow @safe @nogc {
      pragma(inline, true);
      w = ww;
      onlyVisual = onlyvis;
    }

    int opApply(DG) (scope DG dg) if (isGoodIteratorDelegate!DG) {
      int res = 0;
      if (w is null || dg is null) return 0;
      for (w = w.firstChild; w !is null; w = w.nextSibling) {
        if (onlyVisual && w.nonVisual) continue;
        res = dg(w);
        if (res) break;
      }
      return res;
    }
  }

  final auto children () pure nothrow @safe @nogc { pragma(inline, true); return IterChildOnly(this, false); }
  final auto visualChildren () pure nothrow @safe @nogc { pragma(inline, true); return IterChildOnly(this, true); }
  final auto children (in bool onlyvis) pure nothrow @safe @nogc { pragma(inline, true); return IterChildOnly(this, onlyvis); }

public:
  this () {
    assert(creatorCurrentParent !is null);
    assert(cast(RootWidget)this is null);
    creatorCurrentParent.addChild(this);
  }

  this (Widget aparent) {
    if (aparent !is null) aparent.addChild(this);
  }


  // box hierarchy
  Widget enter(DG) (scope DG dg) if (isGoodEnterBoxDelegate!DG) {
    if (dg !is null) {
      Widget oldCCP = creatorCurrentParent;
      creatorCurrentParent = this;
      scope(exit) creatorCurrentParent = oldCCP;
      static if (isEnterBoxDelegateWithArgs!DG) {
        dg(this);
      } else {
        dg();
      }
    }
    return this;
  }

  static Widget getCurrentBox () nothrow @safe @nogc {
    pragma(inline, true);
    return creatorCurrentParent;
  }


  // returns `true` if passed `this`
  final bool isMyChild (in Widget w) pure const nothrow @safe @nogc {
    pragma(inline, true);
    return
      w is null ? false :
      w is this ? true :
      isMyChild(w.parent);
  }

  // should be overriden in root widget
  // should be called only for widgets without a parent
  bool isOwnerFocused () nothrow @safe @nogc {
    if (parent) return parent.isOwnerFocused();
    return true;
  }

  // should be overriden in root widget
  // should be called only for widgets without a parent
  bool isOwnerInWindowList () nothrow @safe @nogc {
    if (parent) return parent.isOwnerInWindowList();
    return false;
  }

  // should be overriden in the root widget
  void releaseGrab () {
  }

  // should be overriden in the root widget
  GxPoint getGlobalOffset () nothrow @safe {
    return GxPoint(0, 0);
  }

  // returns focused widget; it doesn't matter if root widget's owner is focused
  // never returns `null`
  @property Widget focusedWidget () nothrow @safe @nogc {
    if (parent) return parent.focusedWidget;
    Widget res = this;
    while (res.mActive !is null) res = res.mActive;
    return res;
  }

  // it doesn't matter if root widget's owner is focused
  //FIXME: ask all parents too?
  final @property bool focus () {
    Widget oldFocus = focusedWidget;
    if (oldFocus is this) return true;
    if (!canAcceptFocus()) return false;
    if (!oldFocus.canRemoveFocus()) return false;
    widgetChanged();
    releaseGrab();
    oldFocus.onBlur();
    Widget w = this;
    while (w.parent !is null) {
      w.parent.mActive = w;
      w = w.parent;
    }
    onFocus();
    return true;
  }

  protected final void fixRootFocus () nothrow {
    if (!canAcceptFocus() && isFocused) {
      RootWidget rw = cast(RootWidget)rootWidget;
      if (rw !is null) {
        try { rw.moveFocusForward(); } catch (Exception e) {} // sorry
      }
    }
  }

  final @property bool isVisual () const nothrow @safe @nogc { pragma(inline, true); return !nonVisual; }
  final @property bool isNonVisual () const nothrow @safe @nogc { pragma(inline, true); return nonVisual; }

  final @property bool enabled () const nothrow @safe @nogc { pragma(inline, true); return !mDisabled; }
  @property void enabled (in bool v) nothrow { if (mDisabled == v) { mDisabled = !v; widgetChanged(); fixRootFocus(); } }

  final @property bool disabled () const nothrow @safe @nogc { pragma(inline, true); return mDisabled; }
  @property void disabled (in bool v) nothrow { if (mDisabled != v) { mDisabled = v; widgetChanged(); fixRootFocus(); } }

  // use this instead of directly modifying `tabStop`
  // this method will take care of proper focus transfers
  @property void setTabStop (in bool v) nothrow { if (tabStop != v) { tabStop = v; widgetChanged(); fixRootFocus(); } }

  // this returns `false` if root widget's parent is not focused
  final @property bool isFocused () nothrow @safe @nogc {
    pragma(inline, true);
    return (isOwnerFocused() && this is focusedWidget);
  }

  // this works for windows not yet added to window list
  final @property bool isFocusedForStyle () nothrow @safe @nogc {
    pragma(inline, true);
    return (this is focusedWidget && (isOwnerFocused || !isOwnerInWindowList));
  }

  // returns point translated to the topmost parent coords
  // with proper root widget, returns global screen coordinates (useful for drawing)
  final @property GxPoint point2Global (GxPoint pt) nothrow @safe {
    if (parent is null) return pt+getGlobalOffset;
    return parent.point2Global(pt+rect.pos);
  }

  // returns rectangle in topmost parent coords
  // with proper root widget, returns global screen coordinates (useful for drawing)
  final @property GxRect globalRect () nothrow @safe {
    GxPoint pt = point2Global(GxPoint(0, 0));
    return GxRect(pt, rect.size);
  }

  // this need to be called if you did automatic layouting, and then changed size or position
  // it's not done automatically because it is rarely required
  final void rectChanged () nothrow @safe @nogc {
    pragma(inline, true);
    prefSize = GxSize(int.min, int.min);
  }

  final @property ref inout(GxSize) size () inout pure nothrow @safe @nogc { pragma(inline, true); return rect.size; }
  final @property void size (in GxSize sz) nothrow { pragma(inline, true); if (rect.size != sz) { rect.size = sz; widgetChanged(); } }

  final @property ref inout(GxPoint) pos () inout pure nothrow @safe @nogc { pragma(inline, true); return rect.pos; }
  final @property void pos (in GxPoint p) nothrow { pragma(inline, true); if (rect.pos != p) { rect.pos = p; widgetChanged(); } }

  final @property int width () const pure nothrow @safe @nogc { pragma(inline, true); return rect.size.w; }
  final @property int height () const pure nothrow @safe @nogc { pragma(inline, true); return rect.size.h; }

  final @property void width (in int v) nothrow { pragma(inline, true); if (rect.size.w != v) { rect.size.w = v; widgetChanged(); } }
  final @property void height (in int v) nothrow { pragma(inline, true); if (rect.size.h != v) { rect.size.h = v; widgetChanged(); } }

  final @property int posx () const pure nothrow @safe @nogc { pragma(inline, true); return rect.pos.x; }
  final @property int posy () const pure nothrow @safe @nogc { pragma(inline, true); return rect.pos.y; }

  final @property void posx (in int v) nothrow { pragma(inline, true); if (rect.pos.x != v) { rect.pos.x = v; widgetChanged(); } }
  final @property void posy (in int v) nothrow { pragma(inline, true); if (rect.pos.y != v) { rect.pos.y = v; widgetChanged(); } }

  // this never returns `null`, even for out-of-bound coords
  // for out-of-bounds case simply return `this`
  final Widget childAt (GxPoint pt) pure nothrow @safe @nogc {
    if (!pt.inside(rect.size)) return this;
    Widget bestChild;
    foreach (Widget w; visualChildren) {
      if (pt.inside(w.rect)) bestChild = w;
    }
    if (bestChild is null) return this;
    pt -= bestChild.rect.pos;
    return bestChild.childAt(pt);
  }

  final Widget childAt (in int x, in int y) pure nothrow @safe @nogc {
    pragma(inline, true);
    return childAt(GxPoint(x, y));
  }

  // this is called with set clip
  // passed rectangle is global rect
  // clip rect is set to the widget area
  // `doPaint()` is called before painting children
  // `doPaintPost()` is called after painting children
  // it is safe to change clip rect there, it will be restored
  // `grect` is in global screen coordinates (i.e. suitable to use with `egfx` functions)
  // `grect` is never clipped (i.e. its size is equal to widget size)
  protected void doPaint (GxRect grect) {}
  protected void doPaintPost (GxRect grect) {}

  void onPaint () {
    if (nonVisual || rect.empty) return; // nothing to paint here
    if (gxClipRect.empty) return; // totally clipped away
    gxWithSavedClip {
      gxagg.beginFrame();
      scope(exit) gxagg.cancelFrame();
      immutable GxRect grect = globalRect;
      if (!gxClipRect.intersect(grect)) return;
      if (!gxClipRect.intersect(0, 0, VBufWidth, VBufHeight)) return;
      // before-children painter
      gxWithSavedClip { gxagg.cancelFrame(); doPaint(grect); };
      // paint children
      foreach (Widget w; visualChildren) {
        gxWithSavedClip { gxagg.cancelFrame(); w.onPaint(); };
      }
      // after-children painter
      gxWithSavedClip { gxagg.cancelFrame(); doPaintPost(grect); };
    };
  }

  // return `false` to disable focus remove
  bool canRemoveFocus () nothrow { return true; }

  // return `false` to disable focus accept
  bool canAcceptFocus () nothrow { return (!nonVisual && tabStop && !mDisabled); }

  // called before removing the focus
  // you cannot prevent focus remove here (and don't change focus from this method!)
  void onBlur () {}

  // called after setting the focus
  // you cannot prevent focus remove here (and don't change focus from this method!)
  void onFocus () {}

  // return `true` if the event was eaten
  // coordinates are relative to this widget, i.e. (0,0) is top-left widget corner
  bool onKey (KeyEvent event) { return false; }
  bool onMouse (MouseEvent event) { return false; }
  bool onChar (dchar ch) { return false; }

  // coordinates are adjusted to the widget origin (NOT dest!)
  bool onKeySink (Widget dest, KeyEvent event) { return false; }
  bool onMouseSink (Widget dest, MouseEvent event) { return false; }
  bool onCharSink (Widget dest, dchar ch) { return false; }

  // coordinates are adjusted to the widget origin (NOT dest!)
  bool onKeyBubble (Widget dest, KeyEvent event) { return false; }
  bool onMouseBubble (Widget dest, MouseEvent event) { return false; }
  bool onCharBubble (Widget dest, dchar ch) { return false; }

public:
  // drawing utilities
  // all coords are global
  void drawTextCursor (int x, int y, int hgt=-666) {
    if (hgt < 1) return;
    immutable msecs = MonoTime.currTime.ticks*1000/MonoTime.ticksPerSecond;
    immutable int btm = getInt("text-cursor-blink-time", 0);
    immutable bool ctt = (btm >= 20 ? (msecs%btm < btm/2) : false);
    immutable uint clr = getColor(ctt ? "text-cursor-0" : "text-cursor-1");
    version(none) {
      gxVLine(x, y, hgt, clr);
    } else {
      //gxFillRect(x, y, 2, hgt, clr);
      gxFillRect(x, y+1, 2, hgt-2, clr);
      gxHLine(x-2, y, 6, clr);
      if (hgt > 1) gxHLine(x-2, y+hgt-1, 6, clr);
    }
    if (isFocused) {
      int ctm = 0;
      immutable uint clrDot = getColor("text-cursor-dot");
      if (hgt > 2 && !gxIsTransparent(clrDot)) {
        ctm = getInt("text-cursor-dot-time", 0);
        if (ctm >= 20) {
          hgt -= 2;
          int doty = msecs/ctm%(hgt*2-1);
          if (doty >= hgt) doty = hgt*2-doty-1;
          version(none) {
            gxPutPixel(x, y+doty+1, clrDot);
          } else {
            gxHLine(x, y+doty+1, 2, clrDot);
          }
        }
      }
      if (ctm >= 20 || btm >= 20) {
        if (!ctm || ctm > (btm>>1)) ctm = (btm>>1);
        postCurBlink(ctm);
      }
    }
  }

// common event dispatcher
public:
  enum EventPhase {
    Sink,
    Mine,
    Bubble,
  }

  // dispatch event to the given child widget (inlcuding self)
  // it implements sink/bubble model
  // delegate is:
  //   bool delegate (Widget curr, Widget dest, EventPhase phase);
  // returns "event eaten" flag (which stops propagation)
  // `dest` must be a good child, and cannot be `null`
  final bool dispatchTo(DG) (Widget dest, scope DG dg)
  if (is(typeof((inout int=0) { DG dg = void; Widget w; EventPhase ph; immutable bool res = dg(w, w, ph); })))
  {
    // as we're walking from the bottom, first calls itself recursively, and then calls delegate
    bool sinkPhase() (Widget curr) {
      if (curr is null) return false;
      if (sinkPhase(curr.parent)) return true;
      return dg(curr, dest, EventPhase.Sink);
    }

    if (dg is null || dest is null || !isMyChild(dest)) return false;

    if (sinkPhase(dest.parent)) return true;
    if (dg(dest, dest, EventPhase.Mine)) return true;
    for (Widget w = dest.parent; w !is null; w = w.parent) {
      if (dg(w, dest, EventPhase.Bubble)) return true;
    }

    return false;
  }
}
