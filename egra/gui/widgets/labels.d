/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.widgets.labels /*is aliced*/;
private:

import arsd.simpledisplay;

import iv.egra.gfx;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.utfutil;

import iv.egra.gui.subwindows;
import iv.egra.gui.widgets.base;


// ////////////////////////////////////////////////////////////////////////// //
// label with some text, non-focuseable
public class LabelWidget : Widget {
public:
  enum HAlign {
    Left,
    Center,
    Right,
  }

  enum VAlign {
    Top,
    Center,
    Bottom,
  }

protected:
  dynstring mText;
  int hotxpos;
  int hotxlen;

public:
  HAlign halign = HAlign.Left;
  VAlign valign = VAlign.Center;
  int hpad = 0;
  int vpad = 0;

public:
  this(T:const(char)[]) (Widget aparent, T atext, HAlign horiz=HAlign.Left, VAlign vert=VAlign.Center) {
    super(aparent);
    text = atext;
    rect.size.w = gxTextWidthUtf(mText);
    rect.size.h = gxTextHeightUtf;
    tabStop = false;
    halign = horiz;
    valign = vert;
  }

  this(T:const(char)[]) (T atext, HAlign horiz=HAlign.Left, VAlign vert=VAlign.Center) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, atext, horiz, vert);
  }

  mixin(WidgetStringPropertyMixin!("text", "mText"));

  protected void drawLabel (GxRect grect) {
    if (mText.length == 0) return;
    int x;
    final switch (halign) {
      case HAlign.Left: x = grect.x0+hpad; break;
      case HAlign.Center: x = grect.x0+(grect.width-gxTextWidthUtf(mText))/2; break;
      case HAlign.Right: x = grect.x0+grect.width-hpad-gxTextWidthUtf(mText); break;
    }
    int y;
    final switch (valign) {
      case VAlign.Top: y = grect.y0+vpad; break;
      case VAlign.Center: y = grect.y0+(grect.height-gxTextHeightUtf)/2; break;
      case VAlign.Bottom: y = grect.y0+grect.height-vpad-gxTextHeightUtf; break;
    }
    //gxDrawTextUtf(x0+(width-gxTextWidthUtf(mText))/2, y0+(height-gxTextHeightUtf)/2, mText, parent.clrWinText);
    gxDrawTextUtf(x, y, mText, getColor("text"));
    //gxDrawTextOutScaledUtf(1, x, y, mText, getColor("text"), gxRGB!(255, 0, 0));
    //{ import core.stdc.stdio : printf; printf("LBL: 0x%08x\n", getColor("text")); }
    if (hotxlen > 0) gxHLine(x+hotxpos, y+gxTextUnderLineUtf, hotxlen, getColor("hotline"));
  }

  protected override void doPaint (GxRect grect) {
    gxFillRect(grect, getColor("back"));
    drawLabel(grect);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// this label can have a hotkey, and will activate `dest` on hotkey press
// if `dest` is `null`, it will activate next focuseable widget (in the top parent)
// "next" means "next after itself"
public class HotLabelWidget : LabelWidget {
public:
  Widget dest; // if `null`, activate next focusable sibling

public:
  this(T:const(char)[]) (Widget aparent, T atext, HAlign horiz=HAlign.Left, VAlign vert=VAlign.Center) {
    super(aparent, atext, horiz, vert);
    if (extractHotKey(ref mText, ref mHotkey, ref hotxpos, ref hotxlen)) rect.size.w = gxTextWidthUtf(mText);
  }

  this(T:const(char)[]) (T atext, HAlign horiz=HAlign.Left, VAlign vert=VAlign.Center) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, atext, horiz, vert);
  }

  // will be called if `isMyHotkey()` returned `true`
  // return `true` if some action was done
  override bool hotkeyActivated () {
    Widget d = dest;
    if (d is null && parent !is null) {
      bool seenSelf = false;
      Widget top = parent;
      while (top.parent !is null) top = top.parent;
      foreach (Widget w; top.allVisualDepth) {
        if (!seenSelf) {
          seenSelf = (w is this);
        } else {
          if (w.canAcceptFocus()) { d = w; break; }
        }
      }
    }
    if (d is null || !d.canAcceptFocus()) return false;
    d.focus();
    return true;
  }
}
