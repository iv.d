/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.widgets.progbar /*is aliced*/;
private:

import arsd.simpledisplay;

import iv.egra.gfx;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.utfutil;

import iv.egra.gui.subwindows;
import iv.egra.gui.widgets.base;
import iv.egra.gui.widgets.labels;


// ////////////////////////////////////////////////////////////////////////// //
// just a fancy progress bar with some text
public class ProgressBarWidget : LabelWidget {
protected:
  int mMin = 0;
  int mMax = 100;
  int mCurrent = 0;
  int lastWidth = int.min;
  int lastPxFull = int.min;

private:
  final bool updateLast () {
    bool res = false;
    if (width != lastWidth) {
      lastWidth = width;
      res = true;
    }
    if (lastWidth < 1) {
      if (lastPxFull != 0) {
        res = true;
        lastPxFull = 0;
      }
    } else {
      int pxFull;
      if (mMin == mMax) {
        pxFull = lastWidth;
      } else {
        pxFull = cast(int)(cast(long)lastWidth*cast(long)(mCurrent-mMin)/cast(long)(mMax-mMin));
      }
      if (pxFull != lastPxFull) {
        res = true;
        lastPxFull = pxFull;
      }
    }
    if (res) widgetChanged();
    return res;
  }

public:
  this(T:const(char)[]) (Widget aparent, T atext, HAlign horiz=HAlign.Center, VAlign vert=VAlign.Center) {
    super(aparent, atext, horiz, vert);
    height = height+4;
  }

  this(T:const(char)[]) (T atext, HAlign horiz=HAlign.Center, VAlign vert=VAlign.Center) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, atext, horiz, vert);
  }

  final void setMinMax (int amin, int amax) {
    if (amin > amax) { immutable int tmp = amin; amin = amax; amax = tmp; }
    mMin = amin;
    mMax = amax;
    if (mCurrent < mMin) mCurrent = mMin;
    if (mCurrent > mMax) mCurrent = mMax;
    updateLast();
  }

  final @property int current () const nothrow @safe @nogc {
    pragma(inline, true);
    return mCurrent;
  }

  final @property void current (int val) {
    pragma(inline, true);
    if (val < mMin) val = mMin;
    if (val > mMax) val = mMax;
    mCurrent = val;
    updateLast();
  }

  // returns `true` if need to repaint
  final bool setCurrentTotal (int val, int total) {
    if (total < 0) total = 0;
    setMinMax(0, total);
    if (val < 0) val = 0;
    if (val > total) val = total;
    mCurrent = val;
    return updateLast();
  }

  protected void drawStripes (GxRect rect, in bool asfull) {
    if (rect.empty) return;

    immutable int sty = rect.pos.y;
    immutable int sth = rect.size.h;

    GxRect uprc = rect;
         if (rect.height > 4) uprc.size.h = 2;
    else if (rect.height > 2) uprc.size.h = 1;
    else uprc.size.h = 0;
    if (uprc.size.h > 0) {
      rect.pos.y += uprc.size.h;
      rect.size.h -= uprc.size.h;
    }

    if (!uprc.empty) gxFillRect(uprc, getColor(asfull ? "back-full-hishade" : "back-hishade"));
    gxFillRect(rect, getColor(asfull ? "back-full" : "back"));

    immutable uint clrHi = getColor(asfull ? "stripe-full-hishade" : "stripe-hishade");
    immutable uint clrOk = getColor(asfull ? "stripe-full" : "stripe");

    foreach (int y0; 0..sth) {
      gxHStripedLine(rect.pos.x-y0, sty+y0, rect.size.w+y0, 16, (y0 < uprc.size.h ? clrHi : clrOk));
    }
  }

  protected override void doPaint (GxRect grect) {
    immutable uint clrRect = getColor("rect");

    gxDrawRect(grect, clrRect);
    gxClipRect.shrinkBy(1, 1);
    grect.shrinkBy(1, 1);
    if (grect.empty) return;

    if (lastWidth != width) updateLast();
    immutable int pxFull = lastPxFull;

    if (pxFull > 0) {
      gxWithSavedClip{
        GxRect rc = GxRect(grect.pos, pxFull, grect.height);
        if (gxClipRect.intersect(rc)) drawStripes(rc, asfull:true);
      };
    }

    if (pxFull < grect.width) {
      gxWithSavedClip{
        GxRect rc = grect;
        rc.pos.x += pxFull;
        if (gxClipRect.intersect(rc)) drawStripes(grect, asfull:false);
      };
    }

    if (grect.height > 2) grect.moveLeftTopBy(0, 1);
    drawLabel(grect);
  }
}
