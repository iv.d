/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.widgets.buttons /*is aliced*/;
private:

import arsd.simpledisplay;

import iv.egra.gfx;
import iv.egra.gfx.aggmini;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.utfutil;

import iv.egra.gui.subwindows;
import iv.egra.gui.widgets.base;


// ////////////////////////////////////////////////////////////////////////// //
// base widget for various buttons (including checkboxes)
public abstract class BaseButtonWidget : Widget {
protected:
  dynstring mTitle;
  int hotxpos;
  int hotxlen;
  bool wantReturn = true;

public:
  this (Widget aparent, const(char)[] atitle, const(char)[] ahotkey=null) {
    tabStop = true;
    super(aparent);
    title = atitle;
    hotkey = ahotkey;
    extractHotKey(ref mTitle, ref mHotkey, ref hotxpos, ref hotxlen);
    rect.size.w = gxTextWidthUtf(mTitle)+6;
    rect.size.h = gxTextHeightUtf+4;
  }

  this (const(char)[] atitle, const(char)[] ahotkey=null) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, atitle, ahotkey);
  }

  mixin(WidgetStringPropertyMixin!("title", "mTitle"));
  mixin(WidgetStringPropertyMixin!("hotkey", "mHotkey"));

  override bool onKey (KeyEvent event) {
    if (!event.pressed || !isFocused) return super.onKey(event);
    if ((wantReturn && event == "Enter") || event == "Space") {
      doAction();
      return true;
    }
    return super.onKey(event);
  }

  override bool onMouse (MouseEvent event) {
    if (!isFocused) return super.onMouse(event);
    if (GxPoint(event.x, event.y).inside(rect.size)) {
      if (event.type == MouseEventType.buttonReleased && event.button == MouseButton.left) {
        doAction();
      }
      return true;
    }
    return super.onMouse(event);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// normal button
public class ButtonWidget : BaseButtonWidget {
public:
  this (Widget aparent, const(char)[] atitle, const(char)[] ahotkey=null) {
    super(aparent, atitle, ahotkey);
  }

  this (const(char)[] atitle, const(char)[] ahotkey=null) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, atitle, ahotkey);
  }

  protected override void doPaint (GxRect grect) {
    immutable uint bclr = getColor("back");
    immutable uint fclr = getColor("text");
    immutable int tx = grect.x0+(width-gxTextWidthUtf(mTitle))/2;
    int ty = grect.y0+(height-gxTextHeightUtf)/2;
    if (height < 8) {
      gxFillRect(grect.x0+1, grect.y0+1, grect.width-2, grect.height-2, bclr);
      gxHLine(grect.x0+1, grect.y0+0, grect.width-2, bclr);
      gxHLine(grect.x0+1, grect.y1+0, grect.width-2, bclr);
      gxVLine(grect.x0+0, grect.y0+1, grect.height-2, bclr);
      gxVLine(grect.x1+0, grect.y0+1, grect.height-2, bclr);
    } else {
      immutable float rad = (height < 16 ? height*0.5f : 6);
      auto grad = AGGVGradient3(grect.y0, getColor("grad0-back"),
                                grect.y1, getColor("grad2-back"),
                                getInt("grad-midp"), getColor("grad1-back"));
      //conwritefln!"PAINT(%s): y0=%s; c0=0x%08x; y1=%s; c1=0x%08x; y2=%s; c2=0x%08x"(mTitle, grad.getY(0), grad.getColor(0), grad.getY(1), grad.getColor(1), grad.getY(2), grad.getColor(2));
      gxagg
        .resetParams()
        .beginPath()
        .roundedRect(grect.x0, grect.y0, width, height, rad)
        .fill()
        .render(AGGParams.init.fillRule, grad);
      ty += 1;
    }
    gxClipRect.shrinkBy(1, 1);
    gxDrawTextUtf(tx, ty, mTitle, fclr);
    if (hotxlen > 0) gxHLine(tx+hotxpos, ty+gxTextUnderLineUtf, hotxlen, getColor("hotline"));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// fancy button for "yes/no" dialog
public class ButtonExWidget : BaseButtonWidget {
public:
  this (Widget aparent, const(char)[] atitle, const(char)[] ahotkey=null) {
    super(aparent, atitle, ahotkey);
    rect.size.h = rect.size.h+1;
  }

  this (const(char)[] atitle, const(char)[] ahotkey=null) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, atitle, ahotkey);
  }

  protected override void doPaint (GxRect grect) {
    immutable uint bclr = getColor("back");
    immutable uint hclr = getColor("shadowline");
    immutable uint fclr = getColor("text");
    immutable uint brc = getColor("rect");

    gxFillRect(grect.x0+1, grect.y0+1, grect.width-2, grect.height-2, bclr);
    gxDrawRect(grect.x0, grect.y0, grect.width, grect.height, brc);
    gxHLine(grect.x0+1, grect.y0+1, grect.width-2, hclr);

    gxClipRect.shrinkBy(1, 1);
    immutable int tx = grect.x0+(width-gxTextWidthUtf(mTitle))/2;
    immutable int ty = grect.y0+(height-gxTextHeightUtf)/2;
    gxDrawTextUtf(tx, ty, mTitle, fclr);
    if (hotxlen > 0) gxHLine(tx+hotxpos, ty+gxTextUnderLineUtf, hotxlen, getColor("hotline"));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// checkbox
public class CheckboxWidget : BaseButtonWidget {
protected:
  bool mChecked;

public:
  static void drawCheckMark (in float x, in float y, in uint clr) {
    AGGParams p;
    p.width = 2.0f;
    p.lineCap = LineCap.Butt;
    p.lineJoin = LineJoin.Miter;
    gxagg
      .beginPath()
      .moveTo(x+4, y+5)
      .lineTo(x+7, y+8)
      .lineTo(x+14, y+1)
      .stroke(p)
      .render(p.fillRule, clr);
  }

  static void drawCheckRect (in int x, in int y, in int w, in int h, in uint clr) {
    if (w < 1 || h < 1) return;
    AGGParams p = gxagg.params;
    p.width = 1.5f;
    gxagg
      .beginPath()
      .roundedRect(x+0.5f, y+0.5f, w, h, 4)
      .stroke(p)
      .render(p.fillRule, clr);
  }

public:
  this (Widget aparent, const(char)[] atitle, const(char)[] ahotkey=null) {
    tabStop = true;
    wantReturn = false;
    super(aparent, atitle, ahotkey);
    width = gxTextWidthUtf(mTitle)+6+gxTextWidthUtf("[")+gxTextWidthUtf("x")+gxTextWidthUtf("]")+8;
    height = gxTextHeightUtf+4;
  }

  this (const(char)[] atitle, const(char)[] ahotkey=null) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, atitle, ahotkey);
  }

  @property bool checked () const nothrow @safe @nogc { return mChecked; }
  @property void checked (in bool v) nothrow { if (mChecked != v) { mChecked = v; widgetChanged(); } }

  protected override void doPaint (GxRect grect) {
    immutable uint fclr = getColor("text");
    immutable uint bclr = getColor("back");
    immutable uint xclr = getColor("mark");

    gxFillRect(grect, bclr);

    gxClipRect.shrinkBy(1, 1);
    int tx = grect.x0+3;
    immutable int ty = grect.y0+(grect.height-gxTextHeightUtf)/2;

    version(none) {
      gxDrawTextUtf(tx, ty, "[", fclr);
      tx += gxTextWidthUtf("[");

      if (mChecked) gxDrawTextUtf(tx, ty, "x", xclr);
      tx += gxTextWidthUtf("x");

      gxDrawTextUtf(tx, ty, "]", fclr);
      tx += gxTextWidthUtf("]")+8;

    } else {
      drawCheckRect(tx, grect.y0+(grect.height-16)/2+1, 14+3-2, 15, fclr);
      if (mChecked) drawCheckMark(tx-1, grect.y0+(grect.height-8)/2, xclr);
      tx += 14+8-2;
    }

    gxDrawTextUtf(tx, ty, mTitle, fclr);
    if (hotxlen > 0) gxHLine(tx+hotxpos, ty+gxTextUnderLineUtf, hotxlen, fclr);
  }

  protected override void doDefaultAction () {
    mChecked = !mChecked;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// radio button
public class RadioWidget : CheckboxWidget {
public:
  // radio group
  dynstring rgroup;

public:
/// Draw a checkmark for a radio with the given upper left coordinates (ox, oy) with the specified color.
  static void drawRadioMark (in float x, in float y, in uint clr) {
    gxagg
      .beginPath()
      .circle(x+7.0f, y+7.0f, 3)
      .fill()
      .render(AGGParams.init.fillRule, clr);
  }

  static void drawRadioRect (in int x, in int y, in uint clr) {
    AGGParams p = gxagg.params;
    p.width = 1.5f;
    gxagg
      .beginPath()
      .circle(x+7.0f, y+7.0f, 6.5f)
      .stroke(p)
      .render(p.fillRule, clr);
  }

public:
  this (Widget aparent, const(char)[] atitle, const(char)[] ahotkey=null) {
    super(aparent, atitle, ahotkey);
  }

  this (const(char)[] atitle, const(char)[] ahotkey=null) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, atitle, ahotkey);
  }

  override @property bool checked () const nothrow @safe @nogc { return super.checked(); }

  override @property void checked (in bool v) nothrow {
    if (mChecked == v) return;
    if (!v) {
      mChecked = false;
      widgetChanged();
      return;
    }
    foreach (Widget w; rootWidget.allVisualDepth) {
      if (w is this) continue;
      RadioWidget rw = cast(RadioWidget)w;
      if (rw is null) continue;
      if (rw.rgroup != rgroup) continue;
      rw.checked = false;
    }
    mChecked = true;
    widgetChanged();
  }

  protected override void doPaint (GxRect grect) {
    immutable uint fclr = getColor("text");
    immutable uint bclr = getColor("back");
    immutable uint xclr = getColor("mark");

    gxFillRect(grect, bclr);

    gxClipRect.shrinkBy(1, 1);
    int tx = grect.x0+3;
    immutable int ty = grect.y0+(grect.height-gxTextHeightUtf)/2;

    immutable int cy = grect.y0+(grect.height-14)/2;
    drawRadioRect(tx+1, cy, fclr);
    if (mChecked) drawRadioMark(tx+1, cy, xclr);
    tx += 16+4;

    gxDrawTextUtf(tx, ty, mTitle, fclr);
    if (hotxlen > 0) gxHLine(tx+hotxpos, ty+gxTextUnderLineUtf, hotxlen, fclr);
  }

  protected override void doDefaultAction () {
    if (!mChecked) checked = true;
  }
}
