/* Invisible Vector Library
 * simple FlexBox-based TUI engine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.widgets.editors /*is aliced*/;

import arsd.simpledisplay;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.utfutil;
import iv.vfs;

import iv.egeditor.editor;
//import iv.egeditor.highlighters;

import iv.egra.gfx;

import iv.egra.gui.subwindows;
import iv.egra.gui.widgets.base;
import iv.egra.gui.dialogs;


// ////////////////////////////////////////////////////////////////////////// //
final class EgraEdTextMeter : EgTextMeter {
  GxKerning twkern;

  //int currofs; /// x offset for current char (i.e. the last char that was passed to `advance()` should be drawn with this offset)
  //int currwdt; /// current line width (including, the last char that was passed to `advance()`), preferably without trailing empty space between chars
  //int currheight; /// current text height; keep this in sync with the current state; `reset` should set it to "default text height"

  /// this should reset text width iterator (and curr* fields); tabsize > 0: process tabs as... well... tabs ;-)
  override void reset (int tabsize) nothrow {
    twkern.reset(tabsize, egraFontSize);
    currheight = gxTextHeightUtf;
  }

  /// advance text width iterator, return x position for drawing next char
  override void advance (dchar ch, in ref GapBuffer.HighState hs) nothrow {
    twkern.fixWidthPre(ch);
    currofs = twkern.currOfs;
    currwdt = twkern.nextOfsNoSpacing;
  }

  /// finish text iterator; it should NOT reset curr* fields!
  /// WARNING: EditorEngine tries to call this after each `reset()`, but user code may not
  override void finish () nothrow {
    //return twkern.finalWidth;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class TextEditor : EditorEngine {
  enum TEDSingleOnly; // only for single-line mode
  enum TEDMultiOnly; // only for multiline mode
  enum TEDEditOnly; // only for non-readonly mode
  enum TEDROOnly; // only for readonly mode

  static struct TEDKey { string key; string help; bool hidden; } // UDA

  static string TEDImplX(string key, string help, string code, size_t ln) () {
    static assert(key.length > 0, "wtf?!");
    static assert(code.length > 0, "wtf?!");
    string res = "@TEDKey("~key.stringof~", "~help.stringof~") void _ted_";
    int pos = 0;
    while (pos < key.length) {
      char ch = key[pos++];
      if (key.length-pos > 0 && key[pos] == '-') {
        if (ch == 'C' || ch == 'c') { ++pos; res ~= "Ctrl"; continue; }
        if (ch == 'M' || ch == 'm') { ++pos; res ~= "Alt"; continue; }
        if (ch == 'S' || ch == 's') { ++pos; res ~= "Shift"; continue; }
      }
      if (ch == '^') { res ~= "Ctrl"; continue; }
      if (ch >= 'a' && ch <= 'z') ch -= 32;
      if ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'Z') || ch == '_') res ~= ch; else res ~= '_';
    }
    res ~= ln.stringof;
    res ~= " () {"~code~"}";
    return res;
  }

  mixin template TEDImpl(string key, string help, string code, size_t ln=__LINE__) {
    mixin(TEDImplX!(key, help, code, ln));
  }

  mixin template TEDImpl(string key, string code, size_t ln=__LINE__) {
    mixin(TEDImplX!(key, "", code, ln));
  }

protected:
  KeyEvent[32] comboBuf;
  int comboCount; // number of items in `comboBuf`
  Widget pw;

  EgraEdTextMeter chiTextMeter;

public:
  this (Widget apw, int x0, int y0, int w, int h, bool asinglesine=false) {
    pw = apw;
    //coordsInPixels = true;
    lineHeightPixels = gxTextHeightUtf;
    chiTextMeter = new EgraEdTextMeter();
    super(x0, y0, w, h, null, asinglesine);
    textMeter = chiTextMeter;
    utfuck = true;
    visualtabs = true;
    tabsize = 4; // spaces
  }

  final int lineQuoteLevel (int lidx) {
    if (lidx < 0 || lidx >= lc.linecount) return 0;
    int pos = lc.line2pos(lidx);
    auto ts = gb.textsize;
    if (pos >= ts) return 0;
    if (gb[pos] != '>') return 0;
    int count = 1;
    ++pos;
    while (pos < ts) {
      char ch = gb[pos++];
           if (ch == '>') ++count;
      else if (ch == '\n') break;
      else if (ch != ' ') break;
    }
    return count;
  }

  public override void drawCursor () {
    if (pw.isFocused) {
      int lcx, lcy;
      localCursorXY(&lcx, &lcy);
      //drawTextCursor(/*pw.parent.isFocused*/true, x0+lcx, y0+lcy);
      pw.drawTextCursor(x0+lcx, y0+lcy, gxTextHeightUtf);
    }
  }

  // not here, 'cause this is done before text
  public override void drawStatus () {}

  public final paintStatusLine () {
    import core.stdc.stdio : snprintf;
    int sx = x0, sy = y0+height;
    gxFillRect(sx, sy, width, gxTextHeightUtf, pw.getColor("status-back"));
    char[128] buf = void;
    auto len = snprintf(buf.ptr, buf.length, "%04d:%04d  %d", curx, cury, linecount);
    gxDrawTextUtf(sx+2, sy, buf[0..len], pw.getColor("status-text"));
  }

  public override void drawPage () {
    fullDirty(); // HACK!
    gxWithSavedClip{
      gxClipRect.intersect(GxRect(x0, y0, width, height));
      super.drawPage();
    };
    if (!singleline) paintStatusLine();
  }


  public override void drawLine (int lidx, int yofs, int xskip) {
    int x = x0-xskip;
    int y = y0+yofs;
    auto pos = lc.line2pos(lidx);
    auto lea = lc.line2pos(lidx+1);
    auto ts = gb.textsize;
    bool utfucked = utfuck;
    immutable int bs = bstart, be = bend;
    immutable ls = pos;

    uint clr = pw.getColor("text");
    immutable uint markFgClr = pw.getColor("mark-text");
    immutable uint markBgClr = pw.getColor("mark-back");

    if (!singleline) {
      int qlevel = lineQuoteLevel(lidx);
      if (qlevel) {
        final switch (qlevel%2) {
          case 0: clr = pw.getColor("quote0-text"); break;
          case 1: clr = pw.getColor("quote1-text"); break;
        }
      } else {
        char[1024] abuf = void;
        auto aname = getAttachName(abuf[], lidx);
        if (aname.length) {
          try {
            import std.file : exists, isFile;
            clr = (aname.exists && aname.isFile ? pw.getColor("attach-file-text") : pw.getColor("attach-bad-text"));
          } catch (Exception e) {}
        }
      }
    }

    bool checkMarking = false;
    if (hasMarkedBlock) {
      //conwriteln("bs=", bs, "; be=", be, "; pos=", pos, "; lea=", lea);
      if (pos >= bs && lea <= be) {
        // full line
        gxFillRect(x0, y, winw, lineHeightPixels, markBgClr);
        clr = markFgClr;
      } else if (pos < be && lea > bs) {
        // draw block background
        auto tpos = pos;
        int bx0 = x, bx1 = x;
        chiTextMeter.twkern.reset((visualtabs ? tabsize : 0), egraFontSize);
        while (tpos < lea) {
          immutable dchar dch = dcharAtAdvance(tpos);
          if (!singleline && dch == '\n') break;
          chiTextMeter.twkern.fixWidthPre(dch);
          if (tpos > bs) break;
        }
        bx0 = bx1 = x+chiTextMeter.twkern.currOfs;
        bool eolhit = false;
        while (tpos < lea) {
          if (tpos >= be) break;
          immutable dchar dch = dcharAtAdvance(tpos);
          if (!singleline && dch == '\n') { eolhit = (tpos < be); break; }
          chiTextMeter.twkern.fixWidthPre(dch);
          if (tpos > be) break;
        }
        bx1 = (eolhit ? x0+width : x+chiTextMeter.twkern.finalWidth);
        gxFillRect(bx0, y, bx1-bx0+1, lineHeightPixels, markBgClr);
        checkMarking = true;
      }
    }
    if (singleline && hasMarkedBlock) checkMarking = true; // let it be

    //twkern.reset(visualtabs ? tabsize : 0);
    uint cc = clr;

    int epos = pos;
    if (singleline) {
      epos = ts;
    } else {
      while (epos < ts) if (gb[epos++] == '\n') { --epos; break; }
    }

    //FIXME: tabsize
    int wdt = gxDrawTextUtfOpt(GxDrawTextOptions.Tab(tabsize), x, y, this[pos..epos], delegate (in ref state) nothrow @trusted {
      return (checkMarking ? (pos+state.spos >= bs && pos+state.epos <= be ? markFgClr : clr) : clr);
    });
    if (!singleline) {
      if (epos > pos && gb[epos-1] == ' ') {
        gxDrawChar(x+wdt, y, '\u2248', pw.getColor("wrap-mark-text"));
      }
    }

    /*
    while (pos < ts) {
      if (checkMarking) cc = (pos >= bs && pos < be ? markFgClr : clr);
      immutable dchar dch = dcharAtAdvance(pos);
      if (!singleline && dch == '\n') {
        // draw "can wrap" mark
        if (pos-2 >= ls && gb[pos-2] == ' ') {
          int xx = x+twkern.finalWidth+1;
          gxDrawChar(xx, y, '\n', gxRGB!(0, 0, 220));
        }
        break;
      }
      if (dch == '\t' && visualtabs) {
        int xx = x+twkern.fixWidthPre('\t');
        gxHLine(xx, y+lineHeightPixels/2, twkern.tablength, gxRGB!(200, 0, 0));
      } else {
        gxDrawChar(x+twkern.fixWidthPre(dch), y, dch, cc);
      }
    }
    */
  }

  // just clear line
  // use `winXXX` vars to know window dimensions
  public override void drawEmptyLine (int yofs) {
    // nothing to do here
  }

  protected enum Ecc { None, Eaten, Combo }

  // None: not valid
  // Eaten: exact hit
  // Combo: combo start
  // comboBuf should contain comboCount keys!
  protected final Ecc checkKeys (const(char)[] keys) {
    foreach (immutable cidx; 0..comboCount+1) {
      keys = keys.xstrip;
      if (keys.length == 0) return Ecc.Combo;
      usize kepos = 0;
      while (kepos < keys.length && keys.ptr[kepos] > ' ') ++kepos;
      if (comboBuf[cidx] != keys[0..kepos]) return Ecc.None;
      keys = keys[kepos..$];
    }
    return (keys.xstrip.length ? Ecc.Combo : Ecc.Eaten);
  }

  // fuck! `(this ME)` trick doesn't work here
  protected final Ecc doEditorCommandByUDA(ME=typeof(this)) (KeyEvent key) {
    import std.traits;
    bool possibleCombo = false;
    // temporarily add current key to combo
    comboBuf[comboCount] = key;
    // check all known combos
    foreach (string memn; __traits(allMembers, ME)) {
      static if (is(typeof(&__traits(getMember, ME, memn)))) {
        import std.meta : AliasSeq;
        alias mx = AliasSeq!(__traits(getMember, ME, memn))[0];
        static if (isCallable!mx && hasUDA!(mx, TEDKey)) {
          // check modifiers
          bool goodMode = true;
          static if (hasUDA!(mx, TEDSingleOnly)) { if (!singleline) goodMode = false; }
          static if (hasUDA!(mx, TEDMultiOnly)) { if (singleline) goodMode = false; }
          static if (hasUDA!(mx, TEDEditOnly)) { if (readonly) goodMode = false; }
          static if (hasUDA!(mx, TEDROOnly)) { if (!readonly) goodMode = false; }
          if (goodMode) {
            foreach (const TEDKey attr; getUDAs!(mx, TEDKey)) {
              auto cc = checkKeys(attr.key);
              if (cc == Ecc.Eaten) {
                // hit
                static if (is(ReturnType!mx == void)) {
                  comboCount = 0; // reset combo
                  mx();
                  return Ecc.Eaten;
                } else {
                  if (mx()) {
                    comboCount = 0; // reset combo
                    return Ecc.Eaten;
                  }
                }
              } else if (cc == Ecc.Combo) {
                possibleCombo = true;
              }
            }
          }
        }
      }
    }
    // check if we can start/continue combo
    if (possibleCombo) {
      if (++comboCount < comboBuf.length-1) return Ecc.Combo;
    }
    // if we have combo prefix, eat key unconditionally
    if (comboCount > 0) {
      comboCount = 0; // reset combo, too long, or invalid, or none
      return Ecc.Eaten;
    }
    return Ecc.None;
  }

  bool processKey (KeyEvent key) {
    final switch (doEditorCommandByUDA(key)) {
      case Ecc.None: break;
      case Ecc.Combo:
      case Ecc.Eaten:
        return true;
    }

    return false;
  }

  bool processChar (dchar ch) {
    if (ch < ' ' || ch == 127) return false;
    if (ch == ' ') {
      // check if we should reformat
      auto llen = linelen(cury);
      if (llen >= 76) {
        if (curx == 0 || gb[curpos-1] != ' ') doPutChar(' ');
        auto ocx = curx;
        auto ocy = cury;
        // normalize ocx
        {
          int rpos = lc.linestart(ocy);
          int qcnt = 0;
          if (gb[rpos] == '>') {
            while (gb[rpos] == '>') { ++qcnt; ++rpos; }
            if (gb[rpos] == ' ') { ++qcnt; ++rpos; }
          }
          //conwriteln("origcx=", ocx, "; ncx=", ocx-qcnt, "; qcnt=", qcnt);
          if (ocx < qcnt) ocx = qcnt; else ocx -= qcnt;
        }
        reformatFromLine!true(cury);
        // position cursor
        while (ocy < lc.linecount) {
          int rpos = lc.linestart(ocy);
          llen = linelen(ocy);
          //conwriteln("  00: ocy=", ocy, "; llen=", llen, "; ocx=", ocx);
          int qcnt = 0;
          if (gb[rpos] == '>') {
            while (gb[rpos] == '>') { --llen; ++qcnt; ++rpos; }
            if (gb[rpos] == ' ') { --llen; ++qcnt; ++rpos; }
          }
          //conwriteln("  01: ocy=", ocy, "; llen=", llen, "; ocx=", ocx, "; qcnt=", qcnt);
          if (ocx <= llen) { ocx += qcnt; break; }
          ocx -= llen;
          ++ocy;
        }
        gotoXY(ocx, ocy);
        return true;
      }
    }
    doPutDChar(ch);
    return true;
  }

  bool processClick (int x, int y, MouseEvent event) {
    if (x < 0 || y < 0 || x >= winw || y >= winh) return false;
    if (event.type == MouseEventType.buttonPressed && event.button == MouseButton.left) {
      int tx, ty;
      widget2text(x, y, tx, ty);
      gotoXY(tx, ty);
      return true;
    }
    return false;
  }

final:
  void processWordWith (scope char delegate (char ch) dg) {
    if (dg is null) return;
    bool undoAdded = false;
    scope(exit) if (undoAdded) undoGroupEnd();
    auto pos = curpos;
    if (!isWordChar(gb[pos])) return;
    // find word start
    while (pos > 0 && isWordChar(gb[pos-1])) --pos;
    while (pos < gb.textsize) {
      auto ch = gb[pos];
      if (!isWordChar(gb[pos])) break;
      auto nc = dg(ch);
      if (ch != nc) {
        if (!undoAdded) { undoAdded = true; undoGroupStart(); }
        replaceText!"none"(pos, 1, (&nc)[0..1]);
      }
      ++pos;
    }
    gotoPos(pos);
  }

final:
  @TEDMultiOnly mixin TEDImpl!("Up", q{ doUp(); });
  @TEDMultiOnly mixin TEDImpl!("S-Up", q{ doUp(true); });
  @TEDMultiOnly mixin TEDImpl!("C-Up", q{ doScrollUp(); });
  @TEDMultiOnly mixin TEDImpl!("S-C-Up", q{ doScrollUp(true); });

  @TEDMultiOnly mixin TEDImpl!("Down", q{ doDown(); });
  @TEDMultiOnly mixin TEDImpl!("S-Down", q{ doDown(true); });
  @TEDMultiOnly mixin TEDImpl!("C-Down", q{ doScrollDown(); });
  @TEDMultiOnly mixin TEDImpl!("S-C-Down", q{ doScrollDown(true); });

  mixin TEDImpl!("Left", q{ doLeft(); });
  mixin TEDImpl!("S-Left", q{ doLeft(true); });
  mixin TEDImpl!("C-Left", q{ doWordLeft(); });
  mixin TEDImpl!("S-C-Left", q{ doWordLeft(true); });

  mixin TEDImpl!("Right", q{ doRight(); });
  mixin TEDImpl!("S-Right", q{ doRight(true); });
  mixin TEDImpl!("C-Right", q{ doWordRight(); });
  mixin TEDImpl!("S-C-Right", q{ doWordRight(true); });

  @TEDMultiOnly mixin TEDImpl!("PageUp", q{ doPageUp(); });
  @TEDMultiOnly mixin TEDImpl!("S-PageUp", q{ doPageUp(true); });
  @TEDMultiOnly mixin TEDImpl!("C-PageUp", q{ doTextTop(); });
  @TEDMultiOnly mixin TEDImpl!("S-C-PageUp", q{ doTextTop(true); });

  @TEDMultiOnly mixin TEDImpl!("PageDown", q{ doPageDown(); });
  @TEDMultiOnly mixin TEDImpl!("S-PageDown", q{ doPageDown(true); });
  @TEDMultiOnly mixin TEDImpl!("C-PageDown", q{ doTextBottom(); });
  @TEDMultiOnly mixin TEDImpl!("S-C-PageDown", q{ doTextBottom(true); });

                mixin TEDImpl!("Home", q{ doHome(); });
                mixin TEDImpl!("S-Home", q{ doHome(true, true); });
  @TEDMultiOnly mixin TEDImpl!("C-Home", q{ doPageTop(); });
  @TEDMultiOnly mixin TEDImpl!("S-C-Home", q{ doPageTop(true); });

                mixin TEDImpl!("End", q{ doEnd(); });
                mixin TEDImpl!("S-End", q{ doEnd(true); });
  @TEDMultiOnly mixin TEDImpl!("C-End", q{ doPageBottom(); });
  @TEDMultiOnly mixin TEDImpl!("S-C-End", q{ doPageBottom(true); });

  @TEDEditOnly mixin TEDImpl!("Backspace", q{ doBackspace(); });
  @TEDSingleOnly @TEDEditOnly mixin TEDImpl!("M-Backspace", "delete previous word", q{ doDeleteWord(); });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("M-Backspace", "delete previous word or unindent", q{ doBackByIndent(); });

  mixin TEDImpl!("Delete", q{
    doDelete();
  });

  //mixin TEDImpl!("^Insert", "copy block to clipboard file, reset block mark", q{ if (tempBlockFileName.length == 0) return; doBlockWrite(tempBlockFileName); doBlockResetMark(); });

  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("Enter", q{
    auto ly = cury;
    if (lineQuoteLevel(ly)) {
      doLineSplit(false); // no autoindent
      auto ls = lc.linestart(ly);
      undoGroupStart();
      scope(exit) undoGroupEnd();
      while (gb[ls] == '>') {
        ++ls;
        insertText!"end"(curpos, ">");
      }
      if (gb[curpos] > ' ') insertText!"end"(curpos, " ");
    } else {
      doLineSplit();
    }
  });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("M-Enter", "split line without autoindenting", q{ doLineSplit(false); });


               mixin TEDImpl!("F3", "start/stop/reset block marking", q{ doToggleBlockMarkMode(); });
               mixin TEDImpl!("C-F3", "reset block mark", q{ doBlockResetMark(); });
  @TEDEditOnly mixin TEDImpl!("F5", "copy block", q{ doBlockCopy(); });
  //             mixin TEDImpl!("^F5", "copy block to clipboard file", q{ if (tempBlockFileName.length == 0) return; doBlockWrite(tempBlockFileName); });
  //@TEDEditOnly mixin TEDImpl!("S-F5", "insert block from clipboard file", q{ if (tempBlockFileName.length == 0) return; waitingInF5 = true; });
  @TEDEditOnly mixin TEDImpl!("F6", "move block", q{ doBlockMove(); });
  @TEDEditOnly mixin TEDImpl!("F8", "delete block", q{ doBlockDelete(); });

  mixin TEDImpl!("C-A", "move to line start", q{ doHome(); });
  mixin TEDImpl!("C-E", "move to line end", q{ doEnd(); });

  @TEDMultiOnly mixin TEDImpl!("M-I", "jump to previous bookmark", q{ doBookmarkJumpUp(); });
  @TEDMultiOnly mixin TEDImpl!("M-J", "jump to next bookmark", q{ doBookmarkJumpDown(); });
  @TEDMultiOnly mixin TEDImpl!("M-K", "toggle bookmark", q{ doBookmarkToggle(); });

  @TEDEditOnly mixin TEDImpl!("M-C", "capitalize word", q{
    bool first = true;
    processWordWith((char ch) {
      if (first) { first = false; ch = ch.toupper; }
      return ch;
    });
  });
  @TEDEditOnly mixin TEDImpl!("M-Q", "lowercase word", q{ processWordWith((char ch) => ch.tolower); });
  @TEDEditOnly mixin TEDImpl!("M-U", "uppercase word", q{ processWordWith((char ch) => ch.toupper); });

  @TEDMultiOnly mixin TEDImpl!("M-S-L", "force center current line", q{ makeCurLineVisibleCentered(true); });
  @TEDEditOnly mixin TEDImpl!("C-U", "undo", q{ doUndo(); });
  @TEDEditOnly mixin TEDImpl!("M-S-U", "redo", q{ doRedo(); });
  @TEDEditOnly mixin TEDImpl!("C-W", "remove previous word", q{ doDeleteWord(); });
  @TEDEditOnly mixin TEDImpl!("C-Y", "remove current line", q{ doKillLine(); });

  //@TEDMultiOnly @TEDEditOnly mixin TEDImpl!("Tab", q{ doPutText("  "); });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-Tab", "indent block", q{ doIndentBlock(); });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-S-Tab", "unindent block", q{ doUnindentBlock(); });

  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K C-I", "indent block", q{ doIndentBlock(); });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K C-U", "unindent block", q{ doUnindentBlock(); });
                @TEDEditOnly mixin TEDImpl!("C-K C-E", "clear from cursor to EOL", q{ doKillToEOL(); });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K Tab", "indent block", q{ doIndentBlock(); });
  //              @TEDEditOnly mixin TEDImpl!("^K M-Tab", "untabify", q{ doUntabify(gb.tabsize ? gb.tabsize : 2); }); // alt+tab: untabify
  //              @TEDEditOnly mixin TEDImpl!("^K C-space", "remove trailing spaces", q{ doRemoveTailingSpaces(); });
  //                           mixin TEDImpl!("C-K C-T", /*"toggle \"visual tabs\" mode",*/ q{ visualtabs = !visualtabs; });

  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K C-B", q{ doSetBlockStart(); });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K C-K", q{ doSetBlockEnd(); });

  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K C-C", q{ doBlockCopy(); });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K C-M", q{ doBlockMove(); });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K C-Y", q{ doBlockDelete(); });
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K C-H", q{ doBlockResetMark(); });
  // fuckin' vt100!
  @TEDMultiOnly @TEDEditOnly mixin TEDImpl!("C-K Backspace", q{ doBlockResetMark(); });

  @TEDEditOnly mixin TEDImpl!("C-Q Tab", q{ doPutChar('\t'); });
               //mixin TEDImpl!("C-Q C-U", "toggle utfuck mode", q{ utfuck = !utfuck; }); // ^Q^U: switch utfuck mode
               //mixin TEDImpl!("C-Q 1", "switch to koi8", q{ utfuck = false; codepage = CodePage.koi8u; fullDirty(); });
               //mixin TEDImpl!("C-Q 2", "switch to cp1251", q{ utfuck = false; codepage = CodePage.cp1251; fullDirty(); });
               //mixin TEDImpl!("C-Q 3", "switch to cp866", q{ utfuck = false; codepage = CodePage.cp866; fullDirty(); });
               mixin TEDImpl!("C-Q C-B", "go to block start", q{ if (hasMarkedBlock) gotoPos!true(bstart); lastBGEnd = false; });

  @TEDSingleOnly @TEDEditOnly mixin TEDImpl!("C-Q Enter", "intert LF", q{ doPutChar('\n'); });
  @TEDSingleOnly @TEDEditOnly mixin TEDImpl!("C-Q M-Enter", "intert CR", q{ doPutChar('\r'); });

  mixin TEDImpl!("C-Q C-K", "go to block end", q{ if (hasMarkedBlock) gotoPos!true(bend); lastBGEnd = true; });

  @TEDMultiOnly @TEDROOnly mixin TEDImpl!("Space", q{ doPageDown(); });
  @TEDMultiOnly @TEDROOnly mixin TEDImpl!("S-Space", q{ doPageUp(); });

final:
  dynstring[] extractAttaches () {
    dynstring[] res;
    int lidx = 0;
    lineloop: while (lidx < lc.linecount) {
      // find "@@"
      auto pos = lc.linestart(lidx);
      //conwriteln("checking line ", lidx, ": '", gb[pos], "'");
      while (pos < gb.textsize) {
        char ch = gb[pos];
        if (ch == '@' && gb[pos+1] == '@') { pos += 2; break; } // found
        if (ch > ' ' || ch == '\n') { ++lidx; continue lineloop; } // next line
        ++pos;
      }
      //conwriteln("found \"@@\" at line ", lidx);
      // skip spaces after "@@"
      while (pos < gb.textsize) {
        char ch = gb[pos];
        if (ch == '\n') { ++lidx; continue lineloop; } // next line
        if (ch > ' ') break;
        ++pos;
      }
      if (pos >= gb.textsize) break; // no more text
      // extract file name
      dynstring fname;
      while (pos < gb.textsize) {
        char ch = gb[pos];
        if (ch == '\n') break;
        fname ~= ch;
        ++pos;
      }
      if (fname.length) {
        //conwriteln("got fname: '", fname, "'");
        res ~= fname;
      }
      // remove this line
      int ls = lc.linestart(lidx);
      int le = lc.linestart(lidx+1);
      if (ls < le) deleteText!"start"(ls, le-ls);
    }
    return res;
  }

  char[] getAttachName (char[] dest, int lidx) {
    if (lidx < 0 || lidx >= lc.linecount) return null;
    // find "@@"
    auto pos = lc.linestart(lidx);
    while (pos < gb.textsize) {
      char ch = gb[pos];
      if (ch == '@' && gb[pos+1] == '@') { pos += 2; break; } // found
      if (ch > ' ' || ch == '\n') return null; // not found
      ++pos;
    }
    // skip spaces after "@@"
    while (pos < gb.textsize) {
      char ch = gb[pos];
      if (ch == '\n') return null; // not found
      if (ch > ' ') break;
      ++pos;
    }
    if (pos >= gb.textsize) return null; // not found
    // extract file name
    usize dpos = 0;
    while (pos < gb.textsize) {
      char ch = gb[pos];
      if (ch == '\n') break;
      if (dpos >= dest.length) return null;
      dest.ptr[dpos++] = ch;
      ++pos;
    }
    return dest.ptr[0..dpos];
  }

  // ah, who cares about speed?
  void reformatFromLine(bool doUndoGroup) (int lidx) {
    if (lidx < 0) lidx = 0;
    if (lidx >= lc.linecount) return;

    static if (doUndoGroup) {
      bool undoGroupStarted = false;
      scope(exit) if (undoGroupStarted) undoGroupEnd();

      void startUndoGroup () {
        if (!undoGroupStarted) {
          undoGroupStarted = true;
          undoGroupStart();
        }
      }
    } else {
      void startUndoGroup () {}
    }

    void normalizeQuoting (int lidx) {
      while (lidx < lc.linecount) {
        auto pos = lc.linestart(lidx);
        if (gb[pos] == '>') {
          bool lastWasSpace = false;
          auto stpos = pos;
          auto afterlastq = pos;
          while (pos < gb.textsize) {
            char ch = gb[pos];
            if (ch == '\n') break;
            if (ch == '>') { afterlastq = ++pos; continue; }
            if (ch == ' ') { ++pos; continue; }
            break;
          }
          pos = stpos;
          while (pos < afterlastq) {
            char ch = gb[pos];
            assert(ch != '\n');
            if (ch == ' ') { deleteText(pos, 1); --afterlastq; continue; } // remove space (thus normalizing quotes)
            assert(ch == '>');
            ++pos;
          }
          assert(pos == afterlastq);
          if (pos < gb.textsize && gb[pos] > ' ') { startUndoGroup(); insertText!("none", false)(pos, " "); }
        }
        ++lidx;
      }
    }

    // try to join two lines, if it is possible; return `true` if succeed
    bool tryJoinLines (int lidx) {
      assert(lidx >= 0);
      if (lc.linecount == 1) return false; // nothing to do
      if (lidx+1 >= lc.linecount) return false; // nothing to do
      auto ql0 = lineQuoteLevel(lidx);
      auto ql1 = lineQuoteLevel(lidx+1);
      if (ql0 != ql1) return false; // different quote levels, can't join
      auto ls = lc.linestart(lidx);
      auto le = lc.lineend(lidx);
      if (le-ls < 1) return false; // wtf?!
      if (gb[le-1] != ' ') return false; // no trailing space -- can't join
      // don't join if next line is empty one: this is prolly paragraph delimiter
      if (le+1 >= gb.textsize || gb[le+1] == '\n') return false;
      if (gb[le+1] == '>') {
        int pp = le+1;
        while (pp < gb.textsize) {
          char ch = gb[pp];
          if (ch != '>' && ch != ' ') break;
          ++pp;
        }
        if (gb[pp-1] != ' ') return false;
      }
      // remove newline
      startUndoGroup();
      deleteText(le, 1);
      // remove excessive spaces, if any
      while (le > ls && gb[le-1] == ' ') --le;
      assert(gb[le] == ' ');
      ++le; // but leave one space
      while (le < gb.textsize) {
        if (gb[le] == '\n' || gb[le] > ' ') break;
        deleteText(le, 1);
      }
      // remove quoting
      if (ql0) {
        assert(le < gb.textsize && gb[le] == '>');
        while (le < gb.textsize) {
          char ch = gb[le];
          if (ch == '\n' || (ch != '>' && ch != ' ')) break;
          deleteText(le, 1);
        }
      }
      return true; // yay
    }

    // join lines; we'll split 'em later
    for (int l = lidx; l < lc.linecount; ) if (!tryJoinLines(l)) ++l;

    // make quoting consistent
    normalizeQuoting(lidx);

    void conwrlinerest (int pos) {
      if (pos < 0 || pos >= gb.textsize) return;
      while (pos < gb.textsize) {
        char ch = gb[pos++];
        if (ch == '\n') break;
        conwrite(ch);
      }
    }

    void conwrline (int lidx) {
      if (lidx < 0 || lidx >= lc.linecount) return;
      conwrlinerest(lc.linestart(lidx));
    }

    // now split the lines; all lines are joined, so we can only split
    lineloop: while (lidx < lc.linecount) {
      auto ls = lc.linestart(lidx);
      auto le = lc.lineend(lidx);
      // calculate line length without trailing spaces
      auto llen = linelen(lidx);
      {
        auto pe = le;
        while (pe > ls && gb[pe-1] <= ' ') { --pe; --llen; }
      }
      if (llen <= 76) { ++lidx; continue; } // nothing to do here
      // need to wrap it
      auto pos = ls;
      int curlen = 0;
      // skip quotes, if any
      while (gb[pos] == '>') { ++curlen; ++pos; }
      // skip leading spaces
      while (gb[pos] != '\n' && gb[pos] <= ' ') { ++curlen; ++pos; }
      if (pos >= gb.textsize || gb[pos] == '\n') { ++lidx; continue; } // wtf?!
      int lwstart = -1;
      bool inword = true;
      while (pos < gb.textsize) {
        immutable stpos = pos;
        dchar ch = dcharAtAdvance(pos);
        ++curlen;
        if (inword) {
          if (ch <= ' ') {
            if (lwstart >= 0 && curlen > 76) {
              // wrap
              startUndoGroup();
              insertText!("none", false)(lwstart, "\n");
              ++lwstart;
              if (gb[ls] == '>') {
                while (gb[ls] == '>') {
                  insertText!("none", false)(lwstart, ">");
                  ++lwstart;
                  ++ls;
                }
                insertText!("none", false)(lwstart, " ");
              }
              ++lidx;
              continue lineloop;
            }
            if (ch == '\n') break;
            // not in word anymore
            inword = false;
          }
        } else {
          if (ch == '\n') break;
          if (ch > ' ') { lwstart = stpos; inword = true; }
        }
      }
      ++lidx;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct QuoteInfo {
  int level; // quote level
  int length; // quote prefix length, in chars
}

QuoteInfo calcQuote(T:const(char)[]) (T s) {
  static if (is(T == typeof(null))) {
    return QuoteInfo();
  } else {
    QuoteInfo qi;
    if (s.length > 0 && s[0] == '>') {
      while (qi.length < s.length) {
        if (s[qi.length] != ' ') {
          if (s[qi.length] != '>') break;
          ++qi.level;
        }
        ++qi.length;
      }
      if (s.length-qi.length > 1 && s[qi.length] == ' ') ++qi.length;
    }
    return qi;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class EditorWidget : Widget {
  TextEditor editor;
  bool moveToBottom = true;

  this (Widget aparent) {
    tabStop = true;
    super(aparent);
    editor = new TextEditor(this, 0, 0, 10, 10);
  }

  this () {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent);
  }

  void addText (const(char)[] s) {
    immutable GxRect grect = globalRect;
    editor.moveResize(grect.x0, grect.y0, (grect.width < 1 ? 1 : grect.width), (grect.height < 1 ? grect.height : 1));
    editor.doPasteStart();
    scope(exit) editor.doPasteEnd();
    editor.doPutTextUtf(s);
    //editor.doPutChar('\n');
  }

  dynstring getText () {
    dynstring res;
    if (editor.textsize == 0) return res;
    res.reserve(editor.textsize);
    foreach (char ch; editor[]) res ~= ch;
    return res;
  }

  void reformat () {
    editor.clearAndDisableUndo();
    scope(exit) editor.reinstantiateUndo();
    editor.reformatFromLine!false(0);
    editor.gotoXY(0, 0); // HACK
    editor.textChanged = false;
  }

  dynstring[] extractAttaches () {
    return editor.extractAttaches();
  }

  private final void drawScrollBar () {
    //restoreClip(); // the easiest way again
    immutable GxRect grect = globalRect;
    gxDrawScrollBar(GxRect(grect.x0, grect.y0, 4, height), editor.linecount-1, editor.topline+editor.visibleLinesPerWindow-1);
  }

  protected override void doPaint (GxRect grect) {
    if (width < 1 || height < 1) return;
    editor.moveResize(grect.x0+5, grect.y0, grect.width-5*2, grect.height-gxTextHeightUtf);

    if (moveToBottom) { moveToBottom = false; editor.gotoXY(0, editor.linecount); } // HACK!
    gxFillRect(grect, getColor("back"));
    gxWithSavedClip {
      editor.drawPage();
    };

    drawScrollBar();
  }

  // return `true` if event was eaten
  override bool onKey (KeyEvent event) {
    if (!isFocused) return super.onKey(event);
    if (event.pressed) {
      if (editor.processKey(event)) {
        widgetChanged();
        return true;
      }
      if (event == "S-Insert") {
        getClipboardText(vbwin, delegate (in char[] text) {
          if (text.length) {
            editor.doPasteStart();
            scope(exit) editor.doPasteEnd();
            editor.doPutTextUtf(text[]);
            widgetChanged();
          }
        });
        return true;
      }
      if (event == "C-Insert") {
        auto mtr = editor.markedBlockRange();
        if (mtr.length) {
          char[] brng;
          brng.reserve(mtr.length);
          foreach (char ch; mtr) brng ~= ch;
          if (brng.length > 0) {
            setClipboardText(vbwin, cast(string)brng); // it is safe to cast here
            setPrimarySelection(vbwin, cast(string)brng); // it is safe to cast here
          }
          editor.doBlockResetMark();
          widgetChanged();
        }
        return true;
      }

      if (event == "M-Tab") {
        char[1024] abuf = void;
        auto attname = editor.getAttachName(abuf[], editor.cury);
        if (attname.length && editor.curx >= editor.linelen(editor.cury)) {
          auto cplist = buildAutoCompletion(attname);
          auto atnlen = attname.length;
          auto adg = delegate (dynstring s) {
            //conwriteln("attname=[", attname, "]; s=[", s, "]");
            if (s.length <= atnlen /*|| s[0..attname.length] != attname*/) return;
            editor.undoGroupStart();
            scope(exit) editor.undoGroupEnd();
            editor.doPutTextUtf(s[atnlen..$]);
            widgetChanged();
          };
          if (cplist.length == 1) {
            adg(cplist[0]);
          } else {
            auto acw = new SelectCompletionWindow(attname, cplist, true);
            acw.onSelected = adg;
          }
        }
        return true;
      }
    }
    return super.onKey(event);
  }

  override bool onMouse (MouseEvent event) {
    if (!isFocused) return super.onMouse(event);
    if (GxPoint(event.x, event.y).inside(rect.size)) {
      if (editor.processClick(event.x, event.y, event)) {
        widgetChanged();
        return true;
      }
    }
    return super.onMouse(event);
  }

  override bool onChar (dchar ch) {
    if (isFocused && editor.processChar(ch)) {
      widgetChanged();
      return true;
    }
    return super.onChar(ch);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class LineEditWidget : Widget {
  TextEditor editor;

  this (Widget aparent) {
    tabStop = true;
    super(aparent);
    if (width == 0) width = 96;
    height = gxTextHeightUtf+2;
    editor = new TextEditor(this, 0, 0, width, height, true); // singleline
  }

  this () {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent);
  }

  @property bool readonly () const nothrow { return editor.readonly; }
  @property void readonly (bool v) nothrow { editor.readonly = v; }

  @property bool killTextOnChar () const nothrow { return editor.killTextOnChar; }
  @property void killTextOnChar (bool v) nothrow { editor.killTextOnChar = v; }

  @property dynstring str () {
    dynstring res;
    if (editor.textsize == 0) return res;
    res.reserve(editor.textsize);
    foreach (char ch; editor[]) res ~= ch;
    return res;
  }

  @property void str (const(char)[] s) {
    editor.clearAndDisableUndo();
    scope(exit) editor.reinstantiateUndo();
    editor.clear();
    editor.doPutTextUtf(s);
    editor.textChanged = false;
  }

  protected override void doPaint (GxRect grect) {
    if (width < 1 || height < 1) return;
    gxFillRect(grect, getColor("back"));
    grect.shrinkBy(0, 1);
    if (grect.width >= 12) grect.shrinkBy(2, 0);
    if (!grect.empty) {
      editor.moveResize(grect.x0, grect.y0, grect.width, gxTextHeightUtf);
      editor.drawPage();
    }
  }

  // return `true` if event was eaten
  override bool onKey (KeyEvent event) {
    if (!isFocused) return super.onKey(event);
    if (event.pressed) {
      if (editor.processKey(event)) {
        widgetChanged();
        return true;
      }
      if (event == "S-Insert") {
        getClipboardText(vbwin, delegate (in char[] text) {
          if (text.length) {
            editor.doPasteStart();
            scope(exit) editor.doPasteEnd();
            editor.doPutTextUtf(text[]);
            widgetChanged();
          }
        });
        return true;
      }
      if (event == "C-Insert") {
        auto mtr = editor.markedBlockRange();
        if (mtr.length) {
          char[] brng;
          brng.reserve(mtr.length);
          foreach (char ch; mtr) brng ~= ch;
          if (brng.length > 0) {
            setClipboardText(vbwin, cast(string)brng); // it is safe to cast here
            setPrimarySelection(vbwin, cast(string)brng); // it is safe to cast here
          }
          editor.doBlockResetMark();
          widgetChanged();
        }
        return true;
      }
    }
    return super.onKey(event);
  }

  override bool onMouse (MouseEvent event) {
    if (!isFocused) return super.onMouse(event);
    if (GxPoint(event.x, event.y).inside(rect.size)) {
      if (editor.processClick(event.x, event.y, event)) {
        widgetChanged();
        return true;
      }
    }
    return super.onMouse(event);
  }

  override bool onChar (dchar ch) {
    if (isFocused && editor.processChar(ch)) {
      widgetChanged();
      return true;
    }
    return super.onChar(ch);
  }
}
