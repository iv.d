/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.widgets.spacers /*is aliced*/;
private:

import arsd.simpledisplay;

import iv.egra.gfx;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.utfutil;

import iv.egra.gui.subwindows;
import iv.egra.gui.widgets.base;


// ////////////////////////////////////////////////////////////////////////// //
// this widget simply occupies some fixed space
public class SpacerWidget : Widget {
  this (Widget aparent, in int asize) {
    assert(aparent !is null);
    super(aparent);
    tabStop = false;
    nonVisual = true;
    if (aparent.childDir == GxDir.Horiz) rect.size.w = asize; else rect.size.h = asize;
  }

  this (in int asize) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, asize);
  }
}

// this is "flexible spring"
public class SpringWidget : SpacerWidget {
  this (Widget aparent, in int aflex) {
    super(aparent, 0);
    flex = aflex;
  }

  this (in int aflex) {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent, aflex);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// base for horizontal and vertical boxes
public class BoxWidget : Widget {
  this (Widget aparent) {
    super(aparent);
    tabStop = false;
  }

  this () {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent);
  }
}

// horizontal box
public class HBoxWidget : BoxWidget {
  this (Widget aparent) {
    super(aparent);
    childDir = GxDir.Horiz;
  }

  this () {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent);
  }
}

// vertical box
public class VBoxWidget : BoxWidget {
  this (Widget aparent) {
    super(aparent);
    childDir = GxDir.Vert;
  }

  this () {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent);
  }
}
