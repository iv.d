/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.widgets.listboxes /*is aliced*/;
private:

import arsd.simpledisplay;

import iv.egra.gfx;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.utfutil;

import iv.egra.gui.subwindows;
import iv.egra.gui.widgets.base;


// ////////////////////////////////////////////////////////////////////////// //
// very simple listbox widget
public class SimpleListBoxWidget : Widget {
protected:
  dynstring[] mItems;
  int mTopIdx;
  int mCurIdx;

protected:
  void itemAppended () {}

public:
  this (Widget aparent) {
    tabStop = true;
    super(aparent);
  }

  this () {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent);
  }

  final @property int curidx () const nothrow @safe @nogc { return (mCurIdx >= 0 && mCurIdx < mItems.length ? mCurIdx : 0); }

  final @property void curidx (int idx) nothrow {
    if (mItems.length == 0) return;
    if (idx < 0) idx = 0;
    if (idx >= mItems.length) idx = cast(int)(mItems.length-1);
    if (mCurIdx != idx) {
      mCurIdx = idx;
      widgetChanged();
    }
  }

  final @property int length () const nothrow @safe @nogc { return cast(int)mItems.length; }
  final @property dynstring opIndex (usize idx) const nothrow @safe @nogc { return (idx < mItems.length ? mItems[idx] : dynstring()); }

  void appendItem (const(char)[] s) {
    dynstring it = s;
    mItems ~= it;
    itemAppended();
    widgetChanged();
  }

  @property int visibleItemsCount () const nothrow @trusted {
    pragma(inline, true);
    return (height < gxTextHeightUtf*2 ? 1 : height/gxTextHeightUtf);
  }

  void makeCursorVisible (bool needupdate=true) {
    if (mItems.length == 0) return;
    immutable int omci = mCurIdx;
    scope(exit) if (needupdate && mCurIdx != omci) widgetChanged();
    if (mCurIdx < 0) mCurIdx = 0;
    if (mCurIdx >= mItems.length) mCurIdx = cast(int)(mItems.length-1);
    if (mCurIdx < mTopIdx) { mTopIdx = mCurIdx; return; }
    int icnt = visibleItemsCount-1;
    if (mTopIdx+icnt < mCurIdx) mTopIdx = mCurIdx-icnt;
  }

  protected override void doPaint (GxRect grect) {
    makeCursorVisible(needupdate:false);
    uint bclr = getColor("back");
    uint tclr = getColor("text");
    uint cbclr = getColor("cursor-back");
    uint ctclr = getColor("cursor-text");
    gxFillRect(grect, bclr);
    int y = 0;
    int idx = mTopIdx;
    while (idx < mItems.length && y < grect.height) {
      if (idx >= 0) {
        uint clr = tclr;
        if (idx == mCurIdx) {
          gxFillRect(grect.x0, grect.y0+y, grect.width, gxTextHeightUtf, cbclr);
          clr = ctclr;
        }
        gxWithSavedClip {
          gxClipRect.intersect(GxRect(grect.pos, GxPoint(grect.x1-1, grect.y1)));
          gxDrawTextUtf(grect.x0+1, grect.y0+y, mItems[idx], clr);
        };
      }
      ++idx;
      y += gxTextHeightUtf;
    }
  }

  override bool onKey (KeyEvent event) {
    if (!event.pressed || !isFocused) return super.onKey(event);
    if (event == "Up") { curidx = curidx-1; return true; }
    if (event == "Down") { curidx = curidx+1; return true; }
    if (event == "Home") { curidx = 0; return true; }
    if (event == "End") { curidx = cast(int)(mItems.length-1); return true; }
    if (event == "PageUp") {
      makeCursorVisible();
      if (curidx > mTopIdx) {
        curidx = mTopIdx;
      } else {
        curidx = curidx-(visibleItemsCount-1);
      }
      return true;
    }
    if (event == "PageDown") {
      makeCursorVisible();
      int icnt = visibleItemsCount-1;
      if (icnt) {
        if (mTopIdx+icnt < curidx) {
          curidx = mTopIdx+icnt;
        } else {
          curidx = curidx+icnt;
        }
      }
      return true;
    }
    return super.onKey(event);
  }

  override bool onMouse (MouseEvent event) {
    if (!isFocused) return super.onMouse(event);
    if (GxPoint(event.x, event.y).inside(rect.size)) {
      int mx = event.x, my = event.y;
      makeCursorVisible();
      immutable int idx = mTopIdx+my/gxTextHeightUtf;
      if (event.type == MouseEventType.buttonPressed && event.button == MouseButton.left) {
        if (curidx == idx) doAction(); else curidx = idx;
      } else if (event.type == MouseEventType.buttonPressed && event.button == MouseButton.wheelUp) {
        curidx = curidx-1;
      } else if (event.type == MouseEventType.buttonPressed && event.button == MouseButton.wheelDown) {
        curidx = curidx+1;
      }
      return true;
    }
    return super.onMouse(event);
  }
}


public class SimpleListBoxUDataWidget(DT) : SimpleListBoxWidget {
  DT[] mItemData;

protected:
  override void itemAppended () {
    mItemData ~= DT.init;
  }

public:
  this (Widget aparent) {
    super(aparent);
  }

  this () {
    assert(creatorCurrentParent !is null);
    this(creatorCurrentParent);
  }

  final @property DT itemData (usize idx) nothrow @safe @nogc {
    return (idx < mItemData.length ? mItemData[idx] : DT.init);
  }

  void appendItemWithData() (const(char)[] s, in DT dt) {
    super.appendItem(s);
    while (mItemData.length < mItems.length) mItemData ~= DT.init;
    DT cpy = dt;
    mItemData[mItems.length-1] = cpy;
  }
}
