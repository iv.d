/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.subwindows /*is aliced*/;
private:

import core.time;

import arsd.simpledisplay;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.flexlay2;
import iv.strex;
import iv.unarray;
import iv.utfutil;

import iv.egra.gfx;
public import iv.egra.gui.style;
import iv.egra.gui.widgets : Widget, RootWidget;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared SimpleWindow vbwin; // main window; MUST be set!
public __gshared bool vbfocused = false;


// ////////////////////////////////////////////////////////////////////////// //
__gshared public int lastMouseXUnscaled = 10000, lastMouseYUnscaled = 10000;
__gshared public uint lastMouseButton;

public int lastMouseX () nothrow @trusted @nogc { pragma(inline, true); return lastMouseXUnscaled/screenEffScale; }
public int lastMouseY () nothrow @trusted @nogc { pragma(inline, true); return lastMouseYUnscaled/screenEffScale; }

public bool lastMouseLeft () nothrow @trusted @nogc { pragma(inline, true); return ((lastMouseButton&MouseButton.left) != 0); }
public bool lastMouseRight () nothrow @trusted @nogc { pragma(inline, true); return ((lastMouseButton&MouseButton.right) != 0); }
public bool lastMouseMiddle () nothrow @trusted @nogc { pragma(inline, true); return ((lastMouseButton&MouseButton.middle) != 0); }


// ////////////////////////////////////////////////////////////////////////// //
public class ScreenRebuildEvent {}
public class ScreenRepaintEvent {}
public class QuitEvent {}
public class CursorBlinkEvent {}
public class HideMouseEvent {}

__gshared ScreenRebuildEvent evScrRebuild;
__gshared ScreenRepaintEvent evScreenRepaint;
__gshared CursorBlinkEvent evCurBlink;
__gshared HideMouseEvent evHideMouse;

shared static this () {
  evScrRebuild = new ScreenRebuildEvent();
  evScreenRepaint = new ScreenRepaintEvent();
  evCurBlink = new CursorBlinkEvent();
  evHideMouse = new HideMouseEvent();
}


// ////////////////////////////////////////////////////////////////////////// //
public void postScreenRebuild () { if (vbwin !is null && !vbwin.eventQueued!ScreenRebuildEvent) vbwin.postEvent(evScrRebuild); }
public void postScreenRepaint () { if (vbwin !is null && !vbwin.eventQueued!ScreenRepaintEvent && !vbwin.eventQueued!ScreenRebuildEvent) vbwin.postEvent(evScreenRepaint); }
public void postScreenRepaintDelayed () { if (vbwin !is null && !vbwin.eventQueued!ScreenRepaintEvent && !vbwin.eventQueued!ScreenRebuildEvent) vbwin.postTimeout(evScreenRepaint, 35); }

public void postCurBlink (int timeout) {
  if (timeout < 1) return;
  if (vbwin !is null && !vbwin.eventQueued!CursorBlinkEvent) {
    //conwriteln("curblink posted!");
    if (timeout < 100) timeout = 100;
    vbwin.postTimeout(evCurBlink, timeout);
    //vbwin.postTimeout(evCurBlink, 500);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared MonoTime lastMouseMove;
__gshared uint MouseHideTime = 3000;


shared static this () {
  conRegVar("mouse_hide_time", "mouse cursor hiding time (in milliseconds); 0 to not hide it",
    delegate (self) {
      return MouseHideTime;
    },
    delegate (self, uint nv) {
      if (MouseHideTime != nv) {
        if (MouseHideTime == 0) egraMouseMoved();
        MouseHideTime = nv;
        conwriteln("mouse hiding time: ", nv);
      }
    },
  );
}


//==========================================================================
//
//  mshtime_dbg
//
//==========================================================================
public int mshtime_dbg () {
  if (MouseHideTime > 0) {
    auto ctt = MonoTime.currTime;
    auto mt = (ctt-lastMouseMove).total!"msecs";
    return cast(int)mt;
  } else {
    return 0;
  }
}


//==========================================================================
//
//  isMouseVisible
//
//==========================================================================
public bool isMouseVisible () {
  if (MouseHideTime > 0) {
    auto ctt = MonoTime.currTime;
    return ((ctt-lastMouseMove).total!"msecs" < MouseHideTime+500);
  } else {
    return true;
  }
}


//==========================================================================
//
//  mouseAlpha
//
//==========================================================================
public float mouseAlpha () {
  if (MouseHideTime > 0) {
    auto ctt = MonoTime.currTime;
    auto msc = (ctt-lastMouseMove).total!"msecs";
    if (msc >= MouseHideTime+500) return 0.0f;
    if (msc < MouseHideTime) return 1.0f;
    msc -= MouseHideTime;
    return 1.0f-msc/500.0f;
  } else {
    return 1.0f;
  }
}


//==========================================================================
//
//  repostHideMouse
//
//  returns `true` if mouse should be redrawn
//
//==========================================================================
public bool repostHideMouse () {
  if (vbwin is null || vbwin.eventQueued!HideMouseEvent) return false;
  if (MouseHideTime > 0) {
    auto ctt = MonoTime.currTime;
    auto tms = (ctt-lastMouseMove).total!"msecs";
    if (tms >= MouseHideTime) {
      if (tms >= MouseHideTime+500) return true; // hide it
      vbwin.postTimeout(evHideMouse, 50);
      return true; // fade it
    }
    vbwin.postTimeout(evHideMouse, cast(int)(MouseHideTime-tms));
  }
  return false;
}


//==========================================================================
//
//  egraMouseMoved
//
//==========================================================================
public void egraMouseMoved () {
  if (MouseHideTime > 0) {
    lastMouseMove = MonoTime.currTime;
    if (vbwin !is null && !vbwin.eventQueued!HideMouseEvent) vbwin.postTimeout(evHideMouse, MouseHideTime);
  }
  if (vArrowTextureId && isMouseVisible) postScreenRepaint();
}


// ////////////////////////////////////////////////////////////////////////// //
private __gshared SubWindow subwinLast;
private __gshared bool ignoreSubWinChar = false;
// make a package and move that to package
private __gshared SubWindow subwinDrag = null;
private __gshared int subwinDragXSpot, subwinDragYSpot;
private __gshared SubWindow lastHoverWindow = null;


public @property bool isSubWinDragging () nothrow @trusted @nogc { pragma(inline, true); return (subwinDrag !is null); }
public @property bool isSubWinDraggingKeyboard () nothrow @trusted @nogc { pragma(inline, true); return (subwinDrag !is null && subwinDragXSpot == int.min && subwinDragYSpot == int.min); }


//==========================================================================
//
//  eguiLostGlobalFocus
//
//==========================================================================
public void eguiLostGlobalFocus () {
  ignoreSubWinChar = false;
  subwinDrag = null;
  SubWindow aw = getActiveSubWindow();
  if (aw !is null) aw.releaseWidgetGrab();
  lastMouseButton = 0;
}


private bool insertSubWindow (SubWindow nw) {
  if (nw is null || nw.mClosed) return false;
  assert(nw.mPrev is null);
  assert(nw.mNext is null);
  assert(!nw.mInWinList);
  lastHoverWindow = null; // just in case
  nw.releaseWidgetGrab();
  SubWindow law = getActiveSubWindow();
  if (nw.mType == SubWindow.Type.OnBottom) {
    SubWindow w = subwinLast;
    if (w !is null) {
      while (w.mPrev !is null) w = w.mPrev;
      nw.mNext = w;
      if (w !is null) w.mPrev = nw; else subwinLast = nw;
    } else {
      subwinLast = nw;
    }
    nw.mInWinList = true;
    if (law !is null && law !is getActiveSubWindow()) law.releaseWidgetGrab();
    return true;
  }
  SubWindow aw = getActiveSubWindow();
  assert(aw !is nw);
  if (nw.mType == SubWindow.Type.OnTop || aw is null) {
    nw.mPrev = subwinLast;
    if (subwinLast !is null) subwinLast.mNext = nw;
    subwinLast = nw;
    nw.mInWinList = true;
    if (law !is null && law !is getActiveSubWindow()) law.releaseWidgetGrab();
    return true;
  }
  if (aw.mModal && !nw.mModal) return false; // can't insert normal windows while modal window is active
  // insert after aw
  nw.mPrev = aw;
  nw.mNext = aw.mNext;
  aw.mNext = nw;
  if (nw.mNext !is null) nw.mNext.mPrev = nw;
  if (aw is subwinLast) subwinLast = nw;
  nw.mInWinList = true;
  if (law !is null && law !is getActiveSubWindow()) law.releaseWidgetGrab();
  return true;
}


private bool removeSubWindow (SubWindow nw) {
  if (nw is null || !nw.mInWinList) return false;
  lastHoverWindow = null; // just in case
  nw.releaseWidgetGrab();
  SubWindow law = getActiveSubWindow();
  if (nw.mPrev !is null) nw.mPrev.mNext = nw.mNext;
  if (nw.mNext !is null) nw.mNext.mPrev = nw.mPrev;
  if (nw is subwinLast) subwinLast = nw.mPrev;
  nw.mPrev = null;
  nw.mNext = null;
  nw.mInWinList = false;
  if (law !is null && law !is getActiveSubWindow()) law.releaseWidgetGrab();
  return true;
}


//==========================================================================
//
//  mouse2xy
//
//==========================================================================
public void mouse2xy (MouseEvent event, out int mx, out int my) nothrow @trusted @nogc {
  mx = event.x/screenEffScale;
  my = event.y/screenEffScale;
}


//==========================================================================
//
//  subWindowAt
//
//==========================================================================
public SubWindow subWindowAt (in GxPoint p) nothrow {
  for (SubWindow w = subwinLast; w !is null; w = w.mPrev) {
    if (!w.closed) {
      if (w.minimised) {
        if (p.x >= w.winminx && p.y >= w.winminy && p.x < w.winminx+w.MinSizeX && p.y < w.winminy+w.MinSizeY) return w;
      } else {
        if (p.inside(w.winrect)) return w;
      }
    }
  }
  return null;
}


public SubWindow subWindowAt (int mx, int my) nothrow @trusted { return subWindowAt(GxPoint(mx, my)); }


//==========================================================================
//
//  subWindowAt
//
//==========================================================================
public SubWindow subWindowAt (MouseEvent event) nothrow @trusted {
  pragma(inline, true);
  return subWindowAt(event.x/screenEffScale, event.y/screenEffScale);
}


//==========================================================================
//
//  getActiveSubWindow
//
//==========================================================================
public SubWindow getActiveSubWindow () nothrow @trusted @nogc {
  for (SubWindow w = subwinLast; w !is null; w = w.mPrev) {
    if (!w.mClosed && w.type != SubWindow.Type.OnTop && !w.minimised) return w;
  }
  return null;
}


//==========================================================================
//
//  dispatchEvent
//
//==========================================================================
public bool dispatchEvent (KeyEvent event) {
  bool res = false;
  if (isSubWinDragging) {
    if (isSubWinDraggingKeyboard) {
      if (!event.pressed) return true;
           if (event == "Left") subwinDrag.x0 = subwinDrag.x0-1;
      else if (event == "Right") subwinDrag.x0 = subwinDrag.x0+1;
      else if (event == "Up") subwinDrag.y0 = subwinDrag.y0-1;
      else if (event == "Down") subwinDrag.y0 = subwinDrag.y0+1;
      else if (event == "C-Left") subwinDrag.x0 = subwinDrag.x0-8;
      else if (event == "C-Right") subwinDrag.x0 = subwinDrag.x0+8;
      else if (event == "C-Up") subwinDrag.y0 = subwinDrag.y0-8;
      else if (event == "C-Down") subwinDrag.y0 = subwinDrag.y0+8;
      else if (event == "Home") subwinDrag.x0 = 0;
      else if (event == "End") subwinDrag.x0 = screenWidth-subwinDrag.width;
      else if (event == "PageUp") subwinDrag.y0 = 0;
      else if (event == "PageDown") subwinDrag.y0 = screenHeight-subwinDrag.height;
      else if (event == "Escape" || event == "Enter") subwinDrag = null;
      postScreenRebuild();
      return true;
    }
  } else if (auto aw = getActiveSubWindow()) {
    res = aw.onKeyEvent(event);
  }
  return res;
}


//==========================================================================
//
//  dispatchEvent
//
//==========================================================================
public bool dispatchEvent (MouseEvent event) {
  scope(exit) {
         if (event.type == MouseEventType.buttonPressed) lastMouseButton |= cast(uint)event.button;
    else if (event.type == MouseEventType.buttonReleased) lastMouseButton &= ~cast(uint)event.button;
  }

  if (subwinLast is null) {
    if (lastHoverWindow !is null) {
      lastHoverWindow = null;
      postScreenRebuild();
    }
    return false;
  }

  int mx = lastMouseX;
  int my = lastMouseY;

  // drag
  if (isSubWinDragging) {
    assert(subwinDrag !is null);
    lastHoverWindow = subwinDrag;
    subwinDrag.releaseWidgetGrab(); // just in case
    if (!isSubWinDraggingKeyboard) {
      subwinDrag.x0 = mx+subwinDragXSpot;
      subwinDrag.y0 = my+subwinDragYSpot;
      // stop drag?
      if (event.type == MouseEventType.buttonReleased && event.button == MouseButton.left) subwinDrag = null;
      postScreenRebuild();
    }
    return true;
  }

  SubWindow aw = getActiveSubWindow();
  immutable bool curIsModal = (aw !is null && aw.mModal);
  SubWindow msw = (curIsModal || aw.hasGrab ? aw : subWindowAt(event));
  if (msw != lastHoverWindow) {
    if ((msw !is null && msw.minimised) || (lastHoverWindow !is null && lastHoverWindow.minimised)) {
      postScreenRebuild();
    }
    lastHoverWindow = msw;
  }

  // switch window by button press
  if (event.type == MouseEventType.buttonReleased && msw !is aw && !curIsModal) {
    if (msw !is null && msw.mType == SubWindow.Type.Normal) {
      if (aw !is null) aw.releaseWidgetGrab();
      msw.releaseWidgetGrab();
      msw.bringToFront();
      return true;
    }
  }

  if (msw is null || msw !is aw || msw.minimised) {
    if (msw is null || !msw.onTop) {
      return false;
    }
  }
  assert(msw !is null);

  if (msw.onMouseEvent(event)) {
    egraMouseMoved();
    return true;
  }

  egraMouseMoved();
  return false;
}


//==========================================================================
//
//  dispatchEvent
//
//==========================================================================
public bool dispatchEvent (dchar ch) {
  if (ignoreSubWinChar) { ignoreSubWinChar = false; return (subwinLast !is null); }
  bool res = false;
  if (!isSubWinDragging) {
    if (auto aw = getActiveSubWindow()) {
      res = aw.onCharEvent(ch);
    }
  }
  return res;
}


//==========================================================================
//
//  paintSubWindows
//
//==========================================================================
public void paintSubWindows () {
  // get first window
  SubWindow firstWin = subwinLast;
  if (firstWin is null) return;
  while (firstWin.mPrev !is null) firstWin = firstWin.mPrev;

  SubWindow firstMin, firstNormal, firstTop;
  SubWindow lastMin, lastNormal, lastTop;

  //gxClipReset();

  void doDraw (SubWindow w) {
    if (w !is null) {
      gxClipReset();
      w.onPaint();
      if (w is subwinDrag) w.drawDragRect();
    }
  }

  // paint background windows
  for (SubWindow w = firstWin; w !is null; w = w.mNext) {
    if (w.mClosed) continue;
    if (w.minimised) {
      if (firstMin is null) firstMin = w;
      lastMin = w;
    } else if (w.mType == SubWindow.Type.Normal) {
      if (firstNormal is null) firstNormal = w;
      lastNormal = w;
    } else if (w.mType == SubWindow.Type.OnTop) {
      if (firstTop is null) firstTop = w;
      lastTop = w;
    } else if (w.mType == SubWindow.Type.OnBottom) {
      doDraw(w);
    }
  }

  // paint minimised windows
  for (SubWindow w = firstMin; w !is null; w = w.mNext) {
    if (!w.mClosed && w.minimised) doDraw(w);
    if (w is lastMin) break;
  }

  // paint normal windows
  for (SubWindow w = firstNormal; w !is null; w = w.mNext) {
    if (!w.mClosed && !w.minimised && w.mType == SubWindow.Type.Normal) doDraw(w);
    if (w is lastNormal) break;
  }

  // paint ontop windows
  for (SubWindow w = firstTop; w !is null; w = w.mNext) {
    if (!w.mClosed && !w.minimised && w.mType == SubWindow.Type.OnTop) doDraw(w);
    if (w is lastTop) break;
  }

  // paint hint for minimised window
  if (auto msw = /*subWindowAt(lastMouseX, lastMouseY)*/lastHoverWindow) {
    if (!msw.mClosed && msw.minimised && msw.mWinTitle.length) {
      auto wdt = gxTextWidthUtf(msw.mWinTitle)+2;
      auto hgt = gxTextHeightUtf+2;
      int y = msw.winminy-hgt;
      int x;
      if (wdt >= screenWidth) {
        x = (screenWidth-wdt)/2;
      } else {
        x = (msw.winminx+msw.MinSizeX)/2-wdt/2;
        if (x < 0) x = 0;
      }
      gxClipReset();
      gxFillRect(x, y, wdt, hgt, gxRGB!(255, 255, 255));
      gxDrawTextUtf(x+1, y+1, msw.mWinTitle, gxRGB!(0, 0, 0));
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class SubWindow : EgraStyledClass {
protected:
  enum Type {
    Normal,
    OnTop,
    OnBottom,
  }

protected:
  SubWindow mPrev, mNext;
  Type mType = Type.Normal;
  bool mMinimised;
  bool mInWinList;
  bool mModal;
  bool mClosed;
  bool mAllowMinimise = true;
  bool mAllowDragMove = true;
  bool mNoTitle = false;
  RootWidget mRoot;

protected:
  int winminx, winminy;
  GxRect winrect;
  dynstring mWinTitle;

public:
  GxSize minWinSize;
  GxSize maxWinSize;

public:
  override bool isMyModifier (const(char)[] str) nothrow @trusted @nogc {
    if (str.length == 0) return !active;
    if (mMinimised) return false;
    if (strEquCI(str, "focused")) return active;
    return false;
  }

  override bool isMyStyleClass (const(char)[] str) nothrow @trusted @nogc {
    if (mMinimised && str.strEquCI("minimised")) return true;
    return super.isMyStyleClass(str);
  }

  override string getCurrentMod () nothrow @trusted @nogc {
    return (mMinimised ? "" : active ? "focused" : "");
  }

  override EgraStyledClass getParent () nothrow @trusted @nogc { return null; }
  override EgraStyledClass getFirstChild () nothrow @trusted @nogc { return mRoot; }

  override bool isMyClassName (const(char)[] str) nothrow @trusted @nogc {
    if (str.length == 0) return true;
    // sorry for this cast
    for (TypeInfo_Class ti = cast(TypeInfo_Class)typeid(this); ti !is null; ti = ti.base) {
      if (str.strEquCI(classShortName(ti))) return true;
    }
    return false;
  }

  final T querySelector(T:EgraStyledClass=Widget) (const(char)[] sel) { pragma(inline, true); return querySelectorInternal!T(sel); }
  final auto querySelectorAll(T:EgraStyledClass=Widget) (const(char)[] sel) nothrow @safe @nogc { pragma(inline, true); return Iter!T(this, sel); }

protected:
  void createRoot () {
    if (mRoot is null) setRoot(new RootWidget(this)); else fixRoot();
  }

  void fixRoot () {
    if (mRoot) {
      mRoot.rect.pos = GxPoint(0, 0);
      mRoot.rect.size = GxSize(clientWidth, clientHeight);
    }
  }

  void finishConstruction () {
    winrect.size.sanitize();
    createRoot();
    createStyle();
    mRoot.enter(&createWidgets);
    finishCreating();
  }

public:
  this () {
    winrect.pos = GxPoint(0, 0);
    winrect.size = GxSize(0, 0);
    finishConstruction();
  }

  this (const(char)[] atitle) {
    caption = atitle;
    winrect.pos = GxPoint(0, 0);
    winrect.size = GxSize(0, 0);
    finishConstruction();
  }

  this (const(char)[] atitle, in GxPoint apos, in GxSize asize) {
    caption = atitle;
    winrect.pos = apos;
    winrect.size = asize;
    finishConstruction();
  }

  this (const(char)[] atitle, in GxSize asize) {
    caption = atitle;
    winrect.size = asize;
    winrect.size.sanitize();
    winrect.pos.x = (screenWidth-winrect.width)/2;
    winrect.pos.y = (screenHeight-winrect.height)/2;
    finishConstruction();
  }

  @property void caption (const(char)[] atitle) {
    if (atitle is null) {
      mWinTitle.clear();
      mNoTitle = true;
    } else {
      mWinTitle = atitle;
      mNoTitle = false;
    }
  }

  @property dynstring caption () const nothrow @nogc { return dynstring(mWinTitle); }

  bool hasGrab () {
    return (mRoot !is null ? mRoot.hasGrab() : false);
  }

  override void widgetChanged () nothrow {
    if (mInWinList && !minimised) {
      try {
        postScreenRebuild();
      } catch (Exception e) {
        // sorry
      }
    }
  }

  // this doesn't perform relayouting
  void setRoot (RootWidget w) {
    if (mRoot !is w) {
      mRoot = w;
      fixRoot();
      widgetChanged();
    }
  }

  // this is called from constructor
  void createStyle () {
  }

  // this is called from constructor
  void createWidgets () {
  }

  // this is called from constructor
  // you can call `addModal()` here, for example
  void finishCreating () {
    add();
  }

  void relayout (bool resizeWindow=false) {
    if (mRoot is null) return;

    FuiFlexLayouter!Widget lay;

    lay.isValidBoxId = delegate bool (Widget id) { return (id !is null); };

    lay.firstChild = delegate Widget (Widget id) { return id.firstChild; };
    lay.nextSibling = delegate Widget (Widget id) { return id.nextSibling; };

    lay.getMinSize = delegate int (Widget id, in bool horiz) { return (horiz ? id.minSize.w : id.minSize.h); };
    lay.getMaxSize = delegate int (Widget id, in bool horiz) { return (horiz ? id.maxSize.w : id.maxSize.h); };
    lay.getPrefSize = delegate int (Widget id, in bool horiz) { return (horiz ? id.prefSize.w : id.prefSize.h); };

    lay.isHorizBox = delegate bool (Widget id) { return (id.childDir == GxDir.Horiz); };

    lay.getFlex = delegate int (Widget id) { return id.flex; };

    lay.getSize = delegate int (Widget id, in bool horiz) { return (horiz ? id.boxsize.w : id.boxsize.h); };
    lay.setSize = delegate void (Widget id, in bool horiz, int val) { if (horiz) id.boxsize.w = val; else id.boxsize.h = val; };

    lay.getFinalSize = delegate int (Widget id, in bool horiz) { return (horiz ? id.finalSize.w : id.finalSize.h); };
    lay.setFinalSize = delegate void (Widget id, in bool horiz, int val) { if (horiz) id.finalSize.w = val; else id.finalSize.h = val; };

    lay.setFinalPos = delegate void (Widget id, in bool horiz, int val) { if (horiz) id.finalPos.x = val; else id.finalPos.y = val; };

    if (winrect.size.empty) {
      if (maxWinSize.empty) maxWinSize = GxSize(screenWidth-16-decorationSizeX, screenHeight-16-decorationSizeY);
      resizeWindow = true;
    }

    if (winrect.size.w > screenWidth) winrect.size.w = screenWidth;
    if (winrect.size.h > screenHeight) winrect.size.h = screenHeight;

    if (resizeWindow) {
      mRoot.maxSize = maxWinSize;
      mRoot.minSize = minWinSize;
      if (mRoot.minSize.w > screenWidth-decorationSizeX) mRoot.minSize.w = screenWidth-decorationSizeX;
      if (mRoot.minSize.h > screenHeight-decorationSizeY) mRoot.minSize.h = screenHeight-decorationSizeY;
    } else {
      // cannot resize window, so set fixed sizes
      mRoot.maxSize = winrect.size-GxSize(decorationSizeX, decorationSizeY);
      if (mRoot.maxSize.w <= 0) mRoot.maxSize.w = maxWinSize.w;
      if (mRoot.maxSize.h <= 0) mRoot.maxSize.h = maxWinSize.h;
      mRoot.minSize = mRoot.maxSize;
      mRoot.prefSize = mRoot.maxSize;
      mRoot.boxsize = mRoot.maxSize;
    }

    if (!mNoTitle && mWinTitle.length) {
      if (mRoot.boxsize.w <= 0) mRoot.boxsize.w = gxTextWidthUtf(mWinTitle)+2;
    }
    mRoot.prefSize = mRoot.boxsize;

    Widget[] hsizes;
    Widget[] vsizes;
    scope(exit) {
      hsizes.unsafeArrayClear();
      delete hsizes;
      vsizes.unsafeArrayClear();
      delete vsizes;
    }

    foreach (Widget w; mRoot.allDepth) {
      w.preLayout();
      // hsize
      w.hsizeIdNext = null;
      if (w.hsizeId.length) {
        foreach (ref Widget nw; hsizes) {
          if (nw.hsizeId == w.hsizeId) {
            w.hsizeIdNext = nw;
            nw = w;
            break;
          }
        }
        if (w.hsizeIdNext is null) hsizes.unsafeArrayAppend(w);
      }
      // vsize
      w.vsizeIdNext = null;
      if (w.vsizeId.length) {
        foreach (ref Widget nw; vsizes) {
          if (nw.vsizeId == w.vsizeId) {
            w.vsizeIdNext = nw;
            nw = w;
            break;
          }
        }
        if (w.vsizeIdNext is null) vsizes.unsafeArrayAppend(w);
      }
    }

    // fix horiz pref sizes
    foreach (Widget sw; hsizes) {
      int vmax = 0;
      for (Widget w = sw; w !is null; w = w.hsizeIdNext) if (vmax < w.prefSize.w) vmax = w.prefSize.w;
      for (Widget w = sw; w !is null; w = w.hsizeIdNext) w.prefSize.w = vmax;
    }

    // fix vert pref sizes
    foreach (Widget sw; vsizes) {
      int vmax = 0;
      for (Widget w = sw; w !is null; w = w.vsizeIdNext) if (vmax < w.prefSize.h) vmax = w.prefSize.h;
      for (Widget w = sw; w !is null; w = w.vsizeIdNext) w.prefSize.h = vmax;
    }

    lay.layout(mRoot);

    winrect.size = mRoot.boxsize+GxSize(decorationSizeX, decorationSizeY);
    if (winrect.x1 >= screenWidth) winrect.pos.x -= winrect.x1-screenWidth+1;
    if (winrect.y1 >= screenHeight) winrect.pos.y -= winrect.y1-screenHeight+1;
    if (winrect.pos.x < 0) winrect.pos.x = 0;
    if (winrect.pos.y < 0) winrect.pos.y = 0;

    foreach (Widget w; mRoot.allDepth) {
      w.hsizeIdNext = null;
      w.vsizeIdNext = null;
      w.postLayout();
    }
    widgetChanged();
  }

  void relayoutResize () { relayout(true); }

  final @property Widget rootWidget () pure nothrow @safe @nogc { pragma(inline, true); return mRoot; }

  final @property int x0 () nothrow @safe { return (minimised ? winminx : winrect.pos.x); }
  final @property int y0 () nothrow @safe { return (minimised ? winminy : winrect.pos.y); }
  final @property int width () nothrow @safe { return (minimised ? MinSizeX : winrect.size.w); }
  final @property int height () nothrow @safe { return (minimised ? MinSizeY : winrect.size.h); }

  final @property void x0 (in int v) {
    if (minimised) {
      if (winminx != v) { winminx = v; if (mInWinList) postScreenRebuild(); }
    } else {
      if (winrect.pos.x != v) { winrect.pos.x = v; widgetChanged(); }
    }
  }
  final @property void y0 (in int v) {
    if (minimised) {
      if (winminy != v) { winminy = v; if (mInWinList) postScreenRebuild(); }
    } else {
      if (winrect.pos.y != v) { winrect.pos.y = v; widgetChanged(); }
    }
  }
  final @property void width (in int v) { pragma(inline, true); setSize(v, winrect.size.h); }
  final @property void height (in int v) { pragma(inline, true); setSize(winrect.size.w, v); }

  final void setPos (in int newx, in int newy) {
    if (winrect.pos.x != newx || winrect.pos.y != newy) {
      winrect.pos.x = newx;
      winrect.pos.y = newy;
      widgetChanged();
    }
  }

  final void setSize (in int awidth, in int aheight) {
    bool changed = false;
    if (awidth > 0 && awidth != winrect.size.w) {
      changed = true;
      immutable bool chsz = (mRoot !is null && mRoot.width == clientWidth);
      winrect.size.w = awidth;
      if (chsz || mRoot.width > clientWidth) mRoot.width = clientWidth;
    }
    if (aheight > 0 && aheight != winrect.size.w) {
      changed = true;
      immutable bool chsz = (mRoot !is null && mRoot.height == clientHeight);
      winrect.size.h = aheight;
      if (chsz || mRoot.height > clientHeight) mRoot.height = clientHeight;
    }
    if (changed) {
      //fixRoot();
      widgetChanged();
    }
  }

  final void setClientSize (int awidth, int aheight) {
    setSize((awidth > 0 ? awidth+decorationSizeX : awidth), (aheight > 0 ? aheight+decorationSizeY : aheight));
  }

  final void centerWindow () {
    immutable int newx = (screenWidth-winrect.size.w)/2;
    immutable int newy = (screenHeight-winrect.size.h)/2;
    if (winrect.pos.x != newx || winrect.pos.y != newy) {
      winrect.pos.x = newx;
      winrect.pos.y = newy;
      widgetChanged();
    }
  }

  final @property SubWindow prev () pure nothrow @safe @nogc { return mPrev; }
  final @property SubWindow next () pure nothrow @safe @nogc { return mNext; }

  final @property Type type () const pure nothrow @safe @nogc { return mType; }
  final @property bool onTop () const pure nothrow @safe @nogc { return (mType == Type.OnTop); }
  final @property bool onBottom () const pure nothrow @safe @nogc { return (mType == Type.OnBottom); }

  final @property bool inWinList () const pure nothrow @safe @nogc { return mInWinList; }

  final @property bool modal () const pure nothrow @safe @nogc { return mModal; }
  final @property bool closed () const pure nothrow @safe @nogc { return mClosed; }

  final @property bool inWindowList () const pure nothrow @safe @nogc { return mInWinList; }

  final @property bool active () const nothrow @trusted @nogc {
    if (!mInWinList || mClosed || minimised) return false;
    return (getActiveSubWindow is this);
  }

  @property int decorationSizeX () const nothrow @safe { return 2*2; }
  @property int decorationSizeY () const nothrow @safe { immutable hgt = gxTextHeightUtf; return (hgt < 10 ? 10 : hgt+1)+4; }

  @property int clientOffsetX () const nothrow @safe { return 2; }
  @property int clientOffsetY () const nothrow @safe { immutable hgt = gxTextHeightUtf; return (hgt < 10 ? 10 : hgt+1)+2; }

  final @property int clientWidth () const nothrow @safe { pragma(inline, true); return winrect.size.w-decorationSizeX; }
  final @property int clientHeight () const nothrow @safe { pragma(inline, true); return winrect.size.h-decorationSizeY; }

  final @property void clientWidth (in int v) { pragma(inline, true); setClientSize(v, clientHeight); }
  final @property void clientHeight (in int v) { pragma(inline, true); setClientSize(clientWidth, v); }

  final @property Widget focusedWidget () {
    if (mRoot is null) return null;
    Widget w = mRoot.focusedWidget;
    return (w !is null && w.canAcceptFocus ? w : null);
  }

  protected void drawWidgets () {
    setupClientClip();
    if (mRoot !is null) mRoot.onPaint();
  }

  // draw window frame and background in "normal" state
  protected void drawNormalDecoration () {
    //immutable string act = (active ? null : "inactive");
    gxDrawWindow(winrect, (mNoTitle ? null : mWinTitle.length ? mWinTitle.getData : ""),
      getColor("frame"),
      getColor("title-text"),
      getColor("title-back"),
      getColor("back"),
      getColor("shadow-color"),
      getInt("shadow-size", 0),
      (getInt("shadow-dash", 0) > 0));
  }

  // draw window frame and background in "minimised" state
  protected void drawWindowMinimised () {
    //immutable string act = (active ? null : "inactive");
    gxFillRect(winminx, winminy, MinSizeX, MinSizeY, getColor("back"));
    gxDrawRect(winminx, winminy, MinSizeX, MinSizeY, getColor("frame"));
  }

  // this changes clip
  protected void drawDecoration () {
    if (minimised) {
      if (gxClipRect.intersect(GxRect(winminx, winminy, MinSizeX, MinSizeY))) drawWindowMinimised();
    } else {
      setupClip();
      drawNormalDecoration();
    }
  }

  protected void drawDragRect () {
    immutable uint clr = getColor("drag-overlay-back");
    if (gxIsTransparent(clr)) return;
    immutable bool dashed = (getInt("drag-overlay-dash", 0) > 0);
    gxWithSavedClip {
      gxClipReset();
      if (dashed) {
        gxDashRect(x0, y0, width, height, clr);
      } else {
        gxFillRect(x0, y0, width, height, clr);
      }
    };
  }


  void releaseWidgetGrab () {
    if (mRoot !is null) mRoot.releaseGrab();
  }

  // event in our local coords
  bool startMouseDrag (MouseEvent event) {
    if (!mAllowDragMove || !mInWinList) return false;
    releaseWidgetGrab();
    subwinDrag = this;
    subwinDragXSpot = -event.x;
    subwinDragYSpot = -event.y;
    widgetChanged();
    return true;
  }

  bool startKeyboardDrag () {
    if (!mAllowDragMove || !mInWinList) return false;
    releaseWidgetGrab();
    subwinDrag = this;
    subwinDragXSpot = int.min;
    subwinDragYSpot = int.min;
    widgetChanged();
    return true;
  }

  void stopDrag () {
    if (subwinDrag is this) {
      releaseWidgetGrab();
      subwinDrag = null;
      widgetChanged();
    }
  }

  void onPaint () {
    if (closed) return;
    gxWithSavedClip {
      gxagg.beginFrame();
      scope(exit) gxagg.cancelFrame();
      drawDecoration();
      if (!minimised) drawWidgets();
    };
  }


  bool onKeySink (KeyEvent event) {
    return false;
  }

  bool onKeyBubble (KeyEvent event) {
    // global window hotkeys
    if (event.pressed) {
      if (event == "C-F5") { if (startKeyboardDrag()) return true; }
      if (/*event == "M-M" ||*/ event == "M-S-M") { if (minimise()) return true; }
    }
    return false;
  }

  bool onKeyEvent (KeyEvent event) {
    if (closed) return false;
    if (minimised) return false;
    if (onKeySink(event)) return true;
    if (mRoot !is null && mRoot.dispatchKey(event)) return true;
    return onKeyBubble(event);
  }


  bool onMouseSink (MouseEvent event) {
    // start drag?
    if (subwinDrag is null && event.type == MouseEventType.buttonPressed && event.button == MouseButton.left) {
      if (event.x >= 0 && event.y >= 0 &&
          event.x < width && event.y < (!minimised ? gxTextHeightUtf+2 : height))
      {
        startMouseDrag(event);
        return true;
      }
    }

    if (minimised) return false;

    if (event.type == MouseEventType.buttonReleased && event.button == MouseButton.right) {
      if (event.x >= 0 && event.y >= 0 &&
          event.x < winrect.size.w && event.y < gxTextHeightUtf)
      {
        minimise();
        return true;
      }
    }

    return false;
  }

  bool onMouseBubble (MouseEvent event) {
    return false;
  }

  bool onMouseEvent (MouseEvent event) {
    if (!active) return false;
    if (closed) return false;

    int mx, my;
    event.mouse2xy(mx, my);

    MouseEvent ev = event;
    ev.x = mx-x0;
    ev.y = my-y0;
    if (onMouseSink(ev)) return true;

    if (mRoot !is null) {
      ev = event;
      ev.x = mx-(x0+clientOffsetX)-mRoot.rect.x0;
      ev.y = my-(y0+clientOffsetY)-mRoot.rect.y0;
      if (mRoot.dispatchMouse(ev)) return true;
    }

    ev = event;
    ev.x = mx-x0;
    ev.y = my-y0;
    return onMouseBubble(ev);
  }


  bool onCharSink (dchar ch) {
    return false;
  }

  bool onCharBubble (dchar ch) {
    return false;
  }

  bool onCharEvent (dchar ch) {
    if (!active) return false;
    if (closed) return false;
    if (minimised) return false;
    if (onCharSink(ch)) return true;
    if (mRoot !is null && mRoot.dispatchChar(ch)) return true;
    return onCharBubble(ch);
  }

  void setupClip () {
    gxClipRect.intersect(winrect);
  }

  final void setupClientClip () {
    setupClip();
    gxClipRect.intersect(GxRect(
      GxPoint(winrect.pos.x+clientOffsetX, winrect.pos.y+clientOffsetY),
      GxPoint(winrect.pos.x+clientOffsetX+clientWidth-1, winrect.pos.y+clientOffsetY+clientHeight-1)));
  }

  void close () {
    if (!mClosed) {
      mClosed = true;
      if (removeSubWindow(this)) postScreenRebuild();
    }
  }

  protected bool addToSubwinList (bool asModal, bool fromKeyboard) {
    if (fromKeyboard) ignoreSubWinChar = true;
    if (mInWinList) return true;
    mModal = asModal;
    if (insertSubWindow(this)) {
      if (mRoot !is null) mRoot.fixFocus();
      widgetChanged();
      postScreenRebuild();
      return true;
    }
    return false;
  }

  void add (bool fromKeyboard=false) { addToSubwinList(false, fromKeyboard); }

  void addModal (bool fromKeyboard=false) { addToSubwinList(true, fromKeyboard); }

  // return `false` to reject
  protected bool minimiseQuery () {
    return true;
  }

  // return `false` to reject (this is "unminimise")
  protected bool restoreQuery () {
    return true;
  }

  void bringToFront () {
    if (mClosed || !mInWinList) return;
    if (minimised && !restoreQuery()) return;
    auto aw = getActiveSubWindow();
    if (aw is this) {
      if (minimised) {
        mMinimised = false;
        lastHoverWindow = null;
        widgetChanged();
      }
      return;
    }
    if (aw !is null && aw.mModal) return; // alas
    removeSubWindow(this);
    mMinimised = false;
    insertSubWindow(this);
    if (subwinDrag !is this) subwinDrag = null;
    widgetChanged();
  }

  final @property bool allowMinimise () pure const nothrow @safe @nogc { return mAllowMinimise; }
  @property void allowMinimise (in bool v) { mAllowMinimise = v; }

  final @property bool allowDragMove () pure const nothrow @safe @nogc { return mAllowDragMove; }
  @property void allowDragMove (in bool v) { mAllowDragMove = v; }

  final @property bool minimised () pure const nothrow @safe @nogc { return mMinimised; }
  @property void minimised (in bool v) {
    if (v == mMinimised) return;
    if (!mAllowMinimise && v) return;
    if (v) minimise(); else bringToFront();
  }

  bool minimise () {
    if (minimised) return true;
    if (mClosed || !mAllowMinimise || mModal) return false;
    if (!mInWinList) {
      if (minimiseQuery()) mMinimised = true;
      return true;
    }
    assert(subwinLast !is null);
    releaseWidgetGrab();
    findMinimisedPos(winminx, winminy);
    auto aw = getActiveSubWindow();
    if (aw is this) subwinDrag = null;
    mMinimised = true;
    lastHoverWindow = null;
    postScreenRebuild(); // because minimised widgets will not post this
    return true;
  }

  void restore () {
    releaseWidgetGrab();
    bringToFront();
  }

public:
  static T clampval(T) (in T val, in T min, in T max) pure nothrow @safe @nogc {
    pragma(inline, true);
    return (val < min ? min : val > max ? max : val);
  }

  int MinSizeX () nothrow @safe { immutable bool omm = mMinimised; mMinimised = true; scope(exit) mMinimised = omm; return clampval(getInt("icon-size-x", 16), 16, 64); }
  int MinSizeY () nothrow @safe { immutable bool omm = mMinimised; mMinimised = true; scope(exit) mMinimised = omm; return clampval(getInt("icon-size-y", 16), 16, 64); }

  int MinMarginX () nothrow @safe { immutable bool omm = mMinimised; mMinimised = true; scope(exit) mMinimised = omm; return clampval(getInt("icon-margin-x", 3), 1, 16); }
  int MinMarginY () nothrow @safe { immutable bool omm = mMinimised; mMinimised = true; scope(exit) mMinimised = omm; return clampval(getInt("icon-margin-y", 3), 1, 16); }

protected:
  void findMinimisedPos (out int wx, out int wy) {
    static bool isOccupied (int x, int y) {
      for (SubWindow w = subwinLast; w !is null; w = w.mPrev) {
        if (w.mInWinList && !w.closed && w.minimised) {
          if (x >= w.winminx && y >= w.winminy && x < w.winminx+w.MinSizeX && y < w.winminy+w.MinSizeY) return true;
        }
      }
      return false;
    }

    int txcount = screenWidth/(MinSizeX+MinMarginX);
    //int tycount = screenHeight/(MinSizeY+MinMarginY);
    if (txcount < 1) txcount = 1;
    //if (tycount < 1) tycount = 1;
    foreach (immutable int n; 0..6/*5535*/) {
      int x = (n%txcount)*(MinSizeX+MinMarginX)+1;
      int y = screenHeight-MinSizeY-(n/txcount)*(MinSizeY+MinMarginY);
      //conwriteln("trying (", x, ",", y, ")");
      if (!isOccupied(x, y)) { wx = x; wy = y; /*conwriteln("  HIT!");*/ return; }
    }
  }
}
