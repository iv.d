/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gui.dialogs /*is aliced*/;
private:

import arsd.simpledisplay;

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.utfutil;
import iv.vfs;

import iv.egra.gfx;
import iv.egra.gui.subwindows;
import iv.egra.gui.widgets;


// ////////////////////////////////////////////////////////////////////////// //
public dynstring[] buildAutoCompletion (ConString prefix) {
  import std.file : DirEntry, SpanMode, dirEntries;

  if (prefix.length == 0) return null;

  ConString path, namepfx;

  if (prefix[$-1] == '/') {
    path = prefix;
    namepfx = null;
  } else {
    auto lspos = prefix.lastIndexOf('/');
    if (lspos < 0) {
      path = null;
      namepfx = prefix;
    } else {
      path = prefix[0..lspos+1];
      namepfx = prefix[lspos+1..$];
    }
  }

  //conwriteln("path=[", path, "]; namepfx=[", namepfx, "]");

  dynstring[] res;
  foreach (DirEntry de; dirEntries(path.idup, SpanMode.shallow)) {
    if (namepfx.length != 0) {
      import std.path : baseName;
      //conwriteln("  [", de.baseName, "]");
      if (!de.baseName.startsWith(namepfx)) continue;
    }
    try {
      dynstring ds = de.name;
      if (de.isDir) ds ~= "/";
      res ~= ds;
      //if (de.isDir) res ~= de.name~"/"; else res ~= de.name;
    } catch (Exception e) {}
  }

  if (res.length > 1) {
    import std.algorithm : sort;
    sort(res);
  }

  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
public class SelectCompletionWindow : SubWindow {
  dynstring[] list;

  void delegate (dynstring str) onSelected;

  this (const(char)[] prefix, dynstring[] clist, bool aspath) {
    createRoot();
    int xhgt = 0;
    int xwdt = 0;
    bool idxset = false;

    SimpleListBoxWidget lb = new SimpleListBoxWidget(rootWidget);
    lb.id = "main-listbox";
    foreach (dynstring s; clist) {
      if (aspath && s.length) {
        usize lspos = s.length;
        if (s[$-1] == '/') --lspos;
        while (lspos > 0 && s.ptr[lspos-1] != '/') --lspos;
        s = s[lspos..$];
      }
      lb.appendItem(s);
      if (s == prefix) { idxset = true; lb.curidx = lb.length-1; }
      if (!idxset && s.startsWith(prefix)) { idxset = true; lb.curidx = lb.length-1; }
      int w = gxTextWidthUtf(s)+2;
      if (xwdt < w) xwdt = w;
      xhgt += gxTextHeightUtf;
    }

    if (xhgt == 0) { super(); return; }
    if (xhgt > screenHeight) xhgt = screenHeight-decorationSizeY;

    if (xwdt > screenWidth-decorationSizeX) xwdt = screenWidth-decorationSizeX;

    super("Select Completion", GxSize(xwdt+decorationSizeX, xhgt+decorationSizeY));
    if (width < gxTextWidthUtf(caption)+4) width = gxTextWidthUtf(caption)+4;
    lb.width = clientWidth;
    lb.height = clientHeight;
    list = clist;

    lb.onAction = delegate (self) {
      if (onSelected !is null && lb.curidx >= 0 && lb.curidx < list.length) {
        close();
        onSelected(list[lb.curidx]);
        return;
      }
      vbwin.beep();
    };

    addModal();
  }

  override bool onKeyBubble (KeyEvent event) {
    if (event.pressed) {
      if (event == "Escape") { close(); return true; }
      if (event == "Enter") {
        if (auto lb = querySelector!Widget("#main-listbox")) {
          lb.doAction();
          return true;
        }
      }
    }
    return super.onKeyBubble(event);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class YesNoWindow : SubWindow {
public:
  void delegate () onYes;
  void delegate () onNo;

protected:
  dynstring msgMessage;
  bool defaction = true;

public:
  this (const(char)[] atitle, const(char)[] amessage, bool adefaction=true) {
    msgMessage = amessage;
    defaction = adefaction;
    super(atitle);
  }

  override void createWidgets () {
    new SpacerWidget(8);

    (new HBoxWidget).enter{
      new SpacerWidget(4);
      with (new LabelWidget(msgMessage, LabelWidget.HAlign.Center, LabelWidget.VAlign.Center)) {
        flex = 1;
      }
      new SpacerWidget(4);
    };

    new SpacerWidget(8);

    (new HBoxWidget).enter{
      new SpacerWidget(2);
      new SpringWidget(1);
      with (new ButtonExWidget("&Yes")) {
        hsizeId = "yesno";
        deftype = Default.Accept;
        if (width < 96) width = 96;
        onAction = delegate (self) { close(); if (onYes !is null) onYes(); };
        if (defaction) focus();
      }
      new SpacerWidget(4);
      with (new ButtonExWidget("&No")) {
        hsizeId = "yesno";
        deftype = Default.Cancel;
        onAction = delegate (self) { close(); if (onNo !is null) onNo(); };
        onCheckHotkey = delegate (self, event) { return (event == "C-Q"); };
        if (!defaction) focus();
      }
      new SpringWidget(1);
      new SpacerWidget(2);
    };

    new SpacerWidget(2);

    relayoutResize();
    centerWindow();
  }

  override void finishCreating () {
    addModal();
  }
}
