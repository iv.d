/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra /*is aliced*/;

private import arsd.simpledisplay;

public import iv.egra.gfx;
public import iv.egra.gui;

// so rdmd will know that we need it
static if (EGfxOpenGLBackend) {
  private import iv.glbinds;
}
import iv.cmdcon;
import iv.cmdcongl;
public import iv.dynstring;


// ////////////////////////////////////////////////////////////////////////// //
// set this to `true` if you have fullscreen window
public __gshared bool egraSkipScreenClear = false;
public __gshared bool egraX11Direct = false;

private __gshared ubyte vbufNewScale = 1; // new window scale
private __gshared bool vbufVSync = false;
private __gshared bool vbufEffVSync = false;

private __gshared int lastWinWidth, lastWinHeight;
private __gshared bool firstTimeInited = false;

public class EgraDoConsoleCommandsEvent {}
private __gshared EgraDoConsoleCommandsEvent evDoConCommands;

private __gshared QuitEvent evQuitEvent;


public void egraPostDoConCommands () {
  pragma(inline, true);
  if (vbwin !is null && !vbwin.eventQueued!EgraDoConsoleCommandsEvent) vbwin.postEvent(evDoConCommands);
}

public void postQuitEvent () {
  pragma(inline, true);
  if (vbwin !is null && !vbwin.eventQueued!QuitEvent) vbwin.postEvent(evQuitEvent);
}

//public int egraGetScale () nothrow @trusted @nogc { pragma(inline, true); return vbufNewScale; }


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  evDoConCommands = new EgraDoConsoleCommandsEvent();
  evQuitEvent = new QuitEvent();

  conRegVar!vbufNewScale(1, 4, "v_scale", "window scale: [1..3]");

  conRegVar!bool("v_vsync", "sync to video refresh rate?",
    (ConVarBase self) => vbufVSync,
    (ConVarBase self, bool nv) {
      static if (EGfxOpenGLBackend) {
        if (vbufVSync != nv) {
          vbufVSync = nv;
          postScreenRepaint();
        }
      }
    },
  );

  vbufNewScale = cast(ubyte)screenEffScale;
  vbufEffVSync = vbufVSync;
}


// ////////////////////////////////////////////////////////////////////////// //
public void egraProcessConsole () {
  scope(exit) if (!conQueueEmpty()) egraPostDoConCommands();
  consoleLock();
  scope(exit) consoleUnlock();
  conProcessQueue();
}


// ////////////////////////////////////////////////////////////////////////// //
public bool egraOnKey (KeyEvent event) {
  if (vbwin is null) return false;
  scope(exit) if (!conQueueEmpty()) egraPostDoConCommands();
  if (vbwin.closed) return false;
  if (isQuitRequested()) postQuitEvent();
  if (glconKeyEvent(event)) {
    postScreenRepaint();
    return true;
  }
  if ((event.modifierState&ModifierState.numLock) == 0) {
    switch (event.key) {
      case Key.Pad0: event.key = Key.Insert; break;
      case Key.Pad1: event.key = Key.End; break;
      case Key.Pad2: event.key = Key.Down; break;
      case Key.Pad3: event.key = Key.PageDown; break;
      case Key.Pad4: event.key = Key.Left; break;
      //case Key.Pad5: event.key = Key.Insert; break;
      case Key.Pad6: event.key = Key.Right; break;
      case Key.Pad7: event.key = Key.Home; break;
      case Key.Pad8: event.key = Key.Up; break;
      case Key.Pad9: event.key = Key.PageUp; break;
      case Key.PadEnter: event.key = Key.Enter; break;
      case Key.PadDot: event.key = Key.Delete; break;
      default: break;
    }
  } else {
    if (event.key == Key.PadEnter) event.key = Key.Enter;
  }
  if (dispatchEvent(event)) return true;
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
public bool egraOnMouse (MouseEvent event) {
  if (vbwin is null) return false;
  scope(exit) if (!conQueueEmpty()) egraPostDoConCommands();
  if (vbwin.closed) return false;
  lastMouseXUnscaled = event.x;
  lastMouseYUnscaled = event.y;
       if (event.type == MouseEventType.buttonPressed) lastMouseButton |= event.button;
  else if (event.type == MouseEventType.buttonReleased) lastMouseButton &= ~event.button;
  egraMouseMoved();
  if (dispatchEvent(event)) return true;
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
public bool egraOnChar (dchar ch) {
  if (vbwin is null) return false;
  scope(exit) if (!conQueueEmpty()) egraPostDoConCommands();
  if (vbwin.closed) return false;
  if (glconCharEvent(ch)) {
    postScreenRepaint();
    return true;
  }
  if (dispatchEvent(ch)) return true;
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
// vbwin.onFocusChange = &egraSdpyOnFocusChange;
public void egraSdpyOnFocusChange (bool focused) {
  vbfocused = focused;
  if (!focused) {
    lastMouseButton = 0;
    eguiLostGlobalFocus();
  } else {
    lastMouseButton = 0;
  }
  postScreenRebuild();
}


//vbwin.windowResized = &egraSdpyOnWindowResized;
public void egraSdpyOnWindowResized (int wdt, int hgt) {
  // TODO: fix gui sizes
  if (vbwin.closed) return;

  if (lastWinWidth == wdt && lastWinHeight == hgt) return;
  glconResize(wdt, hgt);

  if (wdt < vbufNewScale*32) wdt = vbufNewScale;
  if (hgt < vbufNewScale*32) hgt = vbufNewScale;
  int newwdt = (wdt+vbufNewScale-1)/vbufNewScale;
  int newhgt = (hgt+vbufNewScale-1)/vbufNewScale;

  lastWinWidth = wdt;
  lastWinHeight = hgt;

  vglResizeBuffer(newwdt, newhgt, vbufNewScale);

  egraMouseMoved();

  egraRebuildScreen();
}


// return `true` to prevent backbuffer copying by sdpy
public bool egraX11Expose (int x, int y, int width, int height, int eventsLeft) {
  //conwriteln("EXPOSE! x=", x, "; y=", y, "; width=", width, "; height=", height, "; eventsLeft=", eventsLeft);
  if (eventsLeft == 0) egraRepaintScreen();
  //return egraX11Direct;
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
public SimpleWindow egraCreateSystemWindow (string wintitle, bool allowResize=false) {
  assert(vbwin is null || vbwin.closed);

  firstTimeInited = false;

  static if (EGfxOpenGLBackend) {
    vbwin = new SimpleWindow(screenWidthScaled, screenHeightScaled, wintitle, OpenGlOptions.yes, (allowResize ? Resizability.allowResizing : Resizability.fixedSize));
    vbwin.hideCursor();
    glconAllowOpenGLRender = true;
  } else {
    vbwin = new SimpleWindow(screenWidthScaled, screenHeightScaled, wintitle, OpenGlOptions.no, (allowResize ? Resizability.allowResizing : Resizability.fixedSize));
  }

  version(egfx_opengl_backend) {
  } else {
    vbwin.handleExpose = delegate (int x, int y, int width, int height, int eventsLeft) {
      return egraX11Expose(x, y, width, height, eventsLeft);
    };
    XSetWindowBackground(vbwin.impl.display, vbwin.impl.window, 0);
  }

  vbwin.visibleForTheFirstTime = delegate () {
    egraFirstTimeInit();
  };

  vbwin.addEventListener((EgraDoConsoleCommandsEvent evt) {
    bool sendAnother = false;
    bool prevVisible = isConsoleVisible;
    {
      consoleLock();
      scope(exit) consoleUnlock();
      conProcessQueue();
      sendAnother = !conQueueEmpty();
    }
    if (sendAnother) egraPostDoConCommands();
    if (vbwin.closed) return;
    if (isQuitRequested()) postQuitEvent();
    if (prevVisible || isConsoleVisible) postScreenRepaintDelayed();
  });

  vbwin.onFocusChange = (bool focused) { egraSdpyOnFocusChange(focused); };

  vbwin.windowResized = (int wdt, int hgt) { egraSdpyOnWindowResized(wdt, hgt); };

  vbwin.addEventListener((HideMouseEvent evt) {
    if (vbwin.closed) return;
    if (isQuitRequested()) postQuitEvent();
    if (repostHideMouse) egraRepaintScreen();
  });

  vbwin.addEventListener((ScreenRebuildEvent evt) {
    if (vbwin.closed) return;
    if (isQuitRequested()) postQuitEvent();
    egraRebuildScreen();
    if (isConsoleVisible) postScreenRepaintDelayed();
  });

  vbwin.addEventListener((ScreenRepaintEvent evt) {
    if (vbwin.closed) return;
    if (isQuitRequested()) postQuitEvent();
    egraRepaintScreen();
    if (isConsoleVisible) postScreenRepaintDelayed();
  });

  vbwin.addEventListener((CursorBlinkEvent evt) {
    if (vbwin.closed) return;
    egraRebuildScreen();
  });

  vbwin.redrawOpenGlScene = delegate () {
    if (vbwin.closed) return;
    egraOnGLRepaint();
  };

  return vbwin;
}


// ////////////////////////////////////////////////////////////////////////// //
// call this in `vbwin.visibleForTheFirstTime()` to initialise GUI
// note that you cannot use EGUI with more than one window yet!
public void egraFirstTimeInit () {
  if (firstTimeInited) return;
  firstTimeInited = true;

  vbufEffVSync = vbufVSync;
  static if (EGfxOpenGLBackend) {
    assert(vbwin !is null);
    import iv.glbinds;
    vbwin.setAsCurrentOpenGlContext();
    vbwin.vsync = vbufEffVSync;
  }

  lastWinWidth = screenWidthScaled;
  lastWinHeight = screenHeightScaled;

  vglResizeBuffer(screenWidth, screenHeight);
  vglCreateArrowTexture();

  glconInit(screenWidthScaled, screenHeightScaled);

  egraRebuildScreen();
}


// ////////////////////////////////////////////////////////////////////////// //
// call this from `win.redrawOpenGlScene()`
public void egraOnGLRepaint () {
  pragma(inline, true);
  egraDoRepaint(fromGLHandler:true, doRebuild:false);
}


// ////////////////////////////////////////////////////////////////////////// //
public void egraDoRepaint (immutable bool fromGLHandler, bool doRebuild) {
  scope(exit) if (isQuitRequested()) postQuitEvent();
  if (vbwin is null || vbwin.closed) return;

  bool resizeWin = false;

  {
    consoleLock();
    scope(exit) consoleUnlock();

    if (!conQueueEmpty()) egraPostDoConCommands();

    if (vbufNewScale != screenEffScale) {
      // window scale changed
      resizeWin = true;
    }
    if (vbufEffVSync != vbufVSync) {
      vbufEffVSync = vbufVSync;
      vbwin.vsync = vbufEffVSync;
    }
  }

  if (resizeWin) {
    vbwin.resize(screenWidthScaled, screenHeightScaled);
    glconResize(screenWidthScaled, screenHeightScaled);
    //vglResizeBuffer(screenWidth, screenHeight, vbufNewScale);
    doRebuild = true;
  }

  if (doRebuild) {
    gxClipReset();
    if (!egraSkipScreenClear) gxClearScreen(0);
    paintSubWindows();
    vglUpdateTexture(); // this does nothing for X11, but required for OpenGL
  }

  static if (EGfxOpenGLBackend) {
    if (!fromGLHandler) vbwin.setAsCurrentOpenGlContext();
  }
  scope(exit) {
    static if (EGfxOpenGLBackend) {
      if (!fromGLHandler) vbwin.releaseCurrentOpenGlContext();
    }
  }

  static if (EGfxOpenGLBackend) {
    vglBlitTexture();
  } else {
    //vglBlitTexture(vbwin, egraX11Direct);
  }

  if (vArrowTextureId) {
    if (isMouseVisible) {
      int px = lastMouseX;
      int py = lastMouseY;
      static if (EGfxOpenGLBackend) {
        glColor4f(1, 1, 1, mouseAlpha);
      }
      vglBlitArrow(px, py);
    }
  }

  static if (EGfxOpenGLBackend) {
    glconDrawWindow = null;
    glconDrawDirect = false;
    glconDraw();
  } else {
    if (egraX11Direct) {
      vglBlitTexture(vbwin, true);
      glconDrawWindow = vbwin;
      glconDrawDirect = true;
      glconDraw();
      glconDrawWindow = null;
    } else {
      auto painter = vbwin.draw();
      vglBlitTexture(vbwin, false);
      glconDrawWindow = vbwin;
      glconDrawDirect = false;
      glconDraw();
      glconDrawWindow = null;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void egraRebuildScreen () {
  if (vbwin !is null && !vbwin.closed && !vbwin.hidden) {
    static if (EGfxOpenGLBackend) {
      // rebuild
      gxClipReset();
      if (!egraSkipScreenClear) gxClearScreen(0);
      paintSubWindows();
      vglUpdateTexture(); // this does nothing for X11, but required for OpenGL
      // and redraw
      vbwin.redrawOpenGlSceneNow();
    } else {
      egraDoRepaint(fromGLHandler:false, doRebuild:true);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void egraRepaintScreen () {
  __gshared bool lastvisible = false;
  if (vbwin !is null && !vbwin.closed && !vbwin.hidden) {
    bool curvisible = isConsoleVisible;
    if (lastvisible != curvisible || curvisible) {
      lastvisible = curvisible;
      egraRebuildScreen();
      return;
    }
    static if (EGfxOpenGLBackend) {
      vbwin.redrawOpenGlSceneNow();
    } else {
      egraDoRepaint(fromGLHandler:false, doRebuild:false);
    }
  }
}
