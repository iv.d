/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module test /*is aliced*/;

import arsd.simpledisplay;

import iv.bclamp;
import iv.cmdcon;
import iv.cmdcongl;
import iv.strex;
import iv.utfutil;
import iv.vfs.io;
import iv.vfs.util;

import iv.egra;


// ////////////////////////////////////////////////////////////////////////// //
static immutable string TestStyle = `
MainPaneWindow {
  grouplist-divline: white;
  grouplist-back: #222;

  threadlist-divline: white;
  threadlist-back: #222;
}

TitlerWindow {
  frame: white;
  title-back: white;
  title-text: black;

  back: rgb(0, 92, 0);
  text: #7f0;
  hotline: #7f0;
}
`;


// ////////////////////////////////////////////////////////////////////////// //
//__gshared int lastWinWidth, lastWinHeight;


// ////////////////////////////////////////////////////////////////////////// //
public class TitlerWindow : SubWindow {
public:
  LineEditWidget edtTitle;
  LineEditWidget fromName;
  LineEditWidget fromMail;

protected:
  dynstring name;
  dynstring mail;
  dynstring folder;
  dynstring title;

  bool delegate (dynstring name, dynstring mail, dynstring folder, dynstring title) onSelected;

  override void createWidgets () {
    new SpacerWidget(2);

    version(none) {
      fromName = new LineEditWidget("Name:");
      fromName.flex = 1;

      new SpacerWidget(1);
      fromMail = new LineEditWidget("Mail:");
      fromMail.flex = 1;

      new SpacerWidget(1);
      edtTitle = new LineEditWidget("Title:");
      edtTitle.flex = 1;
    } else {
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&Name:", LabelWidget.HAlign.Right)) { width = width+2; hsizeId = "editors"; }
        new SpacerWidget(4);
        fromName = new LineEditWidget();
        fromName.flex = 1;
      }.flex = 1;

      new SpacerWidget(1);
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&Mail:", LabelWidget.HAlign.Right)) { width = width+2; hsizeId = "editors"; }
        new SpacerWidget(4);
        fromMail = new LineEditWidget();
        fromMail.flex = 1;
      }.flex = 1;

      new SpacerWidget(1);
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&Title:", LabelWidget.HAlign.Right)) { width = width+2; hsizeId = "editors"; }
        new SpacerWidget(4);
        edtTitle = new LineEditWidget();
        edtTitle.flex = 1;
      }.flex = 1;
    }

    new SpacerWidget(4);
    (new HBoxWidget).enter{
      new SpacerWidget(2);
      new SpringWidget(1);
      with (new ButtonWidget(" O&k ")) {
        hsizeId = "okcancel";
        deftype = Default.Accept;
        onAction = delegate (self) {
          if (onSelected !is null) {
            if (!onSelected(fromName.str, fromMail.str, folder, edtTitle.str)) return;
          }
          close();
        };
      }
      new SpacerWidget(2);
      with (new ButtonWidget(" Cancel ")) {
        hsizeId = "okcancel";
        deftype = Default.Cancel;
        onAction = delegate (self) {
          close();
        };
      }
      new SpacerWidget(4);
      with (new ButtonWidget(" ... ")) {
        hsizeId = "okcancel";
        hotkey = "M-L";
        disabled = true;
      }
      new SpringWidget(1);
      new SpacerWidget(2);
    };
    new SpacerWidget(4);

    fromName.str = name;
    fromMail.str = mail;
    edtTitle.str = title;

    relayoutResize();
    centerWindow();

    edtTitle.focus();
  }

  this (const(char)[] aname, const(char)[] amail, const(char)[] afolder, const(char)[] atitle) {
    dynstring caption = "Title for "~aname~" <"~amail~">";
    name = aname;
    mail = amail;
    folder = afolder;
    title = atitle;
    super(caption);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class TagOptionsWindow : SubWindow {
  dynstring tagname;

  void delegate (const(char)[] tagname) onUpdated;

  this (const(char)[] atagname) {
    tagname = atagname;
    dynstring caption = "options for '"~atagname~"'";
    super(caption);
  }

  override void createWidgets () {
    LabelWidget optPath = new LabelWidget("", LabelWidget.HAlign.Center);
    optPath.flex = 1;

    LineEditWidget optMonthes;
    (new HBoxWidget).enter{
      with (new HotLabelWidget("&Monthes:", LabelWidget.HAlign.Right)) { width = width+2; /*hsizeId = "editors";*/ }
      new SpacerWidget(4);
      optMonthes = new LineEditWidget();
      //optMonthes.flex = 1;
      optMonthes.width = gxTextWidthUtf("96669");
      conwriteln("ISMYSEL: ", optMonthes.isMySelector(" Widget#. "));
      conwriteln("ISMYSEL: ", optMonthes.isMySelector(" HBoxWidget LineEditWidget#.  "));
      conwriteln("ISMYSEL: ", optMonthes.isMySelector(" HBoxWidget> #.  "));
      conwriteln("ISMYSEL: ", optMonthes.isMySelector(" SubWindow HBoxWidget LineEditWidget#.  "));
      //new SpringWidget(1);
    }.flex = 1;

    CheckboxWidget optThreaded = new CheckboxWidget("&Threaded");
    optThreaded.flex = 1;

    CheckboxWidget optAttaches = new CheckboxWidget("&Attaches");
    optAttaches.flex = 1;

    with (new RadioWidget("Radio &1")) {
      rgroup = "rg1";
      flex = 1;
      id = "r1";
    }

    with (new RadioWidget("Radio &2")) {
      rgroup = "rg1";
      flex = 1;
      checked = true;
      id = "r2";
    }

    new SpacerWidget(4);
    (new HBoxWidget).enter{
      new SpacerWidget(2);
      new SpringWidget(1);
      with (new ButtonWidget(" O&k ")) {
        hsizeId = "okcancel";
        deftype = Default.Accept;
        onAction = delegate (self) {
          if (onUpdated !is null) onUpdated(tagname);
          close();
          auto rw = self.getSelectedRadio("rg1");
          if (rw !is null) conwriteln("SELECTED RADIO: <", rw.id, ">");
        };
      }
      new SpacerWidget(4);
      with (new ButtonWidget(" Cancel ")) {
        hsizeId = "okcancel";
        deftype = Default.Cancel;
        onAction = delegate (self) {
          close();
        };
      }
      new SpacerWidget(4);
      with (new ButtonWidget(" ... ")) {
        hsizeId = "okcancel";
        hotkey = "M-L";
        onAction = delegate (self) {
          (new SelectCompletionWindow("", buildAutoCompletion("/home/ketmar/"), aspath:true)).onSelected = (str) {
            conwriteln("SELECTED: <", str.getData, ">");
          };
          //close();
        };
        //disabled = true;
      }
      new SpringWidget(1);
      new SpacerWidget(2);
    };
    new SpacerWidget(4);

    optMonthes.focus();

    optPath.text = "booPath";
    optMonthes.str = "666";

    optThreaded.enabled = false;
    optAttaches.enabled = true;

    optMonthes.killTextOnChar = true;

    relayoutResize();
    centerWindow();
    //add(); // for "focused"

    forEachSelector("SubWindow > RootWidget CheckboxWidget", (EgraStyledClass w) {
      import iv.vfs.io; writeln("LEW=", typeid(w).name, "; text=", (cast(CheckboxWidget)w).title.getData);
      //return false;
    });

    forEachSelector("SubWindow > RootWidget :focused", (EgraStyledClass w) {
      import iv.vfs.io; writeln("FOCUSED=", typeid(w).name);
      //return false;
    });

    foreach (HotLabelWidget c; querySelectorAll!HotLabelWidget("SubWindow > RootWidget HotLabelWidget")) {
      import iv.vfs.io;
      writeln("LBL: <", c.text.getData, ">");
    }

    { import iv.vfs.io;
      Widget qf = querySelector(":focused");
      if (qf !is null) writeln("QFOCUSED=", typeid(qf).name); else writeln("FUCK!");
    }
  }
}

// ////////////////////////////////////////////////////////////////////////// //
void initConsole () {
  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    import core.memory : GC;
    conwriteln("starting GC collection...");
    GC.collect();
    GC.minimize();
    conwriteln("GC collection complete.");
  })("gc_collect", "force GC collection cycle");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    auto qww = new YesNoWindow("Quit?", "Do you really want to quit?", true);
    qww.onYes = () { concmd("quit"); };
    qww.addModal();
  })("quit_prompt", "quit with prompt");

  conRegFunc!(() {
    new TitlerWindow("name", "mail", "folder", "title");
  })("window_titler", "titler window test");

  conRegFunc!(() {
    new TagOptionsWindow("xtag");
  })("window_tagoptions", "tag options window test");
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared MainPaneWindow mainPane;


final class MainPaneWindow : SubWindow {
  ProgressBarWidget pbar;
  int tessType = -1;
  AGGTesselation deftess;
  bool timetest;
  int lastMX = -10000, lastMY = -10000;

  this () {
    super(null, GxPoint(0, 0), GxSize(screenWidth, screenHeight));
    mType = Type.OnBottom;

    pbar = new ProgressBarWidget(rootWidget, "progress bar");
    pbar.setMinMax(0, 100);
    pbar.width = clientWidth-64;
    pbar.current = 50;
    pbar.posy = clientHeight-pbar.height-8;
    pbar.posx = (clientWidth-pbar.width)/2;

    deftess = gxagg.tesselator;

    add();
  }

  // //////////////////////////////////////////////////////////////////// //
  override void onPaint () {
    if (closed) return;

    enum guiGroupListWidth = 128;
    enum guiThreadListHeight = 520;

    gxFillRect(0, 0, guiGroupListWidth, screenHeight, getColor("grouplist-back"));
    gxVLine(guiGroupListWidth, 0, screenHeight, getColor("grouplist-divline"));

    gxFillRect(guiGroupListWidth+1, 0, screenWidth, guiThreadListHeight, getColor("threadlist-back"));
    gxHLine(guiGroupListWidth+1, guiThreadListHeight, screenWidth, getColor("threadlist-divline"));

    version(all) {
      import core.stdc.math : roundf;

      if (tessType >= 0) {
        if (tessType > AGGTesselation.max) tessType = AGGTesselation.max;
        gxagg.tesselator = cast(AGGTesselation)tessType;
      } else {
        gxagg.tesselator = deftess;
      }

      gxagg.beginFrame();
      gxagg.params.width = 1.4f;
      //gxagg.params.width = 1.01f;

      float baphx = roundf((screenWidth-gxagg.BaphometDims)*0.5f)+0.5f;
      float baphy = roundf((screenHeight-gxagg.BaphometDims)*0.5f)+0.5f;

      import iv.pxclock;
      ulong estt;
      ubyte hit = 0;
      if (timetest) {
        timetest = false;
        estt = 0;
        foreach (; 0..1000) {
          gxagg.beginFrame();
          immutable tstt = clockMicro();
          gxagg.renderBaphomet(baphx, baphy);
          estt += (clockMicro()-tstt);
        }
        estt /= 1000;
        gxagg.beginFrame();
        gxagg.renderBaphomet(baphx, baphy);
      } else {
        immutable tstt = clockMicro();
        gxagg.renderBaphomet(baphx, baphy);
        estt = clockMicro()-tstt;
      }

      version(none) {
        gxagg
          .moveTo(baphx-1, baphy-1)
          .lineTo(baphx+gxagg.BaphometDims, baphy-1)
          .lineTo(baphx+gxagg.BaphometDims, baphy+gxagg.BaphometDims)
          .lineTo(baphx-1, baphy+gxagg.BaphometDims)
          .lineTo(baphx-1, baphy-1);
      }
      version(all) {
        gxagg.roundedRect(baphx-1, baphy-1, gxagg.BaphometDims+1, gxagg.BaphometDims+1, 16.0f);
      }
      gxagg.stroke();
      //gxagg.contour();
      //gxagg.fill();

      version(none) {
        gxagg
          .beginPath();
          .moveTo(10, 10);
          .lineTo(30, 20);
          .width = 1.0f;
        gxagg.contour();
      }

      hit = gxagg.hitTest(lastMX, lastMY);

      {
        immutable rstt = clockMicro();
        gxagg.render(gxrgba(255, 0, 0, (tessType >= 0 ? 255 : 66)));
        immutable fstt = clockMicro()-rstt;
        import core.stdc.stdio; printf("total Baphomet render microsecs: %u\n", cast(uint)fstt);
      }

      //gxagg.endFrame(gxrgba(255, 0, 0, (tessType >= 0 ? 255 : 66)));
      gxagg.endFrame();

      {
        import std.format : format;
        string s = "tess: %s  level: %d;  mcsecs: %s".format(gxagg.tesselator, gxagg.bezierLimit, estt);
        gxDrawTextUtf(200, 20, s, GxColors.k8orange);
        if (hit) {
          s = "hit: %3s".format(hit);
          gxDrawTextUtf(200, 40, s, GxColors.k8orange);
        }
        enum rxofs = 132;
        enum ryofs = 432;
        enum rwdt = 8;
        foreach (int y; -16..+17) {
          foreach (int x; -16..+17) {
            GxColor c = gxGetPixel(lastMX+x, lastMY+y)|gxAlphaMask;
            int rx = (x+16)*rwdt+rxofs;
            int ry = (y+16)*rwdt+ryofs;
            gxFillRect(rx, ry, rwdt, rwdt, c);
          }
        }
        gxVLine(rxofs+16*rwdt+rwdt/2-1, ryofs, 32*rwdt, GxColors.white);
        gxHLine(rxofs, ryofs+16*rwdt+rwdt/2-1, 32*rwdt, GxColors.white);
      }
    }

    ulong accstt = 0, fstt;

    version(all) {
      gxagg.beginFrame();
      AGGMatrix tmt;
      version(all) {
        tmt
          .rotate(deg2rad(35.0f))
          .translate(20, 100);
        assert(!tmt.isIdentity);
      }
      /*
      tmt
        .translate(0.5f, 0.5f)
        .translate(-160, 0);
      */
      gxagg.withTransform(tmt, {
        gxagg.moveTo(50, 50);
        gxagg.lineTo(60, 60);
        gxagg.moveTo(100, 100);
        gxagg.lineTo(140, 120);
        version(none) {
          // CW (because logical coords are upwards)
          gxagg.moveTo(200, 200);
          gxagg.lineTo(200, 100);
          gxagg.lineTo(100, 100);
          gxagg.lineTo(100, 200);
          gxagg.lineTo(200, 200);
        } else {
          // CCW (because logical coords are upwards)
          gxagg.moveTo(100, 100);
          gxagg.lineTo(200, 100);
          gxagg.lineTo(200, 200);
          gxagg.lineTo(100, 200);
        }
        gxagg.closePoly();
        //gxagg.flipOrientation = true;
        //gxagg.dumpVertices();
      });

      version(all) {
        gxagg.fill();
        fstt = clockMicro();
        gxagg.render(gxrgba(0, 127, 0, 127));
        accstt += clockMicro()-fstt;
      }
      version(all) {
        //gxagg.dumpVertices();
        gxagg.stroke();
        fstt = clockMicro();
        gxagg.render(gxrgba(127, 0, 0, 255));
        accstt += clockMicro()-fstt;
      }

      version(all) {
        gxagg.resetDashes();
        gxagg.addDash(4, 4);
        gxagg.addDash(8, 8);
        gxagg.dashStroke();
        //gxagg.dashContour();
        fstt = clockMicro();
        gxagg.render(gxrgba(255, 255, 0, 255));
        accstt += clockMicro()-fstt;
      }

      version(all) {
        gxagg.paramsContour.width = 6;
        gxagg.contour();
        fstt = clockMicro();
        gxagg.render(gxrgba(255, 255, 255, 255));
        accstt += clockMicro()-fstt;
      }

      version(all) {
        gxagg.flipOrientation = true;
        gxagg.contour();
        fstt = clockMicro();
        gxagg.render(gxrgba(0, 255, 0, 255));
        accstt += clockMicro()-fstt;
        gxagg.flipOrientation = false;
      }

      { import core.stdc.stdio; printf("total rect render microsecs: %u\n", cast(uint)accstt); }

      //gxagg.render(gxrgba(0, 127, 0, 127));

      version(all) {
        AGGVGradient3 grad;
        gxagg
          .beginPath()
          .roundedRect(400.5f, 600.5f, 96, 28, 8)
          .fill()
          //.render(gxRGB!(192, 192, 192));
          /*
          .render(AGGVGradient3(gxagg.rasterMinY, gxRGB!(127, 127, 127),
                                gxagg.rasterMaxY, gxRGB!(142, 142, 142),
                                20, gxRGB!(196, 196, 196)));
          */
          /*
          .render(AGGVGradient3(gxagg.rasterMinY, gxRGB!(255, 0, 0),
                                gxagg.rasterMaxY, gxRGB!(0, 255, 0),
                                20, gxRGB!(0, 0, 255)));
          */
          .render(AGGHGradient(gxagg.rasterMinX, gxRGBA!(255, 0, 0, 127),
                               gxagg.rasterMaxX, gxRGBA!(0, 255, 0, 90)));
      }

      version(all) {
        accstt = 0;
        float[4] bounds;
        dchar dch = '@';
        if (gxFontCharPathBounds(dch, bounds[])) {
          //conwriteln("bounds: (", bounds[0], ",", bounds[1], ")-(", bounds[2], ",", bounds[3], ")");
          float desthgt = 96;
          immutable float gwdt = bounds[2]-bounds[0];
          immutable float ghgt = bounds[3]-bounds[1];
          float scale = desthgt/ghgt;
          AGGMatrix tmx;
          tmx
            .scale(scale, scale)
            .translate(-bounds[0]*scale+100.5f, -bounds[1]*scale+100.5f);
          gxagg.withTransform(tmx, {
            gxagg
              .resetParams()
              .beginPath();
            if (gxFontCharToPath(dch)) {
              gxagg
                //.stroke()
                .fill()
                //.render(GxColors.k8orange);
                .noop();
              fstt = clockMicro();
              gxagg.render(AGGVGradient3(gxagg.rasterMinY, gxRGB!(200, 73, 0),
                                         gxagg.rasterMaxY, gxRGB!(255, 128, 0)));
              accstt += clockMicro()-fstt;
            }
            int emb = cast(int)(4/scale);
            float[4] ebounds;
            gxagg
              .beginPath();
            if (gxFontCharPathEmboldenBounds(dch, emb, ebounds[])) {
              immutable float ewdt = ebounds[2]-ebounds[0];
              immutable float ehgt = ebounds[3]-ebounds[1];
              gxagg.transform.translate(-(ewdt-gwdt)*0.5f*scale, (ehgt-ghgt)*0.5f*scale);
              if (gxFontCharToPathEmbolden(dch, emb)) {
                gxagg
                  .stroke()
                  .noop();
                fstt = clockMicro();
                gxagg.render(GxColors.green);
                accstt += clockMicro()-fstt;
              }
            }
          });
          { import core.stdc.stdio; printf("total glyph render microsecs: %u\n", cast(uint)accstt); }
        }
      }

    static if (is(typeof({immutable string s = import("xbnd.d");}))) {
      mixin(import("xbnd.d"));
      enum hasxbnd = true;
    } else {
      enum hasxbnd = false;
    }

      version(all) {
        float[4] gbounds;
        dchar gdch = '@';
        if (gxFontCharPathBounds(gdch, gbounds[])) {
          float desthgt = 196;
          immutable float gwdt = gbounds[2]-gbounds[0];
          immutable float ghgt = gbounds[3]-gbounds[1];
          float scale = desthgt/ghgt;
          AGGMatrix tmx;
          tmx
            .scale(scale, scale)
            .translate(-bounds[0]*scale+200.5f, -bounds[1]*scale+200.5f);
          gxagg.withTransform(tmx, {
            gxagg.resetParams().beginPath();
            if (gxFontCharToPath(gdch)) {
              //gxagg.flipOrientation = true;
              gxagg.paramsContour.width = 8;
              gxagg.contourFill();
              //gxagg.render(gxrgba(0, 155, 0, 255));
              gxagg.render(gxagg.vgradient(gxrgb(0, 38, 0), gxrgb(0, 98, 0)));
              gxagg.flipOrientation = false;
            }
            gxagg.resetParams().beginPath();
            if (gxFontCharToPath(gdch)) {
              gxagg.fill();
              static if (hasxbnd) {
                gxagg.render(AGGImgFill(0, 0, 32));
              } else {
                gxagg.render(gxagg.vgradient(gxrgb(255, 127, 0), gxrgb(155, 27, 0)));
              }
            }
          });
        }
      }

      static if (hasxbnd) xbnd();

      gxagg.endFrame();
    }

    version(test_round_rect) {
      gxClipReset();
      gxFillRoundedRect(lastMouseX-16, lastMouseY-16, 128, 96, rrad, /*gxSolidWhite*/gxRGBA!(0, 255, 0, 127));
      //gxDrawRoundedRect(lastMouseX-16, lastMouseY-16, 128, 96, rrad, gxRGBA!(0, 255, 0, 127));
    }

    drawWidgets();
  }

  version(test_round_rect) {
    int rrad = 16;
  }

  override bool onKeyBubble (KeyEvent event) {
    if (event.pressed) {
      version(test_round_rect) {
        if (event == "Plus") { ++rrad; return true; }
        if (event == "Minus") { --rrad; return true; }
      }
      if (event == "C-Q") { concmd("quit_prompt"); return true; }
      if (event == "1") { concmd("window_titler"); return true; }
      if (event == "2") { concmd("window_tagoptions"); return true; }
      if (event == "Minus") { pbar.current = pbar.current-1; return true; }
      if (event == "Plus") { pbar.current = pbar.current+1; return true; }

      if (event == "Q") { if (gxagg.bezierLimit > 1) { --gxagg.bezierLimit; widgetChanged(); } return true; }
      if (event == "W") { if (gxagg.bezierLimit < 1000) { ++gxagg.bezierLimit; widgetChanged(); } return true; }

      if (event == "Z") { --tessType; if (tessType < -1) tessType = -1; widgetChanged(); return true; }
      if (event == "X") { ++tessType; widgetChanged(); return true; }

      if (event == "T") { timetest = true; widgetChanged(); return true; }
      //if (dbg_dump_keynames) conwriteln("key: ", event.toStr, ": ", event.modifierState&ModifierState.windows);
      //foreach (const ref kv; mainAppKeyBindings.byKeyValue) if (event == kv.key) concmd(kv.value);
    }
    return false;
  }

  // returning `false` to avoid screen rebuilding by dispatcher
  override bool onMouseBubble (MouseEvent event) {
    if (event.type == MouseEventType.buttonPressed || event.type == MouseEventType.buttonReleased) {
      if (lastMX != event.x || lastMY != event.y) {
        lastMX = event.x;
        lastMY = event.y;
        //widgetChanged();
      }
      widgetChanged();
    } else if (event.modifierState) {
      // for OpenGL, this rebuilds the whole screen anyway
      lastMX = event.x;
      lastMY = event.y;
      widgetChanged();
    }
    return false;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  //egraFontName = "PT Sans:pixelsize=16";

  defaultColorStyle.parseStyle(TestStyle);
  //egraDefaultFontSize = 48;

  glconAllowOpenGLRender = false;

  sdpyWindowClass = "EGRATest";
  //glconShowKey = "M-Grave";

  initConsole();

  conProcessQueue();
  conProcessArgs!true(args);

  egraCreateSystemWindow("EGRA Test", allowResize:false);

  vbwin.addEventListener((QuitEvent evt) {
    if (vbwin.closed) return;
    if (isQuitRequested) { vbwin.close(); return; }
    vbwin.close();
  });

  static if (is(typeof(&vbwin.closeQuery))) {
    vbwin.closeQuery = delegate () { concmd("quit"); egraPostDoConCommands(); };
  }

  mainPane = new MainPaneWindow();
  //egraSkipScreenClear = true; // main pane is fullscreen

  postScreenRebuild();
  repostHideMouse();

  vbwin.eventLoop(1000*10,
    delegate () {
      egraProcessConsole();
    },
    delegate (KeyEvent event) {
      if (egraOnKey(event)) return;
    },
    delegate (MouseEvent event) {
      if (egraOnMouse(event)) return;
    },
    delegate (dchar ch) {
      if (egraOnChar(ch)) return;
    },
  );

  flushGui();
  conProcessQueue(int.max/4);
}
