/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gfx.aggmini.enums;
private:
nothrow @safe @nogc:

private import iv.egra.gfx.lowlevel : gxInterpolateColorI;
private import iv.egra.gfx.base : GxColor, gxTransparent, gxUnknown;


/* Bezier curve rasterizer.
 *
 * De Casteljau Bezier rasterizer is faster, but currently rasterizing curves with cusps sligtly wrong.
 * It doesn't really matter in practice.
 *
 * AFD tesselator is somewhat slower, but does cusps better.
 *
 * McSeem rasterizer should have the best quality, bit it is the slowest method. Basically, you will
 * never notice any visial difference. But it can render something... ahem... sensible with very low
 * recursion levels (as low as 1), while standard De Casteljau produces garbage if its built-in
 * "flatness limit" is not reached. so McSeem tesselator is the default here.
 * On Baphomet test It is slower than De Casteljau by about 100 *micro*seconds. I'm ok with that.
 */
public enum AGGTesselation {
  DeCasteljauMcSeem, // standard well-known tesselation algorithm, with improvements from Maxim Shemanarev; slowest one, but should give best results
  DeCasteljau, // standard well-known tesselation algorithm
  AFD, // adaptive forward differencing (experimental)
}

public enum FillRule {
  NonZero,
  EvenOdd,
}

public enum LineCap {
  Butt,
  Square,
  Round,
}

public enum LineJoin {
  Miter,
  MiterRevert,
  Round,
  Bevel,
  MiterRound,
}

public enum InnerJoin {
  Bevel,
  Miter,
  Jag,
  Round,
}

public enum Winding {
  CW,
  CCW,
}


// drawer params
public struct AGGParams {
private:
  float mWidth = 1.5f;
  float mMiterLimit = 4.0f;
  float mInnerMiterLimit = 1.01f;
  float mApproxScale = 1.0f;
  float mShorten = 0.0f;
  LineCap mLineCap = LineCap.Butt;
  LineJoin mLineJoin = LineJoin.Miter;
  InnerJoin mInnerJoin = InnerJoin.Miter;
  FillRule mFillRule = FillRule.NonZero;
  bool mChanged = false;

  enum PropMixin(string pname, string fldname) =
    `@property void `~pname~` (in typeof(`~fldname~`) v) { pragma(inline, true); mChanged = (mChanged || `~fldname~` != v); `~fldname~` = v; }
     @property typeof(`~fldname~`) `~pname~` () const pure { pragma(inline, true); return `~fldname~`; }`;

nothrow @safe @nogc:
public:
  this() (in auto ref AGGParams other) { pragma(inline, true); opAssign(other); mChanged = true; }

  void opAssign() (in auto ref AGGParams other) @trusted {
    if (&other is &this) return;
    foreach (string mname; __traits(allMembers, typeof(this))) {
      static if (mname != "mChanged" && mname.length > 3 && mname[0] == 'm' && mname[1] >= 'A' && mname[1] <= 'Z') {
        //pragma(msg, mname);
        mixin("if ("~mname~" != other."~mname~") { mChanged = true; "~mname~" = other."~mname~"; }");
      }
    }
  }

  @property bool isDirty () const pure { pragma(inline, true); return mChanged; }
  void setDirty () { pragma(inline, true); mChanged = true; }
  void resetDirty () { pragma(inline, true); mChanged = false; }

  mixin(PropMixin!("lineCap", "mLineCap"));
  mixin(PropMixin!("lineJoin", "mLineJoin"));
  mixin(PropMixin!("innerJoin", "mInnerJoin"));
  mixin(PropMixin!("fillRule", "mFillRule"));

  mixin(PropMixin!("width", "mWidth"));
  mixin(PropMixin!("miterLimit", "mMiterLimit"));
  mixin(PropMixin!("innerMiterLimit", "mInnerMiterLimit"));

  mixin(PropMixin!("approximationScale", "mApproxScale"));

  mixin(PropMixin!("shorten", "mShorten"));

  @property void miterLimitTheta (in float t) {
    pragma(inline, true);
    import core.stdc.math : sinf;
    immutable float v = 1.0f/sinf(t*0.5f);
    mChanged = (mChanged || mMiterLimit != v);
    mMiterLimit = v;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
/*
  vertical gradient interface:
    GxColor colorAtY (int y);

  graident interface:
    // should fill all `cout`
    // `cout` is never of zero length
    void colorSpanAtXY (int x, int y, GxColor[] cout);
*/

public template AGGIsGoodSingleColor(SG) {
  enum AGGIsGoodSingleColor = (__traits(isIntegral, SG) && SG.sizeof == 4);
}

public template AGGIsGoodVerticalGradient(SG) {
  enum AGGIsGoodVerticalGradient =
    is(typeof((inout int=0) nothrow @safe @nogc {
      SG vg = void;
      int y;
      GxColor c = vg.colorAtY(y);
    }));
}

public template AGGIsGoodGradient(SG) {
  enum AGGIsGoodGradient =
    is(typeof((inout int=0) nothrow @safe @nogc {
      SG vg = void;
      int x, y;
      GxColor[2] carr;
      vg.colorSpanAtXY(x, y, carr[]);
    }));
}


public template AGGIsGoodSpanGen(SG) {
  enum AGGIsGoodSpanGen = (AGGIsGoodSingleColor!SG || AGGIsGoodVerticalGradient!SG || AGGIsGoodGradient!SG);
}


// 2 or 3 colors
// 2 if clr[2] is 0
public struct AGGVGradient3 {
private:
  int[3] yp;
  GxColor[3] clr; // 0 is transparent color, what a coincidence!

nothrow @trusted @nogc:
public:
  this (in int y0, in GxColor c0, in int y1, in GxColor c1, in int midpercent=-1, in GxColor midc=gxTransparent) {
    pragma(inline, true);
    set(y0, c0, y1, c1, midpercent, midc);
  }

  int getY (int idx) const pure { return (idx >= 0 && idx < yp.length ? yp[idx] : -666); }
  GxColor getColor (int idx) const pure { return (idx >= 0 && idx < clr.length ? clr[idx] : gxUnknown); }

  void set (in int y0, in GxColor c0, in int y1, in GxColor c1, in int midpercent=-1, in GxColor midc=gxTransparent) {
    if (y0 < y1) {
      yp.ptr[0] = y0;
      clr.ptr[0] = c0;
      yp.ptr[1] = y1;
      clr.ptr[1] = c1;
    } else {
      yp.ptr[0] = y1;
      clr.ptr[0] = c1;
      yp.ptr[1] = y0;
      clr.ptr[1] = c0;
    }
    // no third point yet (i.e. max)
    yp.ptr[2] = yp.ptr[1];
    clr.ptr[2] = clr.ptr[1];
    // check for valid midpoint
    if (midpercent < 0 || midpercent > 100) return;
    // check for min or max midpoint
    if (midpercent == 0) { clr.ptr[0] = midc; return; }
    if (midpercent == 100) { clr.ptr[1] = clr.ptr[2] = midc; return; }
    // add midpoint
    yp.ptr[2] = yp.ptr[1];
    clr.ptr[2] = clr.ptr[1];
    yp.ptr[1] = yp.ptr[0]+(yp.ptr[1]-yp.ptr[0])*midpercent/100;
    clr.ptr[1] = midc;
  }

  GxColor colorAtY (int y) const pure {
    if (y <= yp.ptr[0]) return clr.ptr[0];
    if (y < yp.ptr[1] && yp.ptr[0] != yp.ptr[1]) {
      // scale to [0..65535]
      immutable int t = ((y-yp.ptr[0])<<16)/(yp.ptr[1]-yp.ptr[0]);
      return gxInterpolateColorI(clr.ptr[0], clr.ptr[1], t);
    }
    if (clr.ptr[1] != clr.ptr[2] && y < yp.ptr[2] && yp.ptr[1] != yp.ptr[2]) {
      // scale to [0..65535]
      immutable int t = ((y-yp.ptr[1])<<16)/(yp.ptr[2]-yp.ptr[1]);
      return gxInterpolateColorI(clr.ptr[1], clr.ptr[2], t);
    }
    return clr.ptr[2];
  }
}


public struct AGGHGradient {
private:
  int[2] xp;
  GxColor[2] clr; // 0 is transparent color, what a coincidence!

nothrow @trusted @nogc:
public:
  this (in int x0, in GxColor c0, in int x1, in GxColor c1) {
    pragma(inline, true);
    set(x0, c0, x1, c1);
  }

  void set (in int x0, in GxColor c0, in int x1, in GxColor c1) {
    if (x0 < x1) {
      xp.ptr[0] = x0;
      clr.ptr[0] = c0;
      xp.ptr[1] = x1;
      clr.ptr[1] = c1;
    } else {
      xp.ptr[0] = x1;
      clr.ptr[0] = c1;
      xp.ptr[1] = x0;
      clr.ptr[1] = c0;
    }
  }

  void colorSpanAtXY (int x, int y, GxColor[] cout) {
    --x;
    foreach (ref GxColor c; cout) {
      ++x;
      if (x <= xp.ptr[0]) { c = clr.ptr[0]; continue; }
      if (x >= xp.ptr[1]) { c = clr.ptr[1]; continue; }
      // scale to [0..65535]
      immutable int t = ((x-xp.ptr[0])<<16)/(xp.ptr[1]-xp.ptr[0]);
      c = gxInterpolateColorI(clr.ptr[0], clr.ptr[1], t);
    }
  }
}
