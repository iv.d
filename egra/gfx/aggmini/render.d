/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/* *****************************************************************************
  Anti-Grain Geometry - Version 2.1 Lite
  Copyright (C) 2002-2003 Maxim Shemanarev (McSeem)
  D port, additional work: Ketmar Dark

  Permission to copy, use, modify, sell and distribute this software
  is granted provided this copyright notice appears in all copies.
  This software is provided "as is" without express or implied
  warranty, and with no claim as to its suitability for any purpose.

  The author gratefully acknowleges the support of David Turner,
  Robert Wilhelm, and Werner Lemberg - the authors of the FreeType
  libray - in producing this work. See http://www.freetype.org for details.

  Initially the rendering algorithm was designed by David Turner and the
  other authors of the FreeType library - see the above notice. I nearly
  created a similar renderer, but still I was far from David's work.
  I completely redesigned the original code and adapted it for Anti-Grain
  ideas. Two functions - renderLine and renderScanLine are the core of
  the algorithm - they calculate the exact coverage of each pixel cell
  of the polygon. I left these functions almost as is, because there's
  no way to improve the perfection - hats off to David and his group!

  All other code is very different from the original.
***************************************************************************** */
module iv.egra.gfx.aggmini.render;
private:
nothrow @safe @nogc:

private import iv.egra.gfx.aggmini : DisableCopyingMixin;
private import iv.egra.gfx.aggmini.enums;

import iv.bclamp;
import iv.egra.gfx.base;
import iv.egra.gfx.lowlevel;


// ////////////////////////////////////////////////////////////////////////// //
/* These constants determine the subpixel accuracy, to be more precise,
  the number of bits of the fractional part of the coordinates.
  The possible coordinate capacity in bits can be calculated by formula:
  sizeof(int) * 8 - PolyBaseShift * 2, i.e, for 32-bit integers and
  8-bits fractional part the capacity is 16 bits or [-32768...32767].
*/
enum : uint {
  PolyBaseShift = 8U,
  PolyBaseSize = cast(uint)(1<<PolyBaseShift),
  PolyBaseMask = cast(uint)(PolyBaseSize-1),
}


int polyCoord (in float c) pure nothrow @safe @nogc { pragma(inline, true); return (cast(int)(c*(PolyBaseSize*2.0f))+1)/2; }

//int dbl2fix (in float c) pure nothrow @safe @nogc { pragma(inline, true); return cast(int)(c*PolyBaseSize); }
//float fix2dbl (in int c) pure nothrow @safe @nogc { pragma(inline, true); return cast(float)c/cast(float)PolyBaseSize; }


// ////////////////////////////////////////////////////////////////////////// //
// a pixel cell
align(1) struct Cell {
align(1):
public:
  short x, y;
  int cover;
  int area;

  static assert(x.sizeof == 2 && y.sizeof == 2, "oops");
  static assert(y.offsetof == x.offsetof+2, "oops");

public nothrow @trusted @nogc:
  this (in int cx, in int cy, in int c, in int a) pure {
    pragma(inline, true);
    x = cast(short)cx;
    y = cast(short)cy;
    cover = c;
    area = a;
  }

  /*
  bool isEqual (in int cx, in int cy) pure const {
    pragma(inline, true);
    return (((cast(short)cx)^x)|((cast(short)cy)^y)) == 0;
  }
  */

  uint getPackedCoord () const pure { pragma(inline, true); return *(cast(const uint*)&x); }

  bool isLessThan (in ref Cell other) const pure {
    pragma(inline, true);
    return (y < other.y || (y == other.y && x < other.x));
  }

  void setCover (in int c, in int a) { pragma(inline, true); cover = c; area = a; }
  void addCover (in int c, in int a) { pragma(inline, true); cover += c; area += a; }

  void set (in int cx, in int cy, in int c, in int a) {
    pragma(inline, true);
    x = cast(short)cx;
    if (cast(int)x != cx) { x = (cx < 0 ? short.min+2 : short.max-2); }
    y = cast(short)cy;
    if (cast(int)y != cy) { y = (cy < 0 ? short.min+2 : short.max-2); }
    cover = c;
    area = a;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// an internal class that implements the main rasterization algorithm.
// used in the rasterizer. should not be used direcly.
// this clips by vertical framebuffer extents.
// we are rendering scanline by scanline, so this is safe.
// k8: alas, clipping by horizontal extents is harder due to polygon filling.
// k8: i can limit coords in `addCurCell()`, but the cells still should be kept.
// k8: this is because i don't fully understand how the whole thing works.
struct Outline {
private nothrow @trusted @nogc:
  enum : uint {
    CellBlockShift = 12U,
    CellBlockSize = cast(uint)(1<<CellBlockShift),
    CellBlockMask = cast(uint)(CellBlockSize-1),
    CellBlockPool = 256U,
    CellBlockLimit = 1024U,
  }

  enum QSortThreshold = 9;

  enum : uint {
    NotClosed = 1U,
    SortRequired = 2U,
  }

private:
  uint mNumBlocks;
  uint mMaxBlocks;
  uint mCurBlock;
  uint mNumCells;
  Cell** mCells;
  Cell* mCurCellPtr;
  Cell** mSortedCells;
  uint mSortedSize;
  Cell mCurCell = Cell(0x7fff, 0x7fff, 0, 0);
  int mCurX;
  int mCurY;
  int mCloseX;
  int mCloseY;
  int mMinX = 0x7fffffff;
  int mMinY = 0x7fffffff;
  int mMaxX = -0x7fffffff;
  int mMaxY = -0x7fffffff;
  int mLimitY;
  uint mFlags = SortRequired;

  static struct SortedY {
    uint start;
    uint num;
  }

  SortedY* mSortedY = null;
  uint mSortedYAlloted = 0;

private:
  void allocateBlock () {
    import core.stdc.stdlib : realloc;
    if (mCurBlock >= mNumBlocks) {
      import core.stdc.string : memset;
      if (mNumBlocks >= mMaxBlocks) {
        Cell** newCells = cast(Cell**)realloc(mCells, (mMaxBlocks+CellBlockPool)*(Cell*).sizeof);
        if (newCells is null) assert(0, "out of memory");
        mCells = newCells;
        mMaxBlocks += CellBlockPool;
      }
      auto cc = cast(Cell*)realloc(null, Cell.sizeof*CellBlockSize);
      if (cc is null) assert(0, "out of memory");
      memset(cc, 0, Cell.sizeof*CellBlockSize);
      mCells[mNumBlocks++] = cc;
    }
    mCurCellPtr = mCells[mCurBlock++];
  }

public:
  mixin(DisableCopyingMixin);

  ~this () {
    import core.stdc.stdlib : free;
    if (mSortedCells !is null) free(mSortedCells);
    if (mSortedY !is null) free(mSortedY);
    if (mNumBlocks) {
      Cell** ptr = mCells+mNumBlocks-1;
      while (mNumBlocks--) {
        free(*ptr);
        --ptr;
      }
      free(mCells);
    }
  }

  void reset () {
    mNumCells = mCurBlock = 0;
    mCurCell.set(0x7fff, 0x7fff, 0, 0);
    //mFlags |= SortRequired;
    //mFlags &= ~NotClosed;
    mFlags = SortRequired;
    mMinX = mMinY = +0x7fffffff;
    mMaxX = mMaxY = -0x7fffffff;
    mLimitY = VBufHeight;
  }

  // fixed point
  void moveTo (in int x, in int y) {
    if ((mFlags&SortRequired) == 0) reset();
    if (mFlags&NotClosed) lineTo(mCloseX, mCloseY);
    setCurCell(x>>PolyBaseShift, y>>PolyBaseShift);
    mCloseX = mCurX = x;
    mCloseY = mCurY = y;
  }

  // fixed point
  void lineTo (in int x, in int y) {
    if ((mFlags&SortRequired) && ((mCurX^x)|(mCurY^y))) {
      renderLine(mCurX, mCurY, x, y);
      mCurX = x;
      mCurY = y;
      mFlags |= NotClosed;
    }
  }

  @property float currX () const pure { pragma(inline, true); return cast(float)mCurX/cast(float)PolyBaseSize; }
  @property float currY () const pure { pragma(inline, true); return cast(float)mCurY/cast(float)PolyBaseSize; }

  @property int minX () const pure { pragma(inline, true); return (mMinX <= mMaxX ? mMinX : 0); }
  @property int minY () const pure { pragma(inline, true); return (mMinY <= mMaxY ? mMinY : 0); }
  @property int maxX () const pure { pragma(inline, true); return (mMinX <= mMaxX ? mMaxX : 0); }
  @property int maxY () const pure { pragma(inline, true); return (mMinY <= mMaxY ? mMaxY : 0); }

  @property uint numCells () const pure { pragma(inline, true); return mNumCells; }

  void prepareCells () {
    if (mFlags&NotClosed) {
      lineTo(mCloseX, mCloseY);
      mFlags &= ~NotClosed;
    }
    // perform sort only the first time
    if (mFlags&SortRequired) {
      addCurCell();
      if (mNumCells != 0) sortCells();
      mFlags &= ~SortRequired;
    }
  }

  const(Cell)** cells () {
    prepareCells();
    return cast(const(Cell)**)mSortedCells;
  }

  // should be called after `prepareCells()`!
  const(Cell)** getCellsForY (in int y) {
    prepareCells();
    if (mNumCells == 0) return null;
    if (y < mMinY || y > mMaxY) return null;
    const(SortedY)* cy = &mSortedY[y-mMinY];
    if (cy.num == 0) return null;
    return cast(const(Cell)**)(mSortedCells+cy.start);
  }

  // should be called after `prepareCells()`!
  // returns first cell where `cell.y >= y`
  const(Cell)** getCellsForYOrMore (int y) {
    prepareCells();
    if (mNumCells == 0) return null;
    if (y > mSortedCells[mNumCells-1].y) return null;
    if (y <= mSortedCells[0].y) return cast(const(Cell)**)mSortedCells;
    assert(y >= mMinY);
    const(SortedY)* cy = &mSortedY[y-mMinY];
    while (y++ <= mMaxY) {
      if (cy.num) return cast(const(Cell)**)(mSortedCells+cy.start);
      ++cy;
    }
    return null;
  }

private:
  void fixMinMaxX (int x) {
    pragma(inline, true);
    //if (x < 0) x = 0; else if (x >= VBufWidth) x = VBufWidth-1;
    if (x < mMinX) mMinX = x;
    if (x+1 > mMaxX) mMaxX = x+1;
  }

  void fixMinMaxY (int y) {
    pragma(inline, true);
    if (y < 0) y = 0; else if (y >= mLimitY) y = mLimitY-1;
    if (y < mMinY) mMinY = y;
    if (y+1 > mMaxY) mMaxY = y+1;
  }

  // this clips by vertical framebuffer extents
  // we are rendering scanline by scanline, so this is safe
  // just don't forget that `fixMinMaxY()` should perform clipping too
  void addCurCell () {
    if (mCurCell.area|mCurCell.cover) {
      if (mCurCell.y >= 0 && mCurCell.y < mLimitY) {
        if ((mNumCells&CellBlockMask) == 0) {
          if (mNumBlocks >= CellBlockLimit) return;
          allocateBlock();
        }
        *mCurCellPtr++ = mCurCell;
        ++mNumCells;
      }
      mCurCell.area = mCurCell.cover = 0;
    }
  }

  void setCurCell (in int x, in int y) {
    if (/*!mCurCell.isEqual(x, y)*/(((cast(short)mCurCell.x)^x)|((cast(short)mCurCell.y)^y))) {
      addCurCell();
      mCurCell.set(x, y, 0, 0);
    }
  }

  // `iy`: integral; other: fixed point
  void renderHorizLine (in int iy, in int x1, int y1, in int x2, in int y2) {
    immutable int ix2 = x2>>PolyBaseShift;

    // trivial case; happens often
    if (y1 == y2) {
      setCurCell(ix2, iy);
      return;
    }

    int ix1 = x1>>PolyBaseShift;
    immutable int fx1 = x1&PolyBaseMask;
    immutable int fx2 = x2&PolyBaseMask;

    // everything is located in a single cell: that is easy!
    if (ix1 == ix2) {
      immutable int delta = y2-y1;
      mCurCell.addCover(delta, (fx1+fx2)*delta);
      return;
    }

    // ok, we'll have to render a run of adjacent cells on the same scanline...
    int p = (PolyBaseSize-fx1)*(y2-y1);
    int first = PolyBaseSize;
    int incr = 1;

    int dx = x2-x1;

    if (dx < 0) {
      p = fx1*(y2-y1);
      first = 0;
      incr = -1;
      dx = -dx;
    }

    int delta = p/dx;
    int mod = p%dx;
    if (mod < 0) { --delta; mod += dx; }

    mCurCell.addCover(delta, (fx1+first)*delta);

    ix1 += incr;
    setCurCell(ix1, iy);
    y1 += delta;

    if (ix1 != ix2) {
      p = PolyBaseSize*(y2-y1+delta);
      int lift = p/dx;
      int rem = p%dx;
      if (rem < 0) { --lift; rem += dx; }
      mod -= dx;
      while (ix1 != ix2) {
        delta = lift;
        mod += rem;
        if (mod >= 0) { mod -= dx; ++delta; }
        mCurCell.addCover(delta, PolyBaseSize*delta);
        y1 += delta;
        ix1 += incr;
        setCurCell(ix1, iy);
      }
    }

    delta = y2-y1;
    mCurCell.addCover(delta, (fx2+PolyBaseSize-first)*delta);
  }

  // fixed point
  void renderLine (in int x1, in int y1, in int x2, in int y2) {
    int iy1 = y1>>PolyBaseShift;
    immutable int iy2 = y2>>PolyBaseShift;

    fixMinMaxX(x1>>PolyBaseShift);
    fixMinMaxX(x2>>PolyBaseShift);

    fixMinMaxY(iy1);
    fixMinMaxY(iy2);

    immutable int fy1 = y1&PolyBaseMask;
    immutable int fy2 = y2&PolyBaseMask;

    // everything is on a single scanline
    if (iy1 == iy2) {
      renderHorizLine(iy1, x1, fy1, x2, fy2);
      return;
    }

    int dx = x2-x1;
    int dy = y2-y1;

    // vertical line: we have to calculate start and end cells,
    // and then the common values of the area and coverage for
    // all cells of the line. we know exactly there's only one
    // cell, so, we don't have to call renderHorizLine().
    int incr = 1;
    if (dx == 0) {
      int ix = x1>>PolyBaseShift;
      int twoFx = (x1-(ix<<PolyBaseShift))<<1;
      int area;

      int first = PolyBaseSize;
      if (dy < 0) { first = 0; incr = -1; }

      immutable int xFrom = x1;

      //renderHorizLine(ey1, xFrom, fy1, xFrom, first);
      int delta = first-fy1;
      mCurCell.addCover(delta, twoFx*delta);

      iy1 += incr;
      setCurCell(ix, iy1);

      delta = first+first-PolyBaseSize;
      area = twoFx*delta;
      while (iy1 != iy2) {
        //renderHorizLine(ey1, xFrom, PolyBaseSize-first, xFrom, first);
        mCurCell.setCover(delta, area);
        iy1 += incr;
        setCurCell(ix, iy1);
      }
      //renderHorizLine(ey1, xFrom, PolyBaseSize-first, xFrom, fy2);
      delta = fy2-PolyBaseSize+first;
      mCurCell.addCover(delta, twoFx*delta);
      return;
    }

    // ok, we have to render several scanlines
    int p = (PolyBaseSize-fy1)*dx;
    int first = PolyBaseSize;

    if (dy < 0) {
      p = fy1*dx;
      first = 0;
      incr = -1;
      dy = -dy;
    }

    int delta = p/dy;
    int mod = p%dy;

    if (mod < 0) { --delta; mod += dy; }

    int xFrom = x1+delta;
    renderHorizLine(iy1, x1, fy1, xFrom, first);

    iy1 += incr;
    setCurCell(xFrom>>PolyBaseShift, iy1);

    if (iy1 != iy2) {
      p = PolyBaseSize*dx;
      int lift = p/dy;
      int rem = p%dy;

      if (rem < 0) { --lift; rem += dy; }
      mod -= dy;

      while (iy1 != iy2) {
        delta = lift;
        mod += rem;
        if (mod >= 0) { mod -= dy; ++delta; }

        immutable int xTo = xFrom+delta;
        renderHorizLine(iy1, xFrom, PolyBaseSize-first, xTo, first);
        xFrom = xTo;

        iy1 += incr;
        setCurCell(xFrom>>PolyBaseShift, iy1);
      }
    }

    renderHorizLine(iy1, xFrom, PolyBaseSize-first, x2, fy2);
  }

private:
  static void qsortCells (Cell** start, uint num) {
    // two macros
    static enum swapCellsMixin(string a, string b) = `
      { auto temp_ = *(`~a~`);
      *(`~a~`) = *(`~b~`);
      *(`~b~`) = temp_; }
    `;

    //static enum lessThanMixin(string a, string b) = `(*(`~a~`)).isLessThan(**(`~b~`))`;
    // y is guaranteed to be the same
    static enum lessThanMixin(string a, string b) = `((*(`~a~`)).x < (*(`~b~`)).x)`;

    Cell**[80] stack = void;
    Cell*** top;
    Cell** limit;
    Cell** base;

    limit = start+num;
    base = start;
    top = stack.ptr;

    for (;;) {
      immutable int len = cast(int)(limit-base);

      if (len > QSortThreshold) {
        // we use base + len/2 as the pivot
        //auto pivot = base+len/2;
        auto pivot = base+(len>>1);
        mixin(swapCellsMixin!("base", "pivot"));

        auto i = base+1;
        auto j = limit-1;

        // now ensure that *i <= *base <= *j
        if (mixin(lessThanMixin!("j", "i"))) mixin(swapCellsMixin!("i", "j"));
        if (mixin(lessThanMixin!("base", "i"))) mixin(swapCellsMixin!("base", "i"));
        if (mixin(lessThanMixin!("j", "base"))) mixin(swapCellsMixin!("base", "j"));

        for (;;) {
          do { ++i; } while (mixin(lessThanMixin!("i", "base")));
          do { --j; } while (mixin(lessThanMixin!("base", "j")));
          if (i > j) break;
          mixin(swapCellsMixin!("i", "j"));
        }

        mixin(swapCellsMixin!("base", "j"));

        // now, push the largest sub-array
        if (j-base > limit-i) {
          top[0] = base;
          top[1] = j;
          base = i;
        } else {
          top[0] = i;
          top[1] = limit;
          limit = j;
        }
        top += 2;
      } else {
        // the sub-array is small, perform insertion sort
        auto j = base;
        auto i = j+1;
        for (; i < limit; j = i, ++i) {
          for (; mixin(lessThanMixin!("j+1", "j")); --j) {
            mixin(swapCellsMixin!("j+1", "j"));
            if (j == base) break;
          }
        }
        if (top > stack.ptr) {
          top -= 2;
          base = top[0];
          limit = top[1];
        } else {
          break;
        }
      }
    }
  }

  void sortCells () {
    if (mNumCells == 0) return;

    if (mNumCells >= mSortedSize) {
      import core.stdc.stdlib: realloc;
      mSortedSize = (mNumCells|0x3ff)+1;
      mSortedCells = cast(Cell**)realloc(mSortedCells, mSortedSize*(Cell*).sizeof);
      if (mSortedCells is null) assert(0, "out of memory");
    }

    // allocate and zero the Y array
    immutable uint mSortedYSize = mMaxY-mMinY+1;
    assert(mSortedYSize > 0);
    if (mSortedYSize >= mSortedYAlloted) {
      import core.stdc.stdlib: realloc;
      mSortedYAlloted = (mSortedYSize|0xff)+1;
      mSortedY = cast(SortedY*)realloc(mSortedY, mSortedYAlloted*SortedY.sizeof);
      if (mSortedY is null) assert(0, "out of memory");
    }
    {
      import core.stdc.string : memset;
      memset(mSortedY, 0, mSortedYSize*SortedY.sizeof);
    }

    // create the Y-histogram (count the numbers of cells for each Y)
    Cell** blockPtr = mCells;
    Cell* cellPtr = void;
    uint i = void;

    uint nb = mNumCells>>CellBlockShift;
    while (nb--) {
      cellPtr = *blockPtr++;
      i = CellBlockSize;
      while (i--) {
        mSortedY[cellPtr.y-mMinY].start++;
        ++cellPtr;
      }
    }

    cellPtr = *blockPtr++;
    i = mNumCells&CellBlockMask;
    while (i--) {
      mSortedY[cellPtr.y-mMinY].start++;
      ++cellPtr;
    }

    // convert the Y-histogram into the array of starting indexes
    uint start = 0;
    for (i = 0; i < mSortedYSize; ++i) {
      immutable uint v = mSortedY[i].start;
      mSortedY[i].start = start;
      start += v;
    }

    // fill the cell pointer array sorted by Y
    blockPtr = mCells;
    nb = mNumCells>>CellBlockShift;
    while (nb--) {
      cellPtr = *blockPtr++;
      i = CellBlockSize;
      while (i--) {
        assert(cellPtr.y-mMinY >= 0 && cellPtr.y-mMinY < mSortedYSize);
        SortedY* cy = &mSortedY[cellPtr.y-mMinY];
        mSortedCells[cy.start+(cy.num++)] = cellPtr++;
      }
    }

    cellPtr = *blockPtr++;
    i = mNumCells&CellBlockMask;
    while (i--) {
      assert(cellPtr.y-mMinY >= 0 && cellPtr.y-mMinY < mSortedYSize);
      SortedY* cy = &mSortedY[cellPtr.y-mMinY];
      mSortedCells[cy.start+(cy.num++)] = cellPtr++;
    }
    mSortedCells[mNumCells] = null; // we need this

    // finally arrange the X-arrays
    for (i = 0; i < mSortedYSize; ++i) {
      const(SortedY)* cy = &mSortedY[i];
      if (cy.num) qsortCells(mSortedCells+cy.start, cy.num);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// this also clips to horizontal framebuffer extents
struct ScanLine {
private:
  //int mMinX;
  int mMaxX; // working area is [0..mMaxX) (i.e. `mMaxX` is not included)
  uint mMaxLen;
  int mLastX = /*0x7fff*/-666;
  ubyte* mCovers;
  ubyte** mStartPtrs;
  ushort* mCounts;
  uint mNumSpans;
  ubyte** mCurStartPtr;
  ushort* mCurCount;

public:
  static struct Iterator {
  private:
    const(ubyte)* mCovers;
    const(ushort)* mCurCount;
    const(ubyte*)* mCurStartPtr;

  public nothrow @trusted @nogc:
    mixin(DisableCopyingMixin);

    this (in ref ScanLine sl) pure {
      pragma(inline, true);
      mCovers = sl.mCovers;
      mCurCount = sl.mCounts;
      mCurStartPtr = sl.mStartPtrs;
    }

    int next () {
      pragma(inline, true);
      ++mCurCount;
      ++mCurStartPtr;
      return cast(int)(*mCurStartPtr-mCovers);
    }

    @property int pixelCount () const pure { pragma(inline, true); return cast(int)(*mCurCount); }
    @property const(ubyte)* covers () const pure { pragma(inline, true); return *mCurStartPtr; }
  }

public nothrow @trusted @nogc:
  mixin(DisableCopyingMixin);

  ~this () {
    import core.stdc.stdlib : free;
    if (mCounts !is null) free(mCounts);
    if (mStartPtrs !is null) free(mStartPtrs);
    if (mCovers !is null) free(mCovers);
  }

  auto iterator () const pure { pragma(inline, true); return Iterator(this); }

  void reset (/*int minX, int maxX*/) {
    //immutable uint maxLen = ((minX <= maxX ? maxX-minX+2 : 2)|0x7ff)+1;
    mMaxX = VBufWidth;
    if (mMaxX < 0) mMaxX = 0;
    immutable uint maxLen = (mMaxX|0xff)+1;
    if (maxLen > mMaxLen) {
      import core.stdc.stdlib : realloc;
      mCovers = cast(ubyte*)realloc(mCovers, maxLen*mCovers[0].sizeof);
      if (mCovers is null) assert(0, "out of memory");
      mStartPtrs = cast(ubyte**)realloc(mStartPtrs, mStartPtrs[0].sizeof*maxLen);
      if (mStartPtrs is null) assert(0, "out of memory");
      mCounts = cast(ushort*)realloc(mCounts, mCounts[0].sizeof*maxLen);
      if (mCounts is null) assert(0, "out of memory");
      mMaxLen = maxLen;
    }
    resetSpans();
    //mMinX = minX;
  }

  void resetSpans () {
    pragma(inline, true);
    mLastX = /*0x7fff*/-666;
    mCurCount = mCounts;
    mCurStartPtr = mStartPtrs;
    mNumSpans = 0;
  }

  // WARNING! it really works only with [-32768..32767] range!
  // luckily, that's the max range of the canvas.
  // `num` should be positive! (zeroes are not allowed)
  void addSpan (int x, int num, ubyte cover) {
    import core.stdc.string : memset;
    if (x >= mMaxX) return; // out of bounds
    if (x < 0) {
      if ((num += x) <= 0) return; // out of bounds
      x = 0;
    } else if (x+num > mMaxX) {
      num = mMaxX-x;
    }
    //x -= mMinX;
    memset(mCovers+x, cover, cast(ushort)num);
    if (x == mLastX+1) {
      (*mCurCount) += cast(ushort)num;
    } else {
      *++mCurCount = cast(ushort)num;
      *++mCurStartPtr = mCovers+x;
      ++mNumSpans;
    }
    mLastX = x+num-1;
  }

  void addCell (int x, ubyte cover) {
    if (x < 0 || x >= mMaxX) return; // out of bounds
    //x -= mMinX;
    mCovers[x] = cast(ubyte)cover;
    if (x == mLastX+1) {
      ++(*mCurCount);
    } else {
      *++mCurCount = 1;
      *++mCurStartPtr = mCovers+x;
      ++mNumSpans;
    }
    mLastX = x;
  }

  //@property int baseX () const pure { pragma(inline, true); return mMinX; }
  enum baseX = 0;
  @property uint spanCount () const pure { pragma(inline, true); return mNumSpans; }
}


// ////////////////////////////////////////////////////////////////////////// //
/* Polygon rasterizer that is used to render filled polygons with
  high-quality Anti-Aliasing. Internally, by default, the class uses
  integer coordinates in format 24.8, i.e. 24 bits for integer part
  and 8 bits for fractional - see PolyBaseShift. This class can be
  used in the following  way:

  1. fillRule = FillRule.EvenOdd; // optional
  2. gamma() - optional.
  3. reset()
  4. moveTo(x, y) / lineTo(x, y) - make the polygon. One can create
     more than one contour, but each contour must consist of at least 3
     vertices, i.e. moveTo(x1, y1); lineTo(x2, y2); lineTo(x3, y3);
     is the absolute minimum of vertices that define a triangle.
     The algorithm does not check either the number of vertices nor
     coincidence of their coordinates, but in the worst case it just
     won't draw anything.
     The orger of the vertices (clockwise or counterclockwise)
     is important when using the non-zero filling rule (FillNonZero).
     In this case the vertex order of all the contours must be the same
     if you want your intersecting polygons to be without "holes".
     You actually can use different vertices order. If the contours do not
     intersect each other the order is not important anyway. If they do,
     contours with the same vertex order will be rendered without "holes"
     while the intersecting contours with different orders will have "holes".

  fillRule() and gamma() can be called anytime before "sweeping".
*/
public struct Rasterizer {
public nothrow @trusted @nogc:
  enum {
    AAShift = 8,
    AANum = 1<<AAShift,
    AAMask = AANum-1,
    AA2Num = AANum<<1,
    AA2Mask = AA2Num-1,
  }

private:
  Outline mOutline;
  ScanLine mScanline;
  FillRule mFillingRule = FillRule.NonZero;
  ubyte[256] mGamma = DefaultGamma[];

  // for gradient spans
  GxColor* xsg = null;
  uint xsgAllot = 0;

private:
  void ensureXSG (uint size) {
    if (!size) return;
    if (size > 0x00ff_ffffU) assert(0, "out of memory");
    size = (size|0xffU)+1U;
    if (xsgAllot < size) {
      import core.stdc.stdlib : realloc;
      GxColor* nc = cast(GxColor*)realloc(xsg, size*GxColor.sizeof);
      if (nc is null) assert(0, "out of memory");
      xsg = nc;
      xsgAllot = size;
    }
  }

public:
  static void renderSL (in ref ScanLine sl, in int y, in GxColor clr, in ref GxRect clipRect) {
    uint spanCount = sl.spanCount;
    immutable int baseX = sl.baseX;
    uint* row = vglTexBuf+cast(uint)y*cast(uint)VBufWidth;
    auto span = sl.iterator;
    do {
      int x = span.next+baseX;
      int pixelCount = span.pixelCount;
      int leftSkip = void;
      if (clipRect.clipHStripe(ref x, y, ref pixelCount, &leftSkip)) {
        const(ubyte)* covers = span.covers+leftSkip;
        memBlendColorCoverage(row+x, covers, clr, pixelCount);
      }
    } while (--spanCount);
  }

  //FIXME: this is SLOW!
  void renderSLGrad(SG) (in ref ScanLine sl, in int y, auto ref SG spangen, in ref GxRect clipRect) if (AGGIsGoodGradient!SG) {
    uint spanCount = sl.spanCount;
    immutable int baseX = sl.baseX;
    uint* row = vglTexBuf+cast(uint)y*cast(uint)VBufWidth;
    auto span = sl.iterator;
    do {
      int x = span.next+baseX;
      int pixelCount = span.pixelCount;
      int leftSkip = void;
      if (clipRect.clipHStripe(ref x, y, ref pixelCount, &leftSkip)) {
        const(ubyte)* covers = span.covers+leftSkip;
        ensureXSG(cast(uint)pixelCount);
        spangen.colorSpanAtXY(x, y, xsg[0..cast(uint)pixelCount]);
        //memBlendColorCoverage(row+x, covers, clr, pixelCount);
        version(all) {
          // this is slightly faster than the naive code below
          uint* dp = row+x;
          foreach (GxColor clr; xsg[0..cast(uint)pixelCount]) {
            immutable uint cvr = *covers++;
            if (!cvr || !(clr&gxAlphaMask)) { ++dp; continue; } // nothing to do
            if (cvr == 255 && (clr&gxAlphaMask) == gxAlphaMask) { *dp++ = clr; continue; } // opaque
            // need to mix
            immutable uint a_ = (((clr>>24)*(cvr+1U))>>8)+1u; // to not loose bits
            uint rb_ = (*dp)&0xff00ffu;
            uint g_  = (*dp)&0x00ff00u;
            rb_ += (((clr&0xff00ffu)-rb_)*a_)>>8;
            g_  += (((clr&0x00ff00u)-g_)*a_)>>8;
            *dp++ = (rb_&0xff00ffu)|(g_&0xff_00ff00u)|gxAlphaMask;
          }
        } else {
          ubyte* p = cast(ubyte*)(row+x);
          foreach (GxColor clr; xsg[0..cast(uint)pixelCount]) {
            immutable uint cb = clr&0xff;
            immutable uint cg = (clr>>8)&0xff;
            immutable uint cr = (clr>>16)&0xff;
            immutable uint ca = clr>>24;
            immutable uint cvr = *covers++;
            if (!cvr) { p += 4; continue; }
            immutable uint alpha = ca*(cvr+1); // to not loose bits
            uint v = *p; *p++ = cast(ubyte)((((cb-v)*alpha)>>16)+v);
                 v = *p; *p++ = cast(ubyte)((((cg-v)*alpha)>>16)+v);
                 v = *p; *p++ = cast(ubyte)((((cr-v)*alpha)>>16)+v);
            *p++ = 0xff;
          }
        }
      }
    } while (--spanCount);
  }

private:
  ubyte calculateAlpha (in int area) const pure {
    int cover = area>>(PolyBaseShift*2+1-AAShift);
    if (cover < 0) cover = -cover;
    if (mFillingRule == FillRule.EvenOdd) {
      cover &= AA2Mask;
      if (cover > AANum) cover = AA2Num-cover;
    }
    //if (cover > AAMask) cover = AAMask;
    //return cover;
    return (cover < AAMask ? cast(ubyte)cover : cast(ubyte)AAMask);
  }

public:
  mixin(DisableCopyingMixin);

  ~this () {
    if (xsg !is null) {
      import core.stdc.stdlib : free;
      free(xsg);
      xsg = null;
      xsgAllot = 0;
    }
  }

  void reset () { mOutline.reset(); }

  @property FillRule fillRule () const pure { pragma(inline, true); return mFillingRule; }
  @property void fillRule (in FillRule v) { pragma(inline, true); mFillingRule = v; }

  void gamma (in float g) {
    foreach (immutable uint i; 0..256) {
      //import std.math : pow;
      import core.stdc.math : powf;
      mGamma.ptr[i] = cast(ubyte)(powf(cast(float)i/255.0f, g)*255.0f);
    }
  }

  void gamma (const(ubyte)[] g) {
    if (g.length != 256) assert(0, "invalid gamma array");
    mGamma[] = g[0..256];
  }

  @property float currX () const pure { pragma(inline, true); return mOutline.currX; }
  @property float currY () const pure { pragma(inline, true); return mOutline.currY; }

  void moveToFixed (int x, int y) {
    if ((x>>PolyBaseShift) <= short.min+8 || (x>>PolyBaseShift) >= short.max-8 ||
        (y>>PolyBaseShift) <= short.min+8 || (y>>PolyBaseShift) >= short.max-8) assert(0, "coordinates out of bounds");
    mOutline.moveTo(x, y);
  }

  void lineToFixed (int x, int y) {
    if ((x>>PolyBaseShift) <= short.min+8 || (x>>PolyBaseShift) >= short.max-8 ||
        (y>>PolyBaseShift) <= short.min+8 || (y>>PolyBaseShift) >= short.max-8) assert(0, "coordinates out of bounds");
    mOutline.lineTo(x, y);
  }

  void moveTo (in float x, in float y) { mOutline.moveTo(polyCoord(x), polyCoord(y)); }
  void lineTo (in float x, in float y) { mOutline.lineTo(polyCoord(x), polyCoord(y)); }

  @property int minX () const pure { pragma(inline, true); return mOutline.minX; }
  @property int minY () const pure { pragma(inline, true); return mOutline.minY; }
  @property int maxX () const pure { pragma(inline, true); return mOutline.maxX; }
  @property int maxY () const pure { pragma(inline, true); return mOutline.maxY; }


  //WARNING! `clipRect` should never go out of framebuffer bounds!
  void render(SG) (in ref GxRect clipRect, auto ref SG spangen) if (AGGIsGoodSpanGen!SG) {
    if (clipRect.empty) return;

    immutable int y0 = clipRect.y0;
    immutable int y1 = clipRect.y1;
    immutable int x1 = clipRect.x1;

    //const(Cell)** cells = mOutline.cells();
    //if (mOutline.numCells() == 0) return;
    const(Cell)** cells = mOutline.getCellsForYOrMore(y0);
    if (cells is null || *cells is null) return;
    if ((*cells).y > y1) return;

    if (!clipRect.overlaps(GxRect(mOutline.minX, mOutline.minY, mOutline.maxX-mOutline.minX+1, mOutline.maxY-mOutline.minY+1))) return;

    static if (AGGIsGoodSingleColor!SG) {
      immutable GxColor sc = spangen;
    }

    mScanline.reset(/*mOutline.minX, mOutline.maxX*/);

    /*
      k8: this code resets `cover` on each new scanline. this is what AGG does.
      the original code accumulated `cover` through the whole cell array. why?
     */
    const(Cell)* currCell = *cells++;

    while (currCell !is null) {
      immutable int currY = currCell.y;
      if (currY > y1) break; // no more
      assert(currCell.y >= y0);

      int cover = 0;
      for (;;) {
        const(Cell)* startCell = currCell;
        immutable uint coord = currCell.getPackedCoord;
        int x = currCell.x;
        if (x > x1) break; // there's nothing to render on this line anymore

        int area = startCell.area;
        cover += startCell.cover;

        // accumulate all start cells
        while ((currCell = *cells++) !is null) {
          if (currCell.getPackedCoord != coord) break;
          area += currCell.area;
          cover += currCell.cover;
        }

        if (area) {
          immutable ubyte alpha = calculateAlpha((cover<<(PolyBaseShift+1))-area);
          if (alpha && mGamma.ptr[alpha]) mScanline.addCell(x, mGamma.ptr[alpha]);
          ++x;
        }

        if (currCell is null || currCell.y != currY) break;

        if (currCell.x > x) {
          immutable ubyte alpha = calculateAlpha(cover<<(PolyBaseShift+1));
          if (alpha && mGamma.ptr[alpha]) mScanline.addSpan(x, currCell.x-x, mGamma.ptr[alpha]);
        }
      }

      // flush scanline
      if (mScanline.spanCount) {
        static if (AGGIsGoodSingleColor!SG) {
          renderSL(mScanline, cast(uint)currY, sc, clipRect);
        } else static if (AGGIsGoodVerticalGradient!SG) {
          renderSL(mScanline, cast(uint)currY, spangen.colorAtY(currY), clipRect);
        } else static if (AGGIsGoodGradient!SG) {
          renderSLGrad(mScanline, cast(uint)currY, spangen, clipRect);
        } else {
          static assert(0, "unknown span color generator type");
        }
        mScanline.resetSpans();
      }

      // skip clipped part
      if (currCell !is null && currCell.y == currY) {
        cells = mOutline.getCellsForYOrMore(currY+1);
        if (cells is null) break;
        currCell = *cells++;
      }
    }
  }


  // returns "gamma alpha" (0 means "no hit")
  ubyte hitTest (in int tx, in int ty) {
    // clip by framebuffer
    if (tx < 0 || ty < 0 || tx >= VBufWidth || ty >= VBufHeight) return 0;

    const(Cell)** cells = mOutline.getCellsForY(ty);
    if (cells is null) return 0;

    int cover = 0;
    const(Cell)* currCell = *cells++;
    assert(currCell.y == ty);

    while (currCell !is null) {
      const(Cell)* startCell = currCell;
      immutable uint coord = currCell.getPackedCoord;
      int x = currCell.x;
      if (tx < x) return 0;

      int area = startCell.area;
      cover += startCell.cover;

      while ((currCell = *cells++) !is null) {
        if (currCell.getPackedCoord != coord) break;
        area += currCell.area;
        cover += currCell.cover;
      }

      if (area) {
        if (tx == x) {
          immutable ubyte alpha = calculateAlpha((cover<<(PolyBaseShift+1))-area);
          return (alpha ? mGamma.ptr[alpha] : 0);
        }
        ++x;
      }

      if (currCell is null || currCell.y != ty) break;

      if (currCell.x > x && tx >= x && tx < currCell.x) {
        immutable ubyte alpha = calculateAlpha(cover<<(PolyBaseShift+1));
        return (alpha ? mGamma.ptr[alpha] : 0);
      }
    }

    return 0;
  }

private:
  static immutable ubyte[256] DefaultGamma = [
      0,  0,  1,  1,  2,  2,  3,  4,  4,  5,  5,  6,  7,  7,  8,  8,
      9, 10, 10, 11, 11, 12, 13, 13, 14, 14, 15, 16, 16, 17, 18, 18,
     19, 19, 20, 21, 21, 22, 22, 23, 24, 24, 25, 25, 26, 27, 27, 28,
     29, 29, 30, 30, 31, 32, 32, 33, 34, 34, 35, 36, 36, 37, 37, 38,
     39, 39, 40, 41, 41, 42, 43, 43, 44, 45, 45, 46, 47, 47, 48, 49,
     49, 50, 51, 51, 52, 53, 53, 54, 55, 55, 56, 57, 57, 58, 59, 60,
     60, 61, 62, 62, 63, 64, 65, 65, 66, 67, 68, 68, 69, 70, 71, 71,
     72, 73, 74, 74, 75, 76, 77, 78, 78, 79, 80, 81, 82, 83, 83, 84,
     85, 86, 87, 88, 89, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,
    100,101,101,102,103,104,105,106,107,108,109,110,111,112,114,115,
    116,117,118,119,120,121,122,123,124,126,127,128,129,130,131,132,
    134,135,136,137,139,140,141,142,144,145,146,147,149,150,151,153,
    154,155,157,158,159,161,162,164,165,166,168,169,171,172,174,175,
    177,178,180,181,183,184,186,188,189,191,192,194,195,197,199,200,
    202,204,205,207,209,210,212,214,215,217,219,220,222,224,225,227,
    229,230,232,234,236,237,239,241,242,244,246,248,249,251,253,255
  ];
}
