/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/* *****************************************************************************
  Anti-Grain Geometry (AGG) - Version 2.5
  A high quality rendering engine for C++
  Copyright (C) 2002-2006 Maxim Shemanarev
  Contact: mcseem@antigrain.com
           mcseemagg@yahoo.com
           http://antigrain.com

  AGG is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  AGG is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with AGG; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.
***************************************************************************** */
module iv.egra.gfx.aggmini.stroker;
package(iv.egra.gfx.aggmini):
nothrow @trusted @nogc:

private import iv.egra.gfx.aggmini : DisableCopyingMixin;
private import iv.egra.gfx.aggmini.supmath;
private import iv.egra.gfx.aggmini.enums;


// ////////////////////////////////////////////////////////////////////////// //
public alias AGGPoint = AGGPointImpl!float;

public struct AGGPointImpl(T) if (__traits(isIntegral, T) || __traits(isFloating, T)) {
public nothrow @trusted @nogc:
  alias ValueT = T;
  alias Me = AGGPointImpl!T;

public:
  static if (__traits(isFloating, T)) {
    T x, y;
  } else {
    T x=T.min, y=T.min;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
/*
 * VertexConsumer API:
 *   alias value_type = VertexType; // shoud support `VertexType(x, y)`
 *   vc.remove_all(); -- start new polygon
 *   vc.add(VT); -- add point to the current polygon
 */
struct StrokeCalc(VertexConsumer) {
private:
  float mWidth = 0.5f;
  float mWidthAbs = 0.5f;
  float mWidthEps = 0.5f/1024.0f;
  int mWidthSign = 1;
  float mMiterLimit = 4.0f;
  float mInnerMiterLimit = 1.01f;
  float mApproxScale = 1.0f;
  LineCap mLineCap = LineCap.Butt;
  LineJoin mLineJoin = LineJoin.Miter;
  InnerJoin mInnerJoin = InnerJoin.Miter;

public nothrow @trusted @nogc:
  alias CoordType = VertexConsumer.ValueType;

  mixin(DisableCopyingMixin);

  @property void lineCap (in LineCap lc) { pragma(inline, true); mLineCap = lc; }
  @property void lineJoin (in LineJoin lj) { pragma(inline, true); mLineJoin = lj; }
  @property void innerJoin (in InnerJoin ij) { pragma(inline, true); mInnerJoin = ij; }

  @property LineCap lineCap () const pure { pragma(inline, true); return mLineCap; }
  @property LineJoin lineJoin () const pure { pragma(inline, true); return mLineJoin; }
  @property InnerJoin innerJoin () const pure { pragma(inline, true); return mInnerJoin; }

  @property float width () const pure { pragma(inline, true); return mWidth*2.0f; }
  @property void width (in float w) {
    mWidth = w*0.5f;
    if (mWidth < 0.0f) {
      mWidthAbs = -mWidth;
      mWidthSign = -1;
    } else {
      mWidthAbs = mWidth;
      mWidthSign = 1;
    }
    mWidthEps = mWidth/1024.0f;
  }

  @property void miterLimit (in float ml) { pragma(inline, true); mMiterLimit = ml; }
  @property void miterLimitTheta (in float t) { pragma(inline, true); import core.stdc.math : sinf; mMiterLimit = 1.0f/sinf(t*0.5f); }
  @property void innerMiterLimit (in float ml) { pragma(inline, true); mInnerMiterLimit = ml; }
  @property void approximationScale (in float as) { pragma(inline, true); mApproxScale = as; }

  @property float miterLimit () const pure { pragma(inline, true); return mMiterLimit; }
  @property float innerMiterLimit () const pure { pragma(inline, true); return mInnerMiterLimit; }
  @property float approximationScale () const pure { pragma(inline, true); return mApproxScale; }

  void setFromParams (in ref AGGParams params) {
    lineCap = params.lineCap;
    lineJoin = params.lineJoin;
    innerJoin = params.innerJoin;
    width = params.width;
    miterLimit = params.miterLimit;
    innerMiterLimit = params.innerMiterLimit;
    approximationScale = params.approximationScale;
  }

  void calcCap() (ref VertexConsumer vc, in auto ref VertexDist v0, in auto ref VertexDist v1, in float len) {
    import core.stdc.math : acosf, atan2f, cosf, sinf;

    vc.removeAll();

    float dx1 = (v1.y-v0.y)/len;
    float dy1 = (v1.x-v0.x)/len;
    float dx2 = 0.0f;
    float dy2 = 0.0f;

    dx1 *= mWidth;
    dy1 *= mWidth;

    if (mLineCap != LineCap.Round) {
      if (mLineCap == LineCap.Square) {
        dx2 = dy1*mWidthSign;
        dy2 = dx1*mWidthSign;
      }
      addVertex(vc, v0.x-dx1-dx2, v0.y+dy1-dy2);
      addVertex(vc, v0.x+dx1-dx2, v0.y-dy1-dy2);
    } else {
      float da = acosf(mWidthAbs/(mWidthAbs+0.125f/mApproxScale))*2.0f;
      immutable int n = cast(int)(FLT_PI/da);
      da = FLT_PI/(n+1);
      addVertex(vc, v0.x-dx1, v0.y+dy1);
      if (mWidthSign > 0.0f) {
        float a1 = atan2f(dy1, -dx1);
        a1 += da;
        foreach (immutable int i; 0..n) {
          addVertex(vc, v0.x+cosf(a1)*mWidth, v0.y+sinf(a1)*mWidth);
          a1 += da;
        }
      } else {
        float a1 = atan2f(-dy1, dx1);
        a1 -= da;
        foreach (immutable int i; 0..n) {
          addVertex(vc, v0.x+cosf(a1)*mWidth, v0.y+sinf(a1)*mWidth);
          a1 -= da;
        }
      }
      addVertex(vc, v0.x+dx1, v0.y-dy1);
    }
  }

  void calcJoin() (ref VertexConsumer vc, in auto ref VertexDist v0, in auto ref VertexDist v1, in auto ref VertexDist v2, in float len1, in float len2) {
    import core.stdc.math : sqrtf;

    immutable float dx1 = mWidth*(v1.y-v0.y)/len1;
    immutable float dy1 = mWidth*(v1.x-v0.x)/len1;
    immutable float dx2 = mWidth*(v2.y-v1.y)/len2;
    immutable float dy2 = mWidth*(v2.x-v1.x)/len2;

    vc.removeAll();

    float cp = cross3(v0.x, v0.y, v1.x, v1.y, v2.x, v2.y);
    if (cp != 0.0f && (cp > 0.0f) == (mWidth > 0.0f)) {
      // Inner join
      float limit = (len1 < len2 ? len1 : len2)/mWidthAbs;
      if (limit < mInnerMiterLimit) limit = mInnerMiterLimit;

      switch (mInnerJoin) {
        default: // inner_bevel
          addVertex(vc, v1.x+dx1, v1.y-dy1);
          addVertex(vc, v1.x+dx2, v1.y-dy2);
          break;
        case InnerJoin.Miter:
          calcMiter(vc, v0, v1, v2, dx1, dy1, dx2, dy2, LineJoin.MiterRevert, limit, 0.0f);
          break;
        case InnerJoin.Jag:
        case InnerJoin.Round:
          cp = (dx1-dx2)*(dx1-dx2)+(dy1-dy2)*(dy1-dy2);
          if (cp < len1*len1 && cp < len2*len2) {
            calcMiter(vc, v0, v1, v2, dx1, dy1, dx2, dy2, LineJoin.MiterRevert, limit, 0.0f);
          } else {
            if (mInnerJoin == InnerJoin.Jag) {
              addVertex(vc, v1.x+dx1, v1.y-dy1);
              addVertex(vc, v1.x, v1.y);
              addVertex(vc, v1.x+dx2, v1.y-dy2);
            } else {
              addVertex(vc, v1.x+dx1, v1.y-dy1);
              addVertex(vc, v1.x, v1.y);
              calcArc(vc, v1.x, v1.y, dx2, -dy2, dx1, -dy1);
              addVertex(vc, v1.x, v1.y);
              addVertex(vc, v1.x+dx2, v1.y-dy2);
            }
          }
          break;
      }
    } else {
      // Outer join

      // Calculate the distance between v1 and
      // the central point of the bevel line segment
      float dx = (dx1+dx2)*0.5f;
      float dy = (dy1+dy2)*0.5f;
      immutable float dbevel = sqrtf(dx*dx+dy*dy);

      if (mLineJoin == LineJoin.Round || mLineJoin == LineJoin.Bevel) {
        // This is an optimization that reduces the number of points
        // in cases of almost collinear segments. If there's no
        // visible difference between bevel and miter joins we'd rather
        // use miter join because it adds only one point instead of two.
        //
        // Here we calculate the middle point between the bevel points
        // and then, the distance between v1 and this middle point.
        // At outer joins this distance always less than stroke width,
        // because it's actually the height of an isosceles triangle of
        // v1 and its two bevel points. If the difference between this
        // width and this value is small (no visible bevel) we can
        // add just one point.
        //
        // The constant in the expression makes the result approximately
        // the same as in round joins and caps. You can safely comment
        // out this entire "if".
        if (mApproxScale*(mWidthAbs-dbevel) < mWidthEps) {
          if (intersection(v0.x+dx1, v0.y-dy1, v1.x+dx1, v1.y-dy1, v1.x+dx2, v1.y-dy2, v2.x+dx2, v2.y-dy2, &dx, &dy)) {
            addVertex(vc, dx, dy);
          } else {
            addVertex(vc, v1.x+dx1, v1.y-dy1);
          }
          return;
        }
      }

      switch (mLineJoin) {
        case LineJoin.Miter:
        case LineJoin.MiterRevert:
        case LineJoin.MiterRound:
          calcMiter(vc, v0, v1, v2, dx1, dy1, dx2, dy2, mLineJoin, mMiterLimit, dbevel);
          break;
        case LineJoin.Round:
          calcArc(vc, v1.x, v1.y, dx1, -dy1, dx2, -dy2);
          break;
        default: // Bevel join
          addVertex(vc, v1.x+dx1, v1.y-dy1);
          addVertex(vc, v1.x+dx2, v1.y-dy2);
          break;
      }
    }
  }

private:
  void addVertex (ref VertexConsumer vc, in float x, in float y) {
    pragma(inline, true);
    vc.add(CoordType(x, y));
  }

  void calcArc (ref VertexConsumer vc, in float x, in float y, in float dx1, in float dy1, in float dx2, in float dy2) {
    import core.stdc.math : acosf, atan2f, cosf, sinf;

    float a1 = atan2f(dy1*mWidthSign, dx1*mWidthSign);
    float a2 = atan2f(dy2*mWidthSign, dx2*mWidthSign);

    immutable float da = acosf(mWidthAbs/(mWidthAbs+0.125f/mApproxScale))*2.0f;

    addVertex(vc, x+dx1, y+dy1);
    if (mWidthSign > 0.0f) {
      if (a1 > a2) a2 += 2.0f*FLT_PI;
      immutable int n = cast(int)((a2-a1)/da);
      immutable float daa = (a2-a1)/(n+1);
      a1 += daa;
      foreach (immutable int i; 0..n) {
        addVertex(vc, x+cosf(a1)*mWidth, y+sinf(a1)*mWidth);
        a1 += daa;
      }
    } else {
      if (a1 < a2) a2 -= 2.0f*FLT_PI;
      immutable int n = cast(int)((a1-a2)/da);
      immutable float daa = (a1-a2)/(n+1);
      a1 -= daa;
      foreach (immutable int i; 0..n) {
        addVertex(vc, x+cosf(a1)*mWidth, y+sinf(a1)*mWidth);
        a1 -= daa;
      }
    }
    addVertex(vc, x+dx2, y+dy2);
  }

  void calcMiter (ref VertexConsumer vc, in ref VertexDist v0, in ref VertexDist v1, in ref VertexDist v2,
                   in float dx1, in float dy1, in float dx2, in float dy2, in LineJoin lj,
                   in float mlimit, in float dbevel)
  {
    float xi = v1.x;
    float yi = v1.y;
    float di = 1.0f;
    immutable float lim = mWidthAbs*mlimit;
    bool miterLimitExceeded = true; // assume the worst
    bool intersectionFailed = true; // assume the worst

    if (intersection(v0.x+dx1, v0.y-dy1, v1.x+dx1, v1.y-dy1, v1.x+dx2, v1.y-dy2, v2.x+dx2, v2.y-dy2, &xi, &yi)) {
      // Calculation of the intersection succeeded
      di = distance(v1.x, v1.y, xi, yi);
      if (di <= lim) {
        // Inside the miter limit
        addVertex(vc, xi, yi);
        miterLimitExceeded = false;
      }
      intersectionFailed = false;
    } else {
      // Calculation of the intersection failed, most probably
      // the three points lie one straight line.
      // First check if v0 and v2 lie on the opposite sides of vector:
      // (v1.x, v1.y) -> (v1.x+dx1, v1.y-dy1), that is, the perpendicular
      // to the line determined by vertices v0 and v1.
      // This condition determines whether the next line segments continues
      // the previous one or goes back.
      immutable float x2 = v1.x+dx1;
      immutable float y2 = v1.y-dy1;
      if ((cross3(v0.x, v0.y, v1.x, v1.y, x2, y2) < 0.0f) == (cross3(v1.x, v1.y, v2.x, v2.y, x2, y2) < 0.0f)) {
        // This case means that the next segment continues
        // the previous one (straight line)
        addVertex(vc, v1.x+dx1, v1.y-dy1);
        miterLimitExceeded = false;
      }
    }

    if (miterLimitExceeded) {
      // Miter limit exceeded
      //------------------------
      switch (lj) {
        case LineJoin.MiterRevert:
          // For the compatibility with SVG, PDF, etc,
          // we use a simple bevel join instead of
          // "smart" bevel
          addVertex(vc, v1.x+dx1, v1.y-dy1);
          addVertex(vc, v1.x+dx2, v1.y-dy2);
          break;
        case LineJoin.MiterRound:
          calcArc(vc, v1.x, v1.y, dx1, -dy1, dx2, -dy2);
          break;
        default:
          // If no miter-revert, calculate new dx1, dy1, dx2, dy2
          if (intersectionFailed) {
            immutable float mlimitM = mlimit*mWidthSign;
            addVertex(vc, v1.x+dx1+dy1*mlimitM, v1.y-dy1+dx1*mlimitM);
            addVertex(vc, v1.x+dx2-dy2*mlimitM, v1.y-dy2-dx2*mlimitM);
          } else {
            immutable float x1 = v1.x+dx1;
            immutable float y1 = v1.y-dy1;
            immutable float x2 = v1.x+dx2;
            immutable float y2 = v1.y-dy2;
            di = (lim-dbevel)/(di-dbevel);
            addVertex(vc, x1+(xi-x1)*di, y1+(yi-y1)*di);
            addVertex(vc, x2+(xi-x2)*di, y2+(yi-y2)*di);
          }
          break;
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct Stroker {
public nothrow @trusted @nogc:
  alias VertexStorage = VertexSequence!VertexDist;
  alias CoordStorage = SimpleVector!AGGPoint;

private:
  enum State {
    Initial,
    Ready,
    Cap1,
    Cap2,
    Outline1,
    CloseFirst,
    Outline2,
    OutVertices,
    End_poly1,
    End_poly2,
    Stop,
  }

private:
  StrokeCalc!CoordStorage mStroker;
  VertexStorage mSrcVertices;
  CoordStorage mOutVertices;
  float mShorten = 0;
  uint mClosed = 0;
  State mStatus = State.Initial;
  State mPrevStatus;
  uint mSrcVertex = 0;
  uint mOutVertex = 0;

public:
  mixin(DisableCopyingMixin);

  @property void lineCap (LineCap lc) { pragma(inline, true); mStroker.lineCap(lc); }
  @property void lineJoin (LineJoin lj) { pragma(inline, true); mStroker.lineJoin(lj); }
  @property void innerJoin (InnerJoin ij) { pragma(inline, true); mStroker.innerJoin(ij); }

  @property LineCap lineCap () const pure { pragma(inline, true); return mStroker.lineCap(); }
  @property LineJoin lineJoin () const pure { pragma(inline, true); return mStroker.lineJoin(); }
  @property InnerJoin innerJoin () const pure { pragma(inline, true); return mStroker.innerJoin(); }

  @property void width (float w) { pragma(inline, true); mStroker.width(w); }
  @property void miterLimit (float ml) { pragma(inline, true); mStroker.miterLimit(ml); }
  @property void miterLimitTheta (float t) { pragma(inline, true); mStroker.miterLimitTheta(t); }
  @property void innerMiterLimit (float ml) { pragma(inline, true); mStroker.innerMiterLimit(ml); }
  @property void approximationScale (float as) { pragma(inline, true); mStroker.approximationScale(as); }

  @property float width () const pure { pragma(inline, true); return mStroker.width(); }
  @property float miterLimit () const pure { pragma(inline, true); return mStroker.miterLimit(); }
  @property float innerMiterLimit () const pure { pragma(inline, true); return mStroker.innerMiterLimit(); }
  @property float approximationScale () const pure { pragma(inline, true); return mStroker.approximationScale(); }

  @property void shorten (float s) { pragma(inline, true); mShorten = s; }
  @property float shorten () const pure { pragma(inline, true); return mShorten; }

  void setFromParams (in ref AGGParams params) {
    mStroker.setFromParams(params);
    shorten = params.shorten;
  }

  // Generator interface
  void removeAll () {
    mSrcVertices.removeAll();
    mSrcVertex = 0;
    mOutVertices.removeAll();
    mOutVertex = 0;
    mClosed = 0;
    mStatus = mPrevStatus = State.Initial;
  }

  void addVertex (float x, float y, uint cmd) {
    mStatus = State.Initial;
         if (isMoveTo(cmd)) mSrcVertices.modifyLast(VertexDist(x, y));
    else if (isVertex(cmd)) mSrcVertices.add(VertexDist(x, y));
    else if (isEndPoly(cmd)) mClosed = getCloseFlag(cmd);
  }

  // Vertex Source Interface
  void rewind () {
    if (mStatus == State.Initial) {
      mSrcVertices.close(mClosed != 0);
      shortenPath(mSrcVertices, mShorten, mClosed);
      if (mSrcVertices.length < 3) mClosed = 0;
    }
    mStatus = State.Ready;
    mSrcVertex = 0;
    mOutVertex = 0;
  }

  uint vertex (float* x, float* y) {
    uint cmd = PathCommand.LineTo;
    while (!isStop(cmd)) {
      final switch (mStatus) {
        case State.Initial:
          rewind();
          goto case;

        case State.Ready:
          if (mSrcVertices.length < 2+cast(uint)(mClosed != 0)) {
            cmd = PathCommand.Stop;
            break;
          }
          mStatus = (mClosed ? State.Outline1 : State.Cap1);
          cmd = PathCommand.MoveTo;
          mSrcVertex = 0;
          mOutVertex = 0;
          break;

        case State.Cap1:
          mStroker.calcCap(mOutVertices, mSrcVertices[0], mSrcVertices[1], mSrcVertices[0].dist);
          mSrcVertex = 1;
          mPrevStatus = State.Outline1;
          mStatus = State.OutVertices;
          mOutVertex = 0;
          break;

        case State.Cap2:
          mStroker.calcCap(mOutVertices, mSrcVertices[mSrcVertices.length-1], mSrcVertices[mSrcVertices.length-2], mSrcVertices[mSrcVertices.length-2].dist);
          mPrevStatus = State.Outline2;
          mStatus = State.OutVertices;
          mOutVertex = 0;
          break;

        case State.Outline1:
          if (mClosed) {
            if (mSrcVertex >= mSrcVertices.length) {
              mPrevStatus = State.CloseFirst;
              mStatus = State.End_poly1;
              break;
            }
          } else {
            if (mSrcVertex >= mSrcVertices.length-1) {
              mStatus = State.Cap2;
              break;
            }
          }
          mStroker.calcJoin(mOutVertices, mSrcVertices.prev(mSrcVertex), mSrcVertices.curr(mSrcVertex), mSrcVertices.next(mSrcVertex), mSrcVertices.prev(mSrcVertex).dist, mSrcVertices.curr(mSrcVertex).dist);
          ++mSrcVertex;
          mPrevStatus = mStatus;
          mStatus = State.OutVertices;
          mOutVertex = 0;
          break;

        case State.CloseFirst:
          mStatus = State.Outline2;
          cmd = PathCommand.MoveTo;
          goto case;

        case State.Outline2:
          if (mSrcVertex <= uint(mClosed == 0)) {
            mStatus = State.End_poly2;
            mPrevStatus = State.Stop;
            break;
          }

          --mSrcVertex;
          mStroker.calcJoin(mOutVertices, mSrcVertices.next(mSrcVertex), mSrcVertices.curr(mSrcVertex), mSrcVertices.prev(mSrcVertex), mSrcVertices.curr(mSrcVertex).dist, mSrcVertices.prev(mSrcVertex).dist);

          mPrevStatus = mStatus;
          mStatus = State.OutVertices;
          mOutVertex = 0;
          break;

        case State.OutVertices:
          if (mOutVertex >= mOutVertices.length) {
            mStatus = mPrevStatus;
          } else {
            const(AGGPoint)* c = &mOutVertices[mOutVertex++];
            *x = c.x;
            *y = c.y;
            return cmd;
          }
          break;

        case State.End_poly1:
          mStatus = mPrevStatus;
          return PathCommand.EndPoly|PathFlag.Close|PathFlag.CCW;

        case State.End_poly2:
          mStatus = mPrevStatus;
          return PathCommand.EndPoly|PathFlag.Close|PathFlag.CW;

        case State.Stop:
          cmd = PathCommand.Stop;
          break;
      }
    }
    return cmd;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct Contourer {
public nothrow @trusted @nogc:
  alias VertexStorage = VertexSequence!VertexDist;
  alias CoordStorage = SimpleVector!AGGPoint;

  enum State {
    Initial,
    Ready,
    Outline,
    OutVertices,
    EndPoly,
    Stop,
  }

private:
  StrokeCalc!CoordStorage mStroker;
  float mWidth = 1;
  VertexStorage mSrcVertices;
  CoordStorage mOutVertices;
  State mStatus;
  uint mSrcVertex = 0;
  uint mOutVertex;
  uint mClosed = 0;
  uint mOrientation = 0;
  // default orientation for polys w/o explitict one
  uint mDefaultOrientation = 0;
  // force this orientation for all polys
  uint mForcedOrientation = 0;
  // flip all orientations?
  bool mFlipOrientation = false;

public:
  mixin(DisableCopyingMixin);

  @property void lineCap (LineCap lc) { pragma(inline, true); mStroker.lineCap(lc); }
  @property void lineJoin (LineJoin lj) { pragma(inline, true); mStroker.lineJoin(lj); }
  @property void innerJoin (InnerJoin ij) { pragma(inline, true); mStroker.innerJoin(ij); }

  @property LineCap lineCap () const pure { pragma(inline, true); return mStroker.lineCap(); }
  @property LineJoin lineJoin () const pure { pragma(inline, true); return mStroker.lineJoin(); }
  @property InnerJoin innerJoin () const pure { pragma(inline, true); return mStroker.innerJoin(); }

  @property void width (float w) { pragma(inline, true); mStroker.width(mWidth = w); }
  @property void miterLimit (float ml) { pragma(inline, true); mStroker.miterLimit(ml); }
  @property void miterLimitTheta (float t) { pragma(inline, true); mStroker.miterLimitTheta(t); }
  @property void innerMiterLimit (float ml) { pragma(inline, true); mStroker.innerMiterLimit(ml); }
  @property void approximationScale (float as) { pragma(inline, true); mStroker.approximationScale(as); }

  @property float width () const pure { pragma(inline, true); return mWidth; }
  @property float miterLimit () const pure { pragma(inline, true); return mStroker.miterLimit(); }
  @property float innerMiterLimit () const pure { pragma(inline, true); return mStroker.innerMiterLimit(); }
  @property float approximationScale () const pure { pragma(inline, true); return mStroker.approximationScale(); }

  // 0: autodetect; -1: ccw; 1: cw
  @property void defaultOrientation (in int v) { pragma(inline, true); mDefaultOrientation = (v < 0 ? PathFlag.CCW : v > 0 ? PathFlag.CW : 0); }
  @property int defaultOrientation () const pure { pragma(inline, true); return (mDefaultOrientation == PathFlag.CW ? 1 : mDefaultOrientation == PathFlag.CCW ? -1 : 0); }

  // 0: autodetect; -1: ccw; 1: cw
  @property void forcedOrientation (in int v) { pragma(inline, true); mForcedOrientation = (v < 0 ? PathFlag.CCW : v > 0 ? PathFlag.CW : 0); }
  @property int forcedOrientation () const pure { pragma(inline, true); return (mForcedOrientation == PathFlag.CW ? 1 : mDefaultOrientation == PathFlag.CCW ? -1 : 0); }

  @property void flipOrientation (in bool v) { pragma(inline, true); mFlipOrientation = v; }
  @property bool flipOrientation () const pure { pragma(inline, true); return mFlipOrientation; }

  void setFromParams (in ref AGGParams params) {
    mStroker.setFromParams(params);
    width = params.width;
  }

  // Generator interface
  void removeAll () {
    mSrcVertices.removeAll();
    mSrcVertex = 0;
    mOutVertices.removeAll();
    mOutVertex = 0;
    mClosed = 0;
    mOrientation = 0;
    mStatus = State.Initial;
  }

  void addVertex (float x, float y, uint cmd) {
    mStatus = State.Initial;
    if (isMoveTo(cmd)) {
      mSrcVertices.modifyLast(VertexDist(x, y));
    } else if (isVertex(cmd)) {
      mSrcVertices.add(VertexDist(x, y));
    } else if (isEndPoly(cmd)) {
      mClosed = getCloseFlag(cmd);
      if (!mOrientation) mOrientation = getOrientation(cmd);
    }
  }

  // Vertex Source Interface
  void rewind () {
    if (mStatus == State.Initial) {
      mSrcVertices.close(true);
      if (mForcedOrientation) {
        // forced orientation for all polys?
        mOrientation = mForcedOrientation;
      } else if (!mOrientation) {
        // poly orientation is unknown
        if (mDefaultOrientation) {
          // we have default one, use it
          mOrientation = mDefaultOrientation;
        } else {
          // no default, autodetect
          mOrientation = (calcPolygonArea(mSrcVertices) > 0 ? PathFlag.CCW : PathFlag.CW);
        }
      }
      if (mFlipOrientation) mOrientation = (mOrientation == PathFlag.CCW ? PathFlag.CW : PathFlag.CCW);
      //if (isOriented(mOrientation)) {
      mStroker.width(isCCW(mOrientation) ? mWidth : -mWidth);
    }
    mStatus = State.Ready;
    mSrcVertex = 0;
  }

  uint vertex (float* x, float* y) {
    uint cmd = PathCommand.LineTo;
    while (!isStop(cmd)) {
      final switch (mStatus) {
        case State.Initial:
          rewind();
          goto case;

        case State.Ready:
          if (mSrcVertices.length < 2+cast(uint)(mClosed != 0)) {
            cmd = PathCommand.Stop;
            break;
          }
          mStatus = State.Outline;
          cmd = PathCommand.MoveTo;
          mSrcVertex = 0;
          mOutVertex = 0;
          goto case;

        case State.Outline:
          if (mSrcVertex >= mSrcVertices.length) {
            mStatus = State.EndPoly;
            break;
          }
          mStroker.calcJoin(mOutVertices, mSrcVertices.prev(mSrcVertex), mSrcVertices.curr(mSrcVertex), mSrcVertices.next(mSrcVertex), mSrcVertices.prev(mSrcVertex).dist, mSrcVertices.curr(mSrcVertex).dist);
          ++mSrcVertex;
          mStatus = State.OutVertices;
          mOutVertex = 0;
          goto case;

        case State.OutVertices:
          if (mOutVertex >= mOutVertices.length) {
            mStatus = State.Outline;
          } else {
            const(AGGPoint)* c = &mOutVertices[mOutVertex++];
            *x = c.x;
            *y = c.y;
            return cmd;
          }
          break;

        case State.EndPoly:
          //k8: always emit "endpoly"
          //if (!mClosed) return PathCommand.Stop;
          mStatus = State.Stop;
          return PathCommand.EndPoly|PathFlag.Close|PathFlag.CCW;

        case State.Stop:
          return PathCommand.Stop;
      }
    }
    return cmd;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct Dasher {
private nothrow @trusted @nogc:
  alias VertexStorage = VertexSequence!VertexDist;
  enum MaxDashes = 32;

  enum State {
    Initial,
    Ready,
    Polyline,
    Stop,
  }

private:
  float[MaxDashes] mDashes = 0;
  float mTotalDashLen = 0;
  uint mNumDashes = 0;
  float mDashStart = 0;
  float mShorten = 0;
  float mCurrDashStart = 0;
  uint mCurrDash = 0;
  float mCurrRest = 0;
  const(VertexDist)* m_v1 = null;
  const(VertexDist)* m_v2 = null;

  VertexStorage mSrcVertices;
  uint mClosed = 0;
  State mStatus = State.Initial;
  uint mSrcVertex = 0;

public:
  mixin(DisableCopyingMixin);

  @property void shorten (in float s) { pragma(inline, true); mShorten = s; }
  @property float shorten () const pure { pragma(inline, true); return mShorten; }

  @property bool hasDashes () const pure { pragma(inline, true); return (mNumDashes > 0); }

  void removeAllDashes () {
    mTotalDashLen = 0;
    mNumDashes = 0;
    mCurrDashStart = 0;
    mCurrDash = 0;
  }

  void addDash (in float dashLen, in float gapLen) {
    if (mNumDashes < MaxDashes) {
      mTotalDashLen += dashLen+gapLen;
      mDashes[mNumDashes++] = dashLen;
      mDashes[mNumDashes++] = gapLen;
    }
  }

  void dashStart (in float ds) {
    import core.stdc.math : fabsf;
    mDashStart = ds;
    calcDashStart(fabsf(ds));
  }

  // Vertex Generator Interface
  void removeAll () {
    mStatus = State.Initial;
    mSrcVertices.removeAll();
    mSrcVertex = 0;
    mClosed = 0;
    calcDashStart(mDashStart);
    mCurrRest = 0;
  }

  void addVertex (float x, float y, uint cmd) {
    mStatus = State.Initial;
         if (isMoveTo(cmd)) mSrcVertices.modifyLast(VertexDist(x, y));
    else if (isVertex(cmd)) mSrcVertices.add(VertexDist(x, y));
    else if (isEndPoly(cmd)) mClosed = getCloseFlag(cmd);
  }

  // Vertex Source Interface
  void rewind () {
    if (mStatus == State.Initial) {
      mSrcVertices.close(mClosed != 0);
      shortenPath(mSrcVertices, mShorten, mClosed);
    }
    mStatus = State.Ready;
    mSrcVertex = 0;
  }

  uint vertex (float* x, float* y) {
    uint cmd = PathCommand.MoveTo;

    while (!isStop(cmd)) {
      final switch (mStatus) {
        case State.Initial:
          rewind();
          goto case;

        case State.Ready:
          if (mNumDashes < 2 || mSrcVertices.length < 2) {
            cmd = PathCommand.Stop;
            break;
          }
          mStatus = State.Polyline;
          mSrcVertex = 1;
          m_v1 = &mSrcVertices[0];
          m_v2 = &mSrcVertices[1];
          mCurrRest = m_v1.dist;
          *x = m_v1.x;
          *y = m_v1.y;
          if (mDashStart >= 0) calcDashStart(mDashStart);
          return PathCommand.MoveTo;

        case State.Polyline:
          immutable float dashRest = mDashes[mCurrDash]-mCurrDashStart;
          cmd = (mCurrDash&1 ? PathCommand.MoveTo : PathCommand.LineTo);
          if (mCurrRest > dashRest) {
            mCurrRest -= dashRest;
            ++mCurrDash;
            if (mCurrDash >= mNumDashes) mCurrDash = 0;
            mCurrDashStart = 0;
            *x = m_v2.x-(m_v2.x-m_v1.x)*mCurrRest/m_v1.dist;
            *y = m_v2.y-(m_v2.y-m_v1.y)*mCurrRest/m_v1.dist;
          } else {
            mCurrDashStart += mCurrRest;
            *x = m_v2.x;
            *y = m_v2.y;
            ++mSrcVertex;
            m_v1 = m_v2;
            mCurrRest = m_v1.dist;
            if (mClosed) {
              if (mSrcVertex > mSrcVertices.length) {
                mStatus = State.Stop;
              } else {
                m_v2 = &mSrcVertices[mSrcVertex >= mSrcVertices.length ? 0 :mSrcVertex];
              }
            } else {
              if (mSrcVertex >= mSrcVertices.length) {
                mStatus = State.Stop;
              } else {
                m_v2 = &mSrcVertices[mSrcVertex];
              }
            }
          }
          return cmd;

        case State.Stop:
          cmd = PathCommand.Stop;
          break;
      }
    }
    return PathCommand.Stop;
  }

private:
  void calcDashStart (float ds) {
    mCurrDash = 0;
    mCurrDashStart = 0;
    while (ds > 0) {
      if (ds > mDashes[mCurrDash]) {
        ds -= mDashes[mCurrDash];
        ++mCurrDash;
        mCurrDashStart = 0;
        if (mCurrDash >= mNumDashes) mCurrDash = 0;
      } else {
        mCurrDashStart = ds;
        ds = 0;
      }
    }
  }
}
