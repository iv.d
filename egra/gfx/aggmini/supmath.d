/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
/* *****************************************************************************
  Anti-Grain Geometry - Version 2.1 Lite
  Copyright (C) 2002-2003 Maxim Shemanarev (McSeem)
  D port, additional work: Ketmar Dark

  Permission to copy, use, modify, sell and distribute this software
  is granted provided this copyright notice appears in all copies.
  This software is provided "as is" without express or implied
  warranty, and with no claim as to its suitability for any purpose.

  The author gratefully acknowleges the support of David Turner,
  Robert Wilhelm, and Werner Lemberg - the authors of the FreeType
  libray - in producing this work. See http://www.freetype.org for details.

  Initially the rendering algorithm was designed by David Turner and the
  other authors of the FreeType library - see the above notice. I nearly
  created a similar renderer, but still I was far from David's work.
  I completely redesigned the original code and adapted it for Anti-Grain
  ideas. Two functions - renderLine and renderScanLine are the core of
  the algorithm - they calculate the exact coverage of each pixel cell
  of the polygon. I left these functions almost as is, because there's
  no way to improve the perfection - hats off to David and his group!

  All other code is very different from the original.
***************************************************************************** */
module iv.egra.gfx.aggmini.supmath;

private import iv.egra.gfx.aggmini : DisableCopyingMixin;

package(iv.egra.gfx.aggmini):
nothrow @trusted @nogc:

//enum PI = 3.14159265358979323846;
enum DBL_PI = cast(double)(0x1.921fb54442d18p+1);
enum FLT_PI = cast(float)(0x1.921fb54442d18p+1);

enum DEG2RAD_MULT_D = cast(double)(0x1.1df46a2529d39p-6);
enum RAD2DEG_MULT_D = cast(double)(0x1.ca5dc1a63c1f8p+5);

enum DEG2RAD_MULT_F = cast(float)(0x1.1df46ap-6f);
enum RAD2DEG_MULT_F = cast(float)(0x1.ca5dc2p+5f);


// ////////////////////////////////////////////////////////////////////////// //
//float deg2rad (in float deg) pure nothrow @safe @nogc { pragma(inline, true); return deg*cast(float)(PI/180.0f); }
//float rad2deg (in float rad) pure nothrow @safe @nogc { pragma(inline, true); return rad*cast(float)(180.0f/PI); }
public T deg2rad(T) (in T deg) if (__traits(isFloating, T) && (T.sizeof == 4 || T.sizeof == 8)) {
  pragma(inline, true);
  static if (T.sizeof == 4) {
    return deg*DEG2RAD_MULT_F;
  } else {
    return deg*DEG2RAD_MULT_D;
  }
}

public T rad2deg(T) (in T rad) if (__traits(isFloating, T) && (T.sizeof == 4 || T.sizeof == 8)) {
  pragma(inline, true);
  static if (T.sizeof == 4) {
    return rad*RAD2DEG_MULT_F;
  } else {
    return rad*RAD2DEG_MULT_D;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
/*
  vertex consumer:
    void removeAll ();
    void addVertex (float x, float y, uint cmd);
*/
template IsGoodVertexConsumer(VS) {
  enum IsGoodVertexConsumer =
    is(typeof((inout int=0) {
      VS vs = void;
      vs.removeAll();
      vs.addVertex(0.666f, 0.666f, cast(uint)666u);
    }));
}

/*
  vertex producer:
    void rewind ();
    uint vertex (float* x, float* y);
*/
template IsGoodVertexProducer(VP) {
  enum IsGoodVertexProducer =
    is(typeof((inout int=0) {
      VP vp = void;
      vp.rewind();
      float x, y;
      uint v = vp.vertex(&x, &y);
    }));
}


// ////////////////////////////////////////////////////////////////////////// //
enum PathCommand : uint {
  Stop = 0u,
  MoveTo = 1u,
  LineTo = 2u,
  EndPoly = 0x0Fu,
  Mask = 0x0Fu,
}

enum PathFlag : uint {
  None = 0x00u,
  CCW = 0x10u,
  CW = 0x20u,
  Close = 0x40u,
  Mask = 0xF0u,
}

bool isVertex (in uint c) pure { pragma(inline, true); return (c >= PathCommand.MoveTo && c < PathCommand.EndPoly); }
bool isDrawing (in uint c) pure { pragma(inline, true); return (c >= PathCommand.LineTo && c < PathCommand.EndPoly); }
bool isStop (in uint c) pure { pragma(inline, true); return (c == PathCommand.Stop); }
bool isMoveTo (in uint c) pure { pragma(inline, true); return (c == PathCommand.MoveTo); }
bool isLineTo (in uint c) pure { pragma(inline, true); return (c == PathCommand.LineTo); }
bool isEndPoly (in uint c) pure { pragma(inline, true); return ((c&PathCommand.Mask) == PathCommand.EndPoly); }
//bool isClose (in uint c) pure { pragma(inline, true); return (c&~cast(uint)(PathFlag.CW|PathFlag.CCW)) == (PathCommand.EndPoly|PathFlag.Close); }
bool isCW (in uint c) pure { pragma(inline, true); return ((c&PathFlag.CW) != 0); }
bool isCCW (in uint c) pure { pragma(inline, true); return ((c&PathFlag.CCW) != 0); }
bool isOriented (in uint c) pure { pragma(inline, true); return ((c&(PathFlag.CW|PathFlag.CCW)) != 0); }
bool isClosed (in uint c) pure { pragma(inline, true); return ((c&PathFlag.Close) != 0); }
uint getCloseFlag (in uint c) pure { pragma(inline, true); return (c&PathFlag.Close); }
uint clearOrientation (in uint c) pure { pragma(inline, true); return (c&~(PathFlag.CW|PathFlag.CCW)); }
uint getOrientation (in uint c) pure { pragma(inline, true); return (c&(PathFlag.CW|PathFlag.CCW)); }
uint setOrientation (in uint c, in uint o) pure { pragma(inline, true); return (clearOrientation(c)|o); }


bool ptEquals (in float x0, in float y0, in float x1, in float y1) pure {
  pragma(inline, true);
  enum EPS = cast(float)(1.0f/256.0f);
  static bool eq (in float a, in float b) pure nothrow @safe @nogc {
    pragma(inline, true);
    immutable float df = a-b;
    return (df < 0.0f ? (-df < EPS) : (df < EPS));
  }
  return (eq(x0, x1) && eq(y0, y1));
}


//version = egra_agg_debug_streamer;


// ////////////////////////////////////////////////////////////////////////// //
// this closes polys
void streamAllVertices(bool doClosePolys=true, VS, VD, DG) (ref VS vsrc, ref VD vdest, scope DG flushdg=null)
if (IsGoodVertexProducer!VS && IsGoodVertexConsumer!VD &&
    (is(DG == typeof(null)) ||
     is(typeof((inout int=0) {
       DG dg = void;
       VD vd = void;
       dg(ref vd);
     })) ||
     is(typeof((inout int=0) {
       DG dg = void;
       dg();
     }))
    ))
{
  static enum IsNonEmptyDG(DG) = !is(DG == typeof(null));
  static enum IsDGWithArgs(DG) = is(typeof((inout int=0) { DG dg = void; VD vd = void; dg(ref vd); }));

  version(egra_agg_debug_streamer) import core.stdc.stdio : stderr, fprintf;

  float startX = 0.0f, startY = 0.0f;
  float lastX = 0.0f, lastY = 0.0f;
  uint lineCmdCount = 0;
    // 0: just started/endpoly resed
    // 1: got moveto, not issued yet
    // 2: issued moveto and at least one lineto; lineto count+1
  float x = 0.0f, y = 0.0f;
  vsrc.rewind();
  vdest.removeAll();
  version(egra_agg_debug_streamer) fprintf(stderr, "=== streamAllVertices: started: vdest=0x%08x ===\n", cast(uint)cast(const void *)&vdest);
  for (;;) {
    auto cmd = vsrc.vertex(&x, &y);
    version(egra_agg_debug_streamer) fprintf(stderr, "  0x%08x: %02x (%g, %g)  lineCmdCount=%u; start=(%g,%g); last=(%g,%g)\n",
      cast(uint)cast(const void *)&vdest, cmd, x, y, lineCmdCount, startX, startY, lastX, lastY);

    // stop?
    if (isStop(cmd)) {
      if (lineCmdCount >= 2) {
        // flush poly
        if (lineCmdCount > 2 && ptEquals(startX, startY, lastX, lastY)) {
          version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: flush closed\n", cast(uint)cast(const void *)&vdest);
          vdest.addVertex(startX, startY, PathCommand.EndPoly|PathFlag.Close);
        } else {
          version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: flush unclosed\n", cast(uint)cast(const void *)&vdest);
          vdest.addVertex(startX, startY, PathCommand.EndPoly);
        }
        // call "flush" delegate
        static if (IsNonEmptyDG!DG) {
          vdest.addVertex(x, y, PathCommand.Stop);
          static if (IsDGWithArgs!DG) flushdg(vdest); else flushdg();
          vdest.removeAll();
        }
      }
      // final stop
      vdest.addVertex(x, y, PathCommand.Stop);
      version(egra_agg_debug_streamer) fprintf(stderr, "=== streamAllVertices: complete: vdest=0x%08x ===\n", cast(uint)cast(const void *)&vdest);
      return;
    }

    // move?
    if (isMoveTo(cmd)) {
      if (lineCmdCount >= 2) {
        // flush poly
        if (lineCmdCount > 2 && ptEquals(startX, startY, lastX, lastY)) {
          version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: flush closed\n", cast(uint)cast(const void *)&vdest);
          vdest.addVertex(startX, startY, PathCommand.EndPoly|PathFlag.Close);
        } else {
          version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: flush unclosed\n", cast(uint)cast(const void *)&vdest);
          vdest.addVertex(startX, startY, PathCommand.EndPoly);
        }
        // call "flush" delegate
        static if (IsNonEmptyDG!DG) {
          vdest.addVertex(x, y, PathCommand.Stop);
          static if (IsDGWithArgs!DG) flushdg(vdest); else flushdg();
          vdest.removeAll();
        }
      }
      startX = lastX = x;
      startY = lastY = y;
      lineCmdCount = 1;
      //vdest.addVertex(x, y, cmd); // postponed
      version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: moveto reset\n", cast(uint)cast(const void *)&vdest);
      continue;
    }

    // line?
    if (isLineTo(cmd)) {
      if (lineCmdCount >= 2) {
        // do not add duplicate points
        if (ptEquals(x, y, lastX, lastY)) {
          version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: rejected equal\n", cast(uint)cast(const void *)&vdest);
          continue;
        }
      } else if (lineCmdCount == 0) {
        // move to the first point if we didn't yet
        version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: inserted moveto\n", cast(uint)cast(const void *)&vdest);
        vdest.addVertex(x, y, PathCommand.MoveTo);
        startX = x;
        startY = y;
        lineCmdCount = 1;
      } else if (lineCmdCount == 1) {
        // move to the first point if we didn't yet
        version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: postponed moveto (%g, %g)\n", cast(uint)cast(const void *)&vdest, startX, startY);
        vdest.addVertex(startX, startY, PathCommand.MoveTo);
      }
      ++lineCmdCount;
      lastX = x;
      lastY = y;
      vdest.addVertex(x, y, cmd);
      version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: lineto\n", cast(uint)cast(const void *)&vdest);
      continue;
    }

    // end poly?
    if (isEndPoly(cmd)) {
      // if we haven't seen any lines, only moves -- don't bother
      if (lineCmdCount >= 2) {
        // fix command if the poly is acutally closed (because why not?)
        if (!isClosed(cmd)) {
          if (lineCmdCount > 2 && ptEquals(startX, startY, lastX, lastY)) {
            version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: added \"closed\" flag\n", cast(uint)cast(const void *)&vdest);
            cmd |= PathFlag.Close;
          }
        } else {
          // if we've seen only one line so far, it cannot be closed
          if (lineCmdCount <= 2) {
            version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: removed \"closed\" flag\n", cast(uint)cast(const void *)&vdest);
            cmd &= ~PathFlag.Close;
          }
        }
        // close poly?
        static if (doClosePolys) {
          if (lineCmdCount > 2 && isClosed(cmd) && !ptEquals(startX, startY, lastX, lastY)) {
            version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: inserted poly closing line\n", cast(uint)cast(const void *)&vdest);
            vdest.addVertex(startX, startY, PathCommand.LineTo);
            lastX = startX;
            lastY = startY;
          }
        }
        // don't do "move to" here, it breaks rendering
        vdest.addVertex(x, y, cmd);
        version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: endpoly routed\n", cast(uint)cast(const void *)&vdest);
        // call "flush" delegate
        static if (IsNonEmptyDG!DG) {
          vdest.addVertex(x, y, PathCommand.Stop);
          static if (IsDGWithArgs!DG) flushdg(vdest); else flushdg();
          vdest.removeAll();
        }
      }
      version(egra_agg_debug_streamer) fprintf(stderr, "    0x%08x: endpoly reset\n", cast(uint)cast(const void *)&vdest);
      lineCmdCount = 0;
      continue;
    }

    // wtf?!
    assert(0, "invalid path command");
  }
  assert(0); // the thing that should not be
}


// ////////////////////////////////////////////////////////////////////////// //
static auto CreatePolyTrans(VS, VD) (ref VS avs, ref VD avd, in bool BreakOnMoveTo=true)
if (IsGoodVertexProducer!VS && IsGoodVertexConsumer!VD)
{
  return VertexStreamPolyTrans!(VS, VD)(avs, avd, BreakOnMoveTo);
}


// it routes vertices from VS to VD until "endpoly" or "stop"
struct VertexStreamPolyTrans(VS, VD) if (IsGoodVertexProducer!VS && IsGoodVertexConsumer!VD) {
  VS *vsrc;
  VD *vdest;
  bool wasVertex;
  float startX = 0.0f, startY = 0.0f;
  float lastX = 0.0f, lastY = 0.0f;
  uint lastCmd = uint.max;
  bool breakOnMoveTo;

@trusted nothrow @nogc:
  this (ref VS avs, ref VD avd, in bool BreakOnMoveTo=true) {
    pragma(inline, true);
    avs.rewind();
    avd.removeAll();
    vsrc = &avs;
    vdest = &avd;
    breakOnMoveTo = BreakOnMoveTo;
  }

  enum {
    StopNoData = -1,
    MoreData = 0,
    StopWithData = 1,
  }

  // route one poly from source to dest
  // it routes both "end poly", and "stop"
  // each routed "end poly" will be followed by "stop"
  // returns:
  //   -1 if no data was transferred, and "stop" command read
  //    0 if some data was transferred, and "stop" command read (i.e. `vdest` got some data, but `vsrc` isn't)
  //   +1 if some data was transferred, and "end poly" command read (i.e. `vdest` got some data, and `vsrc` is not empty yet)
  int routeOnePoly () {
    if (isStop(lastCmd)) return -1; // just in case
    // clear destination
    vdest.removeAll();
    float x = 0.0f, y = 0.0f;
    readloop: for (;;) {
      // initial read?
      if (lastCmd == uint.max) {
        for (;;) {
          lastCmd = vsrc.vertex(&x, &y);
          if (isStop(lastCmd)) {
            // no more
            lastCmd = PathCommand.Stop;
            // route "stop"
            vdest.addVertex(x, y, PathCommand.Stop);
            return -1;
          }
          if (isMoveTo(lastCmd) || isLineTo(lastCmd)) { lastX = x; lastY = y; break; }
          if (!isEndPoly(lastCmd)) assert(0, "invalid path command");
        }
      }
      // here, `lastCmd` must be valid move/line
      assert(isMoveTo(lastCmd) || isLineTo(lastCmd));
      // if it is a move command, find next line command
      while (isMoveTo(lastCmd)) {
        // remember poly start
        startX = lastX;
        startY = lastY;
        lastCmd = vsrc.vertex(&x, &y);
        if (isStop(lastCmd)) {
          // no more
          // route "stop"
          vdest.addVertex(x, y, lastCmd);
          return -1;
        }
        if (isMoveTo(lastCmd) || isLineTo(lastCmd)) { lastX = x; lastY = y; continue; }
        if (isEndPoly(lastCmd)) { lastCmd = uint.max; continue readloop; }
        assert(0, "invalid path command");
      }
      // here we should have start coords from the previous move, and lineto
      assert(isLineTo(lastCmd));
      // move to the first point
      vdest.addVertex(startX, startY, PathCommand.MoveTo);
      for (;;) {
        vdest.addVertex(lastX, lastY, lastCmd);
        lastCmd = vsrc.vertex(&x, &y);
        if (isLineTo(lastCmd) || isMoveTo(lastCmd)) {
          lastX = x;
          lastY = y;
          if (isMoveTo(lastCmd)) {
            // end of poly?
            if (breakOnMoveTo) {
              // push "stop" to dest
              vdest.addVertex(x, y, PathCommand.Stop);
              break;
            }
            // if we don't want to stop on "moveTo", keep going
          }
          // route lineto
          vdest.addVertex(x, y, lastCmd);
          continue;
        }
        if (isEndPoly(lastCmd)) {
          // close if necessary
          if (isClose(lastCmd) && !ptEquals(startX, startY, lastX, lastY)) {
            // close it
            vdest.addVertex(startX, startY, PathCommand.LineTo);
            lastX = startX;
            lastY = startY;
          }
          // route "end poly"
          vdest.addVertex(x, y, lastCmd);
          // and push "stop" to dest
          vdest.addVertex(x, y, PathCommand.Stop);
          lastCmd = uint.max; // look for the next poly
          return 1; // was a transfer, has more data
        }
        if (isStop(lastCmd)) {
          // route stop, and stop
          vdest.addVertex(x, y, lastCmd);
          return 0;
        }
      }
    }
    assert(0);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// Vertex (x, y) with the distance to the next one. The last vertex has
// distance between the last and the first points if the polygon is closed
// and 0.0 if it's a polyline.
struct VertexDist {
public nothrow @trusted @nogc:
  float x = 0.0f;
  float y = 0.0f;
  float dist = 0.0f;

  this (in float ax, in float ay) pure { x = ax; y = ay; /*dist = 0.0f;*/ }

  bool opCall() (in auto ref VertexDist val) {
    //enum VertexDistEPS = cast(double)1e-14; // coinciding points maximal distance (epsilon)
    enum VertexDistEPS = cast(float)0.00001f; // coinciding points maximal distance (epsilon)
    immutable bool ret = (dist = distance(x, y, val.x, val.y)) > VertexDistEPS;
    if (!ret) dist = 1.0f/VertexDistEPS;
    return ret;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct SimpleVector(T) {
public nothrow @trusted @nogc:
  alias ValueType = T;
private:
  T* pvec;
  uint pvecSize, pvecAllot;

public:
  mixin(DisableCopyingMixin);

  ~this () {
    import core.stdc.stdlib : free;
    if (pvec !is null) free(pvec);
    pvec = null;
    pvecSize = pvecAllot = 0;
  }

  @property bool isEmpty () const pure { pragma(inline, true); return (pvecSize == 0); }
  @property uint length () const pure { pragma(inline, true); return pvecSize; }
  @property uint capacity () const pure { pragma(inline, true); return pvecAllot; }

  @property uint opDollar () const pure { pragma(inline, true); return pvecSize; }

  ref inout(T) opIndex (in uint idx) inout pure { pragma(inline, true); assert(pvecSize && idx < pvecSize); return pvec[idx]; }

  inout(T)[] opSlice (in uint lo, uint hi) inout pure {
    pragma(inline, true);
    if (hi > pvecSize) hi = pvecSize;
    return (lo < hi ? pvec[lo..hi] : null);
  }

  ref inout(T) curr (in uint idx) inout pure { pragma(inline, true); assert(pvecSize && idx < pvecSize); return pvec[idx]; }
  ref inout(T) prev (in uint idx) inout pure { pragma(inline, true); assert(pvecSize); return pvec[(idx+pvecSize-1u)%pvecSize]; }
  ref inout(T) next (in uint idx) inout pure { pragma(inline, true); assert(pvecSize); return pvec[(idx+1u)%pvecSize]; }

  void clear () {
    pragma(inline, true);
    if (pvec !is null) {
      import core.stdc.stdlib : free;
      free(pvec);
    }
    pvec = null;
    pvecSize = pvecAllot = 0;
  }

  void removeAll () {
    pragma(inline, true);
    pvecSize = 0;
  }

  void removeLast () {
    pragma(inline, true);
    if (pvecSize) --pvecSize;
  }

  void add() (in auto ref T val) {
    import core.stdc.stdlib : realloc;
    import core.stdc.string : memcpy;
    if (pvecSize == pvecAllot) {
      uint newsz = (pvecAllot|0xffu)+1u;
      pvec = cast(T*)realloc(pvec, T.sizeof*newsz);
      if (pvec is null) assert(0, "out of memory");
      pvecAllot = newsz;
      assert(pvecSize < pvecAllot);
    }
    memcpy(pvec+pvecSize, &val, T.sizeof);
    ++pvecSize;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct VertexSequence(T) {
public nothrow @trusted @nogc:
  SimpleVector!T me;
  alias me this;
  alias ValueType = T;

public:
  mixin(DisableCopyingMixin);

  void add() (in auto ref T val) {
    if (pvecSize > 1) {
      if (!pvec[pvecSize-2](pvec[pvecSize-1])) removeLast();
    }
    me.add(val);
  }

  void modifyLast() (in auto ref T val) {
    if (pvecSize > 0) removeLast();
    add(val);
  }

  // closed aka remove_flag
  void close (in bool closed) {
    while (pvecSize > 1) {
      if (pvec[pvecSize-2](pvec[pvecSize-1])) break;
      T t = pvec[pvecSize-1];
      removeLast();
      modifyLast(t);
    }
    if (closed) {
      while (pvecSize > 1) {
        if (pvec[pvecSize-1](pvec[0])) break;
        removeLast();
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
float calcPolygonArea(Storage) (in ref Storage st) {
  float sum = 0;
  float x = st[0].x;
  float y = st[0].y;
  float xs = x;
  float ys = y;
  foreach (immutable uint i; 1..st.length) {
    auto v = &st[i];
    sum += x*v.y-y*v.x;
    x = v.x;
    y = v.y;
  }
  return (sum+x*ys-y*xs)*0.5f;
}


// ////////////////////////////////////////////////////////////////////////// //
void shortenPath(VertexSequence) (ref VertexSequence vs, float s, in uint closed=0) {
  alias VertexType = VertexSequence.ValueType;
  if (s > 0.0f && vs.length > 1) {
    int n = cast(int)(vs.length-2);
    while (n) {
      immutable float d = vs[n].dist;
      if (d > s) break;
      vs.removeLast();
      s -= d;
      --n;
    }
    if (vs.length < 2) {
      vs.removeAll();
    } else {
      n = vs.length-1;
      VertexType* prev = &vs[n-1];
      VertexType* last = &vs[n];
      immutable float d = (prev.dist-s)/prev.dist;
      immutable float x = prev.x+(last.x-prev.x)*d;
      immutable float y = prev.y+(last.y-prev.y)*d;
      last.x = x;
      last.y = y;
      if (!(*prev)(*last)) vs.removeLast();
      vs.close(closed != 0);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// this connects vertex source to vertex generator
struct PathPoint {
  float x = 0.0f, y = 0.0f;
  bool moveto;
  @property bool lineto () const pure nothrow @safe @nogc { pragma(inline, true); return !moveto; }
}


struct PathPointRange {
private:
  SimpleVector!PathPoint pts;
  uint idx = 0;

public nothrow @trusted @nogc:
  mixin(DisableCopyingMixin);

  this(VG) (ref VG vg) {
    // feed dest
    vg.rewind();
    float x, y;
    float sx, sy;
    bool first = true;
    for (;;) {
      auto cmd = vs.vertex(&x, &y);
      if (isStop(cmd)) break;
      if (isMoveTo(cmd)) {
        sx = x;
        sy = y;
        first = true;
        continue;
      }
      if (isLineTo(cmd)) {
        /*
        import std.math : isNaN;
        assert(!sx.isNaN);
        assert(!sy.isNaN);
        */
        if (first) { pts.add(PathPoint(sx, sy, true)); }
        pts.add(PathPoint(x, y, false));
        continue;
      }
      if (isEndPoly(cmd)) {
        if (isClose(cmd) && !first) {
          //writeln("s=(", sx, ",", sy, "); c=(", x, ",", y, ")");
          pts.add(PathPoint(sx, sy, false));
        }
      }
      first = true;
    }
  }

  void rewind () { pragma(inline, true); idx = 0; }

  @property bool empty () const pure { pragma(inline, true); return (idx >= pts.length); }
  @property ref inout(PathPoint) front () inout pure { pragma(inline, true); return pts[idx]; }
  @property int length () const pure { pragma(inline, true); return pts.length-idx; }
  void popFront () { if (idx < pts.length) ++idx; }
}


// ////////////////////////////////////////////////////////////////////////// //
bool intersection (in float ax, in float ay, in float bx, in float by,
                   in float cx, in float cy, in float dx, in float dy, float* x, float* y) pure nothrow @trusted @nogc
{
  //enum IntersectionEPS = cast(double)1.0e-30; // See calc_intersection
  enum IntersectionEPS = cast(float)1.0e-16; // See calc_intersection
  //version(aliced) pragma(inline, true);
  //import std.math : abs;
  //import core.stdc.math : fabsf;
  immutable float num = (ay-cy)*(dx-cx)-(ax-cx)*(dy-cy);
  immutable float den = (bx-ax)*(dy-cy)-(by-ay)*(dx-cx);
  //if (fabsf(den) < IntersectionEPS) return false;
  if ((den < 0.0f ? (-den < IntersectionEPS) : (den < IntersectionEPS))) return false;
  immutable float r = num/den;
  if (x !is null) *x = ax+r*(bx-ax);
  if (y !is null) *y = ay+r*(by-ay);
  return true;
}

float cross3 (in float x1, in float y1, in float x2, in float y2, in float x, in float y) pure nothrow @safe @nogc {
  pragma(inline, true);
  return (x-x2)*(y2-y1)-(y-y2)*(x2-x1);
}

float cross2 (in float dx0, in float dy0, in float dx1, in float dy1) pure nothrow @safe @nogc {
  pragma(inline, true);
  return dx1*dy0-dx0*dy1;
}

float distance (in float x1, in float y1, in float x2, in float y2) /*pure*/ nothrow @safe @nogc {
  pragma(inline, true);
  import core.stdc.math : sqrtf;
  immutable float dx = x2-x1;
  immutable float dy = y2-y1;
  return sqrtf(dx*dx+dy*dy);
}

float distance() (in auto ref VertexDist v0, in auto ref VertexDist v1) /*pure*/ nothrow @safe @nogc {
  pragma(inline, true);
  import core.stdc.math : sqrtf;
  immutable float dx = v1.x-v0.x;
  immutable float dy = v1.y-v0.y;
  return sqrtf(dx*dx+dy*dy);
}

float distPtSegSq (in float x, in float y, in float px, in float py, in float qx, in float qy) pure nothrow @safe @nogc {
  immutable float pqx = qx-px;
  immutable float pqy = qy-py;
  float dx = x-px;
  float dy = y-py;
  immutable float d = pqx*pqx+pqy*pqy;
  float t = pqx*dx+pqy*dy;
  if (d > 0.0f) t /= d;
  if (t < 0.0f) t = 0.0f; else if (t > 1.0f) t = 1.0f;
  dx = px+t*pqx-x;
  dy = py+t*pqy-y;
  return dx*dx+dy*dy;
}

T min(T) (in T a, in T b) pure nothrow @safe @nogc { pragma(inline, true); return (a < b ? a : b); }
T max(T) (in T a, in T b) pure nothrow @safe @nogc { pragma(inline, true); return (a > b ? a : b); }
T sign(T) (in T a) pure nothrow @safe @nogc { pragma(inline, true); return (a >= cast(T)0 ? cast(T)1 : cast(T)(-1)); }

void normalize (ref float x, ref float y) nothrow @safe @nogc {
  import core.stdc.math : sqrtf;
  immutable float d = sqrtf(x*x+y*y);
  if (d > 1e-6f) {
    immutable float id = 1.0f/d;
    x *= id;
    y *= id;
  }
  //return d;
}


// ////////////////////////////////////////////////////////////////////////// //
// Matrices and Transformations

// matrix class
public align(1) struct AGGMatrix {
align(1):
private:
  static immutable float[6] IdentityMat = [
    1.0f, 0.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,
  ];

private:
  /// Matrix values. Initial value is identity matrix.
  float[6] mat = [
    1.0f, 0.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,
  ];
  int mIdentity = 1;

public nothrow @trusted @nogc:
  /// Create Matrix with the given values.
  this (const(float)[] amat...) {
    pragma(inline, true);
    if (amat.length >= 6) {
      mat.ptr[0..6] = amat.ptr[0..6];
    } else {
      mat.ptr[0..6] = 0.0f;
      mat.ptr[0..amat.length] = amat[];
    }
    mIdentity = (mat[] == IdentityMat[]);
  }

  @property int debugGetMIdentity () const pure { pragma(inline, true); return mIdentity; }

  /// Can be used to check validity of [inverted] result
  @property bool valid () const pure {
    pragma(inline, true);
    /*import core.stdc.math : isfinite; return (isfinite(mat.ptr[0]) != 0);*/
    return (mat.ptr[0] == mat.ptr[0]);
  }

  /// Returns `true` if this matrix is identity matrix.
  @property bool isIdentity () const pure { pragma(inline, true); return (mIdentity >= 0 ? (mIdentity != 0) : (mat[] == IdentityMat[])); }

  @property bool isIdentity () pure {
    pragma(inline, true);
    if (mIdentity < 0) mIdentity = (mat[] == IdentityMat[]);
    return (mIdentity != 0);
  }

  float opIndex (in uint idx) const pure { pragma(inline, true); return (idx < 6 ? mat.ptr[idx] : 0.0f); }

  void opIndexAssign (in float v, in uint idx) {
    pragma(inline, true);
    if (idx < 6) { mat.ptr[idx] = v; mIdentity = -1; }
  }

  /// Returns new inverse matrix.
  /// If inverted matrix cannot be calculated, `res.valid` fill be `false`.
  AGGMatrix inverted () const {
    AGGMatrix res = this;
    return res.invert;
  }

  /// Inverts this matrix.
  /// If inverted matrix cannot be calculated, `this.valid` fill be `false`.
  ref AGGMatrix invert () {
    float[6] inv = void;
    immutable double det = cast(double)mat.ptr[0]*mat.ptr[3]-cast(double)mat.ptr[2]*mat.ptr[1];
    if (det > -1e-6 && det < 1e-6) {
      inv[] = float.nan;
    } else {
      immutable double invdet = 1.0/det;
      inv.ptr[0] = cast(float)(mat.ptr[3]*invdet);
      inv.ptr[2] = cast(float)(-mat.ptr[2]*invdet);
      inv.ptr[4] = cast(float)((cast(double)mat.ptr[2]*mat.ptr[5]-cast(double)mat.ptr[3]*mat.ptr[4])*invdet);
      inv.ptr[1] = cast(float)(-mat.ptr[1]*invdet);
      inv.ptr[3] = cast(float)(mat.ptr[0]*invdet);
      inv.ptr[5] = cast(float)((cast(double)mat.ptr[1]*mat.ptr[4]-cast(double)mat.ptr[0]*mat.ptr[5])*invdet);
    }
    mat.ptr[0..6] = inv.ptr[0..6];
    mIdentity = -1;
    return this;
  }

  /// Sets this matrix to identity matrix.
  ref AGGMatrix identity () { pragma(inline, true); mat[] = IdentityMat[]; mIdentity = 1; return this; }

  /// Translate this matrix.
  ref AGGMatrix translate (in float tx, in float ty) {
    pragma(inline, true);
    return this.mul(Translated(tx, ty));
  }

  /// Scale this matrix.
  ref AGGMatrix scale (in float sx, in float sy) {
    pragma(inline, true);
    return this.mul(Scaled(sx, sy));
  }

  /// Rotate this matrix.
  ref AGGMatrix rotate (in float a) {
    pragma(inline, true);
    return this.mul(Rotated(a));
  }

  /// Skew this matrix by X axis.
  ref AGGMatrix skewX (in float a) {
    pragma(inline, true);
    return this.mul(SkewedX(a));
  }

  /// Skew this matrix by Y axis.
  ref AGGMatrix skewY (in float a) {
    pragma(inline, true);
    return this.mul(SkewedY(a));
  }

  /// Skew this matrix by both axes.
  ref AGGMatrix skewY (in float ax, in float ay) {
    pragma(inline, true);
    return this.mul(SkewedXY(ax, ay));
  }

  /// Transform point with this matrix. `null` destinations are allowed.
  /// [sx] and [sy] is the source point. [dx] and [dy] may point to the same variables.
  void point (float* dx, float* dy, in float sx, in float sy) const {
    pragma(inline, true);
    if (mIdentity <= 0) {
      if (dx !is null) *dx = sx*mat.ptr[0]+sy*mat.ptr[2]+mat.ptr[4];
      if (dy !is null) *dy = sx*mat.ptr[1]+sy*mat.ptr[3]+mat.ptr[5];
    } else {
      if (dx !is null) *dx = sx;
      if (dy !is null) *dy = sy;
    }
  }

  /// Transform point with this matrix.
  void point (ref float x, ref float y) const {
    pragma(inline, true);
    if (mIdentity <= 0) {
      immutable float nx = x*mat.ptr[0]+y*mat.ptr[2]+mat.ptr[4];
      immutable float ny = x*mat.ptr[1]+y*mat.ptr[3]+mat.ptr[5];
      x = nx;
      y = ny;
    }
  }

  /// Sets this matrix to the result of multiplication of `this` and [s] (this * S).
  ref AGGMatrix mul() (in auto ref AGGMatrix s) {
    immutable float t0 = mat.ptr[0]*s.mat.ptr[0]+mat.ptr[1]*s.mat.ptr[2];
    immutable float t2 = mat.ptr[2]*s.mat.ptr[0]+mat.ptr[3]*s.mat.ptr[2];
    immutable float t4 = mat.ptr[4]*s.mat.ptr[0]+mat.ptr[5]*s.mat.ptr[2]+s.mat.ptr[4];
    mat.ptr[1] = mat.ptr[0]*s.mat.ptr[1]+mat.ptr[1]*s.mat.ptr[3];
    mat.ptr[3] = mat.ptr[2]*s.mat.ptr[1]+mat.ptr[3]*s.mat.ptr[3];
    mat.ptr[5] = mat.ptr[4]*s.mat.ptr[1]+mat.ptr[5]*s.mat.ptr[3]+s.mat.ptr[5];
    mat.ptr[0] = t0;
    mat.ptr[2] = t2;
    mat.ptr[4] = t4;
    mIdentity = -1;
    return this;
  }

  /// Sets this matrix to the result of multiplication of [s] and `this` (S * this).
  /// Sets the transform to the result of multiplication of two transforms, of A = B*A.
  /// Group: matrices
  ref AGGMatrix premul() (in auto ref AGGMatrix s) {
    AGGMatrix s2 = s;
    s2.mul(this);
    mat[] = s2.mat[];
    mIdentity = -1;
    return this;
  }

  /// Multiply this matrix by [s], return result as new matrix.
  /// Performs operations in this left-to-right order.
  AGGMatrix opBinary(string op="*") (in auto ref AGGMatrix s) const {
    pragma(inline, true);
    AGGMatrix res = this;
    return res.mul(s);
  }

  /// Multiply this matrix by [s].
  /// Performs operations in this left-to-right order.
  ref AGGMatrix opOpAssign(string op="*") (in auto ref AGGMatrix s) {
    pragma(inline, true);
    return this.mul(s);
  }

  float scaleX () const { pragma(inline, true); import core.stdc.math : sqrtf; return sqrtf(mat.ptr[0]*mat.ptr[0]+mat.ptr[2]*mat.ptr[2]); } /// Returns x scaling of this matrix.
  float scaleY () const { pragma(inline, true); import core.stdc.math : sqrtf; return sqrtf(mat.ptr[1]*mat.ptr[1]+mat.ptr[3]*mat.ptr[3]); } /// Returns y scaling of this matrix.
  float rotation () const { pragma(inline, true); import core.stdc.math : atan2f; return atan2f(mat.ptr[1], mat.ptr[0]); } /// Returns rotation of this matrix.
  float tx () const pure { pragma(inline, true); return mat.ptr[4]; } /// Returns x translation of this matrix.
  float ty () const pure { pragma(inline, true); return mat.ptr[5]; } /// Returns y translation of this matrix.

  ref AGGMatrix scaleX (in float v) { pragma(inline, true); return scaleRotateTransform(v, scaleY, rotation, tx, ty); } /// Sets x scaling of this matrix.
  ref AGGMatrix scaleY (in float v) { pragma(inline, true); return scaleRotateTransform(scaleX, v, rotation, tx, ty); } /// Sets y scaling of this matrix.
  ref AGGMatrix rotation (in float v) { pragma(inline, true); return scaleRotateTransform(scaleX, scaleY, v, tx, ty); } /// Sets rotation of this matrix.
  ref AGGMatrix tx (in float v) { pragma(inline, true); mat.ptr[4] = v; mIdentity = -1; return this; } /// Sets x translation of this matrix.
  ref AGGMatrix ty (in float v) { pragma(inline, true); mat.ptr[5] = v; mIdentity = -1; return this; } /// Sets y translation of this matrix.

  /// Utility function to be used in `setXXX()`.
  /// This is the same as doing: `mat.identity.rotate(a).scale(xs, ys).translate(tx, ty)`, only faster
  ref AGGMatrix scaleRotateTransform (in float xscale, in float yscale, in float a, in float tx, in float ty) {
    import core.stdc.math : sinf, cosf;
    immutable float cs = cosf(a), sn = sinf(a);
    mat.ptr[0] = xscale*cs; mat.ptr[1] = yscale*sn;
    mat.ptr[2] = xscale*-sn; mat.ptr[3] = yscale*cs;
    mat.ptr[4] = tx; mat.ptr[5] = ty;
    mIdentity = -1;
    return this;
  }

  /// This is the same as doing: `mat.identity.rotate(a).translate(tx, ty)`, only faster
  ref AGGMatrix rotateTransform (in float a, in float tx, in float ty) {
    import core.stdc.math : sinf, cosf;
    immutable float cs = cosf(a), sn = sinf(a);
    mat.ptr[0] = cs; mat.ptr[1] = sn;
    mat.ptr[2] = -sn; mat.ptr[3] = cs;
    mat.ptr[4] = tx; mat.ptr[5] = ty;
    mIdentity = -1;
    return this;
  }

  /// Returns new identity matrix.
  static AGGMatrix Identity () { pragma(inline, true); return AGGMatrix.init; }

  /// Returns new translation matrix.
  static AGGMatrix Translated (in float tx, in float ty) {
    AGGMatrix res = void;
    res.mat.ptr[0] = 1.0f; res.mat.ptr[1] = 0.0f;
    res.mat.ptr[2] = 0.0f; res.mat.ptr[3] = 1.0f;
    res.mat.ptr[4] = tx; res.mat.ptr[5] = ty;
    res.mIdentity = -1;
    return res;
  }

  /// Returns new scaling matrix.
  static AGGMatrix Scaled (in float sx, in float sy) {
    AGGMatrix res = void;
    res.mat.ptr[0] = sx; res.mat.ptr[1] = 0.0f;
    res.mat.ptr[2] = 0.0f; res.mat.ptr[3] = sy;
    res.mat.ptr[4] = 0.0f; res.mat.ptr[5] = 0.0f;
    res.mIdentity = -1;
    return res;
  }

  /// Returns new rotation matrix. Angle is specified in radians.
  static AGGMatrix Rotated (in float a) {
    import core.stdc.math : sinf, cosf;
    immutable float cs = cosf(a), sn = sinf(a);
    AGGMatrix res = void;
    res.mat.ptr[0] = cs; res.mat.ptr[1] = sn;
    res.mat.ptr[2] = -sn; res.mat.ptr[3] = cs;
    res.mat.ptr[4] = 0.0f; res.mat.ptr[5] = 0.0f;
    res.mIdentity = -1;
    return res;
  }

  /// Returns new x-skewing matrix. Angle is specified in radians.
  static AGGMatrix SkewedX (in float a) {
    import core.stdc.math : tanf;
    AGGMatrix res = void;
    res.mat.ptr[0] = 1.0f; res.mat.ptr[1] = 0.0f;
    res.mat.ptr[2] = tanf(a); res.mat.ptr[3] = 1.0f;
    res.mat.ptr[4] = 0.0f; res.mat.ptr[5] = 0.0f;
    res.mIdentity = -1;
    return res;
  }

  /// Returns new y-skewing matrix. Angle is specified in radians.
  static AGGMatrix SkewedY (in float a) {
    import core.stdc.math : tanf;
    AGGMatrix res = void;
    res.mat.ptr[0] = 1.0f; res.mat.ptr[1] = tanf(a);
    res.mat.ptr[2] = 0.0f; res.mat.ptr[3] = 1.0f;
    res.mat.ptr[4] = 0.0f; res.mat.ptr[5] = 0.0f;
    res.mIdentity = -1;
    return res;
  }

  /// Returns new xy-skewing matrix. Angles are specified in radians.
  static AGGMatrix SkewedXY (in float ax, in float ay) {
    import core.stdc.math : tanf;
    AGGMatrix res = void;
    res.mat.ptr[0] = 1.0f; res.mat.ptr[1] = tanf(ay);
    res.mat.ptr[2] = tanf(ax); res.mat.ptr[3] = 1.0f;
    res.mat.ptr[4] = 0.0f; res.mat.ptr[5] = 0.0f;
    res.mIdentity = -1;
    return res;
  }

  /// Utility function to be used in `setXXX()`.
  /// This is the same as doing: `AGGMatrix.Identity.rotate(a).scale(xs, ys).translate(tx, ty)`, only faster
  static AGGMatrix ScaledRotatedTransformed (in float xscale, in float yscale, in float a, in float tx, in float ty) {
    pragma(inline, true);
    AGGMatrix res = void;
    return res.scaleRotateTransform(xscale, yscale, a, tx, ty);
  }

  /// This is the same as doing: `AGGMatrix.Identity.rotate(a).translate(tx, ty)`, only faster
  static AGGMatrix RotatedTransformed (in float a, in float tx, in float ty) {
    pragma(inline, true);
    AGGMatrix res = void;
    return res.rotateTransform(a, tx, ty);
  }

public:
  // premultiplies current coordinate system by specified matrix
  ref AGGMatrix transform() (in auto ref AGGMatrix mt) { pragma(inline, true); this *= mt; return this; }

  // translates current coordinate system
  ref AGGMatrix translatePre (in float x, in float y) { pragma(inline, true); return premul(AGGMatrix.Translated(x, y)); }

  // rotates current coordinate system; angle is specified in radians
  ref AGGMatrix rotatePre (in float angle) { pragma(inline, true); return premul(AGGMatrix.Rotated(angle)); }

  // skews the current coordinate system along X axis; angle is specified in radians
  ref AGGMatrix skewXPre (in float angle) { pragma(inline, true); return premul(AGGMatrix.SkewedX(angle)); }

  // skews the current coordinate system along Y axis; angle is specified in radians
  ref AGGMatrix skewYPre (in float angle) { pragma(inline, true); return premul(AGGMatrix.SkewedY(angle)); }

  // scales the current coordinate system
  ref AGGMatrix scalePre (in float x, in float y) { pragma(inline, true); return premul(AGGMatrix.Scaled(x, y)); }
}
