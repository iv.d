/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gfx.aggmini /*is aliced*/;


// ////////////////////////////////////////////////////////////////////////// //
package/*(iv.egra.gfx.aggmini)*/ {
  enum DisableCopyingMixin = "@disable this (this); @disable void opAssign() (in auto ref typeof(this));";
}


// ////////////////////////////////////////////////////////////////////////// //
public import iv.egra.gfx.config;
public import iv.egra.gfx.base;
public import iv.egra.gfx.aggmini.enums;
public import iv.egra.gfx.aggmini.render;
public import iv.egra.gfx.aggmini.core;
public import iv.egra.gfx.aggmini.supmath;
public import iv.egra.gfx.aggmini.stroker;

public __gshared AGGDrawer gxagg;
