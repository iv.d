/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gfx.backgl /*is aliced*/;
private:

import arsd.simpledisplay;

import iv.alice;
import iv.cmdcon;
//import iv.glbinds : glTexParameterfv; // rdmd hack

import iv.egra.gfx.config;
import iv.egra.gfx.base;
import iv.egra.gfx.lowlevel;


// ////////////////////////////////////////////////////////////////////////// //
enum GLTexType = GL_BGRA; // GL_RGBA;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared uint vglTexId; // OpenGL texture id
public __gshared uint vArrowTextureId = 0;


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  import core.stdc.stdlib : malloc;
  egfxCheckCPU();
  // always allocate additional 16 bytes for SSE routines
  vglTexBuf = cast(uint*)malloc((cast(uint)VBufWidth*cast(uint)VBufHeight)*vglTexBuf[0].sizeof+16u);
  if (vglTexBuf is null) { import core.exception : onOutOfMemoryErrorNoGC; onOutOfMemoryErrorNoGC(); }
  vglTexBuf[0..VBufWidth*VBufHeight+4] = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
public void vglCreateArrowTexture () {
  //import iv.glbinds;

  enum wrapOpt = GL_REPEAT;
  enum filterOpt = GL_NEAREST; //GL_LINEAR;
  enum ttype = GL_UNSIGNED_BYTE;

  if (vArrowTextureId) glDeleteTextures(1, &vArrowTextureId);
  vArrowTextureId = 0;
  glGenTextures(1, &vArrowTextureId);
  if (vArrowTextureId == 0) assert(0, "can't create arrow texture");

  //GLint gltextbinding;
  //glGetIntegerv(GL_TEXTURE_BINDING_2D, &gltextbinding);
  //scope(exit) glBindTexture(GL_TEXTURE_2D, gltextbinding);

  glBindTexture(GL_TEXTURE_2D, vArrowTextureId);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapOpt);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapOpt);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterOpt);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterOpt);
  //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  //GLfloat[4] bclr = 0.0;
  //glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bclr.ptr);

  uint[16*8] pmap = 0x00_000000U;
  // sprite,sprite,mask,mask
  static immutable ushort[$] spx = [
    0b11111111_10000000, 0b00000000_01111111,
    0b01000000_10000000, 0b10000000_01111111,
    0b00100000_10000000, 0b11000000_01111111,
    0b00010000_01100000, 0b11100000_00011111,
    0b00001001_10011000, 0b11110000_00000111,
    0b00000110_01100110, 0b11111001_10000001,
    0b00000000_00011001, 0b11111111_11100000,
    0b00000000_00000110, 0b11111111_11111001,
  ];

  foreach (immutable dy; 0..8) {
    ushort spr = spx[dy*2+0];
    ushort msk = spx[dy*2+1];
    foreach (immutable dx; 0..16) {
      if ((msk&0x8000) == 0) {
        pmap[dy*16+dx] = (spr&0x8000 ? 0xff_ffffffU : 0xff_000000U);
      }
      msk <<= 1;
      spr <<= 1;
    }
  }
  //pmap = 0xff_ff0000U;
  //pmap[0] = 0;

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 16, 8, 0, GLTexType, GL_UNSIGNED_BYTE, pmap.ptr);
}


// ////////////////////////////////////////////////////////////////////////// //
public void vglBlitArrow (int px, int py) {
  if (vArrowTextureId != 0) {
    glMatrixMode(GL_PROJECTION); // for ortho camera
    glLoadIdentity();
    // left, right, bottom, top, near, far
    //glViewport(0, 0, w*vbufEffScale, h*vbufEffScale);
    //glOrtho(0, w, h, 0, -1, 1); // top-to-bottom
    glViewport(0, 0, VBufWidth*vbufEffScale, VBufHeight*vbufEffScale);
    glOrtho(0, VBufWidth, VBufHeight, 0, -1, 1); // top-to-bottom
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glDisable(GL_DITHER);
    glDisable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glColor4f(1, 1, 1, 1);
    glBindTexture(GL_TEXTURE_2D, vArrowTextureId);
    //scope(exit) glBindTexture(GL_TEXTURE_2D, 0);
    glBegin(GL_QUADS);
      glTexCoord2f(0.0f, 0.0f); glVertex2i(px, py); // top-left
      glTexCoord2f(1.0f, 0.0f); glVertex2i(px+16*2, py); // top-right
      glTexCoord2f(1.0f, 1.0f); glVertex2i(px+16*2, py+8*2); // bottom-right
      glTexCoord2f(0.0f, 1.0f); glVertex2i(px, py+8*2); // bottom-left
    glEnd();
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// resize buffer, reinitialize OpenGL texture
public void vglResizeBuffer (int wdt, int hgt, int ascale=1) {
  //import iv.glbinds;

  if (wdt < 1) wdt = 1;
  if (hgt < 1) hgt = 1;

  if (wdt > 8192) wdt = 8192;
  if (hgt > 8192) hgt = 8192;

  bool sizeChanged = (wdt != VBufWidth || hgt != VBufHeight);
  VBufWidth = wdt;
  VBufHeight = hgt;

  if (vglTexBuf is null || sizeChanged) {
    import core.stdc.stdlib : realloc;
    // always allocate additional 16 bytes for SSE routines
    vglTexBuf = cast(uint*)realloc(vglTexBuf, (cast(uint)VBufWidth*cast(uint)VBufHeight)*vglTexBuf[0].sizeof+16u);
    if (vglTexBuf is null) { import core.exception : onOutOfMemoryErrorNoGC; onOutOfMemoryErrorNoGC(); }
    vglTexBuf[0..VBufWidth*VBufHeight+4] = 0;
  }

  if (vglTexId == 0 || sizeChanged) {
    enum wrapOpt = GL_REPEAT;
    enum filterOpt = GL_NEAREST; //GL_LINEAR;
    enum ttype = GL_UNSIGNED_BYTE;

    if (vglTexId) glDeleteTextures(1, &vglTexId);
    vglTexId = 0;
    glGenTextures(1, &vglTexId);
    if (vglTexId == 0) assert(0, "can't create OpenGL texture");

    //GLint gltextbinding;
    //glGetIntegerv(GL_TEXTURE_BINDING_2D, &gltextbinding);
    //scope(exit) glBindTexture(GL_TEXTURE_2D, gltextbinding);

    glBindTexture(GL_TEXTURE_2D, vglTexId);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapOpt);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapOpt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterOpt);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterOpt);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    //GLfloat[4] bclr = 0.0;
    //glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bclr.ptr);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, VBufWidth, VBufHeight, 0, GLTexType, GL_UNSIGNED_BYTE, vglTexBuf);
  }

  if (ascale < 1) ascale = 1;
  if (ascale > 32) ascale = 32;
  vbufEffScale = cast(ubyte)ascale;
}


// ////////////////////////////////////////////////////////////////////////// //
public void vglUpdateTexture () {
  if (vglTexId != 0) {
    glBindTexture(GL_TEXTURE_2D, vglTexId);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0/*x*/, 0/*y*/, VBufWidth, VBufHeight, GLTexType, GL_UNSIGNED_BYTE, vglTexBuf);
    //glBindTexture(GL_TEXTURE_2D, 0);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void vglBlitTexture () {
  if (vglTexId != 0) {
    glMatrixMode(GL_PROJECTION); // for ortho camera
    glLoadIdentity();
    // left, right, bottom, top, near, far
    //glViewport(0, 0, w*vbufEffScale, h*vbufEffScale);
    //glOrtho(0, w, h, 0, -1, 1); // top-to-bottom
    glViewport(0, 0, VBufWidth*vbufEffScale, VBufHeight*vbufEffScale);
    glOrtho(0, VBufWidth, VBufHeight, 0, -1, 1); // top-to-bottom
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    glDisable(GL_LIGHTING);
    glDisable(GL_DITHER);
    //glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_BLEND);
    //glDisable(GL_STENCIL_TEST);

    if (vglTexId) {
      immutable w = VBufWidth;
      immutable h = VBufHeight;

      glColor4f(1, 1, 1, 1);
      glBindTexture(GL_TEXTURE_2D, vglTexId);
      //scope(exit) glBindTexture(GL_TEXTURE_2D, 0);
      glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f); glVertex2i(0, 0); // top-left
        glTexCoord2f(1.0f, 0.0f); glVertex2i(w, 0); // top-right
        glTexCoord2f(1.0f, 1.0f); glVertex2i(w, h); // bottom-right
        glTexCoord2f(0.0f, 1.0f); glVertex2i(0, h); // bottom-left
      glEnd();
    }
  }
}
