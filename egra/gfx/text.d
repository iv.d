/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gfx.text /*is aliced*/;
private:

import iv.alice;
import iv.bclamp;
import iv.cmdcon;
import iv.dynstring;
import iv.fontconfig;
import iv.freetype;
import iv.utfutil;
import iv.vfs;

import iv.egra.gfx.config;
import iv.egra.gfx.base;
import iv.egra.gfx.aggmini;

version (DigitalMars) {
  version(X86) {
    version = EGRA_GFX_TEXT_ASM_ALLOWED;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
enum ReplacementChar = 0xFFFD;
//__gshared string egraFontName = "Arial:pixelsize=16";
//__gshared string egraFontName = "Verdana:pixelsize=16";
public __gshared string egraFontName = "Verdana:weight=bold:pixelsize=16";
public __gshared int egraFontSize = 100; // percents
__gshared string egraFontFile;
__gshared int fontSize;
__gshared int fontHeight;
__gshared int fontBaselineOfs;


// ////////////////////////////////////////////////////////////////////////// //
__gshared ubyte* ttfontdata;
__gshared uint ttfontdatasize;

__gshared FT_Library ttflibrary;
__gshared FTC_Manager ttfcache;
__gshared FTC_CMapCache ttfcachecmap;
__gshared FTC_ImageCache ttfcacheimage;


shared static ~this () {
  if (ttflibrary) {
    if (ttfcache) {
      FTC_Manager_Done(ttfcache);
      ttfcache = null;
    }
    FT_Done_FreeType(ttflibrary);
    ttflibrary = null;
  }
}


enum FontID = cast(FTC_FaceID)1;


extern(C) nothrow {
  void ttfFontFinalizer (void* obj) {
    import core.stdc.stdlib : free;
    if (obj is null) return;
    auto tf = cast(iv.freetype.FT_Face)obj;
    if (tf.generic.data !is ttfontdata) return;
    if (ttfontdata !is null) {
      version(aliced) conwriteln("TTF CACHE: freeing loaded font...");
      free(ttfontdata);
      ttfontdata = null;
      ttfontdatasize = 0;
    }
  }

  FT_Error ttfFontLoader (FTC_FaceID face_id, FT_Library library, FT_Pointer request_data, iv.freetype.FT_Face* aface) {
    if (face_id == FontID) {
      try {
        if (ttfontdata is null) {
          conwriteln("TTF CACHE: loading '", egraFontFile, "'...");
          import core.stdc.stdlib : malloc;
          auto fl = VFile(egraFontFile);
          auto fsz = fl.size;
          if (fsz < 16 || fsz > int.max/8) throw new Exception("invalid ttf size");
          ttfontdatasize = cast(uint)fsz;
          ttfontdata = cast(ubyte*)malloc(ttfontdatasize);
          if (ttfontdata is null) { import core.exception : onOutOfMemoryErrorNoGC; onOutOfMemoryErrorNoGC(); }
          fl.rawReadExact(ttfontdata[0..ttfontdatasize]);
        }
        auto res = FT_New_Memory_Face(library, cast(const(FT_Byte)*)ttfontdata, ttfontdatasize, 0, aface);
        if (res != 0) throw new Exception("error loading ttf: '"~egraFontFile~"'");
        (*aface).generic.data = ttfontdata;
        (*aface).generic.finalizer = &ttfFontFinalizer;
      } catch (Exception e) {
        if (ttfontdata !is null) {
          import core.stdc.stdlib : free;
          free(ttfontdata);
          ttfontdata = null;
          ttfontdatasize = 0;
        }
        version(aliced) conwriteln("ERROR loading font: ", e.msg);
        return FT_Err_Cannot_Open_Resource;
      }
      return FT_Err_Ok;
    } else {
      version(aliced) conwriteln("TTF CACHE: invalid font id");
    }
    return FT_Err_Cannot_Open_Resource;
  }
}


void ttfLoad () nothrow {
  if (FT_Init_FreeType(&ttflibrary)) assert(0, "can't initialize FreeType");
  if (FTC_Manager_New(ttflibrary, 1/*max faces*/, 0, 0, &ttfFontLoader, null, &ttfcache)) assert(0, "can't initialize FreeType cache manager");
  if (FTC_CMapCache_New(ttfcache, &ttfcachecmap)) assert(0, "can't initialize FreeType cache manager");
  if (FTC_ImageCache_New(ttfcache, &ttfcacheimage)) assert(0, "can't initialize FreeType cache manager");

  FTC_ScalerRec fsc;
  fsc.face_id = FontID;
  fsc.width = 0;
  fsc.height = fontSize;
  fsc.pixel = 1; // size in pixels

  FT_Size ttfontsz;
  if (FTC_Manager_LookupSize(ttfcache, &fsc, &ttfontsz)) assert(0, "cannot find FreeType font");
  fontHeight = cast(int)ttfontsz.metrics.height>>6; // 26.6
  fontBaselineOfs = cast(int)((ttfontsz.metrics.height+ttfontsz.metrics.descender)>>6);
  if (fontHeight < 2 || fontHeight > 128) assert(0, "invalid FreeType font metrics");

  version(aliced) conwriteln("TTF CACHE initialized.");
}


void initFontEngine () nothrow {
  import std.string : fromStringz/*, toStringz*/;
  import std.internal.cstring : tempCString;

  if (ttflibrary !is null) return;
  if (!FcInit()) assert(0, "cannot init fontconfig");

  iv.fontconfig.FcPattern* pat = FcNameParse(egraFontName.tempCString);
  if (pat is null) assert(0, "cannot parse font name");

  if (!FcConfigSubstitute(null, pat, FcMatchPattern)) assert(0, "cannot find fontconfig substitute");
  FcDefaultSubstitute(pat);

  // find the font
  iv.fontconfig.FcResult result;
  iv.fontconfig.FcPattern* font = FcFontMatch(null, pat, &result);
  if (font !is null) {
    char* file = null;
    if (FcPatternGetString(font, FC_FILE, 0, &file) == FcResultMatch) {
      version(aliced) conwriteln("font file: [", file, "]");
      egraFontFile = file.fromStringz.idup;
    }
    double pixelsize;
    if (FcPatternGetDouble(font, FC_PIXEL_SIZE, 0, &pixelsize) == FcResultMatch) {
      version(aliced) conwriteln("pixel size: ", pixelsize);
      fontSize = cast(int)pixelsize;
    }
  }
  FcPatternDestroy(pat);

  // arbitrary limits
  if (fontSize < 6) fontSize = 6;
  if (fontSize > 42) fontSize = 42;

  ttfLoad();
}


// ////////////////////////////////////////////////////////////////////////// //
public void utfByDChar (const(char)[] s, scope void delegate (dchar ch) nothrow @safe dg) nothrow @safe {
  if (dg is null || s.length == 0) return;
  Utf8DecoderFast dc;
  foreach (char ch; s) {
    if (dc.decode(cast(ubyte)ch)) dg(dc.complete ? dc.codepoint : dc.replacement);
  }
}


public void utfByDCharSPos (const(char)[] s, scope void delegate (dchar ch, usize stpos) nothrow @safe dg) nothrow @safe {
  if (dg is null || s.length == 0) return;
  Utf8DecoderFast dc;
  usize stpos = 0;
  foreach (immutable idx, char ch; s) {
    if (dc.decode(cast(ubyte)ch)) {
      dg(dc.complete ? dc.codepoint : dc.replacement, stpos);
      stpos = idx+1;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private void drawMonoBMP (int x, int y, int wdt, int hgt, const(ubyte)* bmp, in usize bpitch, in uint clr) nothrow @trusted @nogc {
  immutable int origWdt = wdt;

  int leftSkip, topSkip;
  if (!gxClipRect.clipHVStripes(ref x, ref y, ref wdt, ref hgt, &leftSkip, &topSkip)) return;
  if (!GxRect(0, 0, VBufWidth, VBufHeight).clipHVStripes(ref x, ref y, ref wdt, ref hgt, &leftSkip, &topSkip)) return;

  // skip unused top part
  if (topSkip) bmp += bpitch*cast(uint)topSkip;
  // skip unused bytes
  bmp += cast(usize)(leftSkip>>3);
  leftSkip &= 0x07;

  if (gxIsSolid(clr) && leftSkip == 0) {
    // yay, the fastest one!
    enum RenderBitMixin = `if (b&0x80) *curptr = clr; ++curptr; b <<= 1;`;
    uint* dptr = vglTexBuf+y*VBufWidth+x;
    while (hgt--) {
      const(ubyte)* ss = bmp;
      int left = wdt;
      uint* curptr = dptr;
      while (left >= 8) {
        ubyte b = *ss++;
        if (b == 0xff) {
          curptr[0..8] = clr;
          curptr += 8;
        } else if (b) {
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
        } else {
          curptr += 8;
        }
        left -= 8;
      }
      //assert(left >= 0 && left < 8);
      if (left) {
        ubyte b = *ss;
        if (b) {
          final switch (left) {
            case 7: mixin(RenderBitMixin); goto case;
            case 6: mixin(RenderBitMixin); goto case;
            case 5: mixin(RenderBitMixin); goto case;
            case 4: mixin(RenderBitMixin); goto case;
            case 3: mixin(RenderBitMixin); goto case;
            case 2: mixin(RenderBitMixin); goto case;
            case 1: if (b&0x80) *curptr = clr; break;
            //case 0: break;
          }
        }
        //while (left--) { mixin(RenderBitMixin); }
      }
      bmp += bpitch;
      dptr += cast(usize)VBufWidth;
    }
    return;
  }

  if (gxIsSolid(clr)) {
    // yay, the fast one!
    enum RenderBitMixin = `if (b&0x80) *curptr = clr; ++curptr; b <<= 1;`;
    uint* dptr = vglTexBuf+y*VBufWidth+x;
    int ldraw = 8-leftSkip;
    if (ldraw > wdt) ldraw = wdt;
    while (hgt--) {
      const(ubyte)* ss = bmp;
      uint* curptr = dptr;
      {
        ubyte b = cast(ubyte)((*ss++)<<cast(ubyte)(leftSkip&0x07u));
        foreach (; 0..ldraw) { mixin(RenderBitMixin); }
      }
      int left = wdt-ldraw;
      while (left >= 8) {
        ubyte b = *ss++;
        if (b == 0xff) {
          curptr[0..8] = clr;
          curptr += 8;
        } else if (b) {
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
          mixin(RenderBitMixin);
        } else {
          curptr += 8;
        }
        left -= 8;
      }
      //assert(left >= 0 && left < 8);
      if (left) {
        ubyte b = *ss;
        if (b) {
          final switch (left) {
            case 7: mixin(RenderBitMixin); goto case;
            case 6: mixin(RenderBitMixin); goto case;
            case 5: mixin(RenderBitMixin); goto case;
            case 4: mixin(RenderBitMixin); goto case;
            case 3: mixin(RenderBitMixin); goto case;
            case 2: mixin(RenderBitMixin); goto case;
            case 1: if (b&0x80) *curptr = clr; break;
            //case 0: break;
          }
        }
        //while (left--) { mixin(RenderBitMixin); }
      }
      bmp += bpitch;
      dptr += cast(usize)VBufWidth;
    }
    return;
  }

  // the slowest path
  x -= leftSkip;
  wdt += leftSkip;
  while (hgt--) {
    const(ubyte)* ss = bmp;
    ubyte count = 1, b = void;
    int cx = x;
    int left = wdt;
    while (left > 0) {
      if (--count == 0) {
        b = *ss++;
        if (!b || b == 0xff) {
          if (b) gxHLine(cx, y, (left >= 8 ? 8 : left), clr);
          cx += 8;
          left -= 8;
          count = 1;
          continue;
        }
        count = 8;
      } else {
        b <<= 1;
      }
      if (b&0x80) gxPutPixel(cx, y, clr);
      ++cx;
      --left;
    }
    ++y;
    bmp += bpitch;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void drawFTBitmap (int x, int y, in ref FT_Bitmap bitmap, in uint clr) nothrow @trusted @nogc {
  if (bitmap.pixel_mode != FT_PIXEL_MODE_MONO) return; // alas
  if (bitmap.rows < 1 || bitmap.width < 1) return; // nothing to do
  if (gxIsTransparent(clr)) return; // just in case
  // prepare
  const(ubyte)* src = bitmap.buffer;
  usize bpitch = void;
  if (bitmap.pitch >= 0) {
    bpitch = cast(usize)bitmap.pitch;
  } else {
    bpitch = cast(usize)0-cast(usize)(-bitmap.pitch);
    src += bpitch*(cast(uint)bitmap.rows-1u);
  }
  drawMonoBMP(x, y, bitmap.width, bitmap.rows, src, bpitch, clr);
}


// return 0 if invalid
int calcScaledValue (in int v, int scalePRC) pure nothrow @trusted @nogc {
  pragma(inline, true);
  if (scalePRC == 100) return v;
  if (scalePRC < 1) return v;
  if (scalePRC > 10000) scalePRC = 10000;
  return v*scalePRC/100;
}

// return 0 if invalid
int calcScaledFontSize (int scalePRC) nothrow @trusted @nogc {
  pragma(inline, true);
  if (scalePRC == 100) return fontSize;
  if (scalePRC < 1) return 0;
  if (scalePRC > 10000) scalePRC = 10000;
  immutable int fsz = fontSize*scalePRC/100;
  return (fsz > 0 ? fsz : 0);
}


// y is baseline; returns advance
int ttfDrawGlyph (bool firstchar, int scalePRC, int x, int y, int glyphidx, uint clr) nothrow @trusted @nogc {
  enum mono = true;
  if (glyphidx == 0) return 0;
  immutable int fsz = calcScaledFontSize(scalePRC);
  if (!fsz) return 0;

  FTC_ImageTypeRec fimg;
  fimg.face_id = FontID;
  fimg.width = 0;
  fimg.height = fsz;
  static if (mono) {
    fimg.flags = FT_LOAD_TARGET_MONO|(gxIsTransparent(clr) ? 0 : FT_LOAD_MONOCHROME|FT_LOAD_RENDER);
  } else {
    fimg.flags = (gxIsTransparent(clr) ? 0 : FT_LOAD_RENDER);
  }

  FT_Glyph fg;
  if (FTC_ImageCache_Lookup(ttfcacheimage, &fimg, glyphidx, &fg, null)) return 0;

  int advdec = 0;
  if (!gxIsTransparent(clr)) {
    if (fg.format != FT_GLYPH_FORMAT_BITMAP) return 0;
    FT_BitmapGlyph fgi = cast(FT_BitmapGlyph)fg;
    int x0 = x+fgi.left;
    if (firstchar && fgi.bitmap.width > 0) { x0 -= fgi.left; advdec = fgi.left; }
    drawFTBitmap(x0, y-fgi.top, fgi.bitmap, clr);
  }
  return cast(int)(fg.advance.x>>16)-advdec;
}


int ttfGetKerning (int scalePRC, int gl0idx, int gl1idx) nothrow @trusted @nogc {
  if (gl0idx == 0 || gl1idx == 0) return 0;
  immutable int fsz = calcScaledFontSize(scalePRC);
  if (!fsz) return 0;

  FTC_ScalerRec fsc;
  fsc.face_id = FontID;
  fsc.width = 0;
  fsc.height = fsz;
  fsc.pixel = 1; // size in pixels

  FT_Size ttfontsz;
  if (FTC_Manager_LookupSize(ttfcache, &fsc, &ttfontsz)) return 0;
  if (!FT_HAS_KERNING(ttfontsz.face)) return 0;

  FT_Vector kk;
  if (FT_Get_Kerning(ttfontsz.face, gl0idx, gl1idx, FT_KERNING_UNSCALED, &kk)) return 0;
  if (!kk.x) return 0;
  auto kadvfrac = FT_MulFix(kk.x, ttfontsz.metrics.x_scale); // 1/64 of pixel
  return cast(int)((kadvfrac/*+(kadvfrac < 0 ? -32 : 32)*/)>>6);
}


// ////////////////////////////////////////////////////////////////////////// //
public int gxCharWidth (in dchar ch) nothrow @trusted {
  immutable int fsz = calcScaledFontSize(egraFontSize);
  if (!fsz) return 0;

  if (ttflibrary is null) initFontEngine();

  int glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ch);
  if (glyph == 0) glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ReplacementChar);
  if (glyph == 0) return 0;

  FTC_ImageTypeRec fimg;
  fimg.face_id = FontID;
  fimg.width = 0;
  fimg.height = fsz;
  fimg.flags = FT_LOAD_TARGET_MONO;

  FT_Glyph fg;
  if (FTC_ImageCache_Lookup(ttfcacheimage, &fimg, glyph, &fg, null)) return -666;

  immutable int res = cast(int)fg.advance.x>>16;
  return (res > 0 ? res : 0);
}


// ////////////////////////////////////////////////////////////////////////// //
// returns char width
public int gxDrawChar (int x, int y, dchar ch, uint clr, int prevcp=-1) nothrow @trusted {
  if (ttflibrary is null) initFontEngine();

  int glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ch);
  if (glyph == 0) glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ReplacementChar);
  if (glyph == 0) return 0;

  int kadv = ttfGetKerning(egraFontSize, (prevcp >= 0 ? FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, prevcp) : 0), glyph);
  return ttfDrawGlyph(false, egraFontSize, x+kadv, y+calcScaledValue(fontBaselineOfs, egraFontSize), glyph, clr);
}

// returns char width
public int gxDrawChar() (in auto ref GxPoint p, char ch, uint fg, int prevcp=-1) nothrow @trusted { pragma(inline, true); return gxDrawChar(p.x, p.y, ch, fg, prevcp); }


// ////////////////////////////////////////////////////////////////////////// //
public struct GxKerning {
  int prevgidx = 0;
  int wdt = 0;
  int lastadv = 0;
  int lastcw = 0;
  int tabsize = 0;
  int scale = int.min;
  bool firstchar = true;

nothrow @trusted:
  this (int atabsize, int ascale, bool firstCharIsFull=false) {
    if (ttflibrary is null) initFontEngine();
    firstchar = !firstCharIsFull;
    if (ascale <= 0) ascale = egraFontSize;
    scale = ascale;
    if ((tabsize = (atabsize > 0 ? atabsize : 0)) != 0) {
      immutable osz = egraFontSize;
      egraFontSize = scale;
      scope(exit) egraFontSize = osz;
      tabsize = tabsize*gxCharWidth(' ');
    }
  }

  void reset (int atabsize, int ascale) {
    if (ttflibrary is null) initFontEngine();
    if (ascale <= 0) ascale = egraFontSize;
    scale = ascale;
    prevgidx = 0;
    wdt = 0;
    lastadv = 0;
    lastcw = 0;
    if ((tabsize = (atabsize > 0 ? atabsize : 0)) != 0) {
      immutable osz = egraFontSize;
      egraFontSize = scale;
      scope(exit) egraFontSize = osz;
      tabsize = tabsize*gxCharWidth(' ');
    }
    firstchar = true;
  }

  // tab length for current position
  int tablength () pure const @nogc { pragma(inline, true); return (tabsize > 0 ? (wdt/tabsize+1)*tabsize-wdt : 0); }

  int fixWidthPre (dchar ch) {
    if (scale == int.min) scale = egraFontSize;
    immutable int prevgl = prevgidx;
    wdt += lastadv;
    lastadv = 0;
    lastcw = 0;
    prevgidx = 0;
    if (ch == '\t' && tabsize > 0) {
      // tab
      lastadv = lastcw = tablength;
      firstchar = false;
    } else {
      if (ttflibrary is null) initFontEngine();
      int glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ch);
      if (glyph == 0) glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ReplacementChar);
      immutable int fsz = calcScaledFontSize(scale);
      if (fsz && glyph != 0) {
        wdt += ttfGetKerning(scale, prevgl, glyph);

        FTC_ImageTypeRec fimg;
        fimg.face_id = FontID;
        fimg.width = 0;
        fimg.height = fsz;
        version(none) {
          fimg.flags = FT_LOAD_TARGET_MONO;
        } else {
          fimg.flags = FT_LOAD_TARGET_MONO|FT_LOAD_MONOCHROME|FT_LOAD_RENDER;
        }

        FT_Glyph fg;
        version(none) {
          if (FTC_ImageCache_Lookup(ttfcacheimage, &fimg, glyph, &fg, null) == 0) {
            prevgidx = glyph;
            lastadv = fg.advance.x>>16;
          }
        } else {
          if (FTC_ImageCache_Lookup(ttfcacheimage, &fimg, glyph, &fg, null) == 0) {
            int advdec = 0;
            if (fg.format == FT_GLYPH_FORMAT_BITMAP) {
              FT_BitmapGlyph fgi = cast(FT_BitmapGlyph)fg;
              if (firstchar && fgi.bitmap.width > 0) {
                lastcw = fgi.bitmap.width;
                advdec = fgi.left;
                if (lastcw < 1) { advdec = 0; lastcw = cast(int)fg.advance.x>>16; }
              } else {
                lastcw = fgi.left+fgi.bitmap.width;
                if (lastcw < 1) lastcw = cast(int)fg.advance.x>>16;
              }
            }
            prevgidx = glyph;
            lastadv = (cast(int)fg.advance.x>>16)-advdec;
            firstchar = false;
          }
        }
      }
    }
    return wdt;
  }

  @property int finalWidth () pure const @nogc { pragma(inline, true); return wdt+/*lastadv*/lastcw; }

  // BUGGY!
  @property int nextCharOfs () pure const @nogc { pragma(inline, true); return wdt+lastadv; }

  @property int currOfs () pure const @nogc { pragma(inline, true); return wdt; }

  @property int nextOfsNoSpacing () pure const @nogc { pragma(inline, true); return wdt+lastcw; }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct GxDrawTextOptions {
  int tabsize = 0;
  uint clr = gxTransparent;
  int scale = 100;
  bool firstCharIsFull = false;

static nothrow @trusted @nogc:
  auto Color (in uint aclr) { pragma(inline, true); return GxDrawTextOptions(0, aclr, egraFontSize, false); }
  auto Tab (in int atabsize) { pragma(inline, true); return GxDrawTextOptions(atabsize, gxTransparent, egraFontSize, false); }
  auto TabColor (in int atabsize, in uint aclr) { pragma(inline, true); return GxDrawTextOptions(atabsize, aclr, egraFontSize, false); }
  auto TabColorFirstFull (in int atabsize, in uint aclr, in bool fcf) { pragma(inline, true); return GxDrawTextOptions(atabsize, aclr, egraFontSize, fcf); }
  auto ScaleTabColor (in int ascale, in int atabsize, in uint aclr) { pragma(inline, true); return GxDrawTextOptions(atabsize, aclr, ascale, false); }
  auto ColorNFC (in uint aclr) { pragma(inline, true); return GxDrawTextOptions(0, aclr, egraFontSize, true); }
  auto TabColorNFC (in int atabsize, in uint aclr) { pragma(inline, true); return GxDrawTextOptions(atabsize, aclr, egraFontSize, true); }
  auto ScaleTabColorNFC (in int ascale, in int atabsize, in uint aclr) { pragma(inline, true); return GxDrawTextOptions(atabsize, aclr, ascale, true); }
  // more ctors?
}

public struct GxDrawTextState {
  usize spos; // current codepoint starting position
  usize epos; // current codepoint ending position (exclusive; i.e. *after* codepoint)
  int curx; // current x (before drawing the glyph)
}


// delegate should return color
public int gxDrawTextUtfOpt(R) (in auto ref GxDrawTextOptions opt, int x, int y, auto ref R srng, uint delegate (in ref GxDrawTextState state) nothrow @safe clrdg=null) nothrow @trusted
if (Imp!"std.range.primitives".isInputRange!R && is(Imp!"std.range.primitives".ElementEncodingType!R == char))
{
  // rely on the assumption that font face won't be unloaded while we are in this function
  if (opt.scale < 1) return 0;

  if (ttflibrary is null) initFontEngine();

  GxDrawTextState state;

  immutable oldEScale = egraFontSize;
  scope(exit) egraFontSize = oldEScale;
  egraFontSize = opt.scale;

  y += calcScaledValue(fontBaselineOfs, egraFontSize);

  immutable int tabpix = (opt.tabsize > 0 ? opt.tabsize*gxCharWidth(' ') : 0);

  FT_Size ttfontsz;

  int prevglyph = 0;
  immutable int stx = x;

  bool dokern = true;
  bool firstchar = !opt.firstCharIsFull;
  Utf8DecoderFast dc;

  while (!srng.empty) {
    immutable ubyte srbyte = cast(ubyte)srng.front;
    srng.popFront();
    ++state.epos;

    if (dc.decode(srbyte)) {
      int ch = (dc.complete ? dc.codepoint : dc.replacement);
      int pgl = prevglyph;
      prevglyph = 0;
      state.curx = x;

      if (opt.tabsize > 0) {
        if (ch == '\t') {
          firstchar = false;
          int wdt = x-stx;
          state.curx = x;
          x += (wdt/tabpix+1)*tabpix-wdt;
          if (clrdg !is null) clrdg(state);
          state.spos = state.epos;
          continue;
        }
      }

      int glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ch);
      if (glyph == 0) glyph = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ReplacementChar);
      immutable int fsz = calcScaledFontSize(egraFontSize);
      if (fsz && glyph != 0) {
        // kerning
        int kadv = 0;
        if (pgl != 0 && dokern) {
          if (ttfontsz is null) {
            FTC_ScalerRec fsc;
            fsc.face_id = FontID;
            fsc.width = 0;
            fsc.height = fsz;
            fsc.pixel = 1; // size in pixels
            if (FTC_Manager_LookupSize(ttfcache, &fsc, &ttfontsz)) {
              dokern = false;
              ttfontsz = null;
            } else {
              dokern = (FT_HAS_KERNING(ttfontsz.face) != 0);
            }
          }
          if (dokern) {
            FT_Vector kk;
            if (FT_Get_Kerning(ttfontsz.face, pgl, glyph, FT_KERNING_UNSCALED, &kk) == 0) {
              if (kk.x) {
                auto kadvfrac = FT_MulFix(kk.x, ttfontsz.metrics.x_scale); // 1/64 of pixel
                kadv = cast(int)((kadvfrac/*+(kadvfrac < 0 ? -32 : 32)*/)>>6);
              }
            }
          }
        }
        x += kadv;
        uint clr = opt.clr;
        if (clrdg !is null) { state.curx = x; clr = clrdg(state); }
        x += ttfDrawGlyph(firstchar, egraFontSize, x, y, glyph, clr);
        firstchar = false;
      }
      state.spos = state.epos;
      prevglyph = glyph;
    }
  }

  return x-stx;
}


public int gxDrawTextUtfOpt() (in auto ref GxDrawTextOptions opt, int x, int y, const(char)[] s, uint delegate (in ref GxDrawTextState state) nothrow @safe clrdg=null) nothrow @trusted {
  static struct StrIterator {
  private:
    const(char)[] str;
  public nothrow @trusted @nogc:
    this (const(char)[] s) { pragma(inline, true); str = s; }
    @property bool empty () pure const { pragma(inline, true); return (str.length == 0); }
    @property char front () pure const { pragma(inline, true); return (str.length ? str.ptr[0] : 0); }
    void popFront () { if (str.length) str = str[1..$]; }
  }

  if (s.length == 0) return 0;
  return gxDrawTextUtfOpt(opt, x, y, StrIterator(s), clrdg);
}


public int gxTextWidthUtfOpt() (in auto ref GxDrawTextOptions opt, const(char)[] s) nothrow @trusted {
  if (s.length == 0) return 0;
  auto kern = GxKerning(opt.tabsize, opt.scale, opt.firstCharIsFull);
  s.utfByDChar(delegate (dchar ch) @trusted { kern.fixWidthPre(ch); });
  return kern.finalWidth;
}

public int gxTextWidthUtf (const(char)[] s, int atabsize=0) nothrow @trusted {
  if (s.length == 0) return 0;
  auto kern = GxKerning(atabsize, egraFontSize);
  s.utfByDChar(delegate (dchar ch) @trusted { kern.fixWidthPre(ch); });
  return kern.finalWidth;
}


// ////////////////////////////////////////////////////////////////////////// //
public int gxTextHeightUtf () nothrow @trusted { if (ttflibrary is null) initFontEngine(); return calcScaledValue(fontHeight, egraFontSize); }

public int gxTextBaseLineUtf () nothrow @trusted { if (ttflibrary is null) initFontEngine(); return calcScaledValue(fontBaselineOfs, egraFontSize); }
public int gxTextUnderLineUtf () nothrow @trusted { if (ttflibrary is null) initFontEngine(); return calcScaledValue(fontBaselineOfs, egraFontSize)+2; }

public int gxDrawTextUtf() (int x, int y, const(char)[] s, uint clr) nothrow @safe { pragma(inline, true); return gxDrawTextUtfOpt(GxDrawTextOptions.Color(clr), x, y, s); }
public int gxDrawTextUtf() (in auto ref GxPoint p, const(char)[] s, uint clr) nothrow @safe { pragma(inline, true); return gxDrawTextUtfOpt(GxDrawTextOptions.Color(clr), p.x, p.y, s); }


public int gxDrawTextOutUtf (int x, int y, const(char)[] s, uint clr, uint clrout) nothrow @trusted {
  if (s.length == 0) return 0;

  auto opt = GxDrawTextOptions.ScaleTabColor(egraFontSize, 0, clrout);
  if (!gxIsTransparent(clrout)) {
    if (ttflibrary is null) initFontEngine();
    int lim = calcScaledValue(1, egraFontSize);
    if (lim < 1) lim = 1;
    foreach (immutable dy; -lim..lim+1) {
      foreach (immutable dx; -lim..lim+1) {
        if (dx || dy) gxDrawTextUtfOpt(opt, x+dx, y+dy, s);
      }
    }
  }

  opt.clr = clr;
  return gxDrawTextUtfOpt(opt, x, y, s);
}


extern(C) nothrow @trusted @nogc {
  int fons__nvg__moveto_cb (const(FT_Vector)* to, void* user) {
    gxagg.moveTo(to.x, -to.y);
    return 0;
  }

  int fons__nvg__lineto_cb (const(FT_Vector)* to, void* user) {
    gxagg.lineTo(to.x, -to.y);
    return 0;
  }

  int fons__nvg__quadto_cb (const(FT_Vector)* c1, const(FT_Vector)* to, void* user) {
    gxagg.quadTo(c1.x, -c1.y, to.x, -to.y);
    return 0;
  }

  int fons__nvg__cubicto_cb (const(FT_Vector)* c1, const(FT_Vector)* c2, const(FT_Vector)* to, void* user) {
    gxagg.bezierTo(c1.x, -c1.y, c2.x, -c2.y, to.x, -to.y);
    return 0;
  }
}


public bool gxFontCharToPath (in dchar dch, float[] bounds=null) nothrow @trusted {
  if (ttflibrary is null) initFontEngine();

  if (bounds.length > 4) bounds = bounds.ptr[0..4];

  int glyphidx = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, dch);
  if (glyphidx == 0) glyphidx = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ReplacementChar);
  if (glyphidx == 0) { bounds[] = 0.0f; return false; }

  FT_Face font;
  if (FTC_Manager_LookupFace(ttfcache, FontID, &font)) assert(0, "cannot find FreeType font");

  auto err = FT_Load_Glyph(font, glyphidx, FT_LOAD_NO_BITMAP|FT_LOAD_NO_SCALE);
  if (err) { bounds[] = 0.0f; return false; }
  if (font.glyph.format != FT_GLYPH_FORMAT_OUTLINE) { bounds[] = 0.0f; return false; }

  FT_Outline outline = font.glyph.outline;

  if (bounds.length) {
    FT_BBox outlineBBox;
    FT_Outline_Get_CBox(&outline, &outlineBBox);
    if (bounds.length > 0) bounds.ptr[0] = outlineBBox.xMin;
    if (bounds.length > 1) bounds.ptr[1] = -outlineBBox.yMax;
    if (bounds.length > 2) bounds.ptr[2] = outlineBBox.xMax;
    if (bounds.length > 3) bounds.ptr[3] = -outlineBBox.yMin;
  }

  FT_Outline_Funcs funcs;
  funcs.move_to = &fons__nvg__moveto_cb;
  funcs.line_to = &fons__nvg__lineto_cb;
  funcs.conic_to = &fons__nvg__quadto_cb;
  funcs.cubic_to = &fons__nvg__cubicto_cb;
  funcs.shift = 0;
  funcs.delta = 0;

  /*
    A contour that contains a single point only is represented by a 'move to' operation
     followed by 'line to' to the same point. In most cases, it is best to filter this
     out before using the outline for stroking purposes (otherwise it would result in
     a visible dot when round caps are used).
   */
  err = FT_Outline_Decompose(&outline, &funcs, null);
  if (err) { bounds[] = 0.0f; return false; }

  return true;
}


public bool gxFontCharToPathEmboldenXY (in dchar dch, in int xstrength, in int ystrength, float[] bounds=null) nothrow @trusted {
  if (ttflibrary is null) initFontEngine();

  if (bounds.length > 4) bounds = bounds.ptr[0..4];

  int glyphidx = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, dch);
  if (glyphidx == 0) glyphidx = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ReplacementChar);
  if (glyphidx == 0) { bounds[] = 0.0f; return false; }

  FT_Face font;
  if (FTC_Manager_LookupFace(ttfcache, FontID, &font)) assert(0, "cannot find FreeType font");

  auto err = FT_Load_Glyph(font, glyphidx, FT_LOAD_NO_BITMAP|FT_LOAD_NO_SCALE);
  if (err) { bounds[] = 0.0f; return false; }
  if (font.glyph.format != FT_GLYPH_FORMAT_OUTLINE) { bounds[] = 0.0f; return false; }

  FT_Outline outline = font.glyph.outline;

  FT_Outline emo;
  if (FT_Outline_New(ttflibrary, outline.n_points, outline.n_contours, &emo)) { bounds[] = 0.0f; return false; }
  scope(exit) { FT_Outline_Done(ttflibrary, &emo); }

  if (FT_Outline_Copy(&outline, &emo)) { bounds[] = 0.0f; return false; }
  if (FT_Outline_EmboldenXY(&emo, xstrength, ystrength)) { bounds[] = 0.0f; return false; }

  if (bounds.length) {
    FT_BBox outlineBBox;
    FT_Outline_Get_CBox(&emo, &outlineBBox);
    if (bounds.length > 0) bounds.ptr[0] = outlineBBox.xMin;
    if (bounds.length > 1) bounds.ptr[1] = -outlineBBox.yMax;
    if (bounds.length > 2) bounds.ptr[2] = outlineBBox.xMax;
    if (bounds.length > 3) bounds.ptr[3] = -outlineBBox.yMin;
  }

  FT_Outline_Funcs funcs;
  funcs.move_to = &fons__nvg__moveto_cb;
  funcs.line_to = &fons__nvg__lineto_cb;
  funcs.conic_to = &fons__nvg__quadto_cb;
  funcs.cubic_to = &fons__nvg__cubicto_cb;
  funcs.shift = 0;
  funcs.delta = 0;

  /*
    A contour that contains a single point only is represented by a 'move to' operation
     followed by 'line to' to the same point. In most cases, it is best to filter this
     out before using the outline for stroking purposes (otherwise it would result in
     a visible dot when round caps are used).
   */
  err = FT_Outline_Decompose(&emo, &funcs, null);
  if (err) { bounds[] = 0.0f; return false; }

  return true;
}

public bool gxFontCharToPathEmbolden (in dchar dch, in int strength, float[] bounds=null) nothrow @trusted {
  pragma(inline, true);
  return gxFontCharToPathEmboldenXY(dch, strength, strength, bounds);
}


public bool gxFontCharPathBounds (in dchar dch, float[] bounds=null) nothrow @trusted {
  if (ttflibrary is null) initFontEngine();

  if (bounds.length > 4) bounds = bounds.ptr[0..4];

  int glyphidx = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, dch);
  if (glyphidx == 0) glyphidx = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ReplacementChar);
  if (glyphidx == 0) { bounds[] = 0.0f; return false; }

  FT_Face font;
  if (FTC_Manager_LookupFace(ttfcache, FontID, &font)) assert(0, "cannot find FreeType font");

  auto err = FT_Load_Glyph(font, glyphidx, FT_LOAD_NO_BITMAP|FT_LOAD_NO_SCALE);
  if (err) { bounds[] = 0.0f; return false; }
  if (font.glyph.format != FT_GLYPH_FORMAT_OUTLINE) { bounds[] = 0.0f; return false; }

  if (bounds.length) {
    FT_Outline outline = font.glyph.outline;

    FT_BBox outlineBBox;
    FT_Outline_Get_CBox(&outline, &outlineBBox);

    if (bounds.length > 0) bounds.ptr[0] = outlineBBox.xMin;
    if (bounds.length > 1) bounds.ptr[1] = -outlineBBox.yMax;
    if (bounds.length > 2) bounds.ptr[2] = outlineBBox.xMax;
    if (bounds.length > 3) bounds.ptr[3] = -outlineBBox.yMin;
  }

  return true;
}


public bool gxFontCharPathEmboldenBoundsXY (in dchar dch, in int xstrength, in int ystrength, float[] bounds=null) nothrow @trusted {
  if (ttflibrary is null) initFontEngine();

  if (bounds.length > 4) bounds = bounds.ptr[0..4];

  int glyphidx = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, dch);
  if (glyphidx == 0) glyphidx = FTC_CMapCache_Lookup(ttfcachecmap, FontID, -1, ReplacementChar);
  if (glyphidx == 0) { bounds[] = 0.0f; return false; }

  FT_Face font;
  if (FTC_Manager_LookupFace(ttfcache, FontID, &font)) assert(0, "cannot find FreeType font");

  auto err = FT_Load_Glyph(font, glyphidx, FT_LOAD_NO_BITMAP|FT_LOAD_NO_SCALE);
  if (err) { bounds[] = 0.0f; return false; }
  if (font.glyph.format != FT_GLYPH_FORMAT_OUTLINE) { bounds[] = 0.0f; return false; }

  FT_Outline outline = font.glyph.outline;

  FT_Outline emo;
  if (FT_Outline_New(ttflibrary, outline.n_points, outline.n_contours, &emo)) { bounds[] = 0.0f; return false; }
  scope(exit) { FT_Outline_Done(ttflibrary, &emo); }

  if (FT_Outline_Copy(&outline, &emo)) { bounds[] = 0.0f; return false; }
  if (FT_Outline_EmboldenXY(&emo, xstrength, ystrength)) { bounds[] = 0.0f; return false; }

  if (bounds.length) {
    FT_BBox outlineBBox;
    FT_Outline_Get_CBox(&emo, &outlineBBox);
    if (bounds.length > 0) bounds.ptr[0] = outlineBBox.xMin;
    if (bounds.length > 1) bounds.ptr[1] = -outlineBBox.yMax;
    if (bounds.length > 2) bounds.ptr[2] = outlineBBox.xMax;
    if (bounds.length > 3) bounds.ptr[3] = -outlineBBox.yMin;
  }

  return true;
}

public bool gxFontCharPathEmboldenBounds (in dchar dch, in int strength, float[] bounds=null) nothrow @trusted {
  pragma(inline, true);
  return gxFontCharPathEmboldenBoundsXY(dch, strength, strength, bounds);
}
