/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gfx.base /*is aliced*/;
private:

import iv.alice;
import iv.bclamp;
import iv.cmdcon;

import iv.egra.gfx.config;
import iv.egra.gfx.lowlevel;


// ////////////////////////////////////////////////////////////////////////// //
package(iv.egra) __gshared int VBufWidth = 740*2;
package(iv.egra) __gshared int VBufHeight = 520*2;
package __gshared ubyte vbufEffScale = 1; // effective (current) window scale

// framebuffer, BGRA format
package(iv.egra) __gshared uint* vglTexBuf;


// ////////////////////////////////////////////////////////////////////////// //
public @property int screenEffScale () nothrow @trusted @nogc { pragma(inline, true); return vbufEffScale; }
public @property void screenEffScale (int scale) nothrow @trusted @nogc { pragma(inline, true); if (scale < 1) scale = 1; if (scale > 32) scale = 32; vbufEffScale = cast(ubyte)scale; }

public @property int screenWidth () nothrow @trusted @nogc { pragma(inline, true); return VBufWidth; }
public @property int screenHeight () nothrow @trusted @nogc { pragma(inline, true); return VBufHeight; }

public @property int screenWidthScaled () nothrow @trusted @nogc { pragma(inline, true); return VBufWidth*vbufEffScale; }
public @property int screenHeightScaled () nothrow @trusted @nogc { pragma(inline, true); return VBufHeight*vbufEffScale; }


// ////////////////////////////////////////////////////////////////////////// //
public enum GxDir {
  Horiz,
  Vert,
}


// ////////////////////////////////////////////////////////////////////////// //
public struct GxPoint {
public:
  int x, y;

nothrow @safe @nogc:
  this() (in auto ref GxPoint p) pure { pragma(inline, true); x = p.x; y = p.y; }
  this (in int ax, in int ay) pure { pragma(inline, true); x = ax; y = ay; }

  bool inside() (in auto ref GxRect rc) pure const { pragma(inline, true); return rc.inside(this); }
  bool inside() (in auto ref GxSize sz) pure const { pragma(inline, true); return sz.inside(this); }

  bool opEquals() (in auto ref GxPoint p) pure const { pragma(inline, true); return !((p.x^x)|(p.y^y)); }

  int opCmp() (in auto ref GxPoint p) pure const {
    pragma(inline, true);
    return
      y < p.y ? -1 :
      y > p.y ? +1 :
      x < p.x ? -1 :
      x > p.x ? +1 :
      0;
  }

  void opAssign() (in auto ref GxPoint p) { pragma(inline, true); x = p.x; y = p.y; }

  void opOpAssign(string op) (in int v) if (op == "+" || op == "-" || op == "*" || op == "/") {
    pragma(inline, true);
    mixin("x"~op~"=v; y"~op~"=v;");
  }

  void opOpAssign(string op) (in auto ref GxPoint pt) if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("x"~op~"=pt.x; y"~op~"=pt.y;");
  }

  GxPoint opBinary(string op) (in auto ref GxPoint pt) pure const if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("return GxPoint(x"~op~"pt.x, y"~op~"pt.y);");
  }

  GxPoint opBinary(string op) (in auto ref GxSize sz) pure const if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("return GxPoint(x"~op~"sz.w, y"~op~"sz.h);");
  }

  GxPoint opBinary(string op) (in int v) pure const if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("return GxPoint(x"~op~"v, y"~op~"v);");
  }

  int opIndex (in GxDir dir) pure const { pragma(inline, true); return (dir == GxDir.Horiz ? x : y); }

  void opIndexAssign (in int v, in GxDir dir) { pragma(inline, true); if (dir == GxDir.Horiz) x = v; else y = v; }

  void opIndexOpAssign(string op) (in int v, in GxDir dir) if (op == "+" || op == "-") {
    pragma(inline, true);
    if (dir == GxDir.Horiz) mixin("x "~op~"= v;"); else mixin("y "~op~"= v;");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct GxSize {
public:
  int w, h;

nothrow @safe @nogc:
  @property bool valid () pure const { pragma(inline, true); return (w >= 0 && h >= 0); }
  @property bool empty () pure const { pragma(inline, true); return (w < 1 || h < 1); }

  bool inside() (in auto ref GxPoint pt) pure const { pragma(inline, true); return (pt.x >= 0 && pt.y >= 0 && pt.x < w && pt.y < h); }

  void sanitize () { pragma(inline, true); if (w < 0) w = 0; if (h < 0) h = 0; }

  void opOpAssign(string op) (in int v) if (op == "+" || op == "-" || op == "*" || op == "/") {
    pragma(inline, true);
    mixin("w"~op~"=v; h"~op~"=v;");
  }

  void opOpAssign(string op) (in auto ref GxSize sz) if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("w"~op~"=sz.w; h"~op~"=sz.h;");
  }

  GxSize opBinary(string op) (in auto ref GxSize sz) pure const if (op == "+" || op == "-") {
    pragma(inline, true);
    mixin("return GxSize(w"~op~"sz.w, h"~op~"sz.h);");
  }

  int opIndex (in GxDir dir) pure const { pragma(inline, true); return (dir == GxDir.Horiz ? w : h); }

  void opIndexAssign (in int v, in GxDir dir) { pragma(inline, true); if (dir == GxDir.Horiz) w = v; else h = v; }

  //FIXME: overflow!
  void opIndexOpAssign(string op) (in int v, in GxDir dir) if (op == "+" || op == "-") {
    pragma(inline, true);
    if (dir == GxDir.Horiz) {
      mixin("w "~op~"= v;");
      if (w < 0) w = 0;
    } else {
      mixin("h "~op~"= v;");
      if (h < 0) h = 0;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct GxRect {
public:
  GxPoint pos;
  GxSize size = GxSize(-1, -1); // <0: invalid rect

  string toString () const @trusted nothrow {
    if (valid) {
      import core.stdc.stdio : snprintf;
      char[128] buf = void;
      return buf[0..snprintf(buf.ptr, buf.length, "(%d,%d)-(%d,%d)", pos.x, pos.y, pos.x+size.w-1, pos.y+size.h-1)].idup;
    } else {
      return "(invalid-rect)";
    }
  }

nothrow @safe @nogc:
  this() (in auto ref GxRect rc) pure {
    pragma(inline, true);
    pos = rc.pos;
    size = rc.size;
  }

  this (in int ax0, in int ay0, in int awidth, in int aheight) pure {
    pragma(inline, true);
    pos.x = ax0;
    pos.y = ay0;
    size.w = awidth;
    size.h = aheight;
  }

  this() (in int ax0, in int ay0, in auto ref GxSize asize) pure {
    pragma(inline, true);
    pos.x = ax0;
    pos.y = ay0;
    size = asize;
  }

  this() (in auto ref GxPoint xy0, in int awidth, in int aheight) pure {
    pragma(inline, true);
    pos = xy0;
    size.w = awidth;
    size.h = aheight;
  }

  this() (in auto ref GxPoint xy0, in auto ref GxSize asize) pure {
    pragma(inline, true);
    pos = xy0;
    size = asize;
  }

  this() (in auto ref GxPoint xy0, in auto ref GxPoint xy1) pure {
    pragma(inline, true);
    pos = xy0;
    size.w = xy1.x-xy0.x+1;
    size.h = xy1.y-xy0.y+1;
  }

  this (in int awidth, in int aheight) pure {
    pragma(inline, true);
    pos.x = pos.y = 0;
    size.w = awidth;
    size.h = aheight;
  }

  this() (in auto ref GxSize asize) pure {
    pragma(inline, true);
    pos.x = pos.y = 0;
    size = asize;
  }

  void setCoords(bool doSwap=true) (in int ax0, in int ay0, in int ax1, in int ay1) {
    pragma(inline, true);
    static if (doSwap) {
      pos.x = (ax0 < ax1 ? ax0 : ax1);
      pos.y = (ay0 < ay1 ? ay0 : ay1);
      size.w = (ax0 < ax1 ? ax1 : ax0)-pos.x+1;
      size.h = (ay0 < ay1 ? ay1 : ay0)-pos.y+1;
    } else {
      pos.x = ax0;
      pos.y = ay0;
      size.w = ax1-ax0+1;
      size.h = ay1-ay0+1;
    }
  }

  void setCoords(bool doSwap=true) (in auto ref GxPoint p0, in int ax1, in int ay1) { pragma(inline, true); setCoords!doSwap(p0.x, p0.y, ax1, ay1); }
  void setCoords(bool doSwap=true) (in int ax0, in int ay0, in auto ref GxPoint p1) { pragma(inline, true); setCoords!doSwap(ax0, ay0, p1.x, p1.y); }
  void setCoords(bool doSwap=true) (in auto ref GxPoint p0, in auto ref GxPoint p1) { pragma(inline, true); setCoords!doSwap(p0.x, p0.y, p1.x, p1.y); }

  void opAssign() (in auto ref GxRect rc) { pragma(inline, true); pos = rc.pos; size = rc.size; }

  bool opEquals() (in auto ref GxRect rc) pure const { pragma(inline, true); return (pos == rc.pos && size == rc.size); }

  int opCmp() (in auto ref GxRect p) pure const { pragma(inline, true); return pos.opCmp(p.pos); }

  @property bool valid () pure const { pragma(inline, true); return size.valid; }
  @property bool invalid () pure const { pragma(inline, true); return !size.valid; }
  @property bool empty () pure const { pragma(inline, true); return size.empty; } // invalid rects are empty

  void invalidate () { pragma(inline, true); size.w = size.h = -1; }

  @property int left () pure const { pragma(inline, true); return pos.x; }
  @property void left (in int v) { pragma(inline, true); pos.x = v; }

  @property int top () pure const { pragma(inline, true); return pos.y; }
  @property void top (in int v) { pragma(inline, true); pos.y = v; }

  @property int right () pure const { pragma(inline, true); return pos.x+size.w-1; }
  @property void right (in int v) { pragma(inline, true); size.w = v-pos.x+1; }

  @property int bottom () pure const { pragma(inline, true); return pos.y+size.h-1; }
  @property void bottom (in int v) { pragma(inline, true); size.h = v-pos.y+1; }

  @property GxPoint lefttop () pure const { pragma(inline, true); return pos; }
  @property GxPoint righttop () pure const { pragma(inline, true); return pos+GxSize(size.w-1, 0); }
  @property GxPoint leftbottom () pure const { pragma(inline, true); return pos+GxSize(0, size.h-1); }
  @property GxPoint rightbottom () pure const { pragma(inline, true); return pos+size-1; }

  @property void lefttop() (in auto ref GxPoint p) { pragma(inline, true); setCoords!false(p, rightbottom); }
  @property void rightbottom() (in auto ref GxPoint p) { pragma(inline, true); setCoords!false(lefttop, p); }

  alias topleft = lefttop;
  alias topright = righttop;
  alias bottomleft = leftbottom;
  alias bottomright = rightbottom;

  @property int x0 () pure const { pragma(inline, true); return pos.x; }
  @property int y0 () pure const { pragma(inline, true); return pos.y; }

  @property void x0 (in int val) { pragma(inline, true); setCoords!false(val, pos.y, rightbottom); }
  @property void y0 (in int val) { pragma(inline, true); setCoords!false(pos.x, val, rightbottom); }

  @property int x1 () pure const { pragma(inline, true); return (width > 0 ? x0+width-1 : x0-1); }
  @property int y1 () pure const { pragma(inline, true); return (height > 0 ? y0+height-1 : y0-1); }

  @property void x1 (in int val) { pragma(inline, true); width = val-x0+1; }
  @property void y1 (in int val) { pragma(inline, true); height = val-y0+1; }

  @property int width () pure const { pragma(inline, true); return size.w; }
  @property int height () pure const { pragma(inline, true); return size.h; }

  @property void width (in int val) { pragma(inline, true); size.w = val; }
  @property void height (in int val) { pragma(inline, true); size.h = val; }

  // is point inside this rect?
  bool inside() (in auto ref GxPoint p) pure const {
    pragma(inline, true);
    return (width > 0 && height > 0 ? (p.x >= x0 && p.y >= y0 && p.x < x0+width && p.y < y0+height) : false);
  }

  // is point inside this rect?
  bool inside (in int ax, in int ay) pure const {
    pragma(inline, true);
    return (width > 0 && height > 0 ? (ax >= x0 && ay >= y0 && ax < x0+width && ay < y0+height) : false);
  }

  // is `r` inside `this`?
  bool contains() (in auto ref GxRect r) pure const {
    pragma(inline, true);
    return
      width > 0 && height > 0 &&
      r.width > 0 && r.height > 0 &&
      r.x0 >= x0 && r.y0 >= y0 &&
      r.x0+r.width <= x0+width && r.y0+r.height <= y0+height;
  }

  // does `r` and `this` overlap?
  bool overlaps() (in auto ref GxRect r) pure const {
    pragma(inline, true);
    return
      width > 0 && height > 0 &&
      r.width > 0 && r.height > 0 &&
      x0 < r.x0+r.width && r.x0 < x0+width &&
      y0 < r.y0+r.height && r.y0 < y0+height;
  }

  // extend `this` so it will include `p`
  void include() (in auto ref GxPoint p) {
    pragma(inline, true);
    if (empty) {
      x0 = p.x;
      y0 = p.y;
      width = 1;
      height = 1;
    } else {
      if (p.x < x0) x0 = p.x0;
      if (p.y < y0) y0 = p.y0;
      if (p.x1 > x1) x1 = p.x1;
      if (p.y1 > y1) y1 = p.y1;
    }
  }

  // extend `this` so it will include `r`
  void include() (in auto ref GxRect r) {
    pragma(inline, true);
    if (!r.empty) {
      if (empty) {
        x0 = r.x;
        y0 = r.y;
        width = r.width;
        height = r.height;
      } else {
        if (r.x < x0) x0 = r.x0;
        if (r.y < y0) y0 = r.y0;
        if (r.x1 > x1) x1 = r.x1;
        if (r.y1 > y1) y1 = r.y1;
      }
    }
  }

  // clip `this` so it will not be larger than `r`
  // returns `false` if the resulting rect (this) is empty or invalid
  bool intersect (in int rx0, in int ry0, in int rwdt, in int rhgt) {
    if (rwdt < 0 || rhgt < 0 || invalid) { size.w = size.h = -1; return false; }
    if (rwdt == 0 || rhgt == 0 || empty) { size.w = size.h = 0; return false; }
    immutable int rx1 = rx0+rwdt-1;
    immutable int ry1 = ry0+rhgt-1;
    if (ry1 < y0 || rx1 < x0 || rx0 > x1 || ry0 > y1) { size.w = size.h = 0; return false; }
    // rc is at least partially inside this rect
    if (x0 < rx0) x0 = rx0;
    if (y0 < ry0) y0 = ry0;
    if (x1 > rx1) x1 = rx1;
    if (y1 > ry1) y1 = ry1;
    assert(!empty); // yeah, always
    return true;
  }

  // clip `this` so it will not be larger than `r`
  // returns `false` if the resulting rect (this) is empty or invalid
  bool intersect (in int rwdt, in int rhgt) {
    pragma(inline, true);
    return intersect(0, 0, rwdt, rhgt);
  }

  // clip `this` so it will not be larger than `r`
  // returns `false` if the resulting rect (this) is empty or invalid
  bool intersect() (in auto ref GxRect r) {
    pragma(inline, true);
    return intersect(r.x0, r.y0, r.width, r.height);
  }

  void shrinkBy (in int dx, in int dy) {
    pragma(inline, true);
    if ((dx|dy) && valid) {
      pos.x += dx;
      pos.y += dy;
      size.w -= dx<<1;
      size.h -= dy<<1;
    }
  }

  void shrinkBy() (in auto ref GxSize sz) { pragma(inline, true); shrinkBy(sz.w, sz.h); }

  void growBy (in int dx, in int dy) {
    pragma(inline, true);
    if ((dx|dy) && valid) {
      pos.x -= dx;
      pos.y -= dy;
      size.w += dx<<1;
      size.h += dy<<1;
    }
  }

  void growBy() (in auto ref GxSize sz) { pragma(inline, true); growBy(sz.w, sz.h); }

  void set (in int ax0, in int ay0, in int awidth, in int aheight) {
    pragma(inline, true);
    x0 = ax0;
    y0 = ay0;
    width = awidth;
    height = aheight;
  }

  void set() (in auto ref GxPoint p0, in int awidth, in int aheight) { pragma(inline, true); set(p0.x, p0.y, awidth, aheight); }
  void set() (in auto ref GxPoint p0, in auto ref GxSize asize) { pragma(inline, true); set(p0.x, p0.y, asize.w, asize.h); }
  void set() (in int ax0, in int ay0, in auto ref GxSize asize) { pragma(inline, true); set(ax0, ay0, asize.w, asize.h); }

  void moveLeftTopBy (in int dx, in int dy) {
    pragma(inline, true);
    pos.x += dx;
    pos.y += dy;
    size.w -= dx;
    size.h -= dy;
  }

  void moveLeftTopBy() (in auto ref GxPoint p) { pragma(inline, true); moveLeftTopBy(p.x, p.y); }

  alias moveTopLeftBy = moveLeftTopBy;

  void moveRightBottomBy (in int dx, in int dy) {
    pragma(inline, true);
    size.w += dx;
    size.h += dy;
  }

  void moveRightBottomBy() (in auto ref GxPoint p) { pragma(inline, true); moveRightBottomBy(p.x, p.y); }

  alias moveBottomRightBy = moveRightBottomBy;

  void moveBy (in int dx, in int dy) {
    pragma(inline, true);
    pos.x += dx;
    pos.y += dy;
  }

  void moveBy() (in auto ref GxPoint p) { pragma(inline, true); moveBy(p.x, p.y); }

  void moveTo (in int nx, in int ny) {
    pragma(inline, true);
    x0 = nx;
    y0 = ny;
  }

  void moveTo() (in auto ref GxPoint p) { pragma(inline, true); moveTo(p.x, p.y); }

  /**
   * clip (x,y,wdt) stripe to this rect
   *
   * Params:
   *  x = stripe start (not relative to rect)
   *  y = stripe start (not relative to rect)
   *  wdt = stripe length
   *
   * Returns:
   *  x = fixed x (invalid if result is false)
   *  wdt = fixed length (invalid if result is false)
   *  leftSkip = how much cells skipped at the left side (invalid if result is false)
   *  result = false if stripe is completely clipped out
   */
  bool clipHStripe (ref int x, int y, ref int wdt, int* leftSkip=null) const @trusted {
    if (empty) return false;
    if (wdt <= 0 || y < y0 || y >= y0+height || x >= x0+width) return false;
    if (x < x0) {
      // left clip
      immutable int dx = x0-x;
      if (dx >= wdt) return false; // avoid overflow
      if (leftSkip !is null) *leftSkip = dx;
      wdt -= dx;
      x = x0;
      assert(wdt > 0); // yeah, always
    } else {
      if (leftSkip !is null) *leftSkip = 0;
    }
    if (wdt > width) wdt = width; // avoid overflow
    if (x+wdt > x0+width) {
      // right clip
      wdt = x0+width-x;
      assert(wdt > 0); // yeah, always
    }
    return true;
  }

  bool clipHStripe (ref GxPoint p, ref int wdt, int* leftSkip=null) const @trusted {
    pragma(inline, true);
    return clipHStripe(ref p.x, p.y, ref wdt, leftSkip);
  }

  /**
   * clip (x,y,hgt) stripe to this rect
   *
   * Params:
   *  x = stripe start (not relative to rect)
   *  y = stripe start (not relative to rect)
   *  hgt = stripe length
   *
   * Returns:
   *  y = fixed y (invalid if result is false)
   *  hgt = fixed length (invalid if result is false)
   *  topSkip = how much cells skipped at the top side (invalid if result is false)
   *  result = false if stripe is completely clipped out
   */
  bool clipVStripe (int x, ref int y, ref int hgt, int* topSkip=null) const @trusted {
    if (empty) return false;
    if (hgt <= 0 || x < x0 || x >= x0+width || y >= y0+height) return false;
    if (y < y0) {
      // top clip
      immutable int dy = y0-y;
      if (dy >= hgt) return false; // avoid overflow
      if (topSkip !is null) *topSkip = dy;
      hgt -= dy;
      y = y0;
      assert(hgt > 0); // yeah, always
    } else {
      if (topSkip !is null) *topSkip = 0;
    }
    if (hgt > height) hgt = height; // avoid overflow
    if (y+hgt > y0+height) {
      // bottom clip
      hgt = y0+height-y;
      assert(hgt > 0); // yeah, always
    }
    return true;
  }

  bool clipVStripe (ref GxPoint p, ref int hgt, int* topSkip=null) const @trusted {
    pragma(inline, true);
    return clipVStripe(p.x, ref p.y, ref hgt, topSkip);
  }

  bool clipHVStripes (ref int x, ref int y, ref int wdt, ref int hgt, int* leftSkip=null, int* topSkip=null) const @trusted {
    if (empty || wdt <= 0 || hgt <= 0) return false;
    if (y >= y0+height || x >= x0+width) return false;
    // use dummy `x` and `y` for horizontal and vertical clippers, because they are only checked for validity there
    if (!clipHStripe(ref x, y0, ref wdt, leftSkip)) return false;
    return clipVStripe(x0, ref y, ref hgt, topSkip);
  }

  bool clipHVStripes (ref GxPoint p, ref int wdt, ref int hgt, int* leftSkip=null, int* topSkip=null) const @trusted {
    pragma(inline, true);
    return clipHVStripes(ref p.x, ref p.y, ref wdt, ref hgt, leftSkip, topSkip);
  }

  bool clipHVStripes (ref GxPoint p, ref GxSize sz, int* leftSkip=null, int* topSkip=null) const @trusted {
    pragma(inline, true);
    return clipHVStripes(ref p.x, ref p.y, ref sz.w, ref sz.h, leftSkip, topSkip);
  }

  bool clipHVStripes (ref int x, ref int y, ref GxSize sz, int* leftSkip=null, int* topSkip=null) const @trusted {
    pragma(inline, true);
    return clipHVStripes(ref x, ref y, ref sz.w, ref sz.h, leftSkip, topSkip);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public alias GxColor = uint;

public align(1) union GxColorU {
align(1):
  uint clr;
  ubyte[4] bgra;
  align(1) struct {
    align(1):
    ubyte b, g, r, a;
  }
  this (in GxColor c) pure nothrow @safe @nogc { pragma(inline, true); clr = c; }
}
static assert(GxColorU.sizeof == GxColor.sizeof);


public enum gxSolidBlack = 0xff000000u;
public enum gxSolidWhite = 0xffffffffu;

public enum gxTransparent = 0x00000000u;
public enum gxColorMask = 0x00ffffffu;
public enum gxAlphaMask = 0xff000000u;

public enum gxUnknown = 0x00010203u;

public bool gxIsTransparent (in uint clr) pure nothrow @safe @nogc { pragma(inline, true); return !(clr&gxAlphaMask); }
public bool gxIsSolid (in uint clr) pure nothrow @safe @nogc { pragma(inline, true); return ((clr&gxAlphaMask) == gxAlphaMask); }

public bool gxIsSolidBlack (in uint clr) pure nothrow @safe @nogc { pragma(inline, true); return (clr == gxAlphaMask); }

public ubyte gxGetBlue (in uint clr) pure nothrow @safe @nogc { pragma(inline, true); return cast(ubyte)clr; }
public ubyte gxGetGreen (in uint clr) pure nothrow @safe @nogc { pragma(inline, true); return cast(ubyte)(clr>>8); }
public ubyte gxGetRed (in uint clr) pure nothrow @safe @nogc { pragma(inline, true); return cast(ubyte)(clr>>16); }
public ubyte gxGetAlpha (in uint clr) pure nothrow @safe @nogc { pragma(inline, true); return cast(ubyte)(clr>>24); }


public uint gxColMix (in uint dc, in uint clr) pure nothrow @trusted @nogc {
  pragma(inline, true);
       if (gxIsSolid(clr)) return clr;
  else if (gxIsTransparent(clr)) return dc|0xff_00_00_00u;
  else {
    uint res = void;
    mixin(GxColMixMixin!("res", "dc", "clr"));
    return res;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private template isGoodRGBInt(T) {
  import std.traits : Unqual;
  alias TT = Unqual!T;
  enum isGoodRGBInt =
    is(TT == ubyte) ||
    is(TT == short) || is(TT == ushort) ||
    is(TT == int) || is(TT == uint) ||
    is(TT == long) || is(TT == ulong);
}


// ////////////////////////////////////////////////////////////////////////// //
public uint gxrgb(T0, T1, T2) (T0 r, T1 g, T2 b) pure nothrow @trusted @nogc
if (isGoodRGBInt!T0 && isGoodRGBInt!T1 && isGoodRGBInt!T2)
{
  pragma(inline, true);
  return (clampToByte(r)<<16)|(clampToByte(g)<<8)|clampToByte(b)|0xff000000u;
}

public uint gxrgba(T0, T1, T2, T3) (T0 r, T1 g, T2 b, T3 a) pure nothrow @trusted @nogc
if (isGoodRGBInt!T0 && isGoodRGBInt!T1 && isGoodRGBInt!T2 && isGoodRGBInt!T3)
{
  pragma(inline, true);
  return (clampToByte(a)<<24)|(clampToByte(r)<<16)|(clampToByte(g)<<8)|clampToByte(b);
}


public enum gxRGB(int r, int g, int b) = (clampToByte(r)<<16)|(clampToByte(g)<<8)|clampToByte(b)|0xff000000u;
public enum gxRGBA(int r, int g, int b, int a) = (clampToByte(a)<<24)|(clampToByte(r)<<16)|(clampToByte(g)<<8)|clampToByte(b);


// ////////////////////////////////////////////////////////////////////////// //
// namespace
public struct GxColors {
  enum k8orange = gxRGB!(255, 127, 0);

  enum aliceblue = gxRGB!(240, 248, 255);
  enum antiquewhite = gxRGB!(250, 235, 215);
  enum aqua = gxRGB!(0, 255, 255);
  enum aquamarine = gxRGB!(127, 255, 212);
  enum azure = gxRGB!(240, 255, 255);
  enum beige = gxRGB!(245, 245, 220);
  enum bisque = gxRGB!(255, 228, 196);
  enum black = gxRGB!(0, 0, 0); // basic color
  enum blanchedalmond = gxRGB!(255, 235, 205);
  enum blue = gxRGB!(0, 0, 255); // basic color
  enum blueviolet = gxRGB!(138, 43, 226);
  enum brown = gxRGB!(165, 42, 42);
  enum burlywood = gxRGB!(222, 184, 135);
  enum cadetblue = gxRGB!(95, 158, 160);
  enum chartreuse = gxRGB!(127, 255, 0);
  enum chocolate = gxRGB!(210, 105, 30);
  enum coral = gxRGB!(255, 127, 80);
  enum cornflowerblue = gxRGB!(100, 149, 237);
  enum cornsilk = gxRGB!(255, 248, 220);
  enum crimson = gxRGB!(220, 20, 60);
  enum cyan = gxRGB!(0, 255, 255); // basic color
  enum darkblue = gxRGB!(0, 0, 139);
  enum darkcyan = gxRGB!(0, 139, 139);
  enum darkgoldenrod = gxRGB!(184, 134, 11);
  enum darkgray = gxRGB!(169, 169, 169);
  enum darkgreen = gxRGB!(0, 100, 0);
  enum darkgrey = gxRGB!(169, 169, 169);
  enum darkkhaki = gxRGB!(189, 183, 107);
  enum darkmagenta = gxRGB!(139, 0, 139);
  enum darkolivegreen = gxRGB!(85, 107, 47);
  enum darkorange = gxRGB!(255, 140, 0);
  enum darkorchid = gxRGB!(153, 50, 204);
  enum darkred = gxRGB!(139, 0, 0);
  enum darksalmon = gxRGB!(233, 150, 122);
  enum darkseagreen = gxRGB!(143, 188, 143);
  enum darkslateblue = gxRGB!(72, 61, 139);
  enum darkslategray = gxRGB!(47, 79, 79);
  enum darkslategrey = gxRGB!(47, 79, 79);
  enum darkturquoise = gxRGB!(0, 206, 209);
  enum darkviolet = gxRGB!(148, 0, 211);
  enum deeppink = gxRGB!(255, 20, 147);
  enum deepskyblue = gxRGB!(0, 191, 255);
  enum dimgray = gxRGB!(105, 105, 105);
  enum dimgrey = gxRGB!(105, 105, 105);
  enum dodgerblue = gxRGB!(30, 144, 255);
  enum firebrick = gxRGB!(178, 34, 34);
  enum floralwhite = gxRGB!(255, 250, 240);
  enum forestgreen = gxRGB!(34, 139, 34);
  enum fuchsia = gxRGB!(255, 0, 255);
  enum gainsboro = gxRGB!(220, 220, 220);
  enum ghostwhite = gxRGB!(248, 248, 255);
  enum gold = gxRGB!(255, 215, 0);
  enum goldenrod = gxRGB!(218, 165, 32);
  enum gray = gxRGB!(128, 128, 128); // basic color
  enum green = gxRGB!(0, 128, 0); // basic color
  enum greenyellow = gxRGB!(173, 255, 47);
  enum grey = gxRGB!(128, 128, 128); // basic color
  enum honeydew = gxRGB!(240, 255, 240);
  enum hotpink = gxRGB!(255, 105, 180);
  enum indianred = gxRGB!(205, 92, 92);
  enum indigo = gxRGB!(75, 0, 130);
  enum ivory = gxRGB!(255, 255, 240);
  enum khaki = gxRGB!(240, 230, 140);
  enum lavender = gxRGB!(230, 230, 250);
  enum lavenderblush = gxRGB!(255, 240, 245);
  enum lawngreen = gxRGB!(124, 252, 0);
  enum lemonchiffon = gxRGB!(255, 250, 205);
  enum lightblue = gxRGB!(173, 216, 230);
  enum lightcoral = gxRGB!(240, 128, 128);
  enum lightcyan = gxRGB!(224, 255, 255);
  enum lightgoldenrodyellow = gxRGB!(250, 250, 210);
  enum lightgray = gxRGB!(211, 211, 211);
  enum lightgreen = gxRGB!(144, 238, 144);
  enum lightgrey = gxRGB!(211, 211, 211);
  enum lightpink = gxRGB!(255, 182, 193);
  enum lightsalmon = gxRGB!(255, 160, 122);
  enum lightseagreen = gxRGB!(32, 178, 170);
  enum lightskyblue = gxRGB!(135, 206, 250);
  enum lightslategray = gxRGB!(119, 136, 153);
  enum lightslategrey = gxRGB!(119, 136, 153);
  enum lightsteelblue = gxRGB!(176, 196, 222);
  enum lightyellow = gxRGB!(255, 255, 224);
  enum lime = gxRGB!(0, 255, 0);
  enum limegreen = gxRGB!(50, 205, 50);
  enum linen = gxRGB!(250, 240, 230);
  enum magenta = gxRGB!(255, 0, 255); // basic color
  enum maroon = gxRGB!(128, 0, 0);
  enum mediumaquamarine = gxRGB!(102, 205, 170);
  enum mediumblue = gxRGB!(0, 0, 205);
  enum mediumorchid = gxRGB!(186, 85, 211);
  enum mediumpurple = gxRGB!(147, 112, 219);
  enum mediumseagreen = gxRGB!(60, 179, 113);
  enum mediumslateblue = gxRGB!(123, 104, 238);
  enum mediumspringgreen = gxRGB!(0, 250, 154);
  enum mediumturquoise = gxRGB!(72, 209, 204);
  enum mediumvioletred = gxRGB!(199, 21, 133);
  enum midnightblue = gxRGB!(25, 25, 112);
  enum mintcream = gxRGB!(245, 255, 250);
  enum mistyrose = gxRGB!(255, 228, 225);
  enum moccasin = gxRGB!(255, 228, 181);
  enum navajowhite = gxRGB!(255, 222, 173);
  enum navy = gxRGB!(0, 0, 128);
  enum oldlace = gxRGB!(253, 245, 230);
  enum olive = gxRGB!(128, 128, 0);
  enum olivedrab = gxRGB!(107, 142, 35);
  enum orange = gxRGB!(255, 165, 0);
  enum orangered = gxRGB!(255, 69, 0);
  enum orchid = gxRGB!(218, 112, 214);
  enum palegoldenrod = gxRGB!(238, 232, 170);
  enum palegreen = gxRGB!(152, 251, 152);
  enum paleturquoise = gxRGB!(175, 238, 238);
  enum palevioletred = gxRGB!(219, 112, 147);
  enum papayawhip = gxRGB!(255, 239, 213);
  enum peachpuff = gxRGB!(255, 218, 185);
  enum peru = gxRGB!(205, 133, 63);
  enum pink = gxRGB!(255, 192, 203);
  enum plum = gxRGB!(221, 160, 221);
  enum powderblue = gxRGB!(176, 224, 230);
  enum purple = gxRGB!(128, 0, 128);
  enum red = gxRGB!(255, 0, 0); // basic color
  enum rosybrown = gxRGB!(188, 143, 143);
  enum royalblue = gxRGB!(65, 105, 225);
  enum saddlebrown = gxRGB!(139, 69, 19);
  enum salmon = gxRGB!(250, 128, 114);
  enum sandybrown = gxRGB!(244, 164, 96);
  enum seagreen = gxRGB!(46, 139, 87);
  enum seashell = gxRGB!(255, 245, 238);
  enum sienna = gxRGB!(160, 82, 45);
  enum silver = gxRGB!(192, 192, 192);
  enum skyblue = gxRGB!(135, 206, 235);
  enum slateblue = gxRGB!(106, 90, 205);
  enum slategray = gxRGB!(112, 128, 144);
  enum slategrey = gxRGB!(112, 128, 144);
  enum snow = gxRGB!(255, 250, 250);
  enum springgreen = gxRGB!(0, 255, 127);
  enum steelblue = gxRGB!(70, 130, 180);
  enum tan = gxRGB!(210, 180, 140);
  enum teal = gxRGB!(0, 128, 128);
  enum thistle = gxRGB!(216, 191, 216);
  enum tomato = gxRGB!(255, 99, 71);
  enum turquoise = gxRGB!(64, 224, 208);
  enum violet = gxRGB!(238, 130, 238);
  enum wheat = gxRGB!(245, 222, 179);
  enum white = gxRGB!(255, 255, 255); // basic color
  enum whitesmoke = gxRGB!(245, 245, 245);
  enum yellow = gxRGB!(255, 255, 0); // basic color
  enum yellowgreen = gxRGB!(154, 205, 50);
}


// A-HSL color
public align(1) struct GxColorHSL {
align(1):
  float h=0.0f, s=0.0f, l=1.0f, a=1.0f;

  string toString () const nothrow {
    import core.stdc.stdio : snprintf;
    char[64] buf = void;
    if (a == 1.0f) {
      immutable len = snprintf(buf.ptr, buf.length, "HSL(%g,%g,%g)", h, s, l);
      return buf[0..len].idup;
    } else {
      immutable len = snprintf(buf.ptr, buf.length, "HSL(%g,%g,%g,%g)", h, s, l, a);
      return buf[0..len].idup;
    }
  }

  public static T clampval(T) (in T a, in T mn, in T mx) pure nothrow @trusted @nogc { pragma(inline, true); return (a < mn ? mn : a > mx ? mx : a); }

nothrow @safe @nogc:
private:
  static float calchue (float h, in float m1, in float m2) pure nothrow @safe @nogc {
    if (h < 0) h += 1;
    if (h > 1) h -= 1;
    if (h < 1.0f/6.0f) return m1+(m2-m1)*h*6.0f;
    if (h < 3.0f/6.0f) return m2;
    if (h < 4.0f/6.0f) return m1+(m2-m1)*(2.0f/3.0f-h)*6.0f;
    return m1;
  }

public:
  this (in float ah, in float as, in float al, in float aa=1.0f) pure { pragma(inline, true); h = ah; s = as; l = al; a = aa; }

  this (in uint clr) pure { pragma(inline, true); fromColor(clr); }

  uint asColor () const {
    import core.stdc.math : fmodf;
    //static if (__VERSION__ >= 2072) pragma(inline, true);
    float xh = fmodf(h, 1.0f);
    if (xh < 0.0f) xh += 1.0f;
    immutable float xs = clampval(s, 0.0f, 1.0f);
    immutable float xl = clampval(l, 0.0f, 1.0f);
    immutable m2 = (xl <= 0.5f ? xl*(1+xs) : xl+xs-xl*xs);
    immutable m1 = 2*xl-m2;
    return
      (clampToByte(cast(int)(clampval(calchue(xh+1.0f/3.0f, m1, m2), 0.0f, 1.0f)*255.0f))<<16)|
      (clampToByte(cast(int)(clampval(calchue(xh, m1, m2), 0.0f, 1.0f)*255.0f))<<8)|
      clampToByte(cast(int)(clampval(calchue(xh-1.0f/3.0f, m1, m2), 0.0f, 1.0f)*255.0f))|
      (clampToByte(cast(int)(clampval(a, 0.0, 1.0f)*255.0f))<<24);
  }

  // taken from Adam's arsd.color
  /* Converts an RGB color into an HSL triplet.
   * [useWeightedLightness] will try to get a better value for luminosity for the human eye,
   * which is more sensitive to green than red and more to red than blue.
   * If it is false, it just does average of the rgb. */
  void fromColor (in uint c, bool useWeightedLightness=false) pure @trusted {
    this.a = gxGetAlpha(c)/255.0f;
    immutable float r1 = gxGetRed(c)/255.0f;
    immutable float g1 = gxGetGreen(c)/255.0f;
    immutable float b1 = gxGetBlue(c)/255.0f;

    float maxColor = r1;
    if (g1 > maxColor) maxColor = g1;
    if (b1 > maxColor) maxColor = b1;
    float minColor = r1;
    if (g1 < minColor) minColor = g1;
    if (b1 < minColor) minColor = b1;

    this.l = (maxColor+minColor)*0.5f;
    if (useWeightedLightness) {
      // the colors don't affect the eye equally
      // this is a little more accurate than plain HSL numbers
      this.l = 0.2126*r1+0.7152*g1+0.0722*b1;
    }
    if (maxColor != minColor) {
      if (this.l < 0.5f) {
        this.s = (maxColor-minColor)/(maxColor+minColor);
      } else {
        this.s = (maxColor-minColor)/(2.0f-maxColor-minColor);
      }
      if (r1 == maxColor) {
        this.h = (g1-b1)/(maxColor-minColor);
      } else if (g1 == maxColor) {
        this.h = 2.0f+(b1-r1)/(maxColor-minColor);
      } else {
        this.h = 4.0f+(r1-g1)/(maxColor-minColor);
      }
    }

    this.h = this.h*60.0f;
    if (this.h < 0.0f) this.h += 360.0f;
    this.h /= 360.0f;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// current clip rect
public __gshared GxRect gxClipRect = GxRect(65535, 65535);

public void gxWithSavedClip(DG) (scope DG dg)
if (is(typeof((inout int=0) { DG dg = void; dg(); })))
{
  pragma(inline, true);
  if (dg !is null) {
    immutable rc = gxClipRect;
    scope(exit) gxClipRect = rc;
    dg();
  }
}

public void gxClipReset () nothrow @trusted @nogc {
  pragma(inline, true);
  gxClipRect = GxRect(VBufWidth, VBufHeight);
}


// ////////////////////////////////////////////////////////////////////////// //
public void gxClearScreen (uint clr) nothrow @trusted @nogc {
  clr &= gxColorMask; // so we could blit OpenGL texture with blending
  memFillDW(vglTexBuf, clr, VBufWidth*VBufHeight);
}


public void gxPutPixel (in int x, in int y, in uint c) nothrow @trusted @nogc {
  pragma(inline, true);
  if (x >= 0 && y >= 0 && x < VBufWidth && y < VBufHeight && !gxIsTransparent(c) && gxClipRect.inside(x, y)) {
    uint* dp = cast(uint*)(cast(ubyte*)vglTexBuf)+y*VBufWidth+x;
    *dp = gxColMix(*dp, c);
  }
}


public void gxPutPixel() (in auto ref GxPoint p, in uint c) nothrow @trusted @nogc {
  pragma(inline, true);
  if (p.x >= 0 && p.y >= 0 && p.x < VBufWidth && p.y < VBufHeight && !gxIsTransparent(c) && gxClipRect.inside(p)) {
    uint* dp = cast(uint*)(cast(ubyte*)vglTexBuf)+p.y*VBufWidth+p.x;
    *dp = gxColMix(*dp, c);
  }
}


public void gxSetPixel (in int x, in int y, in uint c) nothrow @trusted @nogc {
  pragma(inline, true);
  if (x >= 0 && y >= 0 && x < VBufWidth && y < VBufHeight && !gxIsTransparent(c) && gxClipRect.inside(x, y)) {
    *(cast(uint*)(cast(ubyte*)vglTexBuf)+y*VBufWidth+x) = c|gxAlphaMask;
  }
}


public void gxSetPixel() (in auto ref GxPoint p, in uint c) nothrow @trusted @nogc {
  pragma(inline, true);
  if (p.x >= 0 && p.y >= 0 && p.x < VBufWidth && p.y < VBufHeight && !gxIsTransparent(c) && gxClipRect.inside(p)) {
    *(cast(uint*)(cast(ubyte*)vglTexBuf)+p.y*VBufWidth+p.x) = c|gxAlphaMask;
  }
}

public uint gxGetPixel() (in int x, in int y) nothrow @trusted @nogc {
  pragma(inline, true);
  if (x >= 0 && y >= 0 && x < VBufWidth && y < VBufHeight) {
    return *(cast(uint*)(cast(ubyte*)vglTexBuf)+y*VBufWidth+x);
  } else {
    return gxTransparent;
  }
}

public uint gxGetPixel() (in auto ref GxPoint p) nothrow @trusted @nogc {
  pragma(inline, true);
  if (p.x >= 0 && p.y >= 0 && p.x < VBufWidth && p.y < VBufHeight) {
    return *(cast(uint*)(cast(ubyte*)vglTexBuf)+p.y*VBufWidth+p.x);
  } else {
    return gxTransparent;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void gxHStripedLine (int x, int y, int w, in int stlen, in uint stclr) nothrow @trusted @nogc {
  if (stlen < 1 || w < 1) return;
  if (gxIsTransparent(stclr)) return;
  immutable int xstart = x;
  if (!gxClipRect.clipHStripe(x, y, w)) return;
  if (!GxRect(VBufWidth, VBufHeight).clipHStripe(x, y, w)) return;
  immutable uint stFull = cast(uint)stlen<<1;
  int sofs = cast(uint)(x-xstart)%stFull;
  uint* dptr = vglTexBuf+y*VBufWidth+x;
  if (gxIsSolid(stclr)) {
    if (sofs) {
      // fill
      uint nlen = void;
      if (sofs < stlen) {
        nlen = cast(uint)(stlen-sofs);
        if (nlen >= cast(uint)w) {
          memFillDW(dptr, stclr, w);
          return;
        }
        dptr = memFillDW(dptr, stclr, cast(int)nlen);
        w -= nlen;
        sofs = stlen;
      }
      // skip
      assert(sofs >= stlen && sofs < cast(int)stFull);
      nlen = stFull-cast(uint)sofs;
      if (nlen >= cast(uint)w) return;
      dptr += nlen;
      w -= cast(int)nlen;
      // done
      sofs = 0;
    }
    // rest
    while (w > 0) {
      if (stlen >= w) {
        memFillDW(dptr, stclr, w);
        return;
      }
      memFillDW(dptr, stclr, stlen);
      if ((w -= stlen) <= 0) return;
      w -= stlen;
      dptr += stFull;
    }
  } else {
    if (sofs) {
      // fill
      uint nlen = void;
      if (sofs < stlen) {
        nlen = cast(uint)(stlen-sofs);
        if (nlen >= cast(uint)w) {
          //dptr[0..cast(uint)w] = stclr;
          memBlendColor(dptr, stclr, w);
          return;
        }
        //dptr[0..nlen] = stclr;
        dptr = memBlendColor(dptr, stclr, cast(int)nlen);
        w -= nlen;
        sofs = stlen;
      }
      // skip
      assert(sofs >= stlen && sofs < cast(int)stFull);
      nlen = stFull-cast(uint)sofs;
      if (nlen >= cast(uint)w) return;
      dptr += nlen;
      w -= cast(int)nlen;
      // done
      sofs = 0;
    }
    // rest
    while (w > 0) {
      if (stlen >= w) {
        //dptr[0..cast(uint)w] = stclr;
        memBlendColor(dptr, stclr, w);
        return;
      }
      //dptr[0..cast(uint)stlen] = stclr;
      memBlendColor(dptr, stclr, stlen);
      if ((w -= stlen) <= 0) return;
      w -= stlen;
      dptr += stFull;
    }
  }
}

public void gxHStripedLine() (in auto ref GxPoint p, in int w, int stlen, in uint stclr) nothrow @trusted @nogc { pragma(inline, true); gxHStripedLine(p.x, p.y, w, stlen, stclr); }


// ////////////////////////////////////////////////////////////////////////// //
public void gxHLine (int x, int y, int w, in uint clr) nothrow @trusted @nogc {
  if (gxIsTransparent(clr)) return;
  if (!gxClipRect.clipHStripe(x, y, w)) return;
  if (!GxRect(VBufWidth, VBufHeight).clipHStripe(x, y, w)) return;
  if (gxIsSolid(clr)) {
    memFillDW(vglTexBuf+y*VBufWidth+x, clr, w);
  } else {
    memBlendColor(vglTexBuf+y*VBufWidth+x, clr, w);
  }
}

public void gxHLine() (in auto ref GxPoint p, in int w, in uint clr) nothrow @trusted @nogc { pragma(inline, true); gxHLine(p.x, p.y, w, clr); }

public void gxVLine (int x, int y, int h, in uint clr) nothrow @trusted @nogc {
  if (gxIsTransparent(clr)) return;
  if (!gxClipRect.clipVStripe(x, y, h)) return;
  if (!GxRect(VBufWidth, VBufHeight).clipVStripe(x, y, h)) return;
  uint* dptr = vglTexBuf+y*VBufWidth+x;
  if (gxIsSolid(clr)) {
    while (h-- > 0) { *dptr = clr; dptr += VBufWidth; }
  } else {
    while (h-- > 0) { mixin(GxColMixMixin!("*dptr", "*dptr", "clr")); dptr += VBufWidth; }
  }
}

public void gxVLine() (in auto ref GxPoint p, in int h, in uint clr) nothrow @trusted @nogc { pragma(inline, true); gxVLine(p.x, p.y, h, clr); }


// ////////////////////////////////////////////////////////////////////////// //
public void gxDashRect (int x, int y, int w, int h, in uint clr) nothrow @trusted @nogc {
  if (gxIsTransparent(clr)) return;
  if (!gxClipRect.clipHVStripes(x, y, w, h)) return;
  if (!GxRect(VBufWidth, VBufHeight).clipHVStripes(x, y, w, h)) return;
  uint* dptr = vglTexBuf+y*VBufWidth+x;
  immutable uint dinc = VBufWidth-w;
  if (gxIsSolid(clr)) {
    if (w == 1) {
      while (h-- > 0) {
        *dptr = clr;
        dptr += VBufWidth<<1;
        --h;
      }
    } else {
      while (h-- > 0) {
        dptr = memFillDWDash(dptr, clr, w)+dinc;
        if (h-- > 0) dptr = memFillDWDash(dptr+1, clr, w-1)+dinc;
      }
    }
  } else {
    if (w == 1) {
      while (h-- > 0) {
        mixin(GxColMixMixin!("*dptr", "*dptr", "clr"));
        dptr += VBufWidth<<1;
        --h;
      }
    } else {
      while (h-- > 0) {
        dptr = memBlendColorDash(dptr, clr, w)+dinc;
        if (h-- > 0) dptr = memBlendColorDash(dptr+1, clr, w-1)+dinc;
      }
    }
  }
}

public void gxDashRect() (in auto ref GxRect rc, in uint clr) nothrow @trusted @nogc {
  pragma(inline, true);
  gxDashRect(rc.x0, rc.y0, rc.width, rc.height, clr);
}


// ////////////////////////////////////////////////////////////////////////// //
public void gxFillRect (int x, int y, int w, int h, in uint clr) nothrow @trusted @nogc {
  if (gxIsTransparent(clr)) return;
  if (!gxClipRect.clipHVStripes(x, y, w, h)) return;
  if (!GxRect(VBufWidth, VBufHeight).clipHVStripes(x, y, w, h)) return;
  if (w == 1) { gxVLine(x, y, h, clr); return; }
  if (h == 1) { gxHLine(x, y, w, clr); return; }
  uint* dptr = vglTexBuf+y*VBufWidth+x;
  immutable uint dinc = VBufWidth-w;
  if (gxIsSolid(clr)) {
    while (h-- > 0) dptr = memFillDW(dptr, clr, w)+dinc;
  } else {
    while (h-- > 0) dptr = memBlendColor(dptr, clr, w)+dinc;
  }
}

public void gxFillRect() (in auto ref GxRect rc, in uint clr) nothrow @trusted @nogc {
  pragma(inline, true);
  gxFillRect(rc.x0, rc.y0, rc.width, rc.height, clr);
}


public void gxDrawRect (in int x, in int y, in int w, in int h, in uint clr) nothrow @trusted @nogc {
  if (w < 1 || h < 1 || gxIsTransparent(clr)) return;
  gxHLine(x, y, w, clr);
  if (h > 1) gxHLine(x, y+h-1, w, clr);
  if (h > 2) {
    gxVLine(x, y+1, h-2, clr);
    if (w > 1) gxVLine(x+w-1, y+1, h-2, clr);
  }
}

public void gxDrawRect() (in auto ref GxRect rc, in uint clr) nothrow @trusted @nogc {
  pragma(inline, true);
  gxDrawRect(rc.x0, rc.y0, rc.width, rc.height, clr);
}


// ////////////////////////////////////////////////////////////////////////// //
// use clip region as boundaries
public void gxDrawShadow (in GxRect winrect, int size, in uint clrshadow=gxRGBA!(0, 0, 0, 127), bool shadowdash=false) nothrow @trusted @nogc {
  if (size < 1 || winrect.empty || gxIsTransparent(clrshadow)) return;
  gxWithSavedClip{
    gxClipReset();
    if (!shadowdash) {
      gxFillRect(winrect.x1+1, winrect.y0+size, size, winrect.height-size, clrshadow);
      gxFillRect(winrect.x0+size, winrect.y1+1, winrect.width, size, clrshadow);
    } else {
      gxDashRect(winrect.x1+1, winrect.y0+size, size, winrect.height-size, clrshadow);
      //FIXME: this renders wrong shadow for odd window sizes
      gxDashRect(winrect.x0+size, winrect.y1+1, winrect.width, size, clrshadow);
    }
  };
}


public void gxDrawWindow (GxRect winrect,
                          const(char)[] title, in uint framecolor,
                          in uint titlecolor, in uint titlebackcolor, in uint windowcolor,
                          in uint shadowcolor, int shadowsize=8, bool shadowdash=false) nothrow @trusted
{
  import iv.egra.gfx.text;

  if (winrect.empty) return;
  gxDrawShadow(winrect, shadowsize, shadowcolor, shadowdash);

  gxDrawRect(winrect, framecolor);
  if (winrect.width <= 2 || winrect.height <= 2) return;
  winrect.shrinkBy(1, 1);

  if (title is null) {
    gxFillRect(winrect, windowcolor);
    return;
  }

  gxWithSavedClip{
    immutable int hgt = (gxTextHeightUtf < 10 ? 10 : gxTextHeightUtf+1);
    immutable int oh = winrect.size.h;
    if (hgt < winrect.size.h) winrect.size.h = hgt;
    if (gxClipRect.intersect(winrect)) {
      gxFillRect(gxClipRect, titlebackcolor);
      gxDrawTextUtf(winrect.x0+(winrect.width-gxTextWidthUtf(title))/2, winrect.y0+(hgt-gxTextHeightUtf)/2, title, titlecolor);
    }
    winrect.pos.y += hgt;
    winrect.size.h = oh-hgt;
  };
  gxFillRect(winrect, windowcolor);
}


// ////////////////////////////////////////////////////////////////////////// //
public void gxDrawScrollBar() (in auto ref GxRect r, in int max, in int value) nothrow @trusted @nogc { pragma(inline, true); gxDrawScrollBar(r, 0, max, value); }

public void gxDrawScrollBar() (in auto ref GxRect r, int min, int max, int value) nothrow @trusted @nogc {
  enum FrameColor = gxRGB!(220, 220, 220);
  enum EmptyColor = gxRGB!(0, 0, 0);
  enum FullColor = gxRGB!(160, 160, 160);
  if (r.empty) return;
  if (max <= min) min = max = value = 0;
  // move it to 0
  //conwriteln("00: min=", min, "; max=", max, "; value=", value);
  max -= min;
  value -= min;
  if (value < 0) value = 0; else if (value > max) value = max;
  //conwriteln("01: min=", min, "; max=", max, "; value=", value);
  int sx0 = r.x0+1;
  int sy0 = r.y0+1;
  int wdt = r.width-2;
  int hgt = r.height-2;
  bool vert = (r.width < r.height);
  // frame
  if ((vert && wdt > 1) || (!vert && hgt > 1)) {
    gxHLine(r.x0+1, r.y0+0, wdt, FrameColor);
    gxVLine(r.x0+0, r.y0+1, hgt, FrameColor);
    gxVLine(r.x1+0, r.y0+1, hgt, FrameColor);
    gxHLine(r.x0+1, r.y1+0, wdt, FrameColor);
  } else {
    sx0 -= 1;
    sy0 -= 1;
    wdt += 2;
    hgt += 2;
  }
  if (max <= 0) {
    gxFillRect(sx0, sy0, wdt, hgt, FullColor);
    return;
  }
  if (vert) {
    int pix = hgt*value/max;
    if (pix > hgt) pix = hgt; // just in case
    gxFillRect(sx0, sy0, wdt, pix, FullColor);
    gxFillRect(sx0, sy0+pix, wdt, hgt-pix, EmptyColor);
  } else {
    int pix = wdt*value/max;
    if (pix > wdt) pix = wdt; // just in case
    gxFillRect(sx0, sy0, pix, hgt, FullColor);
    gxFillRect(sx0+pix, sy0, wdt-pix, hgt, EmptyColor);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private int abs (int a) pure nothrow @safe @nogc { pragma(inline, true); return (a < 0 ? -a : a); }

public void gxCircle (in int cx, in int cy, in int radius, in uint clr) nothrow @trusted @nogc {
  static void plot4points (in int cx, in int cy, in int x, in int y, in uint clr) nothrow @trusted @nogc {
    pragma(inline, true);
    gxPutPixel(cx+x, cy+y, clr);
    if (x) gxPutPixel(cx-x, cy+y, clr);
    if (y) gxPutPixel(cx+x, cy-y, clr);
    gxPutPixel(cx-x, cy-y, clr);
  }

  if (radius <= 0 || gxIsTransparent(clr)) return;
  if (radius == 1) { gxPutPixel(cx, cy, clr); return; }
  int error = -radius, x = radius, y = 0;
  while (x > y) {
    plot4points(cx, cy, x, y, clr);
    plot4points(cx, cy, y, x, clr);
    error += y*2+1;
    ++y;
    if (error >= 0) { --x; error -= x*2; }
  }
  plot4points(cx, cy, x, y, clr);
}

public void gxCircle() (in auto ref GxPoint c, in int radius, in uint clr) nothrow @trusted @nogc { pragma(inline, true); gxCircle(c.x, c.y, radius, clr); }


public void gxFillCircle (in int cx, in int cy, in int radius, in uint clr) nothrow @trusted @nogc {
  if (radius <= 0 || gxIsTransparent(clr)) return;
  if (radius == 1) { gxPutPixel(cx, cy, clr); return; }
  int error = -radius, x = radius, y = 0;
  while (x >= y) {
    int last_y = y;
    error += y;
    ++y;
    error += y;
    gxHLine(cx-x, cy+last_y, 2*x+1, clr);
    if (x != 0 && last_y != 0) gxHLine(cx-x, cy-last_y, 2*x+1, clr);
    if (error >= 0) {
      if (x != last_y) {
        gxHLine(cx-last_y, cy+x, 2*last_y+1, clr);
        if (last_y != 0 && x != 0) gxHLine(cx-last_y, cy-x, 2*last_y+1, clr);
      }
      error -= x;
      --x;
      error -= x;
    }
  }
}

public void gxFillCircle() (in auto ref GxPoint c, in int radius, in uint clr) nothrow @trusted @nogc { pragma(inline, true); gxFillCircle(c.x, c.y, radius, clr); }


public void gxEllipse (int x0, int y0, int x1, int y1, in uint clr) nothrow @trusted @nogc {
  if (gxIsTransparent(clr)) return;
  if (y0 == y1) { gxHLine(x0, y0, x1-x0+1, clr); return; }
  if (x0 == x1) { gxVLine(x0, y0, y1-y0+1, clr); return; }
  int a = abs(x1-x0), b = abs(y1-y0), b1 = b&1; // values of diameter
  long dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; // error increment
  long err = dx+dy+b1*a*a; // error of 1.step
  if (x0 > x1) { x0 = x1; x1 += a; } // if called with swapped points...
  if (y0 > y1) y0 = y1; // ...exchange them
  y0 += (b+1)/2; y1 = y0-b1;  // starting pixel
  a *= 8*a; b1 = 8*b*b;
  do {
    long e2;
    gxPutPixel(x1, y0, clr); //   I. Quadrant
    gxPutPixel(x0, y0, clr); //  II. Quadrant
    gxPutPixel(x0, y1, clr); // III. Quadrant
    gxPutPixel(x1, y1, clr); //  IV. Quadrant
    e2 = 2*err;
    if (e2 >= dx) { ++x0; --x1; err += dx += b1; } // x step
    if (e2 <= dy) { ++y0; --y1; err += dy += a; }  // y step
  } while (x0 <= x1);
  while (y0-y1 < b) {
    // too early stop of flat ellipses a=1
    gxPutPixel(x0-1, ++y0, clr); // complete tip of ellipse
    gxPutPixel(x0-1, --y1, clr);
  }
}

public void gxEllipse() (in auto ref GxRect rc, in int radius, in uint clr) nothrow @trusted @nogc { pragma(inline, true); gxEllipse(rc.x0, rc.y0, rc.x1, rc.y1, clr); }


public void gxFillEllipse (int x0, int y0, int x1, int y1, in uint clr) nothrow @trusted @nogc {
  if (gxIsTransparent(clr)) return;
  if (y0 == y1) { gxHLine(x0, y0, x1-x0+1, clr); return; }
  if (x0 == x1) { gxVLine(x0, y0, y1-y0+1, clr); return; }
  int a = abs(x1-x0), b = abs(y1-y0), b1 = b&1; // values of diameter
  long dx = 4*(1-a)*b*b, dy = 4*(b1+1)*a*a; // error increment
  long err = dx+dy+b1*a*a; // error of 1.step
  int prev_y0 = -1, prev_y1 = -1;
  if (x0 > x1) { x0 = x1; x1 += a; } // if called with swapped points...
  if (y0 > y1) y0 = y1; // ...exchange them
  y0 += (b+1)/2; y1 = y0-b1; // starting pixel
  a *= 8*a; b1 = 8*b*b;
  do {
    long e2;
    if (y0 != prev_y0) { gxHLine(x0, y0, x1-x0+1, clr); prev_y0 = y0; }
    if (y1 != y0 && y1 != prev_y1) { gxHLine(x0, y1, x1-x0+1, clr); prev_y1 = y1; }
    e2 = 2*err;
    if (e2 >= dx) { ++x0; --x1; err += dx += b1; } // x step
    if (e2 <= dy) { ++y0; --y1; err += dy += a; }  // y step
  } while (x0 <= x1);
  while (y0-y1 < b) {
    // too early stop of flat ellipses a=1
    gxPutPixel(x0-1, ++y0, clr); // complete tip of ellipse
    gxPutPixel(x0-1, --y1, clr);
  }
}

public void gxFillEllipse() (in auto ref GxRect rc, in int radius, in uint clr) nothrow @trusted @nogc { pragma(inline, true); gxFillEllipse(rc.x0, rc.y0, rc.x1, rc.y1, clr); }


// ////////////////////////////////////////////////////////////////////////// //
public void gxDrawRoundedRect (int x0, int y0, int wdt, int hgt, int radius, in uint clr) nothrow @trusted @nogc {
  static void gxArcs (int cx, int cy, int wdt, int hgt, in int radius, in uint clr) nothrow @trusted @nogc {
    static void plot4points (in int radius, in int wdt, in int hgt, in int cx, in int cy, in int x, in int y, in uint clr) nothrow @trusted @nogc {
      pragma(inline, true);
      gxPutPixel(cx+wdt-1+x, cy+hgt-1+y, clr);
      gxPutPixel(cx-x, cy+hgt-1+y, clr);
      gxPutPixel(cx+wdt-1+x, cy-y, clr);
      gxPutPixel(cx-x, cy-y, clr);
    }

    wdt -= (radius<<1); if (wdt <= 0) return;
    hgt -= (radius<<1); if (hgt <= 0) return;
    cx += radius; cy += radius;
    int error = -radius, x = radius, y = 0;
    while (x > y) {
      plot4points(radius, wdt, hgt, cx, cy, x, y, clr);
      plot4points(radius, wdt, hgt, cx, cy, y, x, clr);
      error += y*2+1;
      ++y;
      if (error >= 0) { --x; error -= x*2; }
    }
    if (x || y != radius) plot4points(radius, wdt, hgt, cx, cy, x, y, clr);
  }


  if (wdt < 1 || hgt < 1) return;
  if (radius < 1) { gxDrawRect(x0, y0, wdt, hgt, clr); return; }
  if (gxIsTransparent(clr)) return;
  if (hgt == 1) { gxHLine(x0, y0, wdt, clr); return; }
  if (wdt == 1) { gxVLine(x0, y0, hgt, clr); return; }
  // fix radius
  immutable int minsz = (wdt < hgt ? wdt : hgt);
  if (radius >= (minsz>>1)) {
    radius = (minsz>>1)-1;
    if (radius < 1) { gxDrawRect(x0, y0, wdt, hgt, clr); return; }
  }
  // draw the parts of the rect
  gxHLine(x0+radius+1, y0, wdt-(radius<<1)-2, clr); // top
  gxHLine(x0+radius+1, y0+hgt-1, wdt-(radius<<1)-2, clr); // bottom
  gxVLine(x0, y0+radius+1, hgt-(radius<<1)-2, clr); // left
  gxVLine(x0+wdt-1, y0+radius+1, hgt-(radius<<1)-2, clr); // right
  // left arc
  gxArcs(x0, y0, wdt, hgt, radius, clr);
}

public void gxDrawRoundedRect() (in auto ref GxRect rc, in int radius, in uint clr) nothrow @trusted @nogc {
  pragma(inline, true);
  gxDrawRoundedRect(rc.x0, rc.y0, rc.width, rc.height, radius, clr);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared usize frectXCoords; // array of ints
__gshared usize frectXCSize; // in items


/* cyclic dependency
shared static ~this () {
  if (frectXCoords) {
    import core.stdc.stdlib : free;
    free(cast(void*)frectXCoords);
    frectXCoords = 0;
  }
}
*/


int* ensureXCoords (int radius) nothrow @trusted @nogc {
  if (radius < 1) return null;
  if (radius > 1024*1024) return null;
  if (cast(usize)radius > frectXCSize) {
    import core.stdc.stdlib : realloc;
    immutable usize newsz = (cast(usize)radius|0x7fu)+1;
    void* np = realloc(cast(void*)frectXCoords, newsz*int.sizeof);
    if (np is null) return null; // out of memory
    frectXCSize = newsz;
    frectXCoords = cast(usize)np;
  }
  return cast(int*)frectXCoords;
}


// this is wrong, but i'm ok with it for now
public void gxFillRoundedRect (int x0, int y0, int wdt, int hgt, int radius, in uint clr) nothrow @trusted @nogc {
  if (wdt < 1 || hgt < 1) return;
  if (radius < 1) { gxFillRect(x0, y0, wdt, hgt, clr); return; }
  if (gxIsTransparent(clr)) return;
  if (hgt == 1) { gxHLine(x0, y0, wdt, clr); return; }
  if (wdt == 1) { gxVLine(x0, y0, hgt, clr); return; }
  // fix radius
  immutable int minsz = (wdt < hgt ? wdt : hgt);
  if (radius >= (minsz>>1)) {
    radius = (minsz>>1)-1;
    if (radius < 1) { gxFillRect(x0, y0, wdt, hgt, clr); return; }
  }

  // create border coords
  auto xpt = ensureXCoords(radius+1);
  if (xpt is null) { gxFillRect(x0, y0, wdt, hgt, clr); return; } // do at least something
  xpt[0..radius+1] = int.min;

  // create coords
  {
    int error = -radius, x = radius, y = 0;
    while (x > y) {
      if (y <= radius && xpt[y] < x) xpt[y] = x;
      if (x >= 0 && x <= radius && xpt[x] < y) xpt[x] = y;
      error += y*2+1;
      ++y;
      if (error >= 0) { --x; error -= x*2; }
    }
    if (y <= radius && xpt[y] < x) xpt[y] = x;
    if (x >= 0 && x <= radius && xpt[x] < y) xpt[x] = y;
  }

  // draw the filled rect
  gxFillRect(x0, y0+radius+1, wdt, hgt-(radius<<1)-2, clr);

  // draw arc
  foreach (immutable dy; 0..radius+1) {
    if (xpt[dy] == int.min) continue;
    immutable topy = y0+radius-dy;
    immutable topx0 = x0+radius-xpt[dy];
    immutable topx1 = x0+wdt-radius-1+xpt[dy];
    //gxPutPixel(topx0, topy, clr);
    //gxPutPixel(topx1, topy, clr);
    gxHLine(topx0, topy, topx1-topx0+1, clr);
    immutable boty = y0+hgt-radius+dy-1;
    //gxPutPixel(topx0, boty, clr);
    //gxPutPixel(topx1, boty, clr);
    gxHLine(topx0, boty, topx1-topx0+1, clr);
  }
}

public void gxFillRoundedRect() (in auto ref GxRect rc, in int radius, in uint clr) nothrow @trusted @nogc {
  pragma(inline, true);
  gxFillRoundedRect(rc.x0, rc.y0, rc.width, rc.height, radius, clr);
}


// ////////////////////////////////////////////////////////////////////////// //
// bresenham with clipping
// the idea is that we can simply skip the right number of steps
// if the point is off the drawing area
public void gxDrawLine (int x0, int y0, int x1, int y1, in uint clr, bool lastPoint=true) nothrow @trusted @nogc {
  enum swap(string a, string b) = "{immutable int tmp_="~a~";"~a~"="~b~";"~b~"=tmp_;}";

  if (gxIsTransparent(clr)) return;

  GxRect realClip = gxClipRect;
  if (!realClip.intersect(VBufWidth, VBufHeight)) return;

  // just a point?
  if (x0 == x1 && y0 == y1) {
    if (lastPoint) gxPutPixel(x0, y0, clr);
    return;
  }

  // horizontal line?
  if (y0 == y1) {
    if (x0 > x1) {
      gxHLine(x1+(lastPoint ? 0 : 1), y0, x0-x1+(lastPoint ? 1 : 0), clr);
    } else {
      gxHLine(x0, y0, x1-x0+(lastPoint ? 1 : 0), clr);
    }
    return;
  }

  // clip rectange
  int wx0 = realClip.x0, wy0 = realClip.y0, wx1 = realClip.x1, wy1 = realClip.y1;
  if (wx0 > wx1 || wy0 > wy1) return; // this should not happen, but...

  // vertical setup; always go from top to bottom, so we'll draw the same line regardless of the starting point
  bool skipFirst = false;
  if (y0 > y1) {
    // swap endpoints
    if (!lastPoint) skipFirst = lastPoint = true;
    mixin(swap!("x0", "x1"));
    mixin(swap!("y0", "y1"));
  }
  if (y0 > wy1 || y1 < wy0) return; // out of clip rectange
  int sty = 1; // "step sign" for x axis; we still need the var, because there is a possible swap down there

  // horizontal setup
  int stx = void; // "step sign" for x axis
  if (x0 < x1) {
    // from left to right
    if (x0 > wx1 || x1 < wx0) return; // out of clip rectange
    stx = 1; // going right
  } else {
    // from right to left
    if (x1 > wx1 || x0 < wx0) return; // out of clip rectange
    stx = -1; // going left
    x0 = -x0;
    x1 = -x1;
    wx0 = -wx0;
    wx1 = -wx1;
    mixin(swap!("wx0", "wx1"));
  }

  int dsx = x1-x0; // "length" for x axis
  int dsy = y1-y0; // "length" for y axis
  int xd = void, yd = void; // current coord
  bool xyswapped = false; // if `true`, `xd` and `yd` are swapped
  if (dsx < dsy) {
    xyswapped = true;
    mixin(swap!("x0", "y0"));
    mixin(swap!("x1", "y1"));
    mixin(swap!("dsx", "dsy"));
    mixin(swap!("wx0", "wy0"));
    mixin(swap!("wx1", "wy1"));
    mixin(swap!("stx", "sty"));
  }
  xd = x0;
  yd = y0;
  int dx2 = 2*dsx; // "double length" for x axis
  int dy2 = 2*dsy; // "double length" for y axis
  int e = 2*dsy-dsx; // "error" (as in bresenham algo)
  int term = x1; // termination point
  bool xfixed = false; // will be set if we properly fixed x0 coord while fixing the y0
  // note that clipping can overflow for insane coords
  // if you are completely sure that it can't happen, you can use `int` instead of `long`
  if (y0 < wy0) {
    // clip at top
    immutable long temp = cast(long)dx2*(wy0-y0)-dsx;
    xd += cast(int)(temp/dy2);
    if (xd > wx1) return; // x is moved out of clipping rect, nothing to do
    immutable int rem = cast(int)(temp%dy2);
    if (xd+(rem > 0 ? 1 : 0) >= wx0) {
      xfixed = true; // startx is inside the clipping rect, no need to perform left clip
      yd = wy0;
      e -= rem+dsx;
      if (rem > 0) { ++xd; e += dy2; }
    }
  }
  if (!xfixed && x0 < wx0) {
    // clip at left
    immutable long temp = cast(long)dy2*(wx0-x0);
    yd += cast(int)(temp/dx2);
    immutable int rem = cast(int)(temp%dx2);
    if (yd > wy1 || (yd == wy1 && rem >= dsx)) return; // y is moved out of clipping rect, nothing to do
    xd = wx0;
    e += rem;
    if (rem >= dsx) { ++yd; e -= dx2; }
  }
  if (y1 > wy1) {
    // clip at bottom
    immutable long temp = cast(long)dx2*(wy1-y0)+dsx;
    term = x0+cast(int)(temp/dy2);
    // it should be safe to decrement here
    if (cast(int)(temp%dy2) == 0) --term;
  }
  if (term > wx1) term = wx1; // clip at right

  if (sty == -1) yd = -yd;
  if (stx == -1) { xd = -xd; term = -term; }
  dx2 -= dy2;

  if (lastPoint) term += stx;
  if (skipFirst) {
    if (term == xd) return;
    if (e >= 0) { yd += sty; e -= dx2; } else { e += dy2; }
    xd += stx;
  }

  // draw it; `putPixel()` can omit checks
  if (gxIsSolid(clr)) {
    while (xd != term) {
      // inlined putpixel
      *cast(uint*)((cast(ubyte*)vglTexBuf)+(xyswapped ? xd*VBufWidth+yd : yd*VBufWidth+xd)) = clr;
      // done drawing, move coords
      if (e >= 0) { yd += sty; e -= dx2; } else { e += dy2; }
      xd += stx;
    }
  } else {
    while (xd != term) {
      // inlined putpixel
      uint* dp = cast(uint*)((cast(ubyte*)vglTexBuf)+(xyswapped ? xd*VBufWidth+yd : yd*VBufWidth+xd));
      mixin(GxColMixMixin!("*dp", "*dp", "clr"));
      // done drawing, move coords
      if (e >= 0) { yd += sty; e -= dx2; } else { e += dy2; }
      xd += stx;
    }
  }
}

public void gxDrawLine() (in auto ref GxPoint p0, in int x1, in int y1, in uint clr, in bool lastPoint=true) { pragma(inline, true); gxDrawLine(p0.x, p0.y, x1, y1, clr, lastPoint); }
public void gxDrawLine() (in int x0, in int y0, auto ref GxPoint p1, in uint clr, in bool lastPoint=true) { pragma(inline, true); gxDrawLine(x0, y0, p1.x, p1.y, clr, lastPoint); }
public void gxDrawLine() (in auto ref GxPoint p0, auto ref GxPoint p1, in uint clr, in bool lastPoint=true) { pragma(inline, true); gxDrawLine(p0.x, p0.y, p1.x, p1.y, clr, lastPoint); }
