/*
 * Simple Framebuffer Gfx/GUI lib
 *
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module iv.egra.gfx.backx11 /*is aliced*/;
private:

import arsd.simpledisplay;

import iv.alice;
import iv.cmdcon;
import iv.cmdcongl;

import iv.egra.gfx.config;
import iv.egra.gfx.base;
import iv.egra.gfx.lowlevel;


// ////////////////////////////////////////////////////////////////////////// //
//public __gshared uint vArrowTextureId = 0;
public enum vArrowTextureId = 0;
//public __gshared Image egx11img;


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  import core.stdc.stdlib : malloc;
  egfxCheckCPU();
  // always allocate additional 16 bytes for SSE routines
  vglTexBuf = cast(uint*)malloc((cast(uint)VBufWidth*cast(uint)VBufHeight)*vglTexBuf[0].sizeof+16u);
  if (vglTexBuf is null) { import core.exception : onOutOfMemoryErrorNoGC; onOutOfMemoryErrorNoGC(); }
  vglTexBuf[0..VBufWidth*VBufHeight] = 0;
}


// ////////////////////////////////////////////////////////////////////////// //
/+
private extern(C) nothrow @trusted @nogc {
  import core.stdc.config : c_long, c_ulong;

  XImage* egfx_backx11_xxsimple_create_image (XDisplay* display, Visual* visual, uint depth, int format, int offset, ubyte* data, uint width, uint height, int bitmap_pad, int bytes_per_line) {
    //return XCreateImage(display, visual, depth, format, offset, data, width, height, bitmap_pad, bytes_per_line);
    return null;
  }

  int egfx_backx11_xxsimple_destroy_image (XImage* ximg) {
    ximg.data = null;
    ximg.width = ximg.height = 0;
    return 0;
  }

  c_ulong egfx_backx11_xxsimple_get_pixel (XImage* ximg, int x, int y) {
    if (ximg.data is null) return 0;
    if (x < 0 || y < 0 || x >= ximg.width || y >= ximg.height) return 0;
    auto buf = cast(const(uint)*)ximg.data;
    //uint v = buf[y*ximg.width+x];
    //v = (v&0xff_00ff00u)|((v>>16)&0x00_0000ffu)|((v<<16)&0x00_ff0000u);
    return buf[y*ximg.width+x];
  }

  int egfx_backx11_xxsimple_put_pixel (XImage* ximg, int x, int y, c_ulong clr) {
    return 0;
  }

  XImage* egfx_backx11_xxsimple_sub_image (XImage* ximg, int x, int y, uint wdt, uint hgt) {
    return null;
  }

  int egfx_backx11_xxsimple_add_pixel (XImage* ximg, c_long clr) {
    return 0;
  }

  // create "simple" XImage with allocated buffer
  void egfx_backx11_ximageInitSimple (ref XImage handle, int width, int height, void* data) {
    handle.width = width;
    handle.height = height;
    handle.xoffset = 0;
    handle.format = ImageFormat.ZPixmap;
    handle.data = data;
    handle.byte_order = 0;
    handle.bitmap_unit = 0;
    handle.bitmap_bit_order = 0;
    handle.bitmap_pad = 0;
    handle.depth = 24;
    handle.bytes_per_line = 0;
    handle.bits_per_pixel = 0; // THIS MATTERS!
    handle.red_mask = 0;
    handle.green_mask = 0;
    handle.blue_mask = 0;

    handle.obdata = null;
    handle.f.create_image = &egfx_backx11_xxsimple_create_image;
    handle.f.destroy_image = &egfx_backx11_xxsimple_destroy_image;
    handle.f.get_pixel = &egfx_backx11_xxsimple_get_pixel;
    handle.f.put_pixel = &egfx_backx11_xxsimple_put_pixel;
    handle.f.sub_image = &egfx_backx11_xxsimple_sub_image;
    handle.f.add_pixel = &egfx_backx11_xxsimple_add_pixel;
  }
}
+/


// ////////////////////////////////////////////////////////////////////////// //
public void vglCreateArrowTexture () {
}


// ////////////////////////////////////////////////////////////////////////// //
public void vglBlitArrow (int px, int py) {
}


// ////////////////////////////////////////////////////////////////////////// //
// resize buffer, reinitialize OpenGL texture
public void vglResizeBuffer (int wdt, int hgt, int ascale=1) {
  if (wdt < 1) wdt = 1;
  if (hgt < 1) hgt = 1;

  if (wdt > 8192) wdt = 8192;
  if (hgt > 8192) hgt = 8192;

  bool sizeChanged = (wdt != VBufWidth || hgt != VBufHeight);
  VBufWidth = wdt;
  VBufHeight = hgt;

  if (vglTexBuf is null || sizeChanged) {
    import core.stdc.stdlib : realloc;
    // always allocate additional 16 bytes for SSE routines
    vglTexBuf = cast(uint*)realloc(vglTexBuf, (cast(uint)VBufWidth*cast(uint)VBufHeight)*vglTexBuf[0].sizeof+16u);
    if (vglTexBuf is null) { import core.exception : onOutOfMemoryErrorNoGC; onOutOfMemoryErrorNoGC(); }
    vglTexBuf[0..VBufWidth*VBufHeight] = 0;
  }

  if (ascale < 1) ascale = 1;
  if (ascale > 32) ascale = 32;
  vbufEffScale = cast(ubyte)ascale;

  /+
  if (egx11img is null || egx11img.width != VBufWidth || egx11img.height != VBufHeight) {
    egx11img = new Image(VBufWidth, VBufHeight);
    //glconBackBuffer = egx11img;
  }
  +/
}


// ////////////////////////////////////////////////////////////////////////// //
public void vglUpdateTexture () {
  /*
  if (vglTexBuf is null) return;
  if (egx11img is null || egx11img.width != VBufWidth || egx11img.height != VBufHeight) {
    egx11img = new Image(VBufWidth, VBufHeight);
    glconBackBuffer = egx11img;
  }
  */
  /+
  if (egx11img !is null && vglTexBuf !is null) {
    int bmpw = egx11img.width;
    int bmph = egx11img.height;
    int copyw = (bmpw < VBufWidth ? bmpw : VBufWidth);
    if (bmph > VBufHeight) bmph = VBufHeight;
    const(uint)* src = cast(const(uint)*)vglTexBuf;
    uint* dest = cast(uint*)egx11img.getDataPointer;
    while (bmph-- > 0) {
      import core.stdc.string : memcpy;
      memcpy(dest, src, copyw*4);
      src += VBufWidth;
      dest += bmpw;
    }
  }
  +/
}


// ////////////////////////////////////////////////////////////////////////// //
/*
extern(C) nothrow @trusted @nogc {
  Status XInitImage (XImage* image);
}
*/


public void vglBlitTexture (SimpleWindow w, bool direct) {
  if (w !is null && !w.closed) {
    XImage ximg;
    //egfx_backx11_ximageInitSimple(ximg, VBufWidth, VBufHeight, vglTexBuf);

    ximg.width = VBufWidth;
    ximg.height = VBufHeight;
    ximg.xoffset = 0;
    ximg.format = ImageFormat.ZPixmap;
    ximg.data = vglTexBuf;
    ximg.byte_order = 0;
    ximg.bitmap_unit = 32;
    ximg.bitmap_bit_order = 0;
    ximg.bitmap_pad = 8;
    ximg.depth = 24;
    ximg.bytes_per_line = 0;
    ximg.bits_per_pixel = 32; // THIS MATTERS!
    ximg.red_mask = 0x00ff0000;
    ximg.green_mask = 0x0000ff00;
    ximg.blue_mask = 0x000000ff;
    XInitImage(&ximg);

    if (direct) {
      XPutImage(w.impl.display, cast(Drawable)w.impl.window, w.impl.gc, &ximg, 0, 0, 0/*destx*/, 0/*desty*/, VBufWidth, VBufHeight);
    } else {
      XPutImage(w.impl.display, cast(Drawable)w.impl.buffer, w.impl.gc, &ximg, 0, 0, 0/*destx*/, 0/*desty*/, VBufWidth, VBufHeight);
    }
  }
}
