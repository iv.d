/* converted by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
module iv.dlzma.dec;

private import iv.dlzma;
nothrow:

/* use smaller (by ~3KB), but slightly slower (prolly) decompression code? */
//version = LZMA_SIZE_OPT;


// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
private:
enum kNumTopBits = 24;
enum kTopValue = (cast(uint)1 << kNumTopBits);

enum kNumBitModelTotalBits = 11;
enum kBitModelTotal = (1 << kNumBitModelTotalBits);

enum RC_INIT_SIZE = 5;


enum kNumMoveBits = 5;
enum NORMALIZE = `if (range < kTopValue) { range <<= 8; code = (code << 8) | (*buf++); }`;

enum IF_BIT_0(string p) = `ttt = *(`~p~`); `~NORMALIZE~` bound = (range >> kNumBitModelTotalBits) * cast(uint)ttt;`; // if (code < bound)

enum UPDATE_0(string p) = `range = bound; *(`~p~`) = cast(CLzmaProb)(ttt + ((kBitModelTotal - ttt) >> kNumMoveBits));`;
enum UPDATE_1(string p) = `range -= bound; code -= bound; *(`~p~`) = cast(CLzmaProb)(ttt - (ttt >> kNumMoveBits));`;

enum GET_BIT2(string p, string i, string A0, string A1) =
  IF_BIT_0!(p)~` if (code < bound)
  { `~UPDATE_0!(p)~` `~i~` = (`~i~` + `~i~`); `~A0~` } else
  { `~UPDATE_1!(p)~` `~i~` = (`~i~` + `~i~`) + 1; `~A1~` }
`;

enum TREE_GET_BIT(string probs, string i) = `{ `~GET_BIT2!(probs~` + `~i, i, `{}`, `{}`)~` }`;

enum REV_BIT(string p, string i, string A0, string A1) =
  IF_BIT_0!(p~` + `~i)~` if (code < bound)
  { `~UPDATE_0!(p~` + `~i)~` `~A0~` } else
  { `~UPDATE_1!(p~` + `~i)~` `~A1~` }
`;

enum REV_BIT_VAR(string p, string i, string m) = REV_BIT!(p, i, i~` += `~m~`; `~m~` += `~m~`;`, m~` += `~m~`; `~i~` += `~m~`;`);
enum REV_BIT_CONST(string p, string i, string m) = REV_BIT!(p, i, i~` += `~m~`;`, i~` += `~m~` * 2;`);
enum REV_BIT_LAST(string p, string i, string m) = REV_BIT!(p, i, i~` -= `~m~`;`, `{}`);

enum TREE_DECODE(string probs, string limit, string i) = `
  { `~i~` = 1; do { `~TREE_GET_BIT!(probs, i)~` } while (`~i~` < `~limit~`); `~i~` -= `~limit~`; }`;

/* #define LZMA_SIZE_OPT */

version(LZMA_SIZE_OPT) {
  enum TREE_6_DECODE(string probs, string i) = TREE_DECODE!(probs, "(1 << 6)", i);
} else {
enum TREE_6_DECODE(string probs, string i) = `
  { `~i~` = 1;`~
  TREE_GET_BIT!(probs, i)~
  TREE_GET_BIT!(probs, i)~
  TREE_GET_BIT!(probs, i)~
  TREE_GET_BIT!(probs, i)~
  TREE_GET_BIT!(probs, i)~
  TREE_GET_BIT!(probs, i)~`
  `~i~` -= 0x40; }
`;
}

enum NORMAL_LITER_DEC = TREE_GET_BIT!("prob", "symbol");

enum MATCHED_LITER_DEC = `
  matchByte += matchByte;
  bit = offs;
  offs &= matchByte;
  probLit = prob + (offs + bit + symbol);
  `~GET_BIT2!("probLit", "symbol", "offs ^= bit;", "{}");


enum NORMALIZE_CHECK = `if (range < kTopValue) { if (buf >= bufLimit) return DUMMY_INPUT_EOF; range <<= 8; code = (code << 8) | (*buf++); }`;

enum IF_BIT_0_CHECK(string p) = `ttt = *(`~p~`); `~NORMALIZE_CHECK~` bound = (range >> kNumBitModelTotalBits) * cast(uint)ttt;`; //if (code < bound)

enum UPDATE_0_CHECK = `range = bound;`;
enum UPDATE_1_CHECK = `range -= bound; code -= bound;`;

enum GET_BIT2_CHECK(string p, string i, string A0, string A1) =
  IF_BIT_0_CHECK!(p)~` if (code < bound)
  { `~UPDATE_0_CHECK~` `~i~` = (`~i~` + `~i~`); `~A0~` } else
  { `~UPDATE_1_CHECK~` `~i~` = (`~i~` + `~i~`) + 1; `~A1~` }
`;

enum GET_BIT_CHECK(string p, string i) = GET_BIT2_CHECK!(p, i, `{}` , `{}`);

enum TREE_DECODE_CHECK(string probs, string limit, string i) =
  `{ `~i~` = 1; do { `~GET_BIT_CHECK!(probs~` + `~i, i)~` } while (`~i~` < `~limit~`); `~i~` -= `~limit~`; }`;


enum REV_BIT_CHECK(string p, string i, string m) =
  IF_BIT_0_CHECK!(p~` + `~i)~` if (code < bound)
  { `~UPDATE_0_CHECK~` `~i~` += `~m~`; `~m~` += `~m~`; } else
  { `~UPDATE_1_CHECK~` `~m~` += `~m~`; `~i~` += `~m~`; }`;


enum kNumPosBitsMax = 4;
enum kNumPosStatesMax = (1 << kNumPosBitsMax);

enum kLenNumLowBits = 3;
enum kLenNumLowSymbols = (1 << kLenNumLowBits);
enum kLenNumHighBits = 8;
enum kLenNumHighSymbols = (1 << kLenNumHighBits);

enum LenLow = 0;
enum LenHigh = (LenLow + 2 * (kNumPosStatesMax << kLenNumLowBits));
enum kNumLenProbs = (LenHigh + kLenNumHighSymbols);

enum LenChoice = LenLow;
enum LenChoice2 = (LenLow + (1 << kLenNumLowBits));

enum kNumStates = 12;
enum kNumStates2 = 16;
enum kNumLitStates = 7;

enum kStartPosModelIndex = 4;
enum kEndPosModelIndex = 14;
enum kNumFullDistances = (1 << (kEndPosModelIndex >> 1));

enum kNumPosSlotBits = 6;
enum kNumLenToPosStates = 4;

enum kNumAlignBits = 4;
enum kAlignTableSize = (1 << kNumAlignBits);

enum kMatchMinLen = 2;
enum kMatchSpecLenStart = (kMatchMinLen + kLenNumLowSymbols * 2 + kLenNumHighSymbols);

enum kMatchSpecLen_Error_Data = (1 << 9);
enum kMatchSpecLen_Error_Fail = (kMatchSpecLen_Error_Data - 1);

/* External ASM code needs same CLzmaProb array layout. So don't change it. */

/* (probs_1664) is faster and better for code size at some platforms */
/*
#ifdef MY_CPU_X86_OR_AMD64
*/
enum kStartOffset = 1664;
//#define GET_PROBS p.probs_1664
/*
#define GET_PROBS p.probs + kStartOffset
#else
#define kStartOffset 0
#define GET_PROBS p.probs
#endif
*/

enum SpecPos = (-kStartOffset);
enum IsRep0Long = (SpecPos + kNumFullDistances);
enum RepLenCoder = (IsRep0Long + (kNumStates2 << kNumPosBitsMax));
enum LenCoder = (RepLenCoder + kNumLenProbs);
enum IsMatch = (LenCoder + kNumLenProbs);
enum Align = (IsMatch + (kNumStates2 << kNumPosBitsMax));
enum IsRep = (Align + kAlignTableSize);
enum IsRepG0 = (IsRep + kNumStates);
enum IsRepG1 = (IsRepG0 + kNumStates);
enum IsRepG2 = (IsRepG1 + kNumStates);
enum PosSlot = (IsRepG2 + kNumStates);
enum Literal = (PosSlot + (kNumLenToPosStates << kNumPosSlotBits));
enum NUM_BASE_PROBS = (Literal + kStartOffset);

static assert(Align == 0 || kStartOffset == 0, "Stop_Compiling_Bad_LZMA_kAlign");

static assert(NUM_BASE_PROBS == 1984, "Stop_Compiling_Bad_LZMA_PROBS");


enum LZMA_LIT_SIZE = 0x300;

enum LzmaProps_GetNumProbs(string p) = `(NUM_BASE_PROBS + (cast(uint)LZMA_LIT_SIZE << ((`~p~`).lc + (`~p~`).lp)))`;


enum CALC_POS_STATE(string processedPos, string pbMask) = `(((`~processedPos~`) & (`~pbMask~`)) << 4)`;
enum COMBINED_PS_STATE = `(posState + state)`;
enum GET_LEN_STATE = `(posState)`;

enum LZMA_DIC_MIN = (1 << 12);

/*
p.remainLen : shows status of LZMA decoder:
    < kMatchSpecLenStart  : the number of bytes to be copied with (p.rep0) offset
    = kMatchSpecLenStart  : the LZMA stream was finished with end mark
    = kMatchSpecLenStart + 1  : need init range coder
    = kMatchSpecLenStart + 2  : need init range coder and state
    = kMatchSpecLen_Error_Fail                : Internal Code Failure
    = kMatchSpecLen_Error_Data + [0 ... 273]  : LZMA Data Error
*/

/* ---------- LZMA_DECODE_REAL ---------- */
/*
LzmaDec_DecodeReal_3() can be implemented in external ASM file.
3 - is the code compatibility version of that function for check at link time.
*/

/*
LZMA_DECODE_REAL()
In:
  RangeCoder is normalized
  if (p.dicPos == limit)
  {
    LzmaDec_TryDummy() was called before to exclude LITERAL and MATCH-REP cases.
    So first symbol can be only MATCH-NON-REP. And if that MATCH-NON-REP symbol
    is not END_OF_PAYALOAD_MARKER, then the function doesn't write any byte to dictionary,
    the function returns SZ_OK, and the caller can use (p.remainLen) and (p.reps[0]) later.
  }

Processing:
  The first LZMA symbol will be decoded in any case.
  All main checks for limits are at the end of main loop,
  It decodes additional LZMA-symbols while (p.buf < bufLimit && dicPos < limit),
  RangeCoder is still without last normalization when (p.buf < bufLimit) is being checked.
  But if (p.buf < bufLimit), the caller provided at least (LZMA_REQUIRED_INPUT_MAX + 1) bytes for
  next iteration  before limit (bufLimit + LZMA_REQUIRED_INPUT_MAX),
  that is enough for worst case LZMA symbol with one additional RangeCoder normalization for one bit.
  So that function never reads bufLimit [LZMA_REQUIRED_INPUT_MAX] byte.

Out:
  RangeCoder is normalized
  Result:
    SZ_OK - OK
      p.remainLen:
        < kMatchSpecLenStart : the number of bytes to be copied with (p.reps[0]) offset
        = kMatchSpecLenStart : the LZMA stream was finished with end mark

    SZ_ERROR_DATA - error, when the MATCH-Symbol refers out of dictionary
      p.remainLen : undefined
      p.reps[*]    : undefined
*/


private int LZMA_DECODE_REAL (CLzmaDec *p, usize limit, const(ubyte)* bufLimit) @nogc {
  CLzmaProb *probs = /*GET_PROBS*/p.probs_1664;
  uint state = cast(uint)p.state;
  uint rep0 = p.reps[0], rep1 = p.reps[1], rep2 = p.reps[2], rep3 = p.reps[3];
  uint pbMask = (cast(uint)1 << (p.prop.pb)) - 1;
  uint lc = p.prop.lc;
  uint lpMask = (cast(uint)0x100 << p.prop.lp) - (cast(uint)0x100 >> lc);

  ubyte *dic = p.dic;
  usize dicBufSize = p.dicBufSize;
  usize dicPos = p.dicPos;

  uint processedPos = p.processedPos;
  uint checkDicSize = p.checkDicSize;
  uint len = 0;

  const(ubyte)* buf = p.buf;
  uint range = p.range;
  uint code = p.code;

  do
  {
    CLzmaProb *prob;
    uint bound;
    uint ttt;
    uint posState = mixin(CALC_POS_STATE!("processedPos", "pbMask"));

    prob = probs + IsMatch + mixin(COMBINED_PS_STATE);
    mixin(IF_BIT_0!"prob");
    if (code < bound)
    {
      uint symbol;
      mixin(UPDATE_0!"prob");
      prob = probs + Literal;
      if (processedPos != 0 || checkDicSize != 0)
        prob += cast(uint)3 * ((((processedPos << 8) + dic[(dicPos == 0 ? dicBufSize : dicPos) - 1]) & lpMask) << lc);
      processedPos++;

      if (state < kNumLitStates)
      {
        state -= (state < 4) ? state : 3;
        symbol = 1;
        version(LZMA_SIZE_OPT) {
          do { mixin(NORMAL_LITER_DEC); } while (symbol < 0x100);
        } else {
          mixin(NORMAL_LITER_DEC);
          mixin(NORMAL_LITER_DEC);
          mixin(NORMAL_LITER_DEC);
          mixin(NORMAL_LITER_DEC);
          mixin(NORMAL_LITER_DEC);
          mixin(NORMAL_LITER_DEC);
          mixin(NORMAL_LITER_DEC);
          mixin(NORMAL_LITER_DEC);
        }
      }
      else
      {
        uint matchByte = dic[dicPos - rep0 + (dicPos < rep0 ? dicBufSize : 0)];
        uint offs = 0x100;
        state -= (state < 10) ? 3 : 6;
        symbol = 1;
        version(LZMA_SIZE_OPT) {
          do
          {
            uint bit;
            CLzmaProb *probLit;
            mixin(MATCHED_LITER_DEC);
          }
          while (symbol < 0x100);
        } else {
        {
          uint bit;
          CLzmaProb *probLit;
          mixin(MATCHED_LITER_DEC);
          mixin(MATCHED_LITER_DEC);
          mixin(MATCHED_LITER_DEC);
          mixin(MATCHED_LITER_DEC);
          mixin(MATCHED_LITER_DEC);
          mixin(MATCHED_LITER_DEC);
          mixin(MATCHED_LITER_DEC);
          mixin(MATCHED_LITER_DEC);
        }
        }
      }

      dic[dicPos++] = cast(ubyte)symbol;
      continue;
    }

    {
      mixin(UPDATE_1!"prob");
      prob = probs + IsRep + state;
      mixin(IF_BIT_0!"prob");
      if (code < bound)
      {
        mixin(UPDATE_0!"prob");
        state += kNumStates;
        prob = probs + LenCoder;
      }
      else
      {
        mixin(UPDATE_1!"prob");
        prob = probs + IsRepG0 + state;
        mixin(IF_BIT_0!"prob");
        if (code < bound)
        {
          mixin(UPDATE_0!"prob");
          prob = probs + IsRep0Long + mixin(COMBINED_PS_STATE);
          mixin(IF_BIT_0!"prob");
          if (code < bound)
          {
            mixin(UPDATE_0!"prob");

            // that case was checked before with kBadRepCode
            // if (checkDicSize == 0 && processedPos == 0) { len = kMatchSpecLen_Error_Data + 1; break; }
            // The caller doesn't allow (dicPos == limit) case here
            // so we don't need the following check:
            // if (dicPos == limit) { state = state < kNumLitStates ? 9 : 11; len = 1; break; }

            dic[dicPos] = dic[dicPos - rep0 + (dicPos < rep0 ? dicBufSize : 0)];
            dicPos++;
            processedPos++;
            state = state < kNumLitStates ? 9 : 11;
            continue;
          }
          mixin(UPDATE_1!"prob");
        }
        else
        {
          uint distance;
          mixin(UPDATE_1!"prob");
          prob = probs + IsRepG1 + state;
          mixin(IF_BIT_0!"prob");
          if (code < bound)
          {
            mixin(UPDATE_0!"prob");
            distance = rep1;
          }
          else
          {
            mixin(UPDATE_1!"prob");
            prob = probs + IsRepG2 + state;
            mixin(IF_BIT_0!"prob");
            if (code < bound)
            {
              mixin(UPDATE_0!"prob");
              distance = rep2;
            }
            else
            {
              mixin(UPDATE_1!"prob");
              distance = rep3;
              rep3 = rep2;
            }
            rep2 = rep1;
          }
          rep1 = rep0;
          rep0 = distance;
        }
        state = state < kNumLitStates ? 8 : 11;
        prob = probs + RepLenCoder;
      }

      version(LZMA_SIZE_OPT) {
        {
          uint lim, offset;
          CLzmaProb *probLen = prob + LenChoice;
          mixin(IF_BIT_0!"probLen");
          if (code < bound)
          {
            mixin(UPDATE_0!"probLen");
            probLen = prob + LenLow + mixin(GET_LEN_STATE);
            offset = 0;
            lim = (1 << kLenNumLowBits);
          }
          else
          {
            mixin(UPDATE_1!"probLen");
            probLen = prob + LenChoice2;
            mixin(IF_BIT_0!"probLen");
            if (code < bound)
            {
              mixin(UPDATE_0!"probLen");
              probLen = prob + LenLow + mixin(GET_LEN_STATE) + (1 << kLenNumLowBits);
              offset = kLenNumLowSymbols;
              lim = (1 << kLenNumLowBits);
            }
            else
            {
              mixin(UPDATE_1!"probLen");
              probLen = prob + LenHigh;
              offset = kLenNumLowSymbols * 2;
              lim = (1 << kLenNumHighBits);
            }
          }
          mixin(TREE_DECODE!("probLen", "lim", "len"));
          len += offset;
        }
      } else {
        {
          CLzmaProb *probLen = prob + LenChoice;
          mixin(IF_BIT_0!"probLen");
          if (code < bound)
          {
            mixin(UPDATE_0!"probLen");
            probLen = prob + LenLow + mixin(GET_LEN_STATE);
            len = 1;
            mixin(TREE_GET_BIT!("probLen", "len"));
            mixin(TREE_GET_BIT!("probLen", "len"));
            mixin(TREE_GET_BIT!("probLen", "len"));
            len -= 8;
          }
          else
          {
            mixin(UPDATE_1!"probLen");
            probLen = prob + LenChoice2;
            mixin(IF_BIT_0!"probLen");
            if (code < bound)
            {
              mixin(UPDATE_0!"probLen");
              probLen = prob + LenLow + mixin(GET_LEN_STATE) + (1 << kLenNumLowBits);
              len = 1;
              mixin(TREE_GET_BIT!("probLen", "len"));
              mixin(TREE_GET_BIT!("probLen", "len"));
              mixin(TREE_GET_BIT!("probLen", "len"));
            }
            else
            {
              mixin(UPDATE_1!"probLen");
              probLen = prob + LenHigh;
              mixin(TREE_DECODE!("probLen", "(1 << kLenNumHighBits)", "len"));
              len += kLenNumLowSymbols * 2;
            }
          }
        }
      }

      if (state >= kNumStates)
      {
        uint distance;
        prob = probs + PosSlot +
            ((len < kNumLenToPosStates ? len : kNumLenToPosStates - 1) << kNumPosSlotBits);
        mixin(TREE_6_DECODE!("prob", "distance"));
        if (distance >= kStartPosModelIndex)
        {
          uint posSlot = cast(uint)distance;
          uint numDirectBits = cast(uint)(((distance >> 1) - 1));
          distance = (2 | (distance & 1));
          if (posSlot < kEndPosModelIndex)
          {
            distance <<= numDirectBits;
            prob = probs + SpecPos;
            {
              uint m = 1;
              distance++;
              do
              {
                mixin(REV_BIT_VAR!("prob", "distance", "m"));
              }
              while (--numDirectBits);
              distance -= m;
            }
          }
          else
          {
            numDirectBits -= kNumAlignBits;
            do
            {
              mixin(NORMALIZE);
              range >>= 1;

              {
                uint t;
                code -= range;
                t = (0 - (cast(uint)code >> 31)); /* (uint)((int32_t)code >> 31) */
                distance = (distance << 1) + (t + 1);
                code += range & t;
              }
              /*
              distance <<= 1;
              if (code >= range)
              {
                code -= range;
                distance |= 1;
              }
              */
            }
            while (--numDirectBits);
            prob = probs + Align;
            distance <<= kNumAlignBits;
            {
              uint i = 1;
              mixin(REV_BIT_CONST!("prob", "i", "1"));
              mixin(REV_BIT_CONST!("prob", "i", "2"));
              mixin(REV_BIT_CONST!("prob", "i", "4"));
              mixin(REV_BIT_LAST!("prob", "i", "8"));
              distance |= i;
            }
            if (distance == cast(uint)0xFFFFFFFFU)
            {
              len = kMatchSpecLenStart;
              state -= kNumStates;
              break;
            }
          }
        }

        rep3 = rep2;
        rep2 = rep1;
        rep1 = rep0;
        rep0 = distance + 1;
        state = (state < kNumStates + kNumLitStates) ? kNumLitStates : kNumLitStates + 3;
        if (distance >= (checkDicSize == 0 ? processedPos: checkDicSize))
        {
          len += kMatchSpecLen_Error_Data + kMatchMinLen;
          // len = kMatchSpecLen_Error_Data;
          // len += kMatchMinLen;
          break;
        }
      }

      len += kMatchMinLen;

      {
        usize rem;
        uint curLen;
        usize pos;

        if ((rem = limit - dicPos) == 0)
        {
          /*
          We stop decoding and return SZ_OK, and we can resume decoding later.
          Any error conditions can be tested later in caller code.
          For more strict mode we can stop decoding with error
          // len += kMatchSpecLen_Error_Data;
          */
          break;
        }

        curLen = ((rem < len) ? cast(uint)rem : len);
        pos = dicPos - rep0 + (dicPos < rep0 ? dicBufSize : 0);

        processedPos += cast(uint)curLen;

        len -= curLen;
        if (curLen <= dicBufSize - pos)
        {
          ubyte *dest = dic + dicPos;
          ptrdiff_t src = cast(ptrdiff_t)pos - cast(ptrdiff_t)dicPos;
          const(ubyte)* lim = dest + curLen;
          dicPos += cast(usize)curLen;
          do
            *(dest) = cast(ubyte)*(dest + src);
          while (++dest != lim);
        }
        else
        {
          do
          {
            dic[dicPos++] = dic[pos];
            if (++pos == dicBufSize)
              pos = 0;
          }
          while (--curLen != 0);
        }
      }
    }
  }
  while (dicPos < limit && buf < bufLimit);

  mixin(NORMALIZE);

  p.buf = buf;
  p.range = range;
  p.code = code;
  p.remainLen = cast(uint)len; // & (kMatchSpecLen_Error_Data - 1); // we can write real length for error matches too.
  p.dicPos = dicPos;
  p.processedPos = processedPos;
  p.reps[0] = rep0;
  p.reps[1] = rep1;
  p.reps[2] = rep2;
  p.reps[3] = rep3;
  p.state = cast(uint)state;
  if (len >= kMatchSpecLen_Error_Data)
    return SZ_ERROR_DATA;
  return SZ_OK;
}


private void LzmaDec_WriteRem (CLzmaDec* p, usize limit) @nogc {
  uint len = cast(uint)p.remainLen;
  if (len == 0 /* || len >= kMatchSpecLenStart */)
    return;
  {
    usize dicPos = p.dicPos;
    ubyte *dic;
    usize dicBufSize;
    usize rep0;   /* we use usize to avoid the BUG of VC14 for AMD64 */
    {
      usize rem = limit - dicPos;
      if (rem < len)
      {
        len = cast(uint)(rem);
        if (len == 0)
          return;
      }
    }

    if (p.checkDicSize == 0 && p.prop.dicSize - p.processedPos <= len)
      p.checkDicSize = p.prop.dicSize;

    p.processedPos += cast(uint)len;
    p.remainLen -= cast(uint)len;
    dic = p.dic;
    rep0 = p.reps[0];
    dicBufSize = p.dicBufSize;
    do
    {
      dic[dicPos] = dic[dicPos - rep0 + (dicPos < rep0 ? dicBufSize : 0)];
      dicPos++;
    }
    while (--len);
    p.dicPos = dicPos;
  }
}


/*
At staring of new stream we have one of the following symbols:
  - Literal        - is allowed
  - Non-Rep-Match  - is allowed only if it's end marker symbol
  - Rep-Match      - is not allowed
We use early check of (RangeCoder:Code) over kBadRepCode to simplify main decoding code
*/

enum kRange0 = 0xFFFFFFFF;
enum kBound0 = ((kRange0 >> kNumBitModelTotalBits) << (kNumBitModelTotalBits - 1));
enum kBadRepCode = (kBound0 + (((kRange0 - kBound0) >> kNumBitModelTotalBits) << (kNumBitModelTotalBits - 1)));
static assert(kBadRepCode == (0xC0000000 - 0x400), "Stop_Compiling_Bad_LZMA_Check");


/*
LzmaDec_DecodeReal2():
  It calls LZMA_DECODE_REAL() and it adjusts limit according (p.checkDicSize).

We correct (p.checkDicSize) after LZMA_DECODE_REAL() and in LzmaDec_WriteRem(),
and we support the following state of (p.checkDicSize):
  if (total_processed < p.prop.dicSize) then
  {
    (total_processed == p.processedPos)
    (p.checkDicSize == 0)
  }
  else
    (p.checkDicSize == p.prop.dicSize)
*/

private int LzmaDec_DecodeReal2 (CLzmaDec *p, usize limit, const(ubyte)* bufLimit) @nogc {
  if (p.checkDicSize == 0)
  {
    uint rem = p.prop.dicSize - p.processedPos;
    if (limit - p.dicPos > rem)
      limit = p.dicPos + rem;
  }
  {
    int res = LZMA_DECODE_REAL(p, limit, bufLimit);
    if (p.checkDicSize == 0 && p.processedPos >= p.prop.dicSize)
      p.checkDicSize = p.prop.dicSize;
    return res;
  }
}



alias ELzmaDummy = int;
enum
{
  DUMMY_INPUT_EOF, /* need more input data */
  DUMMY_LIT,
  DUMMY_MATCH,
  DUMMY_REP
}


enum IS_DUMMY_END_MARKER_POSSIBLE(string dummyRes) = `((`~dummyRes~`) == DUMMY_MATCH)`;

private ELzmaDummy LzmaDec_TryDummy (const(CLzmaDec)* p, const(ubyte)* buf, const(ubyte)** bufOut) @nogc {
  uint range = p.range;
  uint code = p.code;
  const(ubyte)* bufLimit = *bufOut;
  const(CLzmaProb)* probs = /*GET_PROBS*/p.probs_1664;
  uint state = cast(uint)p.state;
  ELzmaDummy res;

  for (;;)
  {
    const(CLzmaProb)* prob;
    uint bound;
    uint ttt;
    uint posState = mixin(CALC_POS_STATE!("p.processedPos", "(cast(uint)1 << p.prop.pb) - 1"));

    prob = probs + IsMatch + mixin(COMBINED_PS_STATE);
    mixin(IF_BIT_0_CHECK!"prob");
    if (code < bound)
    {
      mixin(UPDATE_0_CHECK);

      prob = probs + Literal;
      if (p.checkDicSize != 0 || p.processedPos != 0)
        prob += (cast(uint)LZMA_LIT_SIZE *
            ((((p.processedPos) & ((cast(uint)1 << (p.prop.lp)) - 1)) << p.prop.lc) +
            (cast(uint)p.dic[(p.dicPos == 0 ? p.dicBufSize : p.dicPos) - 1] >> (8 - p.prop.lc))));

      if (state < kNumLitStates)
      {
        uint symbol = 1;
        do { mixin(GET_BIT_CHECK!("prob + symbol", "symbol")); } while (symbol < 0x100);
      }
      else
      {
        uint matchByte = p.dic[p.dicPos - p.reps[0] +
            (p.dicPos < p.reps[0] ? p.dicBufSize : 0)];
        uint offs = 0x100;
        uint symbol = 1;
        do
        {
          uint bit;
          const(CLzmaProb)* probLit;
          matchByte += matchByte;
          bit = offs;
          offs &= matchByte;
          probLit = prob + (offs + bit + symbol);
          mixin(GET_BIT2_CHECK!("probLit", "symbol", "offs ^= bit;" , "{}"));
        }
        while (symbol < 0x100);
      }
      res = DUMMY_LIT;
    }
    else
    {
      uint len;
      mixin(UPDATE_1_CHECK);

      prob = probs + IsRep + state;
      mixin(IF_BIT_0_CHECK!"prob");
      if (code < bound)
      {
        mixin(UPDATE_0_CHECK);
        state = 0;
        prob = probs + LenCoder;
        res = DUMMY_MATCH;
      }
      else
      {
        mixin(UPDATE_1_CHECK);
        res = DUMMY_REP;
        prob = probs + IsRepG0 + state;
        mixin(IF_BIT_0_CHECK!"prob");
        if (code < bound)
        {
          mixin(UPDATE_0_CHECK);
          prob = probs + IsRep0Long + mixin(COMBINED_PS_STATE);
          mixin(IF_BIT_0_CHECK!"prob");
          if (code < bound)
          {
            mixin(UPDATE_0_CHECK);
            break;
          }
          else
          {
            mixin(UPDATE_1_CHECK);
          }
        }
        else
        {
          mixin(UPDATE_1_CHECK);
          prob = probs + IsRepG1 + state;
          mixin(IF_BIT_0_CHECK!"prob");
          if (code < bound)
          {
            mixin(UPDATE_0_CHECK);
          }
          else
          {
            mixin(UPDATE_1_CHECK);
            prob = probs + IsRepG2 + state;
            mixin(IF_BIT_0_CHECK!"prob");
            if (code < bound)
            {
              mixin(UPDATE_0_CHECK);
            }
            else
            {
              mixin(UPDATE_1_CHECK);
            }
          }
        }
        state = kNumStates;
        prob = probs + RepLenCoder;
      }
      {
        uint limit, offset;
        const(CLzmaProb)* probLen = prob + LenChoice;
        mixin(IF_BIT_0_CHECK!"probLen");
        if (code < bound)
        {
          mixin(UPDATE_0_CHECK);
          probLen = prob + LenLow + mixin(GET_LEN_STATE);
          offset = 0;
          limit = 1 << kLenNumLowBits;
        }
        else
        {
          mixin(UPDATE_1_CHECK);
          probLen = prob + LenChoice2;
          mixin(IF_BIT_0_CHECK!"probLen");
          if (code < bound)
          {
            mixin(UPDATE_0_CHECK);
            probLen = prob + LenLow + mixin(GET_LEN_STATE) + (1 << kLenNumLowBits);
            offset = kLenNumLowSymbols;
            limit = 1 << kLenNumLowBits;
          }
          else
          {
            mixin(UPDATE_1_CHECK);
            probLen = prob + LenHigh;
            offset = kLenNumLowSymbols * 2;
            limit = 1 << kLenNumHighBits;
          }
        }
        mixin(TREE_DECODE_CHECK!("probLen", "limit", "len"));
        len += offset;
      }

      if (state < 4)
      {
        uint posSlot;
        prob = probs + PosSlot +
            ((len < kNumLenToPosStates - 1 ? len : kNumLenToPosStates - 1) <<
            kNumPosSlotBits);
        mixin(TREE_DECODE_CHECK!("prob", "1 << kNumPosSlotBits", "posSlot"));
        if (posSlot >= kStartPosModelIndex)
        {
          uint numDirectBits = ((posSlot >> 1) - 1);

          if (posSlot < kEndPosModelIndex)
          {
            prob = probs + SpecPos + ((2 | (posSlot & 1)) << numDirectBits);
          }
          else
          {
            numDirectBits -= kNumAlignBits;
            do
            {
              mixin(NORMALIZE_CHECK);
              range >>= 1;
              code -= range & (((code - range) >> 31) - 1);
              /* if (code >= range) code -= range; */
            }
            while (--numDirectBits);
            prob = probs + Align;
            numDirectBits = kNumAlignBits;
          }
          {
            uint i = 1;
            uint m = 1;
            do
            {
              mixin(REV_BIT_CHECK!("prob", "i", "m"));
            }
            while (--numDirectBits);
          }
        }
      }
    }
    break;
  }
  mixin(NORMALIZE_CHECK);

  *bufOut = buf;
  return res;
}


private void LzmaDec_InitDicAndState (CLzmaDec *p, /*BoolInt*/int initDic, /*BoolInt*/int initState) @nogc {
  p.remainLen = kMatchSpecLenStart + 1;
  p.tempBufSize = 0;

  if (initDic)
  {
    p.processedPos = 0;
    p.checkDicSize = 0;
    p.remainLen = kMatchSpecLenStart + 2;
  }
  if (initState)
    p.remainLen = kMatchSpecLenStart + 2;
}


//==========================================================================
//
//  LzmaDec_Init
//
//==========================================================================
public void LzmaDec_Init (CLzmaDec* p) @nogc {
  p.dicPos = 0;
  LzmaDec_InitDicAndState(p, 1/*True*/, 1/*True*/);
}


/*
LZMA supports optional end_marker.
So the decoder can lookahead for one additional LZMA-Symbol to check end_marker.
That additional LZMA-Symbol can require up to LZMA_REQUIRED_INPUT_MAX bytes in input stream.
When the decoder reaches dicLimit, it looks (finishMode) parameter:
  if (finishMode == LZMA_FINISH_ANY), the decoder doesn't lookahead
  if (finishMode != LZMA_FINISH_ANY), the decoder lookahead, if end_marker is possible for current position

When the decoder lookahead, and the lookahead symbol is not end_marker, we have two ways:
  1) Strict mode (default) : the decoder returns SZ_ERROR_DATA.
  2) The relaxed mode (alternative mode) : we could return SZ_OK, and the caller
     must check (status) value. The caller can show the error,
     if the end of stream is expected, and the (status) is noit
     LZMA_STATUS_FINISHED_WITH_MARK or LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK.
*/


enum RETURN__NOT_FINISHED__FOR_FINISH = q{
  *status = LZMA_STATUS_NOT_FINISHED;
  return SZ_ERROR_DATA; /* for strict mode */
  /* return SZ_OK;*/ /* for relaxed mode */
};


//==========================================================================
//
//  LzmaDec_DecodeToDic
//
//==========================================================================
public SRes LzmaDec_DecodeToDic (CLzmaDec* p, usize dicLimit, const(ubyte)* src, usize *srcLen,
                                 ELzmaFinishMode finishMode, ELzmaStatus *status) @nogc
{
  usize inSize = *srcLen;
  (*srcLen) = 0;
  *status = LZMA_STATUS_NOT_SPECIFIED;

  if (p.remainLen > kMatchSpecLenStart)
  {
    if (p.remainLen > kMatchSpecLenStart + 2)
      return p.remainLen == kMatchSpecLen_Error_Fail ? SZ_ERROR_FAIL : SZ_ERROR_DATA;

    for (; inSize > 0 && p.tempBufSize < RC_INIT_SIZE; (*srcLen)++, inSize--)
      p.tempBuf[p.tempBufSize++] = *src++;
    if (p.tempBufSize != 0 && p.tempBuf[0] != 0)
      return SZ_ERROR_DATA;
    if (p.tempBufSize < RC_INIT_SIZE)
    {
      *status = LZMA_STATUS_NEEDS_MORE_INPUT;
      return SZ_OK;
    }
    p.code =
        (cast(uint)p.tempBuf[1] << 24)
      | (cast(uint)p.tempBuf[2] << 16)
      | (cast(uint)p.tempBuf[3] << 8)
      | (cast(uint)p.tempBuf[4]);

    if (p.checkDicSize == 0
        && p.processedPos == 0
        && p.code >= kBadRepCode)
      return SZ_ERROR_DATA;

    p.range = 0xFFFFFFFF;
    p.tempBufSize = 0;

    if (p.remainLen > kMatchSpecLenStart + 1)
    {
      usize numProbs = mixin(LzmaProps_GetNumProbs!"&p.prop");
      usize i;
      CLzmaProb *probs = p.probs;
      for (i = 0; i < numProbs; i++)
        probs[i] = kBitModelTotal >> 1;
      p.reps[0] = p.reps[1] = p.reps[2] = p.reps[3] = 1;
      p.state = 0;
    }

    p.remainLen = 0;
  }

  for (;;)
  {
    if (p.remainLen == kMatchSpecLenStart)
    {
      if (p.code != 0)
        return SZ_ERROR_DATA;
      *status = LZMA_STATUS_FINISHED_WITH_MARK;
      return SZ_OK;
    }

    LzmaDec_WriteRem(p, dicLimit);

    {
      // (p.remainLen == 0 || p.dicPos == dicLimit)

      int checkEndMarkNow = 0;

      if (p.dicPos >= dicLimit)
      {
        if (p.remainLen == 0 && p.code == 0)
        {
          *status = LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK;
          return SZ_OK;
        }
        if (finishMode == LZMA_FINISH_ANY)
        {
          *status = LZMA_STATUS_NOT_FINISHED;
          return SZ_OK;
        }
        if (p.remainLen != 0)
        {
          mixin(RETURN__NOT_FINISHED__FOR_FINISH);
        }
        checkEndMarkNow = 1;
      }

      // (p.remainLen == 0)

      if (p.tempBufSize == 0)
      {
        const(ubyte)* bufLimit;
        int dummyProcessed = -1;

        if (inSize < LZMA_REQUIRED_INPUT_MAX || checkEndMarkNow)
        {
          const(ubyte)* bufOut = src + inSize;

          ELzmaDummy dummyRes = LzmaDec_TryDummy(p, src, &bufOut);

          if (dummyRes == DUMMY_INPUT_EOF)
          {
            usize i;
            if (inSize >= LZMA_REQUIRED_INPUT_MAX)
              break;
            (*srcLen) += inSize;
            p.tempBufSize = cast(uint)inSize;
            for (i = 0; i < inSize; i++)
              p.tempBuf[i] = src[i];
            *status = LZMA_STATUS_NEEDS_MORE_INPUT;
            return SZ_OK;
          }

          dummyProcessed = cast(int)(bufOut - src);
          if (cast(uint)dummyProcessed > LZMA_REQUIRED_INPUT_MAX)
            break;

          if (checkEndMarkNow && !mixin(IS_DUMMY_END_MARKER_POSSIBLE!"dummyRes"))
          {
            uint i;
            (*srcLen) += cast(uint)dummyProcessed;
            p.tempBufSize = cast(uint)dummyProcessed;
            for (i = 0; i < cast(uint)dummyProcessed; i++)
              p.tempBuf[i] = src[i];
            // p.remainLen = kMatchSpecLen_Error_Data;
            mixin(RETURN__NOT_FINISHED__FOR_FINISH);
          }

          bufLimit = src;
          // we will decode only one iteration
        }
        else
          bufLimit = src + inSize - LZMA_REQUIRED_INPUT_MAX;

        p.buf = src;

        {
          int res = LzmaDec_DecodeReal2(p, dicLimit, bufLimit);

          usize processed = cast(usize)(p.buf - src);

          if (dummyProcessed < 0)
          {
            if (processed > inSize)
              break;
          }
          else if (cast(uint)dummyProcessed != processed)
            break;

          src += processed;
          inSize -= processed;
          (*srcLen) += processed;

          if (res != SZ_OK)
          {
            p.remainLen = kMatchSpecLen_Error_Data;
            return SZ_ERROR_DATA;
          }
        }
        continue;
      }

      {
        // we have some data in (p.tempBuf)
        // in strict mode: tempBufSize is not enough for one Symbol decoding.
        // in relaxed mode: tempBufSize not larger than required for one Symbol decoding.

        uint rem = p.tempBufSize;
        uint ahead = 0;
        int dummyProcessed = -1;

        while (rem < LZMA_REQUIRED_INPUT_MAX && ahead < inSize)
          p.tempBuf[rem++] = src[ahead++];

        // ahead - the size of new data copied from (src) to (p.tempBuf)
        // rem   - the size of temp buffer including new data from (src)

        if (rem < LZMA_REQUIRED_INPUT_MAX || checkEndMarkNow)
        {
          const(ubyte)* bufOut = p.tempBuf.ptr + rem;

          ELzmaDummy dummyRes = LzmaDec_TryDummy(p, p.tempBuf.ptr, &bufOut);

          if (dummyRes == DUMMY_INPUT_EOF)
          {
            if (rem >= LZMA_REQUIRED_INPUT_MAX)
              break;
            p.tempBufSize = rem;
            (*srcLen) += cast(usize)ahead;
            *status = LZMA_STATUS_NEEDS_MORE_INPUT;
            return SZ_OK;
          }

          dummyProcessed = cast(int)(bufOut - p.tempBuf.ptr);

          if (cast(uint)dummyProcessed < p.tempBufSize)
            break;

          if (checkEndMarkNow && !mixin(IS_DUMMY_END_MARKER_POSSIBLE!"dummyRes"))
          {
            (*srcLen) += cast(uint)dummyProcessed - p.tempBufSize;
            p.tempBufSize = cast(uint)dummyProcessed;
            // p.remainLen = kMatchSpecLen_Error_Data;
            mixin(RETURN__NOT_FINISHED__FOR_FINISH);
          }
        }

        p.buf = p.tempBuf.ptr;

        {
          // we decode one symbol from (p.tempBuf) here, so the (bufLimit) is equal to (p.buf)
          int res = LzmaDec_DecodeReal2(p, dicLimit, p.buf);

          usize processed = cast(usize)(p.buf - p.tempBuf.ptr);
          rem = p.tempBufSize;

          if (dummyProcessed < 0)
          {
            if (processed > LZMA_REQUIRED_INPUT_MAX)
              break;
            if (processed < rem)
              break;
          }
          else if (cast(uint)dummyProcessed != processed)
            break;

          processed -= rem;

          src += processed;
          inSize -= processed;
          (*srcLen) += processed;
          p.tempBufSize = 0;

          if (res != SZ_OK)
          {
            p.remainLen = kMatchSpecLen_Error_Data;
            return SZ_ERROR_DATA;
          }
        }
      }
    }
  }

  /*  Some unexpected error: internal error of code, memory corruption or hardware failure */
  p.remainLen = kMatchSpecLen_Error_Fail;
  return SZ_ERROR_FAIL;
}



//==========================================================================
//
//  LzmaDec_DecodeToBuf
//
//==========================================================================
public SRes LzmaDec_DecodeToBuf (CLzmaDec* p, ubyte* dest, usize* destLen, const(ubyte)* src, usize *srcLen,
                                 ELzmaFinishMode finishMode, ELzmaStatus *status) @nogc
{
  usize outSize = *destLen;
  usize inSize = *srcLen;
  *srcLen = *destLen = 0;
  for (;;)
  {
    usize inSizeCur = inSize, outSizeCur, dicPos;
    ELzmaFinishMode curFinishMode;
    SRes res;
    if (p.dicPos == p.dicBufSize)
      p.dicPos = 0;
    dicPos = p.dicPos;
    if (outSize > p.dicBufSize - dicPos)
    {
      outSizeCur = p.dicBufSize;
      curFinishMode = LZMA_FINISH_ANY;
    }
    else
    {
      outSizeCur = dicPos + outSize;
      curFinishMode = finishMode;
    }

    res = LzmaDec_DecodeToDic(p, outSizeCur, src, &inSizeCur, curFinishMode, status);
    src += inSizeCur;
    inSize -= inSizeCur;
    *srcLen += inSizeCur;
    outSizeCur = p.dicPos - dicPos;
    import core.stdc.string : memcpy;
    memcpy(dest, p.dic + dicPos, outSizeCur);
    dest += outSizeCur;
    outSize -= outSizeCur;
    *destLen += outSizeCur;
    if (res != 0)
      return res;
    if (outSizeCur == 0 || outSize == 0)
      return SZ_OK;
  }
}


private void LzmaDec_FreeDict(CLzmaDec *p, ISzAllocPtr alloc) {
  ISzAlloc_Free(alloc, p.dic);
  p.dic = null;
}


//==========================================================================
//
//  LzmaDec_FreeProbs
//
//==========================================================================
public void LzmaDec_FreeProbs (CLzmaDec *p, ISzAllocPtr alloc) {
  ISzAlloc_Free(alloc, p.probs);
  p.probs = null;
}

//==========================================================================
//
//  LzmaDec_Free
//
//==========================================================================
public void LzmaDec_Free (CLzmaDec* p, ISzAllocPtr alloc) {
  LzmaDec_FreeProbs(p, alloc);
  LzmaDec_FreeDict(p, alloc);
}


//==========================================================================
//
//  LzmaProps_Decode
//
//==========================================================================
public SRes LzmaProps_Decode (CLzmaProps* p, const(ubyte)* data, uint size) @nogc {
  uint dicSize;
  ubyte d;

  if (size < LZMA_PROPS_SIZE)
    return SZ_ERROR_UNSUPPORTED;
  else
    dicSize = data[1] | (cast(uint)data[2] << 8) | (cast(uint)data[3] << 16) | (cast(uint)data[4] << 24);

  if (dicSize < LZMA_DIC_MIN)
    dicSize = LZMA_DIC_MIN;
  p.dicSize = dicSize;

  d = data[0];
  if (d >= (9 * 5 * 5))
    return SZ_ERROR_UNSUPPORTED;

  p.lc = cast(ubyte)(d % 9);
  d /= 9;
  p.pb = cast(ubyte)(d / 5);
  p.lp = cast(ubyte)(d % 5);

  return SZ_OK;
}

private SRes LzmaDec_AllocateProbs2(CLzmaDec *p, const(CLzmaProps)* propNew, ISzAllocPtr alloc)
{
  uint numProbs = mixin(LzmaProps_GetNumProbs!"propNew");
  if (!p.probs || numProbs != p.numProbs)
  {
    LzmaDec_FreeProbs(p, alloc);
    p.probs = cast(CLzmaProb *)ISzAlloc_Alloc(alloc, numProbs * CLzmaProb.sizeof);
    if (!p.probs)
      return SZ_ERROR_MEM;
    p.probs_1664 = p.probs + 1664;
    p.numProbs = numProbs;
  }
  return SZ_OK;
}


//==========================================================================
//
//  LzmaDec_AllocateProbs
//
//==========================================================================
public SRes LzmaDec_AllocateProbs (CLzmaDec* p, const(ubyte)* props, uint propsSize, ISzAllocPtr alloc) {
  CLzmaProps propNew;
  int rinres = LzmaProps_Decode(&propNew, props, propsSize);
  if (rinres != 0) return rinres;
  rinres = LzmaDec_AllocateProbs2(p, &propNew, alloc);
  if (rinres != 0) return rinres;
  p.prop = propNew;
  return SZ_OK;
}


//==========================================================================
//
//  LzmaDec_Allocate
//
//==========================================================================
public SRes LzmaDec_Allocate (CLzmaDec* p, const(ubyte)* props, uint propsSize, ISzAllocPtr alloc) {
  CLzmaProps propNew;
  usize dicBufSize;
  int rinres = LzmaProps_Decode(&propNew, props, propsSize);
  if (rinres != 0) return rinres;
  rinres = LzmaDec_AllocateProbs2(p, &propNew, alloc);
  if (rinres != 0) return rinres;

  {
    uint dictSize = propNew.dicSize;
    usize mask = (cast(uint)1 << 12) - 1;
         if (dictSize >= (cast(uint)1 << 30)) mask = (cast(uint)1 << 22) - 1;
    else if (dictSize >= (cast(uint)1 << 22)) mask = (cast(uint)1 << 20) - 1;
    dicBufSize = (cast(usize)dictSize + mask) & ~mask;
    if (dicBufSize < dictSize)
      dicBufSize = dictSize;
  }

  if (!p.dic || dicBufSize != p.dicBufSize)
  {
    LzmaDec_FreeDict(p, alloc);
    p.dic = cast(ubyte *)ISzAlloc_Alloc(alloc, dicBufSize);
    if (!p.dic)
    {
      LzmaDec_FreeProbs(p, alloc);
      return SZ_ERROR_MEM;
    }
  }
  p.dicBufSize = dicBufSize;
  p.prop = propNew;
  return SZ_OK;
}


//==========================================================================
//
//  LzmaDecode
//
//==========================================================================
public SRes LzmaDecode (ubyte* dest, usize* destLen, const(ubyte)* src, usize *srcLen,
                        const(ubyte)* propData, uint propSize, ELzmaFinishMode finishMode,
                        ELzmaStatus *status, ISzAllocPtr alloc)
{
  CLzmaDec p;
  SRes res;
  usize outSize = *destLen, inSize = *srcLen;
  *destLen = *srcLen = 0;
  *status = LZMA_STATUS_NOT_SPECIFIED;
  if (inSize < RC_INIT_SIZE)
    return SZ_ERROR_INPUT_EOF;
  //LzmaDec_Construct(&p);
  { p.dic = null; p.probs = null; }
  int rinres = LzmaDec_AllocateProbs(&p, propData, propSize, alloc);
  if (rinres != 0) return rinres;
  p.dic = dest;
  p.dicBufSize = outSize;
  LzmaDec_Init(&p);
  *srcLen = inSize;
  res = LzmaDec_DecodeToDic(&p, outSize, src, srcLen, finishMode, status);
  *destLen = p.dicPos;
  if (res == SZ_OK && *status == LZMA_STATUS_NEEDS_MORE_INPUT)
    res = SZ_ERROR_INPUT_EOF;
  LzmaDec_FreeProbs(&p, alloc);
  return res;
}
