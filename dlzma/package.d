/* converted by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// conversion of LZMA SDK 2103 (lzma2103.7z)
module iv.dlzma;
public:


// ////////////////////////////////////////////////////////////////////////// //
static if (!is(typeof(usize))) public alias usize = size_t;


// ////////////////////////////////////////////////////////////////////////// //
/* LZMA_PROB32 can increase the speed on some CPUs,
   but memory usage for CLzmaDec::probs will be doubled in that case */
//version = LZMA_PROB32;

version(LZMA_PROB32) {
  alias CLzmaProb = uint;
} else {
  alias CLzmaProb = ushort;
}


// ////////////////////////////////////////////////////////////////////////// //
enum {
  SZ_OK = 0,

  SZ_ERROR_DATA = 1,
  SZ_ERROR_MEM = 2,
  // SZ_ERROR_CRC = 3,
  SZ_ERROR_UNSUPPORTED = 4,
  SZ_ERROR_PARAM = 5,
  SZ_ERROR_INPUT_EOF = 6,
  SZ_ERROR_OUTPUT_EOF = 7,
  SZ_ERROR_READ = 8,
  SZ_ERROR_WRITE = 9,
  SZ_ERROR_PROGRESS = 10,
  SZ_ERROR_FAIL = 11,
}


alias SRes = int;
alias WRes = int;


/* The following interfaces use first parameter as pointer to structure */

struct ISeqInStream {
  SRes delegate (/*const*/ISeqInStream* p, void* buf, usize* size) nothrow Read;
    /* if (input(*size) != 0 && output(*size) == 0) means end_of_stream.
       (output(*size) < input(*size)) is allowed */
};


struct ISeqOutStream {
  usize delegate (/*const*/ISeqOutStream* p, const(void)* buf, usize size) nothrow Write;
    /* Returns: result - the number of actually written bytes.
       (result < size) means error */
};


struct ICompressProgress {
  SRes delegate (/*const*/ICompressProgress* p, ulong inSize, ulong outSize) nothrow Progress;
    /* Returns: result. (result != SZ_OK) means break.
       Value (ulong)(long)-1 for size means unknown value. */
};


alias ISzAllocPtr = ISzAlloc*;
struct ISzAlloc {
  void *delegate (ISzAllocPtr p, usize size) nothrow Alloc;
  void delegate (ISzAllocPtr p, void *address) nothrow Free; /* address can be 0 */
};

public void *ISzAlloc_Alloc (ISzAllocPtr p, usize size) nothrow {
  pragma(inline, true);
  return p.Alloc(p, size);
}

public void ISzAlloc_Free (ISzAllocPtr p, void *address) nothrow {
  pragma(inline, true);
  p.Free(p, address);
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared ISzAlloc lzmaDefAllocator = ISzAlloc(
  delegate void* (ISzAllocPtr p, usize size) nothrow {
    import core.stdc.stdlib : malloc;
    size += !size;
    return malloc(size);
  },
  delegate void (ISzAllocPtr p, void* address) nothrow {
    import core.stdc.stdlib : free;
    if (address !is null) free(address);
  }
);


// ////////////////////////////////////////////////////////////////////////// //
enum LZMA_PROPS_SIZE = 5;


//**************************************************************************
//
// encoder API
//
//**************************************************************************

struct CLzmaEncProps {
  int level;        /* 0 <= level <= 9 */
  uint dictSize;    /* (1 << 12) <= dictSize <= (1 << 27) for 32-bit version
                       (1 << 12) <= dictSize <= (3 << 29) for 64-bit version
                       default = (1 << 24) */
  int lc;           /* 0 <= lc <= 8, default = 3 */
  int lp;           /* 0 <= lp <= 4, default = 0 */
  int pb;           /* 0 <= pb <= 4, default = 2 */
  int algo;         /* 0 - fast, 1 - normal, default = 1 */
  int fb;           /* 5 <= fb <= 273, default = 32 */
  int btMode;       /* 0 - hashChain Mode, 1 - binTree mode - normal, default = 1 */
  int numHashBytes; /* 2, 3 or 4, default = 4 */
  uint mc;          /* 1 <= mc <= (1 << 30), default = 32 */
  uint writeEndMark;/* 0 - do not write EOPM, 1 - write EOPM, default = 0 */

  ulong reduceSize; /* estimated size of data that will be compressed. default = (ulong)(long)-1.
                       Encoder uses this value to reduce dictionary size */
}

alias CLzmaEncHandle = void*;

version(none) {
public void LzmaEncProps_Init (CLzmaEncProps* p) @nogc;
public void LzmaEncProps_Normalize (CLzmaEncProps* p) @nogc;
public uint LzmaEncProps_GetDictSize (const(CLzmaEncProps)* props2) @nogc;

/* LzmaEnc* functions can return the following exit codes:
SRes:
  SZ_OK           - OK
  SZ_ERROR_MEM    - Memory allocation error
  SZ_ERROR_PARAM  - Incorrect paramater in props
  SZ_ERROR_WRITE  - ISeqOutStream write callback error
  SZ_ERROR_OUTPUT_EOF - output buffer overflow - version with (ubyte *) output
  SZ_ERROR_PROGRESS - some break from progress callback
*/

public CLzmaEncHandle LzmaEnc_Create (ISzAllocPtr alloc);
public void LzmaEnc_Destroy (CLzmaEncHandle p, ISzAllocPtr alloc, ISzAllocPtr allocBig);

public SRes LzmaEnc_SetProps (CLzmaEncHandle pp, const(CLzmaEncProps)* props2) @nogc;
public void LzmaEnc_SetDataSize (CLzmaEncHandle pp, ulong expectedDataSiize) @nogc;
public SRes LzmaEnc_WriteProperties (CLzmaEncHandle pp, ubyte* props, uint* size) @nogc;
public uint LzmaEnc_IsWriteEndMark (CLzmaEncHandle pp) @nogc;

public SRes LzmaEnc_MemEncode (CLzmaEncHandle pp, ubyte* dest, usize* destLen, const(ubyte)* src, usize srcLen,
                               int writeEndMark, ICompressProgress *progress, ISzAllocPtr alloc, ISzAllocPtr allocBig);

public SRes LzmaEnc_Encode (CLzmaEncHandle pp, ISeqOutStream *outStream, ISeqInStream *inStream,
                            ICompressProgress *progress, ISzAllocPtr alloc, ISzAllocPtr allocBig);

/* ---------- One Call Interface ---------- */
public SRes LzmaEncode (ubyte* dest, usize* destLen, const(ubyte)* src, usize srcLen,
                        const(CLzmaEncProps)* props, ubyte* propsEncoded, uint* propsSize,
                        int writeEndMark, ICompressProgress *progress, ISzAllocPtr alloc, ISzAllocPtr allocBig);

}


//**************************************************************************
//
// decoder API
//
//**************************************************************************
struct CLzmaProps {
  ubyte lc;
  ubyte lp;
  ubyte pb;
  ubyte _pad_;
  uint dicSize;
}

/* LzmaProps_Decode - decodes properties
Returns:
  SZ_OK
  SZ_ERROR_UNSUPPORTED - Unsupported properties
*/

//SRes LzmaProps_Decode(CLzmaProps *p, const uint8_t *data, unsigned size);


/* ---------- LZMA Decoder state ---------- */

/* LZMA_REQUIRED_INPUT_MAX = number of required input bytes for worst case.
   Num bits = log2((2^11 / 31) ^ 22) + 26 < 134 + 26 = 160; */

enum LZMA_REQUIRED_INPUT_MAX = 20;

struct CLzmaDec {
  CLzmaProps prop;
  CLzmaProb *probs;
  CLzmaProb *probs_1664;
  ubyte *dic;
  usize dicBufSize;
  usize dicPos;
  const(ubyte)* buf;
  uint range;
  uint code;
  uint processedPos;
  uint checkDicSize;
  uint[4] reps;
  uint state;
  uint remainLen;

  uint numProbs;
  uint tempBufSize;
  ubyte[LZMA_REQUIRED_INPUT_MAX] tempBuf = void;
}

//#define LzmaDec_Construct(p) { (p)->dic = NULL; (p)->probs = NULL; }

//public void LzmaDec_Construct (CLzmaDec *p) nothrow @nogc { p.dic = null; p.probs = null; }

/* There are two types of LZMA streams:
     - Stream with end mark. That end mark adds about 6 bytes to compressed size.
     - Stream without end mark. You must know exact uncompressed size to decompress such stream. */

alias ELzmaFinishMode = int;
enum {
  LZMA_FINISH_ANY,   /* finish at any point */
  LZMA_FINISH_END    /* block must be finished at the end */
}

/* ELzmaFinishMode has meaning only if the decoding reaches output limit !!!

   You must use LZMA_FINISH_END, when you know that current output buffer
   covers last bytes of block. In other cases you must use LZMA_FINISH_ANY.

   If LZMA decoder sees end marker before reaching output limit, it returns SZ_OK,
   and output value of destLen will be less than output buffer size limit.
   You can check status result also.

   You can use multiple checks to test data integrity after full decompression:
     1) Check Result and "status" variable.
     2) Check that output(destLen) = uncompressedSize, if you know real uncompressedSize.
     3) Check that output(srcLen) = compressedSize, if you know real compressedSize.
        You must use correct finish mode in that case. */

alias ELzmaStatus = int;
enum {
  LZMA_STATUS_NOT_SPECIFIED,               /* use main error code instead */
  LZMA_STATUS_FINISHED_WITH_MARK,          /* stream was finished with end mark. */
  LZMA_STATUS_NOT_FINISHED,                /* stream was not finished */
  LZMA_STATUS_NEEDS_MORE_INPUT,            /* you must provide more input bytes */
  LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK  /* there is probability that stream was finished without end mark */
}

/* ELzmaStatus is used only as output value for function call */


/* ---------- Interfaces ---------- */

/* There are 3 levels of interfaces:
     1) Dictionary Interface
     2) Buffer Interface
     3) One Call Interface
   You can select any of these interfaces, but don't mix functions from different
   groups for same object. */


/* There are two variants to allocate state for Dictionary Interface:
     1) LzmaDec_Allocate / LzmaDec_Free
     2) LzmaDec_AllocateProbs / LzmaDec_FreeProbs
   You can use variant 2, if you set dictionary buffer manually.
   For Buffer Interface you must always use variant 1.

LzmaDec_Allocate* can return:
  SZ_OK
  SZ_ERROR_MEM         - Memory allocation error
  SZ_ERROR_UNSUPPORTED - Unsupported properties
*/

version(none) {
// you can use this to check and decode properties
public SRes LzmaProps_Decode (CLzmaProps* p, const(ubyte)* data, uint size) @nogc;

public void LzmaDec_Init (CLzmaDec* p) @nogc;

public SRes LzmaDec_Allocate (CLzmaDec* p, const(ubyte)* props, uint propsSize, ISzAllocPtr alloc);
public void LzmaDec_Free (CLzmaDec* p, ISzAllocPtr alloc);

//k8: the following two functions are used for... something, i don't know.
//k8: you don't need to call them, `LzmaDec_Allocate()` and `LzmaDec_Free()` will do it for you.
public SRes LzmaDec_AllocateProbs (CLzmaDec* p, const(ubyte)* props, uint propsSize, ISzAllocPtr alloc);
public void LzmaDec_FreeProbs (CLzmaDec *p, ISzAllocPtr alloc);

/* ---------- Dictionary Interface ---------- */

/* You can use it, if you want to eliminate the overhead for data copying from
   dictionary to some other external buffer.
   You must work with CLzmaDec variables directly in this interface.

   STEPS:
     LzmaDec_Construct()
     LzmaDec_Allocate()
     for (each new stream)
     {
       LzmaDec_Init()
       while (it needs more decompression)
       {
         LzmaDec_DecodeToDic()
         use data from CLzmaDec::dic and update CLzmaDec::dicPos
       }
     }
     LzmaDec_Free()
*/

/* LzmaDec_DecodeToDic

   The decoding to internal dictionary buffer (CLzmaDec::dic).
   You must manually update CLzmaDec::dicPos, if it reaches CLzmaDec::dicBufSize !!!

finishMode:
  It has meaning only if the decoding reaches output limit (dicLimit).
  LZMA_FINISH_ANY - Decode just dicLimit bytes.
  LZMA_FINISH_END - Stream must be finished after dicLimit.

Returns:
  SZ_OK
    status:
      LZMA_STATUS_FINISHED_WITH_MARK
      LZMA_STATUS_NOT_FINISHED
      LZMA_STATUS_NEEDS_MORE_INPUT
      LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK
  SZ_ERROR_DATA - Data error
  SZ_ERROR_FAIL - Some unexpected error: internal error of code, memory corruption or hardware failure
*/

public SRes LzmaDec_DecodeToDic (CLzmaDec* p, usize dicLimit, const(ubyte)* src, usize *srcLen,
                                 ELzmaFinishMode finishMode, ELzmaStatus *status) @nogc;


/* ---------- Buffer Interface ---------- */

/* It's zlib-like interface.
   See LzmaDec_DecodeToDic description for information about STEPS and return results,
   but you must use LzmaDec_DecodeToBuf instead of LzmaDec_DecodeToDic and you don't need
   to work with CLzmaDec variables manually.

finishMode:
  It has meaning only if the decoding reaches output limit (*destLen).
  LZMA_FINISH_ANY - Decode just destLen bytes.
  LZMA_FINISH_END - Stream must be finished after (*destLen).
*/

public SRes LzmaDec_DecodeToBuf (CLzmaDec* p, ubyte* dest, usize* destLen, const(ubyte)* src, usize *srcLen,
                                 ELzmaFinishMode finishMode, ELzmaStatus *status) @nogc;


/* ---------- One Call Interface ---------- */

/* LzmaDecode

finishMode:
  It has meaning only if the decoding reaches output limit (*destLen).
  LZMA_FINISH_ANY - Decode just destLen bytes.
  LZMA_FINISH_END - Stream must be finished after (*destLen).

Returns:
  SZ_OK
    status:
      LZMA_STATUS_FINISHED_WITH_MARK
      LZMA_STATUS_NOT_FINISHED
      LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK
  SZ_ERROR_DATA - Data error
  SZ_ERROR_MEM  - Memory allocation error
  SZ_ERROR_UNSUPPORTED - Unsupported properties
  SZ_ERROR_INPUT_EOF - It needs more bytes in input buffer (src).
  SZ_ERROR_FAIL - Some unexpected error: internal error of code, memory corruption or hardware failure
*/

public SRes LzmaDecode (ubyte* dest, usize* destLen, const(ubyte)* src, usize *srcLen,
                        const(ubyte)* propData, uint propSize, ELzmaFinishMode finishMode,
                        ELzmaStatus *status, ISzAllocPtr alloc);
}


public import iv.dlzma.dec;
public import iv.dlzma.enc;
