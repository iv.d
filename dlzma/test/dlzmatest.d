/* converted by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
module dlzmatest;

import std.digest.ripemd;

import iv.pxclock;
import iv.strex;
import iv.vfs;
import iv.vfs.io;

import iv.dlzma;


// ////////////////////////////////////////////////////////////////////////// //
void compressFile (ref VFile fi, ref VFile fo) {
  fi.seek(0);
  ulong insize = fi.size;
  //fi.rawReadExact(inbuf[0..insize]);
  //fi.close();

  CLzmaEncProps props;
  LzmaEncProps_Init(&props);
  props.level = 9;
  //props.dictSize = 1;
  //while (props.dictSize < insize) props.dictSize <<= 1;
  props.dictSize = 1<<27; //128MB
  //props.dictSize = 1<<22; //4MB
  props.reduceSize = insize;

  ubyte[LZMA_PROPS_SIZE+8] header;
  uint headerSize = cast(uint)header.sizeof;

  CLzmaEncHandle enc = LzmaEnc_Create(&lzmaDefAllocator);
  scope(exit) LzmaEnc_Destroy(enc, &lzmaDefAllocator, &lzmaDefAllocator);

  if (LzmaEnc_SetProps(enc, &props) != SZ_OK) throw new Exception("cannot set encoder properties");
  LzmaEnc_SetDataSize(enc, insize); // just in case

  if (LzmaEnc_WriteProperties(enc, header.ptr, &headerSize) != SZ_OK) throw new Exception("cannot encode encoder properties");
  assert(headerSize > 0 && headerSize < 256);

  writeln("compressing...");
  fo.writeNum!ushort(cast(ushort)1); // version and endianness check
  fo.writeNum!ulong(insize); // unpacked file size
  fo.writeNum!ubyte(cast(ubyte)headerSize); // properties size
  fo.rawWriteExact(header[0..headerSize]);

  ISeqInStream inStream;
  ISeqOutStream outStream;
  ICompressProgress progress;

  immutable origInSize = insize;
  ulong destSize = 0;

  auto csum = makeDigest!RIPEMD160;

  /* if (input(*size) != 0 && output(*size) == 0) means end_of_stream.
     (output(*size) < input(*size)) is allowed */
  inStream.Read = delegate SRes (ISeqInStream* p, void* buf, usize* size) nothrow {
    if (*size > insize) *size = cast(usize)insize;
    if (*size) {
      try {
        fi.rawReadExact(buf[0..*size]);
      } catch (Exception e) {
        return SZ_ERROR_READ;
      }
      csum.put((cast(const(ubyte)*)buf)[0..*size]);
      insize -= *size;
    }
    return SZ_OK;
  };

  /* Returns: result - the number of actually written bytes.
     (result < size) means error */
  outStream.Write = delegate usize (ISeqOutStream* p, const(void)* buf, usize size) nothrow {
    try {
      fo.rawWriteExact(buf[0..size]);
      destSize += size;
    } catch (Exception e) {
      return 0;
    }
    return size;
  };

  uint prevPrc = uint.max;
  ulong prevReport = clockMilli();

  progress.Progress = delegate SRes (ICompressProgress* p, ulong inSize, ulong outSize) nothrow {
    if (origInSize == 0) return SZ_OK; // just in case
    immutable uint prc = cast(uint)(inSize*100U/origInSize);
    if (prc == prevPrc) {
      ulong rtt = clockMilli();
      if (rtt-prevReport < 500) return SZ_OK;
    }
    prevPrc = prc;
    char[128] i0 = void;
    char[128] i1 = void;
    auto num0 = intWithCommas(i0[], inSize);
    auto num1 = intWithCommas(i1[], origInSize);
    try {
      write(" [", num0[], "/", num1[], "]  ", prc, "%\x1b[K\r");
      //writeln;
    } catch (Exception) {}
    prevReport = clockMilli();
    return SZ_OK;
  };

  progress.Progress(&progress, 0, 0);

  SRes res = LzmaEnc_Encode(enc, &outStream, &inStream, &progress, &lzmaDefAllocator, &lzmaDefAllocator);

  switch (res) {
    case SZ_OK: break;
    case SZ_ERROR_MEM: throw new Exception("FUCK: memory");
    case SZ_ERROR_PARAM: throw new Exception("FUCK: param");
    case SZ_ERROR_OUTPUT_EOF: throw new Exception("FUCK: compressed is bigger");
    default: throw new Exception("FUCK: something else");
  }

  ubyte[20] hash = csum.finish()[];
  fo.rawWriteExact(hash[]);

  writeln("\rcompressed ", intWithCommas(origInSize), " to ", intWithCommas(destSize), "; ratio: ", destSize*100U/(origInSize ? origInSize : 1), "%\x1b[K");
}


// ////////////////////////////////////////////////////////////////////////// //
void decompressFile (ref VFile fi, ref VFile fo) {
  ubyte[LZMA_PROPS_SIZE+8] header;

  fi.seek(0);
  ulong pksize = fi.size;
  if (fi.readNum!ushort != 1) throw new Exception("invalid archive version");
  ulong unsize = fi.readNum!ulong; // unpacked size
  ubyte hdrSize = fi.readNum!ubyte;
  if (hdrSize == 0 || hdrSize > header.sizeof) throw new Exception("invalid properties size");
  fi.rawReadExact(header[0..hdrSize]);
  pksize -= fi.tell;

  if (pksize < 20) throw new Exception("invalid archive size");
  if (pksize == 0) {
    if (unsize != 0) throw new Exception("invalid archive size");
    return; // nothing to do
  }
  pksize -= 20; // digest size

  auto csum = makeDigest!RIPEMD160;

  enum InBufSize = 1024*1024;
  enum OutBufSize = 1024*1024;

  ubyte *inbuf = cast(ubyte*)ISzAlloc_Alloc(&lzmaDefAllocator, InBufSize);
  ubyte *outbuf = cast(ubyte*)ISzAlloc_Alloc(&lzmaDefAllocator, OutBufSize);
  scope(exit) {
    ISzAlloc_Free(&lzmaDefAllocator, inbuf);
    ISzAlloc_Free(&lzmaDefAllocator, outbuf);
  }

  CLzmaDec dec;
  LzmaDec_Init(&dec);

  SRes res = LzmaDec_Allocate(&dec, header.ptr, hdrSize, &lzmaDefAllocator);
  if (res != SZ_OK) throw new Exception("cannot initialize decoder");
  scope(exit) LzmaDec_Free(&dec, &lzmaDefAllocator);

  ulong unpackedTotal = 0;
  ulong readleft = pksize;
  usize inused = 0;

  uint prevPrc = uint.max;
  ulong prevReport = clockMilli();
  immutable ulong sttime = prevReport;

  void showProgress () nothrow {
    immutable rds = unpackedTotal; //pksize-readleft;
    immutable uint prc = cast(uint)(rds*100U/unsize/*pksize*/);
    if (prc == prevPrc && unpackedTotal != unsize) {
      ulong rtt = clockMilli();
      if (rtt-prevReport < 500) return;
    }
    prevPrc = prc;
    char[128] i0 = void;
    char[128] i1 = void;
    auto num0 = intWithCommas(i0[], rds);
    auto num1 = intWithCommas(i1[], unsize/*pksize*/);
    try {
      write(" [", num0[], "/", num1[], "]  ", prc, "%\x1b[K\r");
      //writeln;
    } catch (Exception) {}
    prevReport = clockMilli();
  }

  while (readleft || inused) {
    // read more
    if (readleft && inused < InBufSize) {
      uint rd = InBufSize-cast(uint)inused;
      if (rd > readleft) rd = cast(usize)readleft;
      fi.rawReadExact(inbuf[inused..inused+rd]);
      inused += rd;
      readleft -= rd;
      //showProgress();
    }
    usize outSize = OutBufSize;
    usize inSize = inused;
    ELzmaStatus status;
    // as we don't have a proper EOF mark, make sure to not unpack extra data
    if (unsize-unpackedTotal < outSize) outSize = cast(usize)(unsize-unpackedTotal);
    //writeln("\nunsize=", unsize, "; unpackedTotal=", unpackedTotal, "; outSize=", outSize);
    res = LzmaDec_DecodeToBuf(&dec, outbuf, &outSize, inbuf, &inSize, LZMA_FINISH_ANY, &status);
    if (res != SZ_OK) {
      writeln;
      writeln("ERROR: readleft=", readleft, "; inused=", inused, "; written=", unpackedTotal, " of ", unsize);
      switch (res) {
        case SZ_ERROR_DATA: throw new Exception("corrupted data");
        case SZ_ERROR_MEM: throw new Exception("out of memory");
        case SZ_ERROR_UNSUPPORTED: throw new Exception("unsupported properties");
        case SZ_ERROR_INPUT_EOF: throw new Exception("need bigger input buffer, but we don't have any");
        default: throw new Exception("some other error");
      }
    }
    if (outSize) {
      fo.rawWriteExact(outbuf[0..outSize]);
      unpackedTotal += outSize;
      csum.put((cast(const(ubyte)*)outbuf)[0..outSize]);
      showProgress();
      if (unpackedTotal == unsize) break; // we're done (we don't have EOF mark, so...)
    }
    if (inSize < inused) {
      import core.stdc.string : memmove;
      memmove(inbuf, inbuf+inSize, inused-inSize);
    }
    inused -= inSize;
    switch (status) {
      case LZMA_STATUS_FINISHED_WITH_MARK: throw new Exception("found EOF mark, but there should not be one");
      case LZMA_STATUS_NOT_FINISHED: break; // it is ok
      case LZMA_STATUS_NEEDS_MORE_INPUT: break; // it is ok
      case LZMA_STATUS_MAYBE_FINISHED_WITHOUT_MARK: break; // ok ;-)
      default: break; // ignore others
    }
  }

  if (unpackedTotal != unsize) {
    //import std.conv : to;
    throw new Exception("invalid unpacked file size; expected "~intWithCommas(unsize).idup~" but got "~intWithCommas(unpackedTotal).idup);
  }

  // check hash
  ubyte[20] origcsum;
  fi.rawReadExact(origcsum[]);
  ubyte[20] hash = csum.finish()[];
  if (origcsum[] != hash[]) throw new Exception("invalid unpacked file hash");

  ulong etime = clockMilli()-sttime;
  if (!etime) etime = 1;
  writeln("\rsuccesfully unpacked ", intWithCommas(unpackedTotal), " bytes (", intWithCommas(unpackedTotal*1000U/etime), " MB/sec).\x1b[K");
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  if (args.length < 2) {
    writeln("usage: fuckme <c|x> infile outfile");
    return;
  }

  if (args[1] == "c") {
    if (args.length < 4) throw new Exception("out of args");
    if (args[2] == args[3]) throw new Exception("cannot compress in-place");
    auto fi = VFile(args[2]);
    auto fo = VFile(args[3], "w");
    compressFile(fi, fo);
  } else if (args[1] == "x") {
    if (args.length < 4) throw new Exception("out of args");
    if (args[2] == args[3]) throw new Exception("cannot decompress in-place");
    auto fi = VFile(args[2]);
    auto fo = VFile(args[3], "w");
    decompressFile(fi, fo);
  } else if (args[1] == "t") {
    if (args.length < 3) throw new Exception("out of args");
    auto fi = VFile(args[2]);
    auto fo = VFile("/dev/null", "w");
    decompressFile(fi, fo);
  }
}
