/* converted by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// LZMA Reference Decoder
// 2015-06-14 : Igor Pavlov : Public domain
// This code implements LZMA file decoding according to LZMA specification.
// This code is not optimized for speed.
module iv.dlzma.refdec;

/*
usage:

  CLzmaDecoder lzmaDecoder;

  ubyte[5] header = void; // lzma properties
  fi.rawReadExact(header[]);
  lzmaDecoder.decodeProperties(header.ptr);
  lzmaDecoder.markerIsMandatory = false;
  lzmaDecoder.create(totalUnpackedSize);

  main_loop: for (;;) {
    auto res = lzmaDecoder.decodeStep(
      delegate () { ubyte b; fi.rawReadExact((&b)[0..1]); return b; },
      (ubyte b) { fo.rawWriteExact((&b)[0..1]); }
    );
    switch (res) {
      case CLzmaDecoder.Result.Continue: break;
      case CLzmaDecoder.Result.Error: throw new VFSException("LZMA stream corrupted");
      case CLzmaDecoder.Result.FinishedWithMarker:
      case CLzmaDecoder.Result.FinishedWithoutMarker:
        break main_loop;
      default: assert(0, "LZMA internal error");
    }
  }
*/


// ////////////////////////////////////////////////////////////////////////// //
struct CLzmaDecoder {
public:
  enum Result {
    Error = -1,
    Continue = 0,
    FinishedWithMarker = 1,
    FinishedWithoutMarker = 2,
  }

private:
  CProb[kNumStates<<kNumPosBitsMax] isMatch;
  CProb[kNumStates] isRep;
  CProb[kNumStates] isRepG0;
  CProb[kNumStates] isRepG1;
  CProb[kNumStates] isRepG2;
  CProb[kNumStates<<kNumPosBitsMax] isRep0Long;

  CLenDecoder lenDecoder;
  CLenDecoder repLenDecoder;

  CProb* litProbs;
  usize litProbsAllotedBytes;
  CBitTreeDecoder!6[kNumLenToPosStates] posSlotDecoder;
  CBitTreeDecoder!kNumAlignBits alignDecoder;
  CProb[1+kNumFullDistances-kEndPosModelIndex] posDecoders;

  uint rep0, rep1, rep2, rep3;
  uint state;
  bool unpackSizeDefined;
  ulong unpackSize;

public:
  CRangeDecoder rangeDec;
  COutWindow outWindow;

  bool markerIsMandatory;
  uint lc, pb, lp;
  uint dictSize;
  uint dictSizeInProperties;
  bool inited;

  //@disable this (this);

  void close () {
    import core.stdc.stdlib : free;
    if (litProbs !is null) free(litProbs);
    litProbs = null;
    inited = false;
    outWindow.close();
  }

  void decodeProperties (const(ubyte)* properties) {
    enum LZMAMinDictSize = 1U<<12;
    uint d = properties[0];
    if (d >= 9*5*5) throw new Exception("Incorrect LZMA properties");
    lc = d%9;
    d /= 9;
    pb = d/5;
    lp = d%5;
    dictSizeInProperties = 0;
    for (int i = 0; i < 4; ++i) dictSizeInProperties |= cast(uint)properties[i+1]<<(8*i);
    dictSize = dictSizeInProperties;
    if (dictSize < LZMAMinDictSize) dictSize = LZMAMinDictSize;
  }

  void create (ulong aunpackSize) {
    inited = false;
    unpackSizeDefined = true;
    unpackSize = aunpackSize;
  }

  void create () {
    inited = false;
    unpackSizeDefined = false;
    unpackSize = 0;
  }


  Result decodeStep (scope ubyte delegate () readByte, scope void delegate (ubyte b) writeByte) {
    void decodeLiteral (uint state, uint rep0) {
      uint prevByte = 0;
      if (!outWindow.isEmpty()) prevByte = outWindow.getByte(1);

      uint symbol = 1;
      uint litState = ((outWindow.totalPos&((1<<lp)-1))<<lc)+(prevByte>>(8-lc));
      CProb* probs = litProbs+(0x300U*litState);

      if (state >= 7) {
        uint matchByte = outWindow.getByte(rep0+1);
        do {
          uint matchBit = (matchByte>>7)&1;
          matchByte <<= 1;
          uint bit = rangeDec.decodeBit(&probs[((1+matchBit)<<8)+symbol], readByte);
          symbol = (symbol<<1)|bit;
          if (matchBit != bit) break;
        } while (symbol < 0x100);
      } while (symbol < 0x100)
      symbol = (symbol<<1)|rangeDec.decodeBit(&probs[symbol], readByte);
      outWindow.putByte(cast(ubyte)(symbol-0x100), writeByte);
    }

    uint decodeDistance (uint len) {
      uint lenState = len;
      if (lenState > kNumLenToPosStates-1) lenState = kNumLenToPosStates-1;

      uint posSlot = posSlotDecoder[lenState].decode(rangeDec, readByte);
      if (posSlot < 4) return posSlot;

      uint numDirectBits = cast(uint)((posSlot>>1)-1);
      uint dist = ((2|(posSlot&1))<<numDirectBits);
      if (posSlot < kEndPosModelIndex) {
        dist += bitTreeReverseDecode(posDecoders.ptr+dist-posSlot, numDirectBits, rangeDec, readByte);
      } else {
        dist += rangeDec.decodeDirectBits(numDirectBits-kNumAlignBits, readByte)<<kNumAlignBits;
        dist += alignDecoder.reverseDecode(rangeDec, readByte);
      }
      return dist;
    }

    if (!inited) {
      inited = true;
      outWindow.create(dictSize);
      createLiterals();
      if (!rangeDec.initialize(readByte)) throw new Exception("can't initialize lzma range decoder");
      initialize();
      rep0 = rep1 = rep2 = rep3 = 0;
      state = 0;
    }

    if (unpackSizeDefined && unpackSize == 0 && !markerIsMandatory) {
      if (rangeDec.isFinishedOK()) return Result.FinishedWithoutMarker;
    }

    uint posState = outWindow.totalPos&((1<<pb)-1);

    if (rangeDec.decodeBit(&isMatch[(state<<kNumPosBitsMax)+posState], readByte) == 0) {
      if (unpackSizeDefined && unpackSize == 0) return Result.Error;
      decodeLiteral(state, rep0);
      state = updateStateLiteral(state);
      --unpackSize;
      return Result.Continue;
    }

    uint len;

    if (rangeDec.decodeBit(&isRep[state], readByte) != 0) {
      if (unpackSizeDefined && unpackSize == 0) return Result.Error;
      if (outWindow.isEmpty()) return Result.Error;
      if (rangeDec.decodeBit(&isRepG0[state], readByte) == 0) {
        if (rangeDec.decodeBit(&isRep0Long[(state<<kNumPosBitsMax)+posState], readByte) == 0) {
          state = updateStateShortRep(state);
          outWindow.putByte(outWindow.getByte(rep0+1), writeByte);
          --unpackSize;
          return Result.Continue;
        }
      } else {
        uint dist;
        if (rangeDec.decodeBit(&isRepG1[state], readByte) == 0) {
          dist = rep1;
        } else {
          if (rangeDec.decodeBit(&isRepG2[state], readByte) == 0) {
            dist = rep2;
          } else {
            dist = rep3;
            rep3 = rep2;
          }
          rep2 = rep1;
        }
        rep1 = rep0;
        rep0 = dist;
      }
      len = repLenDecoder.decode(rangeDec, posState, readByte);
      state = updateStateRep(state);
    } else {
      rep3 = rep2;
      rep2 = rep1;
      rep1 = rep0;
      len = lenDecoder.decode(rangeDec, posState, readByte);
      state = updateStateMatch(state);
      rep0 = decodeDistance(len);
      if (rep0 == 0xFFFFFFFF) return (rangeDec.isFinishedOK() ? Result.FinishedWithMarker : Result.Error);
      if (unpackSizeDefined && unpackSize == 0) return Result.Error;
      if (rep0 >= dictSize || !outWindow.checkDistance(rep0)) return Result.Error;
    }
    len += kMatchMinLen;
    bool isError = false;
    if (unpackSizeDefined && unpackSize < len) {
      len = cast(uint)unpackSize;
      isError = true;
    }
    outWindow.copyMatch(rep0+1, len, writeByte);
    unpackSize -= len;
    if (isError) return Result.Error;
    return Result.Continue;
  }

private:
  void createLiterals () {
    //litProbs = new CProb[](0x300U<<(lc+lp));
    import core.stdc.stdlib : realloc;
    //import core.stdc.string : memset;
    usize toalloc = (0x300U<<(lc+lp))*CProb.sizeof;
    if (litProbs is null || toalloc > litProbsAllotedBytes) {
      auto nb = cast(CProb*)realloc(litProbs, toalloc);
      if (nb is null) throw new Exception("LZMA: out of memory");
      litProbsAllotedBytes = toalloc;
      litProbs = nb;
    }
    //memset(litProbs, 0, (0x300U<<(lc+lp))*CProb.sizeof);
  }

  void initLiterals () {
    uint num = 0x300U<<(lc+lp);
    //for (uint i = 0; i < num; ++i) litProbs[i] = ProbInitValue;
    litProbs[0..num] = ProbInitValue;
  }

  void initDist () {
    for (uint i = 0; i < kNumLenToPosStates; i++) posSlotDecoder[i].initialize();
    alignDecoder.initialize();
    posDecoders[] = ProbInitValue;
  }

  void initialize () {
    initLiterals();
    initDist();

    isMatch[] = ProbInitValue;
    isRep[] = ProbInitValue;
    isRepG0[] = ProbInitValue;
    isRepG1[] = ProbInitValue;
    isRepG2[] = ProbInitValue;
    isRep0Long[] = ProbInitValue;

    lenDecoder.initialize();
    repLenDecoder.initialize();
  }

static:
  struct COutWindow {
  private:
    ubyte* buf;
    uint pos;
    uint size;
    uint bufsize;
    bool isFull;

  public:
    uint totalPos;

    //@disable this (this);

    void close () {
      import core.stdc.stdlib : free;
      if (buf !is null) {
        //{ import core.stdc.stdio : printf; printf("LZMA: freeing: buf=%p; bufsize=%u\n", buf, bufsize); }
        free(buf);
        buf = null;
      }
    }

    void create (uint dictSize) @trusted {
      import core.stdc.stdlib : realloc;
      if (buf is null || bufsize < dictSize) {
        auto nb = cast(ubyte*)realloc(buf, dictSize);
        if (nb is null) {
          //{ import core.stdc.stdio : printf; printf("*** buf=%p; nb=%p; bufsize=%u; dictSize=%u\n", buf, nb, bufsize, dictSize); }
          throw new Exception("LZMA: cannot allocate sliding window");
        }
        buf = nb;
        bufsize = dictSize;
      }
      size = dictSize;
      reset();
    }

    void reset () {
      pos = 0;
      isFull = false;
      totalPos = 0;
      buf[0..size] = 0; // just in case
    }

    ubyte getByte (uint dist) const pure nothrow @trusted @nogc { pragma(inline, true); return (buf[dist <= pos ? pos-dist : size-dist+pos]); }

    void putByte (ubyte b, scope void delegate (ubyte b) writeByte) {
      ++totalPos;
      buf[pos++] = b;
      if (pos == size) { pos = 0; isFull = true; }
      writeByte(b);
    }

    void copyMatch (uint dist, uint len, scope void delegate (ubyte b) writeByte) {
      pragma(inline, true);
      while (len--) {
        //putByte(getByte(dist));
        ubyte b = getByte(dist);
        ++totalPos;
        buf[pos++] = b;
        if (pos == size) { pos = 0; isFull = true; }
        writeByte(b);
      }
    }

    bool checkDistance (uint dist) const pure nothrow @trusted @nogc { pragma(inline, true); return (dist <= pos || isFull); }

    bool isEmpty () const pure nothrow @trusted @nogc { pragma(inline, true); return (pos == 0 && !isFull); }
  }


  enum kNumBitModelTotalBits = 11;
  enum kNumMoveBits = 5;

  alias CProb = ushort;

  enum ProbInitValue = (1U<<kNumBitModelTotalBits)/2;


  struct CRangeDecoder {
  private:
    enum kTopValue = 1U<<24;

    uint range;
    uint code;

  private:
    void normalize (scope ubyte delegate () readByte) {
      if (range < kTopValue) {
        range <<= 8;
        code = (code<<8)|readByte();
      }
    }

  public:
    bool corrupted;

    //@disable this (this);

    bool initialize (scope ubyte delegate () readByte) {
      corrupted = false;
      range = 0xFFFFFFFFU;
      code = 0;
      ubyte b = readByte();
      for (int i = 0; i < 4; i++) code = (code<<8)|readByte();
      if (b != 0 || code == range) corrupted = true;
      return (b == 0);
    }

    bool isFinishedOK () const pure nothrow @safe @nogc { pragma(inline, true); return (code == 0); }

    uint decodeDirectBits (uint numBits, scope ubyte delegate () readByte) {
      uint res = 0;
      do {
        range >>= 1;
        code -= range;
        uint t = 0U-(cast(uint)code>>31);
        code += range&t;
        if (code == range) corrupted = true;
        normalize(readByte);
        res <<= 1;
        res += t+1;
      } while (--numBits);
      return res;
    }

    uint decodeBit (CProb* prob, scope ubyte delegate () readByte) {
      uint v = *prob;
      uint bound = (range>>kNumBitModelTotalBits)*v;
      uint symbol;
      if (code < bound) {
        v += ((1<<kNumBitModelTotalBits)-v)>>kNumMoveBits;
        range = bound;
        symbol = 0;
      } else {
        v -= v>>kNumMoveBits;
        code -= bound;
        range -= bound;
        symbol = 1;
      }
      *prob = cast(CProb)v;
      normalize(readByte);
      return symbol;
    }
  }


  uint bitTreeReverseDecode (CProb* probs, uint numBits, ref CRangeDecoder rc, scope ubyte delegate () readByte) {
    uint m = 1;
    uint symbol = 0;
    for (uint i = 0; i < numBits; ++i) {
      uint bit = rc.decodeBit(probs+m, readByte);
      m <<= 1;
      m += bit;
      symbol |= bit<<i;
    }
    return symbol;
  }


  struct CBitTreeDecoder(uint NumBits) {
    CProb[1U<<NumBits] probs = ProbInitValue;

  public:
    //@disable this (this);

    void initialize () { probs[] = ProbInitValue; }

    uint decode (ref CRangeDecoder rc, scope ubyte delegate () readByte) {
      uint m = 1;
      for (uint i = 0; i < NumBits; ++i) m = (m<<1)+rc.decodeBit(&probs[m], readByte);
      return m-(1U<<NumBits);
    }

    uint reverseDecode (ref CRangeDecoder rc, scope ubyte delegate () readByte) { pragma(inline, true); return bitTreeReverseDecode(probs.ptr, NumBits, rc, readByte); }
  }


  enum kNumPosBitsMax = 4;

  enum kNumStates = 12;
  enum kNumLenToPosStates = 4;
  enum kNumAlignBits = 4;
  enum kStartPosModelIndex = 4;
  enum kEndPosModelIndex = 14;
  enum kNumFullDistances = 1U<<(kEndPosModelIndex>>1);
  enum kMatchMinLen = 2;


  struct CLenDecoder {
    CProb choice;
    CProb choice2;
    CBitTreeDecoder!3[1U<<kNumPosBitsMax] lowCoder;
    CBitTreeDecoder!3[1U<<kNumPosBitsMax] midCoder;
    CBitTreeDecoder!8 highCoder;

  public:
    //@disable this (this);

    void initialize () {
      choice = ProbInitValue;
      choice2 = ProbInitValue;
      highCoder.initialize();
      for (uint i = 0; i < (1<<kNumPosBitsMax); ++i) {
        lowCoder[i].initialize();
        midCoder[i].initialize();
      }
    }

    uint decode (ref CRangeDecoder rc, uint posState, scope ubyte delegate () readByte) {
      if (rc.decodeBit(&choice, readByte) == 0) return lowCoder[posState].decode(rc, readByte);
      if (rc.decodeBit(&choice2, readByte) == 0) return 8+midCoder[posState].decode(rc, readByte);
      return 16+highCoder.decode(rc, readByte);
    }
  }


  uint updateStateLiteral (uint state) pure nothrow @safe @nogc {
    pragma(inline, true);
    /*
    if (state < 4) return 0;
    if (state < 10) return state-3;
    return state-6;
    */
    return (state < 4 ? 0 : state < 10 ? state-3 : state-6);
  }
  uint updateStateMatch (uint state) pure nothrow @safe @nogc { pragma(inline, true); return (state < 7 ? 7 : 10); }
  uint updateStateRep (uint state) pure nothrow @safe @nogc { pragma(inline, true); return (state < 7 ? 8 : 11); }
  uint updateStateShortRep (uint state) pure nothrow @safe @nogc { pragma(inline, true); return (state < 7 ? 9 : 11); }
}
