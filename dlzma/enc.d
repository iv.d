/* converted by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
module iv.dlzma.enc;
nothrow:

/* use slightly smaller (around 300 bytes) code with branching in compressor? */
//version = LZMA_ENC_USE_BRANCH;

//version = LZMA_SANITY_MAGIC;

private import iv.dlzma;


// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
private:

private void SetUi32 (const(void)* p, in uint v) nothrow @trusted @nogc { pragma(inline, true); *(cast(uint*)p) = v; }
private ushort GetUi16 (const(void)* v) nothrow @trusted @nogc { pragma(inline, true); return *(cast(const(ushort)*)v); }

alias CLzRef = uint;

struct CMatchFinder {
  ubyte *buffer;
  uint pos;
  uint posLimit;
  uint streamPos;  /* wrap over Zero is allowed (streamPos < pos). Use (uint32_t)(streamPos - pos) */
  uint lenLimit;

  uint cyclicBufferPos;
  uint cyclicBufferSize; /* it must be = (historySize + 1) */

  ubyte streamEndWasReached;
  ubyte btMode;
  ubyte bigHash;
  ubyte directInput;

  uint matchMaxLen;
  CLzRef *hash;
  CLzRef *son;
  uint hashMask;
  uint cutValue;

  ubyte *bufferBase;
  ISeqInStream *stream;

  uint blockSize;
  uint keepSizeBefore;
  uint keepSizeAfter;

  uint numHashBytes;
  usize directInputRem;
  uint historySize;
  uint fixedHashSize;
  uint hashSizeSum;
  SRes result;
  uint[256] crc;
  usize numRefs;

  ulong expectedDataSize;
}

//#define Inline_MatchFinder_GetPointerToCurrentPos(p) ((const ubyte *)(p)->buffer)

//#define Inline_MatchFinder_GetNumAvailableBytes(p) ((uint)((p)->streamPos - (p)->pos))
enum Inline_MatchFinder_GetNumAvailableBytes(string p) = `(cast(uint)((`~p~`).streamPos - (`~p~`).pos))`;

/*
#define Inline_MatchFinder_IsFinishedOK(p) \
    ((p)->streamEndWasReached \
        && (p)->streamPos == (p)->pos \
        && (!(p)->directInput || (p)->directInputRem == 0))
*/

/+
int MatchFinder_NeedMove(CMatchFinder *p);
/* uint8_t *MatchFinder_GetPointerToCurrentPos(CMatchFinder *p); */
void MatchFinder_MoveBlock(CMatchFinder *p);
void MatchFinder_ReadIfRequired(CMatchFinder *p);

void MatchFinder_Construct(CMatchFinder *p);

/* Conditions:
     historySize <= 3 GB
     keepAddBufferBefore + matchMaxLen + keepAddBufferAfter < 511MB
*/
int MatchFinder_Create(CMatchFinder *p, uint historySize,
    uint keepAddBufferBefore, uint matchMaxLen, uint keepAddBufferAfter,
    ISzAllocPtr alloc);
void MatchFinder_Free(CMatchFinder *p, ISzAllocPtr alloc);
void MatchFinder_Normalize3(uint subValue, CLzRef *items, usize numItems);
// void MatchFinder_ReduceOffsets(CMatchFinder *p, uint32_t subValue);
+/

/*
#define Inline_MatchFinder_InitPos(p, val) \
    (p)->pos = (val); \
    (p)->streamPos = (val);
*/

/*
#define Inline_MatchFinder_ReduceOffsets(p, subValue) \
    (p)->pos -= (subValue); \
    (p)->streamPos -= (subValue);
*/

/*
uint* GetMatchesSpec1(uint lenLimit, uint curMatch, uint pos, const ubyte *buffer, CLzRef *son,
    usize _cyclicBufferPos, uint _cyclicBufferSize, uint _cutValue,
    uint *distances, uint maxLen);
*/

/*
Conditions:
  Mf_GetNumAvailableBytes_Func must be called before each Mf_GetMatchLen_Func.
  Mf_GetPointerToCurrentPos_Func's result must be used only before any other function
*/

alias Mf_Init_Func = void function (void *object);
alias Mf_GetNumAvailableBytes_Func = uint function (void *object);
alias Mf_GetPointerToCurrentPos_Func = const(ubyte)* function (void *object);
alias Mf_GetMatches_Func = uint* function (void *object, uint *distances);
alias Mf_Skip_Func = void function (void *object, uint);

struct /*_IMatchFinder*/ IMatchFinder2 {
  Mf_Init_Func Init;
  Mf_GetNumAvailableBytes_Func GetNumAvailableBytes;
  Mf_GetPointerToCurrentPos_Func GetPointerToCurrentPos;
  Mf_GetMatches_Func GetMatches;
  Mf_Skip_Func Skip;
}

/*
void MatchFinder_CreateVTable(CMatchFinder *p, IMatchFinder2 *vTable);

void MatchFinder_Init_LowHash(CMatchFinder *p);
void MatchFinder_Init_HighHash(CMatchFinder *p);
void MatchFinder_Init_4(CMatchFinder *p);
void MatchFinder_Init(CMatchFinder *p);

uint* Bt3Zip_MatchFinder_GetMatches(CMatchFinder *p, uint *distances);
uint* Hc3Zip_MatchFinder_GetMatches(CMatchFinder *p, uint *distances);

void Bt3Zip_MatchFinder_Skip(CMatchFinder *p, uint num);
void Hc3Zip_MatchFinder_Skip(CMatchFinder *p, uint num);

void LzFindPrepare(void);
*/


/* LzHash.h -- HASH functions for LZ algorithms
2019-10-30 : Igor Pavlov : Public domain */

/*
  (kHash2Size >= (1 <<  8)) : Required
  (kHash3Size >= (1 << 16)) : Required
*/

enum kHash2Size = (1 << 10);
enum kHash3Size = (1 << 16);
// #define kHash4Size (1 << 20)

enum kFix3HashSize = (kHash2Size);
enum kFix4HashSize = (kHash2Size + kHash3Size);
// #define kFix5HashSize (kHash2Size + kHash3Size + kHash4Size)

/*
  We use up to 3 crc values for hash:
    crc0
    crc1 << Shift_1
    crc2 << Shift_2
  (Shift_1 = 5) and (Shift_2 = 10) is good tradeoff.
  Small values for Shift are not good for collision rate.
  Big value for Shift_2 increases the minimum size
  of hash table, that will be slow for small files.
*/

enum kLzHash_CrcShift_1 = 5;
enum kLzHash_CrcShift_2 = 10;



enum kBlockMoveAlign      = (1 << 7);   // alignment for memmove()
enum kBlockSizeAlign      = (1 << 16);  // alignment for block allocation
enum kBlockSizeReserveMin = (1 << 24);  // it's 1/256 from 4 GB dictinary

enum kEmptyHashValue = 0;

enum kMaxValForNormalize = cast(uint)0;
// #define kMaxValForNormalize ((uint)(1 << 20) + 0xFFF) // for debug

// #define kNormalizeAlign (1 << 7) // alignment for speculated accesses


// #define kFix5HashSize (kHash2Size + kHash3Size + kHash4Size)
enum kFix5HashSize = kFix4HashSize;

// (crc[0 ... 255] & 0xFF) provides one-to-one correspondence to [0 ... 255]

/*
 HASH3_CALC:
   if (cur[0]) and (h2) match, then cur[1]            also match
   if (cur[0]) and (hv) match, then cur[1] and cur[2] also match
*/
enum HASH3_CALC = q{
  uint temp = p.crc[cur[0]] ^ cur[1];
  h2 = temp & (kHash2Size - 1);
  hv = (temp ^ (cast(uint)cur[2] << 8)) & p.hashMask;
};

enum HASH4_CALC = q{
  uint temp = p.crc[cur[0]] ^ cur[1];
  h2 = temp & (kHash2Size - 1);
  temp ^= (cast(uint)cur[2] << 8);
  h3 = temp & (kHash3Size - 1);
  hv = (temp ^ (p.crc[cur[3]] << kLzHash_CrcShift_1)) & p.hashMask;
};

enum HASH5_CALC = q{
  uint temp = p.crc[cur[0]] ^ cur[1];
  h2 = temp & (kHash2Size - 1);
  temp ^= (cast(uint)cur[2] << 8);
  h3 = temp & (kHash3Size - 1);
  temp ^= (p.crc[cur[3]] << kLzHash_CrcShift_1);
  /* h4 = temp & p->hash4Mask; */ /* (kHash4Size - 1); */
  hv = (temp ^ (p.crc[cur[4]] << kLzHash_CrcShift_2)) & p.hashMask;
};

enum HASH_ZIP_CALC = `hv = ((cur[2] | (cast(uint)cur[0] << 8)) ^ p.crc[cur[1]]) & 0xFFFF;`;


private void LzInWindow_Free(CMatchFinder *p, ISzAllocPtr alloc)
{
  if (!p.directInput)
  {
    ISzAlloc_Free(alloc, p.bufferBase);
    p.bufferBase = null;
  }
}


private int LzInWindow_Create2(CMatchFinder *p, uint blockSize, ISzAllocPtr alloc)
{
  if (blockSize == 0)
    return 0;
  if (!p.bufferBase || p.blockSize != blockSize)
  {
    // usize blockSizeT;
    LzInWindow_Free(p, alloc);
    p.blockSize = blockSize;
    // blockSizeT = blockSize;

    // printf("\nblockSize = 0x%x\n", blockSize);
    /*
    #if defined _WIN64
    // we can allocate 4GiB, but still use uint for (p->blockSize)
    // we use uint type for (p->blockSize), because
    // we don't want to wrap over 4 GiB,
    // when we use (p->streamPos - p->pos) that is uint.
    if (blockSize >= (uint)0 - (uint)kBlockSizeAlign)
    {
      blockSizeT = ((usize)1 << 32);
      printf("\nchanged to blockSizeT = 4GiB\n");
    }
    #endif
    */

    p.bufferBase = cast(ubyte *)ISzAlloc_Alloc(alloc, blockSize);
    // printf("\nbufferBase = %p\n", p->bufferBase);
    // return 0; // for debug
  }
  return (p.bufferBase != null);
}

private const(ubyte)* MatchFinder_GetPointerToCurrentPos(CMatchFinder *p) { pragma(inline, true); return p.buffer; }

private uint MatchFinder_GetNumAvailableBytes(CMatchFinder *p) { pragma(inline, true); return mixin(Inline_MatchFinder_GetNumAvailableBytes!"p"); }


//MY_NO_INLINE
private void MatchFinder_ReadBlock(CMatchFinder *p)
{
  if (p.streamEndWasReached || p.result != SZ_OK)
    return;

  /* We use (p->streamPos - p->pos) value.
     (p->streamPos < p->pos) is allowed. */

  if (p.directInput)
  {
    uint curSize = 0xFFFFFFFF - mixin(Inline_MatchFinder_GetNumAvailableBytes!"p");
    if (curSize > p.directInputRem)
      curSize = cast(uint)p.directInputRem;
    p.directInputRem -= curSize;
    p.streamPos += curSize;
    if (p.directInputRem == 0)
      p.streamEndWasReached = 1;
    return;
  }

  for (;;)
  {
    ubyte *dest = p.buffer + mixin(Inline_MatchFinder_GetNumAvailableBytes!"p");
    usize size = cast(usize)(p.bufferBase + p.blockSize - dest);
    if (size == 0)
    {
      /* we call ReadBlock() after NeedMove() and MoveBlock().
         NeedMove() and MoveBlock() povide more than (keepSizeAfter)
         to the end of (blockSize).
         So we don't execute this branch in normal code flow.
         We can go here, if we will call ReadBlock() before NeedMove(), MoveBlock().
      */
      // p->result = SZ_ERROR_FAIL; // we can show error here
      return;
    }

    // #define kRead 3
    // if (size > kRead) size = kRead; // for debug

    //p.result = ISeqInStream_Read(p.stream, dest, &size);
    p.result = p.stream.Read(p.stream, dest, &size);
    if (p.result != SZ_OK)
      return;
    if (size == 0)
    {
      p.streamEndWasReached = 1;
      return;
    }
    p.streamPos += cast(uint)size;
    if (mixin(Inline_MatchFinder_GetNumAvailableBytes!"p") > p.keepSizeAfter)
      return;
    /* here and in another (p->keepSizeAfter) checks we keep on 1 byte more than was requested by Create() function
         (Inline_MatchFinder_GetNumAvailableBytes(p) >= p->keepSizeAfter) - minimal required size */
  }

  // on exit: (p->result != SZ_OK || p->streamEndWasReached || Inline_MatchFinder_GetNumAvailableBytes(p) > p->keepSizeAfter)
}



//MY_NO_INLINE
private void MatchFinder_MoveBlock (CMatchFinder *p) @nogc {
  import core.stdc.string : memmove;
  const usize offset = cast(usize)(p.buffer - p.bufferBase) - p.keepSizeBefore;
  const usize keepBefore = (offset & (kBlockMoveAlign - 1)) + p.keepSizeBefore;
  p.buffer = p.bufferBase + keepBefore;
  memmove(p.bufferBase,
      p.bufferBase + (offset & ~(cast(usize)kBlockMoveAlign - 1)),
      keepBefore + cast(usize)mixin(Inline_MatchFinder_GetNumAvailableBytes!"p"));
}

/* We call MoveBlock() before ReadBlock().
   So MoveBlock() can be wasteful operation, if the whole input data
   can fit in current block even without calling MoveBlock().
   in important case where (dataSize <= historySize)
     condition (p->blockSize > dataSize + p->keepSizeAfter) is met
     So there is no MoveBlock() in that case case.
*/

private int MatchFinder_NeedMove (CMatchFinder *p) @nogc {
  if (p.directInput)
    return 0;
  if (p.streamEndWasReached || p.result != SZ_OK)
    return 0;
  return (cast(usize)(p.bufferBase + p.blockSize - p.buffer) <= p.keepSizeAfter);
}

private void MatchFinder_ReadIfRequired (CMatchFinder *p) {
  if (p.keepSizeAfter >= mixin(Inline_MatchFinder_GetNumAvailableBytes!"p"))
    MatchFinder_ReadBlock(p);
}


private void MatchFinder_SetDefaultSettings (CMatchFinder* p) @nogc {
  p.cutValue = 32;
  p.btMode = 1;
  p.numHashBytes = 4;
  p.bigHash = 0;
}

enum kCrcPoly = 0xEDB88320;

private void MatchFinder_Construct (CMatchFinder* p) @nogc {
  uint i;
  p.bufferBase = null;
  p.directInput = 0;
  p.hash = null;
  p.expectedDataSize = cast(ulong)cast(long)-1;
  MatchFinder_SetDefaultSettings(p);

  for (i = 0; i < 256; i++)
  {
    uint r = cast(uint)i;
    uint j;
    for (j = 0; j < 8; j++)
      r = (r >> 1) ^ (kCrcPoly & (cast(uint)0 - (r & 1)));
    p.crc[i] = r;
  }
}

private void MatchFinder_FreeThisClassMemory(CMatchFinder *p, ISzAllocPtr alloc)
{
  ISzAlloc_Free(alloc, p.hash);
  p.hash = null;
}

private void MatchFinder_Free(CMatchFinder *p, ISzAllocPtr alloc)
{
  MatchFinder_FreeThisClassMemory(p, alloc);
  LzInWindow_Free(p, alloc);
}

private CLzRef* AllocRefs(usize num, ISzAllocPtr alloc)
{
  usize sizeInBytes = cast(usize)num * CLzRef.sizeof;
  if (sizeInBytes / CLzRef.sizeof != num)
    return null;
  return cast(CLzRef *)ISzAlloc_Alloc(alloc, sizeInBytes);
}

static assert(kBlockSizeReserveMin >= kBlockSizeAlign * 2, "Stop_Compiling_Bad_Reserve");



private uint GetBlockSize (CMatchFinder* p, uint historySize) @nogc {
  uint blockSize = (p.keepSizeBefore + p.keepSizeAfter);
  /*
  if (historySize > kMaxHistorySize)
    return 0;
  */
  // printf("\nhistorySize == 0x%x\n", historySize);

  if (p.keepSizeBefore < historySize || blockSize < p.keepSizeBefore)  // if 32-bit overflow
    return 0;

  {
    const uint kBlockSizeMax = cast(uint)0 - cast(uint)kBlockSizeAlign;
    const uint rem = kBlockSizeMax - blockSize;
    const uint reserve = (blockSize >> (blockSize < (cast(uint)1 << 30) ? 1 : 2))
        + (1 << 12) + kBlockMoveAlign + kBlockSizeAlign; // do not overflow 32-bit here
    if (blockSize >= kBlockSizeMax
        || rem < kBlockSizeReserveMin) // we reject settings that will be slow
      return 0;
    if (reserve >= rem)
      blockSize = kBlockSizeMax;
    else
    {
      blockSize += reserve;
      blockSize &= ~cast(uint)(kBlockSizeAlign - 1);
    }
  }
  // printf("\n LzFind_blockSize = %x\n", blockSize);
  // printf("\n LzFind_blockSize = %d\n", blockSize >> 20);
  return blockSize;
}


private int MatchFinder_Create(CMatchFinder *p, uint historySize,
    uint keepAddBufferBefore, uint matchMaxLen, uint keepAddBufferAfter,
    ISzAllocPtr alloc)
{
  /* we need one additional byte in (p->keepSizeBefore),
     since we use MoveBlock() after (p->pos++) and before dictionary using */
  // keepAddBufferBefore = (uint)0xFFFFFFFF - (1 << 22); // for debug
  p.keepSizeBefore = historySize + keepAddBufferBefore + 1;

  keepAddBufferAfter += matchMaxLen;
  /* we need (p->keepSizeAfter >= p->numHashBytes) */
  if (keepAddBufferAfter < p.numHashBytes)
    keepAddBufferAfter = p.numHashBytes;
  // keepAddBufferAfter -= 2; // for debug
  p.keepSizeAfter = keepAddBufferAfter;

  if (p.directInput)
    p.blockSize = 0;
  if (p.directInput || LzInWindow_Create2(p, GetBlockSize(p, historySize), alloc))
  {
    const uint newCyclicBufferSize = historySize + 1; // do not change it
    uint hs;
    p.matchMaxLen = matchMaxLen;
    {
      // uint hs4;
      p.fixedHashSize = 0;
      hs = (1 << 16) - 1;
      if (p.numHashBytes != 2)
      {
        hs = historySize;
        if (hs > p.expectedDataSize)
          hs = cast(uint)p.expectedDataSize;
        if (hs != 0)
          hs--;
        hs |= (hs >> 1);
        hs |= (hs >> 2);
        hs |= (hs >> 4);
        hs |= (hs >> 8);
        // we propagated 16 bits in (hs). Low 16 bits must be set later
        hs >>= 1;
        if (hs >= (1 << 24))
        {
          if (p.numHashBytes == 3)
            hs = (1 << 24) - 1;
          else
            hs >>= 1;
          /* if (bigHash) mode, GetHeads4b() in LzFindMt.c needs (hs >= ((1 << 24) - 1))) */
        }

        // hs = ((uint)1 << 25) - 1; // for test

        // (hash_size >= (1 << 16)) : Required for (numHashBytes > 2)
        hs |= (1 << 16) - 1; /* don't change it! */

        // bt5: we adjust the size with recommended minimum size
        if (p.numHashBytes >= 5)
          hs |= (256 << kLzHash_CrcShift_2) - 1;
      }
      p.hashMask = hs;
      hs++;

      /*
      hs4 = (1 << 20);
      if (hs4 > hs)
        hs4 = hs;
      // hs4 = (1 << 16); // for test
      p->hash4Mask = hs4 - 1;
      */

      if (p.numHashBytes > 2) p.fixedHashSize += kHash2Size;
      if (p.numHashBytes > 3) p.fixedHashSize += kHash3Size;
      // if (p->numHashBytes > 4) p->fixedHashSize += hs4; // kHash4Size;
      hs += p.fixedHashSize;
    }

    {
      usize newSize;
      usize numSons;
      p.historySize = historySize;
      p.hashSizeSum = hs;
      p.cyclicBufferSize = newCyclicBufferSize; // it must be = (historySize + 1)

      numSons = newCyclicBufferSize;
      if (p.btMode)
        numSons <<= 1;
      newSize = hs + numSons;

      // aligned size is not required here, but it can be better for some loops
      enum NUM_REFS_ALIGN_MASK = 0xF;
      newSize = (newSize + NUM_REFS_ALIGN_MASK) & ~cast(usize)NUM_REFS_ALIGN_MASK;

      if (p.hash && p.numRefs == newSize)
        return 1;

      MatchFinder_FreeThisClassMemory(p, alloc);
      p.numRefs = newSize;
      p.hash = AllocRefs(newSize, alloc);

      if (p.hash)
      {
        p.son = p.hash + p.hashSizeSum;
        return 1;
      }
    }
  }

  MatchFinder_Free(p, alloc);
  return 0;
}


private void MatchFinder_SetLimits (CMatchFinder* p) @nogc {
  uint k;
  uint n = kMaxValForNormalize - p.pos;
  if (n == 0)
    n = cast(uint)cast(int)-1;  // we allow (pos == 0) at start even with (kMaxValForNormalize == 0)

  k = p.cyclicBufferSize - p.cyclicBufferPos;
  if (k < n)
    n = k;

  k = mixin(Inline_MatchFinder_GetNumAvailableBytes!"p");
  {
    const uint ksa = p.keepSizeAfter;
    uint mm = p.matchMaxLen;
    if (k > ksa)
      k -= ksa; // we must limit exactly to keepSizeAfter for ReadBlock
    else if (k >= mm)
    {
      // the limitation for (p->lenLimit) update
      k -= mm;   // optimization : to reduce the number of checks
      k++;
      // k = 1; // non-optimized version : for debug
    }
    else
    {
      mm = k;
      if (k != 0)
        k = 1;
    }
    p.lenLimit = mm;
  }
  if (k < n)
    n = k;

  p.posLimit = p.pos + n;
}


private void MatchFinder_Init_LowHash (CMatchFinder* p) @nogc {
  usize i;
  CLzRef *items = p.hash;
  const usize numItems = p.fixedHashSize;
  for (i = 0; i < numItems; i++)
    items[i] = kEmptyHashValue;
}


private void MatchFinder_Init_HighHash (CMatchFinder* p) @nogc {
  usize i;
  CLzRef *items = p.hash + p.fixedHashSize;
  const usize numItems = cast(usize)p.hashMask + 1;
  for (i = 0; i < numItems; i++)
    items[i] = kEmptyHashValue;
}


private void MatchFinder_Init_4 (CMatchFinder* p) @nogc {
  p.buffer = p.bufferBase;
  {
    /* kEmptyHashValue = 0 (Zero) is used in hash tables as NO-VALUE marker.
       the code in CMatchFinderMt expects (pos = 1) */
    p.pos =
    p.streamPos =
        1; // it's smallest optimal value. do not change it
        // 0; // for debug
  }
  p.result = SZ_OK;
  p.streamEndWasReached = 0;
}


// (CYC_TO_POS_OFFSET == 0) is expected by some optimized code
enum CYC_TO_POS_OFFSET = 0;
// #define CYC_TO_POS_OFFSET 1 // for debug

private void MatchFinder_Init (CMatchFinder* p) {
  MatchFinder_Init_HighHash(p);
  MatchFinder_Init_LowHash(p);
  MatchFinder_Init_4(p);
  // if (readData)
  MatchFinder_ReadBlock(p);

  /* if we init (cyclicBufferPos = pos), then we can use one variable
     instead of both (cyclicBufferPos) and (pos) : only before (cyclicBufferPos) wrapping */
  p.cyclicBufferPos = (p.pos - CYC_TO_POS_OFFSET); // init with relation to (pos)
  // p->cyclicBufferPos = 0; // smallest value
  // p->son[0] = p->son[1] = 0; // unused: we can init skipped record for speculated accesses.
  MatchFinder_SetLimits(p);
}



// kEmptyHashValue must be zero
// #define SASUB_32(i) v = items[i];  m = v - subValue;  if (v < subValue) m = kEmptyHashValue;  items[i] = m;
//#define SASUB_32(i) v = items[i];  if (v < subValue) v = subValue; items[i] = v - subValue;
enum SASUB_32(string i) = `v = items[`~i~`];  if (v < subValue) v = subValue; items[`~i~`] = v - subValue;`;

//MY_NO_INLINE
private void LzFind_SaturSub_32 (uint subValue, CLzRef* items, const(CLzRef)* lim) @nogc {
  do
  {
    uint v;
    mixin(SASUB_32!"0");
    mixin(SASUB_32!"1");
    mixin(SASUB_32!"2");
    mixin(SASUB_32!"3");
    mixin(SASUB_32!"4");
    mixin(SASUB_32!"5");
    mixin(SASUB_32!"6");
    mixin(SASUB_32!"7");
    items += 8;
  }
  while (items != lim);
}


//MY_NO_INLINE
private void MatchFinder_Normalize3 (uint subValue, CLzRef* items, usize numItems) @nogc {
  enum K_NORM_ALIGN_BLOCK_SIZE = (1 << 6);

  CLzRef *lim;

  for (; numItems != 0 && (cast(uint)cast(ptrdiff_t)items & (K_NORM_ALIGN_BLOCK_SIZE - 1)) != 0; numItems--)
  {
    uint v;
    mixin(SASUB_32!"0");
    items++;
  }

  {
    enum K_NORM_ALIGN_MASK = (K_NORM_ALIGN_BLOCK_SIZE / 4 - 1);
    lim = items + (numItems & ~cast(usize)K_NORM_ALIGN_MASK);
    numItems &= K_NORM_ALIGN_MASK;
    if (items != lim)
    {
      LzFind_SaturSub_32(subValue, items, lim);
    }
    items = lim;
  }


  for (; numItems != 0; numItems--)
  {
    uint v;
    mixin(SASUB_32!"0");
    items++;
  }
}



// call MatchFinder_CheckLimits() only after (p->pos++) update

//MY_NO_INLINE
private void MatchFinder_CheckLimits (CMatchFinder* p) {
  if (// !p->streamEndWasReached && p->result == SZ_OK &&
      p.keepSizeAfter == mixin(Inline_MatchFinder_GetNumAvailableBytes!"p"))
  {
    // we try to read only in exact state (p->keepSizeAfter == Inline_MatchFinder_GetNumAvailableBytes(p))
    if (MatchFinder_NeedMove(p))
      MatchFinder_MoveBlock(p);
    MatchFinder_ReadBlock(p);
  }

  if (p.pos == kMaxValForNormalize)
  if (mixin(Inline_MatchFinder_GetNumAvailableBytes!"p") >= p.numHashBytes) // optional optimization for last bytes of data.
    /*
       if we disable normalization for last bytes of data, and
       if (data_size == 4 GiB), we don't call wastfull normalization,
       but (pos) will be wrapped over Zero (0) in that case.
       And we cannot resume later to normal operation
    */
  {
    // MatchFinder_Normalize(p);
    /* after normalization we need (p->pos >= p->historySize + 1); */
    /* we can reduce subValue to aligned value, if want to keep alignment
       of (p->pos) and (p->buffer) for speculated accesses. */
    const uint subValue = (p.pos - p.historySize - 1) /* & ~(uint)(kNormalizeAlign - 1) */;
    // const uint subValue = (1 << 15); // for debug
    // printf("\nMatchFinder_Normalize() subValue == 0x%x\n", subValue);
    usize numSonRefs = p.cyclicBufferSize;
    if (p.btMode)
      numSonRefs <<= 1;

    //Inline_MatchFinder_ReduceOffsets(p, subValue);
    p.pos -= subValue;
    p.streamPos -= subValue;

    MatchFinder_Normalize3(subValue, p.hash, cast(usize)p.hashSizeSum + numSonRefs);
  }

  if (p.cyclicBufferPos == p.cyclicBufferSize)
    p.cyclicBufferPos = 0;

  MatchFinder_SetLimits(p);
}


/*
  (lenLimit > maxLen)
*/
//MY_FORCE_INLINE
private uint * Hc_GetMatchesSpec (usize lenLimit, uint curMatch, uint pos, const(ubyte)* cur, CLzRef *son,
    usize _cyclicBufferPos, uint _cyclicBufferSize, uint cutValue,
    uint *d, uint maxLen) @nogc
{
  /*
  son[_cyclicBufferPos] = curMatch;
  for (;;)
  {
    uint delta = pos - curMatch;
    if (cutValue-- == 0 || delta >= _cyclicBufferSize)
      return d;
    {
      const ubyte *pb = cur - delta;
      curMatch = son[_cyclicBufferPos - delta + ((delta > _cyclicBufferPos) ? _cyclicBufferSize : 0)];
      if (pb[maxLen] == cur[maxLen] && *pb == *cur)
      {
        uint len = 0;
        while (++len != lenLimit)
          if (pb[len] != cur[len])
            break;
        if (maxLen < len)
        {
          maxLen = len;
          *d++ = len;
          *d++ = delta - 1;
          if (len == lenLimit)
            return d;
        }
      }
    }
  }
  */

  const(ubyte)* lim = cur + lenLimit;
  son[_cyclicBufferPos] = curMatch;

  do
  {
    uint delta;

    if (curMatch == 0)
      break;
    // if (curMatch2 >= curMatch) return null;
    delta = pos - curMatch;
    if (delta >= _cyclicBufferSize)
      break;
    {
      ptrdiff_t diff;
      curMatch = son[_cyclicBufferPos - delta + ((delta > _cyclicBufferPos) ? _cyclicBufferSize : 0)];
      diff = cast(ptrdiff_t)0 - cast(ptrdiff_t)delta;
      if (cur[maxLen] == cur[cast(ptrdiff_t)maxLen + diff])
      {
        const(ubyte)* c = cur;
        while (*c == c[diff])
        {
          if (++c == lim)
          {
            d[0] = cast(uint)(lim - cur);
            d[1] = delta - 1;
            return d + 2;
          }
        }
        {
          const uint len = cast(uint)(c - cur);
          if (maxLen < len)
          {
            maxLen = len;
            d[0] = cast(uint)len;
            d[1] = delta - 1;
            d += 2;
          }
        }
      }
    }
  }
  while (--cutValue);

  return d;
}


//MY_FORCE_INLINE
private uint* GetMatchesSpec1 (uint lenLimit, uint curMatch, uint pos, const(ubyte)* cur, CLzRef *son,
    usize _cyclicBufferPos, uint _cyclicBufferSize, uint cutValue,
    uint *d, uint maxLen) @nogc
{
  CLzRef *ptr0 = son + (cast(usize)_cyclicBufferPos << 1) + 1;
  CLzRef *ptr1 = son + (cast(usize)_cyclicBufferPos << 1);
  uint len0 = 0, len1 = 0;

  uint cmCheck;

  // if (curMatch >= pos) { *ptr0 = *ptr1 = kEmptyHashValue; return null; }

  cmCheck = cast(uint)(pos - _cyclicBufferSize);
  if (cast(uint)pos <= _cyclicBufferSize)
    cmCheck = 0;

  if (cmCheck < curMatch)
  do
  {
    const uint delta = pos - curMatch;
    {
      CLzRef *pair = son + (cast(usize)(_cyclicBufferPos - delta + ((delta > _cyclicBufferPos) ? _cyclicBufferSize : 0)) << 1);
      const ubyte *pb = cur - delta;
      uint len = (len0 < len1 ? len0 : len1);
      const uint pair0 = pair[0];
      if (pb[len] == cur[len])
      {
        if (++len != lenLimit && pb[len] == cur[len])
          while (++len != lenLimit)
            if (pb[len] != cur[len])
              break;
        if (maxLen < len)
        {
          maxLen = cast(uint)len;
          *d++ = cast(uint)len;
          *d++ = delta - 1;
          if (len == lenLimit)
          {
            *ptr1 = pair0;
            *ptr0 = pair[1];
            return d;
          }
        }
      }
      if (pb[len] < cur[len])
      {
        *ptr1 = curMatch;
        // const uint curMatch2 = pair[1];
        // if (curMatch2 >= curMatch) { *ptr0 = *ptr1 = kEmptyHashValue;  return null; }
        // curMatch = curMatch2;
        curMatch = pair[1];
        ptr1 = pair + 1;
        len1 = len;
      }
      else
      {
        *ptr0 = curMatch;
        curMatch = pair[0];
        ptr0 = pair;
        len0 = len;
      }
    }
  }
  while(--cutValue && cmCheck < curMatch);

  *ptr0 = *ptr1 = kEmptyHashValue;
  return d;
}


private void SkipMatchesSpec (uint lenLimit, uint curMatch, uint pos, const(ubyte)* cur, CLzRef *son,
    usize _cyclicBufferPos, uint _cyclicBufferSize, uint cutValue) @nogc
{
  CLzRef *ptr0 = son + (cast(usize)_cyclicBufferPos << 1) + 1;
  CLzRef *ptr1 = son + (cast(usize)_cyclicBufferPos << 1);
  uint len0 = 0, len1 = 0;

  uint cmCheck;

  cmCheck = cast(uint)(pos - _cyclicBufferSize);
  if (cast(uint)pos <= _cyclicBufferSize)
    cmCheck = 0;

  if (// curMatch >= pos ||  // failure
      cmCheck < curMatch)
  do
  {
    const uint delta = pos - curMatch;
    {
      CLzRef *pair = son + (cast(usize)(_cyclicBufferPos - delta + ((delta > _cyclicBufferPos) ? _cyclicBufferSize : 0)) << 1);
      const ubyte *pb = cur - delta;
      uint len = (len0 < len1 ? len0 : len1);
      if (pb[len] == cur[len])
      {
        while (++len != lenLimit)
          if (pb[len] != cur[len])
            break;
        {
          if (len == lenLimit)
          {
            *ptr1 = pair[0];
            *ptr0 = pair[1];
            return;
          }
        }
      }
      if (pb[len] < cur[len])
      {
        *ptr1 = curMatch;
        curMatch = pair[1];
        ptr1 = pair + 1;
        len1 = len;
      }
      else
      {
        *ptr0 = curMatch;
        curMatch = pair[0];
        ptr0 = pair;
        len0 = len;
      }
    }
  }
  while(--cutValue && cmCheck < curMatch);

  *ptr0 = *ptr1 = kEmptyHashValue;
  return;
}


enum MOVE_POS = q{
  ++p.cyclicBufferPos;
  p.buffer++;
  { const uint pos1 = p.pos + 1; p.pos = pos1; if (pos1 == p.posLimit) MatchFinder_CheckLimits(p); }
};

enum MOVE_POS_RET = MOVE_POS~`return distances;`;

//MY_NO_INLINE
private void MatchFinder_MovePos (CMatchFinder *p) {
  pragma(inline, true);
  /* we go here at the end of stream data, when (avail < num_hash_bytes)
     We don't update sons[cyclicBufferPos << btMode].
     So (sons) record will contain junk. And we cannot resume match searching
     to normal operation, even if we will provide more input data in buffer.
     p->sons[p->cyclicBufferPos << p->btMode] = 0;  // kEmptyHashValue
     if (p->btMode)
        p->sons[(p->cyclicBufferPos << p->btMode) + 1] = 0;  // kEmptyHashValue
  */
  mixin(MOVE_POS);
}

enum GET_MATCHES_HEADER2(string minLen, string ret_op) = `
  uint lenLimit; uint hv; ubyte *cur; uint curMatch;
  lenLimit = cast(uint)p.lenLimit; { if (lenLimit < `~minLen~`) { MatchFinder_MovePos(p); `~ret_op~`; } }
  cur = p.buffer;
`;

enum GET_MATCHES_HEADER(string minLen) = GET_MATCHES_HEADER2!(minLen, "return distances");
//enum SKIP_HEADER(string minLen) = `do { `~GET_MATCHES_HEADER2!(minLen, "continue");
enum SKIP_HEADER(string minLen) = GET_MATCHES_HEADER2!(minLen, "continue");

enum MF_PARAMS(string p) = `lenLimit, curMatch, `~p~`.pos, `~p~`.buffer, `~p~`.son, `~p~`.cyclicBufferPos, `~p~`.cyclicBufferSize, `~p~`.cutValue`;

//#define SKIP_FOOTER  SkipMatchesSpec(MF_PARAMS(p)); mixin(MOVE_POS); } while (--num);
enum SKIP_FOOTER = `SkipMatchesSpec(`~MF_PARAMS!"p"~`);`~MOVE_POS;


enum GET_MATCHES_FOOTER_BASE(string _maxLen_, string func) = `
  distances = `~func~`(`~MF_PARAMS!"p"~`, distances, cast(uint)`~_maxLen_~`);`~MOVE_POS_RET;

enum GET_MATCHES_FOOTER_BT(string _maxLen_) = GET_MATCHES_FOOTER_BASE!(_maxLen_, "GetMatchesSpec1");

enum GET_MATCHES_FOOTER_HC(string _maxLen_) = GET_MATCHES_FOOTER_BASE!(_maxLen_, "Hc_GetMatchesSpec");



enum UPDATE_maxLen = q{
  const ptrdiff_t diff = cast(ptrdiff_t)0 - cast(ptrdiff_t)d2;
  const(ubyte)* c = cur + maxLen;
  const(ubyte)* lim = cur + lenLimit;
  for (; c != lim; c++) if (*(c + diff) != *c) break;
  maxLen = cast(uint)(c - cur);
};


private uint* Bt2_MatchFinder_GetMatches (CMatchFinder* p, uint* distances) {
  mixin(GET_MATCHES_HEADER!"2");
  hv = GetUi16(cur);
  curMatch = p.hash[hv];
  p.hash[hv] = p.pos;
  mixin(GET_MATCHES_FOOTER_BT!"1");
}

private uint* Bt3Zip_MatchFinder_GetMatches (CMatchFinder* p, uint* distances) {
  mixin(GET_MATCHES_HEADER!"3");
  mixin(HASH_ZIP_CALC);
  curMatch = p.hash[hv];
  p.hash[hv] = p.pos;
  mixin(GET_MATCHES_FOOTER_BT!"2");
}


enum SET_mmm = q{
  mmm = p.cyclicBufferSize;
  if (pos < mmm) mmm = pos;
};


private uint* Bt3_MatchFinder_GetMatches (CMatchFinder* p, uint* distances) {
  uint mmm;
  uint h2, d2, pos;
  uint maxLen;
  uint *hash;
  mixin(GET_MATCHES_HEADER!"3");

  mixin(HASH3_CALC);

  hash = p.hash;
  pos = p.pos;

  d2 = pos - hash[h2];

  curMatch = (hash + kFix3HashSize)[hv];

  hash[h2] = pos;
  (hash + kFix3HashSize)[hv] = pos;

  mixin(SET_mmm);

  maxLen = 2;

  if (d2 < mmm && *(cur - d2) == *cur)
  {
    mixin(UPDATE_maxLen);
    distances[0] = cast(uint)maxLen;
    distances[1] = d2 - 1;
    distances += 2;
    if (maxLen == lenLimit)
    {
      mixin(`SkipMatchesSpec(`~MF_PARAMS!"p"~`);`);
      mixin(MOVE_POS_RET);
    }
  }

  mixin(GET_MATCHES_FOOTER_BT!"maxLen");
}


private uint* Bt4_MatchFinder_GetMatches (CMatchFinder* p, uint* distances) {
  uint mmm;
  uint h2, h3, d2, d3, pos;
  uint maxLen;
  uint *hash;
  mixin(GET_MATCHES_HEADER!"4");

  mixin(HASH4_CALC);

  hash = p.hash;
  pos = p.pos;

  d2 = pos - hash                  [h2];
  d3 = pos - (hash + kFix3HashSize)[h3];
  curMatch = (hash + kFix4HashSize)[hv];

  hash                  [h2] = pos;
  (hash + kFix3HashSize)[h3] = pos;
  (hash + kFix4HashSize)[hv] = pos;

  mixin(SET_mmm);

  maxLen = 3;

  for (;;)
  {
    if (d2 < mmm && *(cur - d2) == *cur)
    {
      distances[0] = 2;
      distances[1] = d2 - 1;
      distances += 2;
      if (*(cur - d2 + 2) == cur[2])
      {
        // distances[-2] = 3;
      }
      else if (d3 < mmm && *(cur - d3) == *cur)
      {
        d2 = d3;
        distances[1] = d3 - 1;
        distances += 2;
      }
      else
        break;
    }
    else if (d3 < mmm && *(cur - d3) == *cur)
    {
      d2 = d3;
      distances[1] = d3 - 1;
      distances += 2;
    }
    else
      break;

    mixin(UPDATE_maxLen);
    distances[-2] = cast(uint)maxLen;
    if (maxLen == lenLimit)
    {
      mixin(`SkipMatchesSpec(`~MF_PARAMS!"p"~`);`);
      mixin(MOVE_POS_RET);
    }
    break;
  }

  mixin(GET_MATCHES_FOOTER_BT!"maxLen");
}


private uint* Bt5_MatchFinder_GetMatches (CMatchFinder* p, uint* distances) {
  uint mmm;
  uint h2, h3, d2, d3, maxLen, pos;
  uint *hash;
  mixin(GET_MATCHES_HEADER!"5");

  mixin(HASH5_CALC);

  hash = p.hash;
  pos = p.pos;

  d2 = pos - hash                  [h2];
  d3 = pos - (hash + kFix3HashSize)[h3];
  // d4 = pos - (hash + kFix4HashSize)[h4];

  curMatch = (hash + kFix5HashSize)[hv];

  hash                  [h2] = pos;
  (hash + kFix3HashSize)[h3] = pos;
  // (hash + kFix4HashSize)[h4] = pos;
  (hash + kFix5HashSize)[hv] = pos;

  mixin(SET_mmm);

  maxLen = 4;

  for (;;)
  {
    if (d2 < mmm && *(cur - d2) == *cur)
    {
      distances[0] = 2;
      distances[1] = d2 - 1;
      distances += 2;
      if (*(cur - d2 + 2) == cur[2])
      {
      }
      else if (d3 < mmm && *(cur - d3) == *cur)
      {
        distances[1] = d3 - 1;
        distances += 2;
        d2 = d3;
      }
      else
        break;
    }
    else if (d3 < mmm && *(cur - d3) == *cur)
    {
      distances[1] = d3 - 1;
      distances += 2;
      d2 = d3;
    }
    else
      break;

    distances[-2] = 3;
    if (*(cur - d2 + 3) != cur[3])
      break;
    mixin(UPDATE_maxLen);
    distances[-2] = cast(uint)maxLen;
    if (maxLen == lenLimit)
    {
      mixin(`SkipMatchesSpec(`~MF_PARAMS!"p"~`);`);
      mixin(MOVE_POS_RET);
    }
    break;
  }

  mixin(GET_MATCHES_FOOTER_BT!"maxLen");
}


private uint* Hc4_MatchFinder_GetMatches (CMatchFinder* p, uint* distances) {
  uint mmm;
  uint h2, h3, d2, d3, pos;
  uint maxLen;
  uint *hash;
  mixin(GET_MATCHES_HEADER!"4");

  mixin(HASH4_CALC);

  hash = p.hash;
  pos = p.pos;

  d2 = pos - hash                  [h2];
  d3 = pos - (hash + kFix3HashSize)[h3];
  curMatch = (hash + kFix4HashSize)[hv];

  hash                  [h2] = pos;
  (hash + kFix3HashSize)[h3] = pos;
  (hash + kFix4HashSize)[hv] = pos;

  mixin(SET_mmm);

  maxLen = 3;

  for (;;)
  {
    if (d2 < mmm && *(cur - d2) == *cur)
    {
      distances[0] = 2;
      distances[1] = d2 - 1;
      distances += 2;
      if (*(cur - d2 + 2) == cur[2])
      {
        // distances[-2] = 3;
      }
      else if (d3 < mmm && *(cur - d3) == *cur)
      {
        d2 = d3;
        distances[1] = d3 - 1;
        distances += 2;
      }
      else
        break;
    }
    else if (d3 < mmm && *(cur - d3) == *cur)
    {
      d2 = d3;
      distances[1] = d3 - 1;
      distances += 2;
    }
    else
      break;

    mixin(UPDATE_maxLen);
    distances[-2] = cast(uint)maxLen;
    if (maxLen == lenLimit)
    {
      p.son[p.cyclicBufferPos] = curMatch;
      mixin(MOVE_POS_RET);
    }
    break;
  }

  mixin(GET_MATCHES_FOOTER_HC!"maxLen");
}


private uint *Hc5_MatchFinder_GetMatches (CMatchFinder* p, uint* distances) {
  uint mmm;
  uint h2, h3, d2, d3, maxLen, pos;
  uint *hash;
  mixin(GET_MATCHES_HEADER!"5");

  mixin(HASH5_CALC);

  hash = p.hash;
  pos = p.pos;

  d2 = pos - hash                  [h2];
  d3 = pos - (hash + kFix3HashSize)[h3];
  // d4 = pos - (hash + kFix4HashSize)[h4];

  curMatch = (hash + kFix5HashSize)[hv];

  hash                  [h2] = pos;
  (hash + kFix3HashSize)[h3] = pos;
  // (hash + kFix4HashSize)[h4] = pos;
  (hash + kFix5HashSize)[hv] = pos;

  mixin(SET_mmm);

  maxLen = 4;

  for (;;)
  {
    if (d2 < mmm && *(cur - d2) == *cur)
    {
      distances[0] = 2;
      distances[1] = d2 - 1;
      distances += 2;
      if (*(cur - d2 + 2) == cur[2])
      {
      }
      else if (d3 < mmm && *(cur - d3) == *cur)
      {
        distances[1] = d3 - 1;
        distances += 2;
        d2 = d3;
      }
      else
        break;
    }
    else if (d3 < mmm && *(cur - d3) == *cur)
    {
      distances[1] = d3 - 1;
      distances += 2;
      d2 = d3;
    }
    else
      break;

    distances[-2] = 3;
    if (*(cur - d2 + 3) != cur[3])
      break;
    mixin(UPDATE_maxLen);
    distances[-2] = maxLen;
    if (maxLen == lenLimit)
    {
      p.son[p.cyclicBufferPos] = curMatch;
      mixin(MOVE_POS_RET);
    }
    break;
  }

  mixin(GET_MATCHES_FOOTER_HC!"maxLen");
}


private uint* Hc3Zip_MatchFinder_GetMatches (CMatchFinder* p, uint* distances) {
  mixin(GET_MATCHES_HEADER!"3");
  mixin(HASH_ZIP_CALC);
  curMatch = p.hash[hv];
  p.hash[hv] = p.pos;
  mixin(GET_MATCHES_FOOTER_HC!"2");
}


private void Bt2_MatchFinder_Skip (CMatchFinder* p, uint num) {
  do { mixin(SKIP_HEADER!"2");
  {
    hv = GetUi16(cur);
    curMatch = p.hash[hv];
    p.hash[hv] = p.pos;
  }
  mixin(SKIP_FOOTER); } while (--num);
}

private void Bt3Zip_MatchFinder_Skip (CMatchFinder* p, uint num) {
  do { mixin(SKIP_HEADER!"3");
  {
    mixin(HASH_ZIP_CALC);
    curMatch = p.hash[hv];
    p.hash[hv] = p.pos;
  }
  mixin(SKIP_FOOTER); } while (--num);
}

private void Bt3_MatchFinder_Skip (CMatchFinder* p, uint num) {
  do { mixin(SKIP_HEADER!"3");
  {
    uint h2;
    uint *hash;
    mixin(HASH3_CALC);
    hash = p.hash;
    curMatch = (hash + kFix3HashSize)[hv];
    hash[h2] =
    (hash + kFix3HashSize)[hv] = p.pos;
  }
  mixin(SKIP_FOOTER); } while (--num);
}

private void Bt4_MatchFinder_Skip (CMatchFinder* p, uint num) {
  do { mixin(SKIP_HEADER!"4");
  {
    uint h2, h3;
    uint *hash;
    mixin(HASH4_CALC);
    hash = p.hash;
    curMatch = (hash + kFix4HashSize)[hv];
    hash                  [h2] =
    (hash + kFix3HashSize)[h3] =
    (hash + kFix4HashSize)[hv] = p.pos;
  }
  mixin(SKIP_FOOTER); } while (--num);
}

private void Bt5_MatchFinder_Skip (CMatchFinder* p, uint num) {
  do { mixin(SKIP_HEADER!"5");
  {
    uint h2, h3;
    uint *hash;
    mixin(HASH5_CALC);
    hash = p.hash;
    curMatch = (hash + kFix5HashSize)[hv];
    hash                  [h2] =
    (hash + kFix3HashSize)[h3] =
    // (hash + kFix4HashSize)[h4] =
    (hash + kFix5HashSize)[hv] = p.pos;
  }
  mixin(SKIP_FOOTER); } while (--num);
}


private void Hc4_MatchFinder_Skip (CMatchFinder* p, uint num) {
  enum minLenSKH = 4;
    do { if (p.lenLimit < minLenSKH) { MatchFinder_MovePos(p); num--; continue; } {
    ubyte *cur;
    uint *hash;
    uint *son;
    uint pos = p.pos;
    uint num2 = num;
    /* (p->pos == p->posLimit) is not allowed here !!! */
    { const uint rem = p.posLimit - pos; if (num2 > rem) num2 = rem; }
    num -= num2;
    { const uint cycPos = p.cyclicBufferPos;
      son = p.son + cycPos;
      p.cyclicBufferPos = cycPos + num2; }
    cur = p.buffer;
    hash = p.hash;
  do {
    uint curMatch;
    uint hv;

    uint h2, h3;
    mixin(HASH4_CALC);
    curMatch = (hash + kFix4HashSize)[hv];
    hash                  [h2] =
    (hash + kFix3HashSize)[h3] =
    (hash + kFix4HashSize)[hv] = pos;

  //HC_SKIP_FOOTER
    cur++;  pos++;  *son++ = curMatch;
    } while (--num2);
    p.buffer = cur;
    p.pos = pos;
    if (pos == p.posLimit) MatchFinder_CheckLimits(p);
    }} while(num);
}


private void Hc5_MatchFinder_Skip (CMatchFinder* p, uint num) {
  enum minLenSKH = 5;
    do { if (p.lenLimit < minLenSKH) { MatchFinder_MovePos(p); num--; continue; } {
    ubyte *cur;
    uint *hash;
    uint *son;
    uint pos = p.pos;
    uint num2 = num;
    /* (p->pos == p->posLimit) is not allowed here !!! */
    { const uint rem = p.posLimit - pos; if (num2 > rem) num2 = rem; }
    num -= num2;
    { const uint cycPos = p.cyclicBufferPos;
      son = p.son + cycPos;
      p.cyclicBufferPos = cycPos + num2; }
    cur = p.buffer;
    hash = p.hash;
  do {
    uint curMatch;
    uint hv;

    uint h2, h3;
    mixin(HASH5_CALC);
    curMatch = (hash + kFix5HashSize)[hv];
    hash                  [h2] =
    (hash + kFix3HashSize)[h3] =
    // (hash + kFix4HashSize)[h4] =
    (hash + kFix5HashSize)[hv] = pos;

  //HC_SKIP_FOOTER
    cur++;  pos++;  *son++ = curMatch;
    } while (--num2);
    p.buffer = cur;
    p.pos = pos;
    if (pos == p.posLimit) MatchFinder_CheckLimits(p);
    }} while(num);
}


private void Hc3Zip_MatchFinder_Skip (CMatchFinder* p, uint num) {
  enum minLenSKH = 3;
    do { if (p.lenLimit < minLenSKH) { MatchFinder_MovePos(p); num--; continue; } {
    ubyte *cur;
    uint *hash;
    uint *son;
    uint pos = p.pos;
    uint num2 = num;
    /* (p->pos == p->posLimit) is not allowed here !!! */
    { const uint rem = p.posLimit - pos; if (num2 > rem) num2 = rem; }
    num -= num2;
    { const uint cycPos = p.cyclicBufferPos;
      son = p.son + cycPos;
      p.cyclicBufferPos = cycPos + num2; }
    cur = p.buffer;
    hash = p.hash;
  do {
    uint curMatch;
    uint hv;

    mixin(HASH_ZIP_CALC);
    curMatch = hash[hv];
    hash[hv] = pos;

  //HC_SKIP_FOOTER
    cur++;  pos++;  *son++ = curMatch;
    } while (--num2);
    p.buffer = cur;
    p.pos = pos;
    if (pos == p.posLimit) MatchFinder_CheckLimits(p);
    }} while(num);
}


private void MatchFinder_CreateVTable (CMatchFinder* p, IMatchFinder2* vTable) @nogc {
  vTable.Init = cast(Mf_Init_Func)&MatchFinder_Init;
  vTable.GetNumAvailableBytes = cast(Mf_GetNumAvailableBytes_Func)&MatchFinder_GetNumAvailableBytes;
  vTable.GetPointerToCurrentPos = cast(Mf_GetPointerToCurrentPos_Func)&MatchFinder_GetPointerToCurrentPos;
  if (!p.btMode)
  {
    if (p.numHashBytes <= 4)
    {
      vTable.GetMatches = cast(Mf_GetMatches_Func)&Hc4_MatchFinder_GetMatches;
      vTable.Skip = cast(Mf_Skip_Func)&Hc4_MatchFinder_Skip;
    }
    else
    {
      vTable.GetMatches = cast(Mf_GetMatches_Func)&Hc5_MatchFinder_GetMatches;
      vTable.Skip = cast(Mf_Skip_Func)&Hc5_MatchFinder_Skip;
    }
  }
  else if (p.numHashBytes == 2)
  {
    vTable.GetMatches = cast(Mf_GetMatches_Func)&Bt2_MatchFinder_GetMatches;
    vTable.Skip = cast(Mf_Skip_Func)&Bt2_MatchFinder_Skip;
  }
  else if (p.numHashBytes == 3)
  {
    vTable.GetMatches = cast(Mf_GetMatches_Func)&Bt3_MatchFinder_GetMatches;
    vTable.Skip = cast(Mf_Skip_Func)&Bt3_MatchFinder_Skip;
  }
  else if (p.numHashBytes == 4)
  {
    vTable.GetMatches = cast(Mf_GetMatches_Func)&Bt4_MatchFinder_GetMatches;
    vTable.Skip = cast(Mf_Skip_Func)&Bt4_MatchFinder_Skip;
  }
  else
  {
    vTable.GetMatches = cast(Mf_GetMatches_Func)&Bt5_MatchFinder_GetMatches;
    vTable.Skip = cast(Mf_Skip_Func)&Bt5_MatchFinder_Skip;
  }
}


private void LzFindPrepare () @nogc {
}


// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
/* for good normalization speed we still reserve 256 MB before 4 GB range */
enum kLzmaMaxHistorySize = (cast(uint)15 << 28);

enum kNumTopBits = 24;
enum kTopValue = (cast(uint)1 << kNumTopBits);

enum kNumBitModelTotalBits = 11;
enum kBitModelTotal = (1 << kNumBitModelTotalBits);
enum kNumMoveBits = 5;
enum kProbInitValue = (kBitModelTotal >> 1);

enum kNumMoveReducingBits = 4;
enum kNumBitPriceShiftBits = 4;
// #define kBitPrice (1 << kNumBitPriceShiftBits)

enum REP_LEN_COUNT = 64;


//==========================================================================
//
//  LzmaEncProps_Init
//
//==========================================================================
public void LzmaEncProps_Init (CLzmaEncProps* p) @nogc {
  p.level = 5;
  p.dictSize = p.mc = 0;
  p.reduceSize = cast(ulong)cast(long)-1;
  p.lc = p.lp = p.pb = p.algo = p.fb = p.btMode = p.numHashBytes = -1;
  p.writeEndMark = 0;
}


//==========================================================================
//
//  LzmaEncProps_Normalize
//
//==========================================================================
public void LzmaEncProps_Normalize (CLzmaEncProps* p) @nogc {
  int level = p.level;
  if (level < 0) level = 5;
  p.level = level;

  if (p.dictSize == 0)
    p.dictSize =
      ( level <= 3 ? (cast(uint)1 << (level * 2 + 16)) :
      ( level <= 6 ? (cast(uint)1 << (level + 19)) :
      ( level <= 7 ? (cast(uint)1 << 25) : (cast(uint)1 << 26)
      )));

  if (p.dictSize > p.reduceSize)
  {
    uint v = cast(uint)p.reduceSize;
    const uint kReduceMin = (cast(uint)1 << 12);
    if (v < kReduceMin)
      v = kReduceMin;
    if (p.dictSize > v)
      p.dictSize = v;
  }

  if (p.lc < 0) p.lc = 3;
  if (p.lp < 0) p.lp = 0;
  if (p.pb < 0) p.pb = 2;

  if (p.algo < 0) p.algo = (level < 5 ? 0 : 1);
  if (p.fb < 0) p.fb = (level < 7 ? 32 : 64);
  if (p.btMode < 0) p.btMode = (p.algo == 0 ? 0 : 1);
  if (p.numHashBytes < 0) p.numHashBytes = (p.btMode ? 4 : 5);
  if (p.mc == 0) p.mc = (16 + (cast(uint)p.fb >> 1)) >> (p.btMode ? 0 : 1);
}


//==========================================================================
//
//  LzmaEncProps_GetDictSize
//
//==========================================================================
public uint LzmaEncProps_GetDictSize (const(CLzmaEncProps)* props2) @nogc {
  CLzmaEncProps props = *props2;
  LzmaEncProps_Normalize(&props);
  return props.dictSize;
}


/*
x86/x64:

BSR:
  IF (SRC == 0) ZF = 1, DEST is undefined;
                  AMD : DEST is unchanged;
  IF (SRC != 0) ZF = 0; DEST is index of top non-zero bit
  BSR is slow in some processors

LZCNT:
  IF (SRC  == 0) CF = 1, DEST is size_in_bits_of_register(src) (32 or 64)
  IF (SRC  != 0) CF = 0, DEST = num_lead_zero_bits
  IF (DEST == 0) ZF = 1;

LZCNT works only in new processors starting from Haswell.
if LZCNT is not supported by processor, then it's executed as BSR.
LZCNT can be faster than BSR, if supported.
*/

/+
//k8: define this for ARM (for some reason)
// #define LZMA_LOG_BSR

#ifdef LZMA_LOG_BSR

/*
  C code:                  : (30 - __builtin_clz(x))
    gcc9/gcc10 for x64 /x86  : 30 - (bsr(x) xor 31)
    clang10 for x64          : 31 + (bsr(x) xor -32)
*/

  #define MY_clz(x)  ((uint)__builtin_clz(x))
  // __lzcnt32
  // __builtin_ia32_lzcnt_u32


 #ifndef BSR2_RET

    #define BSR2_RET(pos, res) { uint zz = 30 - MY_clz(pos); \
      res = (zz + zz) + (pos >> zz); }

 #endif


uint GetPosSlot1(uint pos);
uint GetPosSlot1(uint pos)
{
  uint res;
  BSR2_RET(pos, res);
  return res;
}
#define GetPosSlot2(pos, res) { BSR2_RET(pos, res); }
#define GetPosSlot(pos, res) { if (pos < 2) res = pos; else BSR2_RET(pos, res); }


#else // ! LZMA_LOG_BSR
+/

enum kNumLogBits = (11 + usize.sizeof / 8 * 3);

enum kDicLogSizeMaxCompress = ((kNumLogBits - 1) * 2 + 7);

private void LzmaEnc_FastPosInit (ubyte* g_FastPos) @nogc {
  uint slot;
  g_FastPos[0] = 0;
  g_FastPos[1] = 1;
  g_FastPos += 2;

  for (slot = 2; slot < kNumLogBits * 2; slot++)
  {
    usize k = (cast(usize)1 << ((slot >> 1) - 1));
    usize j;
    for (j = 0; j < k; j++)
      g_FastPos[j] = cast(ubyte)slot;
    g_FastPos += k;
  }
}

/* we can use ((limit - pos) >> 31) only if (pos < ((uint)1 << 31)) */
/*
#define BSR2_RET(pos, res) { uint zz = 6 + ((kNumLogBits - 1) & \
  (0 - (((((uint)1 << (kNumLogBits + 6)) - 1) - pos) >> 31))); \
  res = p->g_FastPos[pos >> zz] + (zz * 2); }
*/

/*
#define BSR2_RET(pos, res) { uint zz = 6 + ((kNumLogBits - 1) & \
  (0 - (((((uint)1 << (kNumLogBits)) - 1) - (pos >> 6)) >> 31))); \
  res = p->g_FastPos[pos >> zz] + (zz * 2); }
*/

enum BSR2_RET(string pos, string res) = `
  { uint zz = (`~pos~` < (1 << (kNumLogBits + 6))) ? 6 : 6 + kNumLogBits - 1;
  `~res~` = p.g_FastPos[`~pos~` >> zz] + (zz * 2); }
`;

/*
#define BSR2_RET(pos, res) { res = (pos < (1 << (kNumLogBits + 6))) ? \
  p->g_FastPos[pos >> 6] + 12 : \
  p->g_FastPos[pos >> (6 + kNumLogBits - 1)] + (6 + (kNumLogBits - 1)) * 2; }
*/

enum GetPosSlot1(string pos) = `p.g_FastPos[`~pos~`]`;

enum GetPosSlot2(string pos, string res) = BSR2_RET!(pos, res);

enum GetPosSlot(string pos, string res) = `{
  if (`~pos~` < kNumFullDistances) `~res~` = p.g_FastPos[`~pos~` & (kNumFullDistances - 1)];
  else {`~BSR2_RET!(pos, res)~`}
}`;

//#endif // LZMA_LOG_BSR


enum LZMA_NUM_REPS = 4;

alias CState = ushort;
alias CExtra = ushort;

struct COptimal {
  uint price;
  CState state;
  CExtra extra;
      // 0   : normal
      // 1   : LIT : MATCH
      // > 1 : MATCH (extra-1) : LIT : REP0 (len)
  uint len;
  uint dist;
  uint[LZMA_NUM_REPS] reps;
}


// 18.06
enum kNumOpts = (1 << 11);
enum kPackReserve = (kNumOpts * 8);
// #define kNumOpts (1 << 12)
// #define kPackReserve (1 + kNumOpts * 2)

enum kNumLenToPosStates = 4;
enum kNumPosSlotBits = 6;
// #define kDicLogSizeMin 0
enum kDicLogSizeMax = 32;
enum kDistTableSizeMax = (kDicLogSizeMax * 2);

enum kNumAlignBits = 4;
enum kAlignTableSize = (1 << kNumAlignBits);
enum kAlignMask = (kAlignTableSize - 1);

enum kStartPosModelIndex = 4;
enum kEndPosModelIndex = 14;
enum kNumFullDistances = (1 << (kEndPosModelIndex >> 1));

enum LZMA_PB_MAX = 4;
enum LZMA_LC_MAX = 8;
enum LZMA_LP_MAX = 4;

enum LZMA_NUM_PB_STATES_MAX = (1 << LZMA_PB_MAX);

enum kLenNumLowBits = 3;
enum kLenNumLowSymbols = (1 << kLenNumLowBits);
enum kLenNumHighBits = 8;
enum kLenNumHighSymbols = (1 << kLenNumHighBits);
enum kLenNumSymbolsTotal = (kLenNumLowSymbols * 2 + kLenNumHighSymbols);

enum LZMA_MATCH_LEN_MIN = 2;
enum LZMA_MATCH_LEN_MAX = (LZMA_MATCH_LEN_MIN + kLenNumSymbolsTotal - 1);

enum kNumStates = 12;


struct CLenEnc {
  CLzmaProb[LZMA_NUM_PB_STATES_MAX << (kLenNumLowBits + 1)] low;
  CLzmaProb[kLenNumHighSymbols] high;
}


struct CLenPriceEnc {
  uint tableSize;
  uint[kLenNumSymbolsTotal][LZMA_NUM_PB_STATES_MAX] prices;
  // uint prices1[LZMA_NUM_PB_STATES_MAX][kLenNumLowSymbols * 2];
  // uint prices2[kLenNumSymbolsTotal];
}

enum GET_PRICE_LEN(string p, string posState, string len) =
    `((`~p~`).prices[`~posState~`][cast(usize)(`~len~`) - LZMA_MATCH_LEN_MIN])`;

/*
#define GET_PRICE_LEN(p, posState, len) \
    ((p)->prices2[(usize)(len) - 2] + ((p)->prices1[posState][((len) - 2) & (kLenNumLowSymbols * 2 - 1)] & (((len) - 2 - kLenNumLowSymbols * 2) >> 9)))
*/

struct CRangeEnc {
  uint range;
  uint cache;
  ulong low;
  ulong cacheSize;
  ubyte *buf;
  ubyte *bufLim;
  ubyte *bufBase;
  ISeqOutStream *outStream;
  ulong processed;
  SRes res;
}


struct CSaveState {
  CLzmaProb *litProbs;

  uint state;
  uint[LZMA_NUM_REPS] reps;

  CLzmaProb[1 << kNumAlignBits] posAlignEncoder;
  CLzmaProb[kNumStates] isRep;
  CLzmaProb[kNumStates] isRepG0;
  CLzmaProb[kNumStates] isRepG1;
  CLzmaProb[kNumStates] isRepG2;
  CLzmaProb[LZMA_NUM_PB_STATES_MAX][kNumStates] isMatch;
  CLzmaProb[LZMA_NUM_PB_STATES_MAX][kNumStates] isRep0Long;

  CLzmaProb[1 << kNumPosSlotBits][kNumLenToPosStates] posSlotEncoder;
  CLzmaProb[kNumFullDistances] posEncoders;

  CLenEnc lenProbs;
  CLenEnc repLenProbs;
}


alias CProbPrice = uint;
alias BoolInt = int;


struct CLzmaEnc {
  void *matchFinderObj;
  IMatchFinder2 matchFinder;

  uint optCur;
  uint optEnd;

  uint longestMatchLen;
  uint numPairs;
  uint numAvail;

  uint state;
  uint numFastBytes;
  uint additionalOffset;
  uint[LZMA_NUM_REPS] reps;
  uint lpMask, pbMask;
  CLzmaProb *litProbs;
  CRangeEnc rc;

  uint backRes;

  uint lc, lp, pb;
  uint lclp;

  BoolInt fastMode;
  BoolInt writeEndMark;
  BoolInt finished;
  BoolInt multiThread;
  BoolInt needInit;
  // BoolInt _maxMode;

  ulong nowPos64;

  uint matchPriceCount;
  // uint alignPriceCount;
  int repLenEncCounter;

  uint distTableSize;

  uint dictSize;
  SRes result;

  CMatchFinder matchFinderBase;


  // we suppose that we have 8-bytes alignment after CMatchFinder

  // LZ thread
  CProbPrice[kBitModelTotal >> kNumMoveReducingBits] ProbPrices;

  // we want {len , dist} pairs to be 8-bytes aligned in matches array
  uint[LZMA_MATCH_LEN_MAX * 2 + 2] matches;

  // we want 8-bytes alignment here
  uint[kAlignTableSize] alignPrices;
  uint[kDistTableSizeMax][kNumLenToPosStates] posSlotPrices;
  uint[kNumFullDistances][kNumLenToPosStates] distancesPrices;

  CLzmaProb[1 << kNumAlignBits] posAlignEncoder;
  CLzmaProb[kNumStates] isRep;
  CLzmaProb[kNumStates] isRepG0;
  CLzmaProb[kNumStates] isRepG1;
  CLzmaProb[kNumStates] isRepG2;
  CLzmaProb[LZMA_NUM_PB_STATES_MAX][kNumStates] isMatch;
  CLzmaProb[LZMA_NUM_PB_STATES_MAX][kNumStates] isRep0Long;
  CLzmaProb[1 << kNumPosSlotBits][kNumLenToPosStates] posSlotEncoder;
  CLzmaProb[kNumFullDistances] posEncoders;

  CLenEnc lenProbs;
  CLenEnc repLenProbs;

  //#ifndef LZMA_LOG_BSR
  ubyte[1 << kNumLogBits] g_FastPos;
  //#endif

  CLenPriceEnc lenEnc;
  CLenPriceEnc repLenEnc;

  COptimal[kNumOpts] opt;

  CSaveState saveState;

  // BoolInt mf_Failure;
}


//#define MFB (p.matchFinderBase)


//==========================================================================
//
//  LzmaEnc_SetProps
//
//==========================================================================
public SRes LzmaEnc_SetProps (CLzmaEncHandle pp, const(CLzmaEncProps)* props2) @nogc {
  CLzmaEnc *p = cast(CLzmaEnc *)pp;
  CLzmaEncProps props = *props2;
  LzmaEncProps_Normalize(&props);

  if (props.lc > LZMA_LC_MAX
      || props.lp > LZMA_LP_MAX
      || props.pb > LZMA_PB_MAX)
    return SZ_ERROR_PARAM;


  if (props.dictSize > kLzmaMaxHistorySize)
    props.dictSize = kLzmaMaxHistorySize;

  //#ifndef LZMA_LOG_BSR
  {
    const ulong dict64 = props.dictSize;
    if (dict64 > (cast(ulong)1 << kDicLogSizeMaxCompress))
      return SZ_ERROR_PARAM;
  }
  //#endif

  p.dictSize = props.dictSize;
  {
    uint fb = cast(uint)props.fb;
    if (fb < 5)
      fb = 5;
    if (fb > LZMA_MATCH_LEN_MAX)
      fb = LZMA_MATCH_LEN_MAX;
    p.numFastBytes = fb;
  }
  p.lc = cast(uint)props.lc;
  p.lp = cast(uint)props.lp;
  p.pb = cast(uint)props.pb;
  p.fastMode = (props.algo == 0);
  // p->_maxMode = True;
  (p.matchFinderBase).btMode = cast(ubyte)(props.btMode ? 1 : 0);
  {
    uint numHashBytes = 4;
    if (props.btMode)
    {
           if (props.numHashBytes <  2) numHashBytes = 2;
      else if (props.numHashBytes <  4) numHashBytes = cast(uint)props.numHashBytes;
    }
    if (props.numHashBytes >= 5) numHashBytes = 5;

    (p.matchFinderBase).numHashBytes = numHashBytes;
  }

  (p.matchFinderBase).cutValue = props.mc;

  p.writeEndMark = cast(BoolInt)props.writeEndMark;

  return SZ_OK;
}


//==========================================================================
//
//  LzmaEnc_SetDataSize
//
//==========================================================================
public void LzmaEnc_SetDataSize (CLzmaEncHandle pp, ulong expectedDataSiize) @nogc {
  CLzmaEnc *p = cast(CLzmaEnc *)pp;
  (p.matchFinderBase).expectedDataSize = expectedDataSiize;
}


enum kState_Start = 0;
enum kState_LitAfterMatch = 4;
enum kState_LitAfterRep   = 5;
enum kState_MatchAfterLit = 7;
enum kState_RepAfterLit   = 8;

immutable ubyte[kNumStates] kLiteralNextStates = [0, 0, 0, 0, 1, 2, 3, 4,  5,  6,   4, 5];
immutable ubyte[kNumStates] kMatchNextStates   = [7, 7, 7, 7, 7, 7, 7, 10, 10, 10, 10, 10];
immutable ubyte[kNumStates] kRepNextStates     = [8, 8, 8, 8, 8, 8, 8, 11, 11, 11, 11, 11];
immutable ubyte[kNumStates] kShortRepNextStates= [9, 9, 9, 9, 9, 9, 9, 11, 11, 11, 11, 11];

enum IsLitState(string s) = `((`~s~`) < 7)`;
enum GetLenToPosState2(string len) = `(((`~len~`) < kNumLenToPosStates - 1) ? (`~len~`) : kNumLenToPosStates - 1)`;
enum GetLenToPosState(string len) = `(((`~len~`) < kNumLenToPosStates + 1) ? (`~len~`) - 2 : kNumLenToPosStates - 1)`;

enum kInfinityPrice = (1 << 30);

private void RangeEnc_Construct (CRangeEnc* p) @nogc {
  p.outStream = null;
  p.bufBase = null;
}

enum RangeEnc_GetProcessed(string p) = `((`~p~`).processed + cast(usize)((`~p~`).buf - (`~p~`).bufBase) + (`~p~`).cacheSize)`;
enum RangeEnc_GetProcessed_sizet(string p) = `(cast(usize)(`~p~`).processed + cast(usize)((`~p~`).buf - (`~p~`).bufBase) + cast(usize)(`~p~`).cacheSize)`;

enum RC_BUF_SIZE = (1 << 16);

private int RangeEnc_Alloc (CRangeEnc *p, ISzAllocPtr alloc) {
  if (!p.bufBase)
  {
    p.bufBase = cast(ubyte *)ISzAlloc_Alloc(alloc, RC_BUF_SIZE);
    if (!p.bufBase)
      return 0;
    p.bufLim = p.bufBase + RC_BUF_SIZE;
  }
  return 1;
}

private void RangeEnc_Free (CRangeEnc *p, ISzAllocPtr alloc) {
  ISzAlloc_Free(alloc, p.bufBase);
  p.bufBase = null;
}


private void RangeEnc_Init (CRangeEnc* p) @nogc {
  /* Stream.Init(); */
  p.range = 0xFFFFFFFF;
  p.cache = 0;
  p.low = 0;
  p.cacheSize = 0;

  p.buf = p.bufBase;

  p.processed = 0;
  p.res = SZ_OK;
}

//MY_NO_INLINE
private void RangeEnc_FlushStream (CRangeEnc* p) {
  usize num;
  if (p.res != SZ_OK) {
    //k8: the following code line was absent, and it looks like
    //k8: the encoder could perform OOB writes on some incompressible data.
    //k8: most of the time it is harmless, but it's still a bug.
    p.buf = p.bufBase;
    return;
  }
  num = cast(usize)(p.buf - p.bufBase);

  /*
  if (num != ISeqOutStream_Write(p.outStream, p.bufBase, num))
    p.res = SZ_ERROR_WRITE;
  */
  if (num && p.outStream.Write(p.outStream, p.bufBase, num) != num) p.res = SZ_ERROR_WRITE;

  p.processed += num;
  p.buf = p.bufBase;
}

//MY_NO_INLINE
private void RangeEnc_ShiftLow(CRangeEnc *p)
{
  uint low = cast(uint)p.low;
  uint high = cast(uint)(p.low >> 32);
  p.low = cast(uint)(low << 8);
  if (low < cast(uint)0xFF000000 || high != 0)
  {
    {
      ubyte *buf = p.buf;
      *buf++ = cast(ubyte)(p.cache + high);
      p.cache = cast(uint)(low >> 24);
      p.buf = buf;
      if (buf == p.bufLim)
        RangeEnc_FlushStream(p);
      if (p.cacheSize == 0)
        return;
    }
    high += 0xFF;
    for (;;)
    {
      ubyte *buf = p.buf;
      *buf++ = cast(ubyte)(high);
      p.buf = buf;
      if (buf == p.bufLim)
        RangeEnc_FlushStream(p);
      if (--p.cacheSize == 0)
        return;
    }
  }
  p.cacheSize++;
}

private void RangeEnc_FlushData(CRangeEnc *p)
{
  int i;
  for (i = 0; i < 5; i++)
    RangeEnc_ShiftLow(p);
}

enum RC_NORM(string p) = `if (range < kTopValue) { range <<= 8; RangeEnc_ShiftLow(`~p~`); }`;

enum RC_BIT_PRE(string p, string prob) = `
  ttt = *(`~prob~`);
  newBound = (range >> kNumBitModelTotalBits) * ttt;
`;

// #define LZMA_ENC_USE_BRANCH

version(LZMA_ENC_USE_BRANCH) {

enum RC_BIT(string p, string prob, string bit) = `{
  `~RC_BIT_PRE!(p, prob)~`
  if (`~bit~` == 0) { range = newBound; ttt += (kBitModelTotal - ttt) >> kNumMoveBits; }
  else { (`~p~`).low += newBound; range -= newBound; ttt -= ttt >> kNumMoveBits; }
  *(`~prob~`) = cast(CLzmaProb)ttt;
  `~RC_NORM!p~`
  }
`;

} else {

enum RC_BIT(string p, string prob, string bit) = `{
  uint mask;
  `~RC_BIT_PRE!(p, prob)~`
  mask = 0 - cast(uint)`~bit~`;
  range &= mask;
  mask &= newBound;
  range -= mask;
  (`~p~`).low += mask;
  mask = cast(uint)`~bit~` - 1;
  range += newBound & mask;
  mask &= (kBitModelTotal - ((1 << kNumMoveBits) - 1));
  mask += ((1 << kNumMoveBits) - 1);
  ttt += cast(uint)(cast(int)(mask - ttt) >> kNumMoveBits);
  *(`~prob~`) = cast(CLzmaProb)ttt;
  `~RC_NORM!p~`
  }
`;

}


enum RC_BIT_0_BASE(string p, string prob) =
  `range = newBound; *(`~prob~`) = cast(CLzmaProb)(ttt + ((kBitModelTotal - ttt) >> kNumMoveBits));`;

enum RC_BIT_1_BASE(string p, string prob) =
  `range -= newBound; (`~p~`).low += newBound; *(`~prob~`) = cast(CLzmaProb)(ttt - (ttt >> kNumMoveBits));`;

enum RC_BIT_0(string p, string prob) =
  RC_BIT_0_BASE!(p, prob)~
  RC_NORM!(p);

enum RC_BIT_1(string p, string prob) =
  RC_BIT_1_BASE!(p, prob)~
  RC_NORM!(p);

private void RangeEnc_EncodeBit_0(CRangeEnc *p, CLzmaProb *prob)
{
  uint range, ttt, newBound;
  range = p.range;
  mixin(RC_BIT_PRE!("p", "prob"));
  mixin(RC_BIT_0!("p", "prob"));
  p.range = range;
}

private void LitEnc_Encode(CRangeEnc *p, CLzmaProb *probs, uint sym)
{
  uint range = p.range;
  sym |= 0x100;
  do
  {
    uint ttt, newBound;
    // RangeEnc_EncodeBit(p, probs + (sym >> 8), (sym >> 7) & 1);
    CLzmaProb *prob = probs + (sym >> 8);
    uint bit = (sym >> 7) & 1;
    sym <<= 1;
    mixin(RC_BIT!("p", "prob", "bit"));
  }
  while (sym < 0x10000);
  p.range = range;
}

private void LitEnc_EncodeMatched(CRangeEnc *p, CLzmaProb *probs, uint sym, uint matchByte)
{
  uint range = p.range;
  uint offs = 0x100;
  sym |= 0x100;
  do
  {
    uint ttt, newBound;
    CLzmaProb *prob;
    uint bit;
    matchByte <<= 1;
    // RangeEnc_EncodeBit(p, probs + (offs + (matchByte & offs) + (sym >> 8)), (sym >> 7) & 1);
    prob = probs + (offs + (matchByte & offs) + (sym >> 8));
    bit = (sym >> 7) & 1;
    sym <<= 1;
    offs &= ~(matchByte ^ sym);
    mixin(RC_BIT!("p", "prob", "bit"));
  }
  while (sym < 0x10000);
  p.range = range;
}



private void LzmaEnc_InitPriceTables(CProbPrice *ProbPrices)
{
  uint i;
  for (i = 0; i < (kBitModelTotal >> kNumMoveReducingBits); i++)
  {
    const uint kCyclesBits = kNumBitPriceShiftBits;
    uint w = (i << kNumMoveReducingBits) + (1 << (kNumMoveReducingBits - 1));
    uint bitCount = 0;
    uint j;
    for (j = 0; j < kCyclesBits; j++)
    {
      w = w * w;
      bitCount <<= 1;
      while (w >= (cast(uint)1 << 16))
      {
        w >>= 1;
        bitCount++;
      }
    }
    ProbPrices[i] = cast(CProbPrice)((cast(uint)kNumBitModelTotalBits << kCyclesBits) - 15 - bitCount);
    // printf("\n%3d: %5d", i, ProbPrices[i]);
  }
}


enum GET_PRICE(string prob, string bit) =
  `p.ProbPrices[((`~prob~`) ^ cast(uint)(((-cast(int)(`~bit~`))) & (kBitModelTotal - 1))) >> kNumMoveReducingBits]`;

enum GET_PRICEa(string prob, string bit) =
     `ProbPrices[((`~prob~`) ^ cast(uint)((-(cast(int)(`~bit~`))) & (kBitModelTotal - 1))) >> kNumMoveReducingBits]`;

enum GET_PRICE_0(string prob) = `p.ProbPrices[(`~prob~`) >> kNumMoveReducingBits]`;
enum GET_PRICE_1(string prob) = `p.ProbPrices[((`~prob~`) ^ (kBitModelTotal - 1)) >> kNumMoveReducingBits]`;

enum GET_PRICEa_0(string prob) = `ProbPrices[(`~prob~`) >> kNumMoveReducingBits]`;
enum GET_PRICEa_1(string prob) = `ProbPrices[((`~prob~`) ^ (kBitModelTotal - 1)) >> kNumMoveReducingBits]`;


private uint LitEnc_GetPrice(const CLzmaProb *probs, uint sym, const CProbPrice *ProbPrices)
{
  uint price = 0;
  sym |= 0x100;
  do
  {
    uint bit = sym & 1;
    sym >>= 1;
    price += mixin(GET_PRICEa!("probs[sym]", "bit"));
  }
  while (sym >= 2);
  return price;
}


private uint LitEnc_Matched_GetPrice(const CLzmaProb *probs, uint sym, uint matchByte, const CProbPrice *ProbPrices)
{
  uint price = 0;
  uint offs = 0x100;
  sym |= 0x100;
  do
  {
    matchByte <<= 1;
    price += mixin(GET_PRICEa!("probs[offs + (matchByte & offs) + (sym >> 8)]", "(sym >> 7) & 1"));
    sym <<= 1;
    offs &= ~(matchByte ^ sym);
  }
  while (sym < 0x10000);
  return price;
}


private void RcTree_ReverseEncode(CRangeEnc *rc, CLzmaProb *probs, uint numBits, uint sym)
{
  uint range = rc.range;
  uint m = 1;
  do
  {
    uint ttt, newBound;
    uint bit = sym & 1;
    // RangeEnc_EncodeBit(rc, probs + m, bit);
    sym >>= 1;
    mixin(RC_BIT!("rc", "probs + m", "bit"));
    m = (m << 1) | bit;
  }
  while (--numBits);
  rc.range = range;
}



private void LenEnc_Init (CLenEnc* p) @nogc {
  uint i;
  for (i = 0; i < (LZMA_NUM_PB_STATES_MAX << (kLenNumLowBits + 1)); i++)
    p.low[i] = kProbInitValue;
  for (i = 0; i < kLenNumHighSymbols; i++)
    p.high[i] = kProbInitValue;
}

private void LenEnc_Encode(CLenEnc *p, CRangeEnc *rc, uint sym, uint posState)
{
  uint range, ttt, newBound;
  CLzmaProb *probs = p.low.ptr;
  range = rc.range;
  mixin(RC_BIT_PRE!("rc", "probs"));
  if (sym >= kLenNumLowSymbols)
  {
    mixin(RC_BIT_1!("rc", "probs"));
    probs += kLenNumLowSymbols;
    mixin(RC_BIT_PRE!("rc", "probs"));
    if (sym >= kLenNumLowSymbols * 2)
    {
      mixin(RC_BIT_1!("rc", "probs"));
      rc.range = range;
      // RcTree_Encode(rc, p->high, kLenNumHighBits, sym - kLenNumLowSymbols * 2);
      LitEnc_Encode(rc, p.high.ptr, sym - kLenNumLowSymbols * 2);
      return;
    }
    sym -= kLenNumLowSymbols;
  }

  // RcTree_Encode(rc, probs + (posState << kLenNumLowBits), kLenNumLowBits, sym);
  {
    uint m;
    uint bit;
    mixin(RC_BIT_0!("rc", "probs"));
    probs += (posState << (1 + kLenNumLowBits));
    bit = (sym >> 2)    ; mixin(RC_BIT!("rc", "probs + 1", "bit")); m = (1 << 1) + bit;
    bit = (sym >> 1) & 1; mixin(RC_BIT!("rc", "probs + m", "bit")); m = (m << 1) + bit;
    bit =  sym       & 1; mixin(RC_BIT!("rc", "probs + m", "bit"));
    rc.range = range;
  }
}

private void SetPrices_3 (const(CLzmaProb)* probs, uint startPrice, uint* prices, const(CProbPrice)* ProbPrices) @nogc {
  uint i;
  for (i = 0; i < 8; i += 2)
  {
    uint price = startPrice;
    uint prob;
    price += mixin(GET_PRICEa!("probs[1           ]", "(i >> 2)"));
    price += mixin(GET_PRICEa!("probs[2 + (i >> 2)]", "(i >> 1) & 1"));
    prob = probs[4 + (i >> 1)];
    prices[i    ] = price + mixin(GET_PRICEa_0!"prob");
    prices[i + 1] = price + mixin(GET_PRICEa_1!"prob");
  }
}


//MY_NO_INLINE
private void LenPriceEnc_UpdateTables(
    CLenPriceEnc *p,
    uint numPosStates,
    const(CLenEnc)* enc,
    const(CProbPrice)* ProbPrices)
{
  uint b;

  {
    uint prob = enc.low[0];
    uint a, c;
    uint posState;
    b = mixin(GET_PRICEa_1!"prob");
    a = mixin(GET_PRICEa_0!"prob");
    c = b + mixin(GET_PRICEa_0!"enc.low[kLenNumLowSymbols]");
    for (posState = 0; posState < numPosStates; posState++)
    {
      uint *prices = p.prices[posState].ptr;
      const(CLzmaProb)* probs = enc.low.ptr + (posState << (1 + kLenNumLowBits));
      SetPrices_3(probs, a, prices, ProbPrices);
      SetPrices_3(probs + kLenNumLowSymbols, c, prices + kLenNumLowSymbols, ProbPrices);
    }
  }

  /*
  {
    uint i;
    uint b;
    a = GET_PRICEa_0(enc->low[0]);
    for (i = 0; i < kLenNumLowSymbols; i++)
      p->prices2[i] = a;
    a = GET_PRICEa_1(enc->low[0]);
    b = a + GET_PRICEa_0(enc->low[kLenNumLowSymbols]);
    for (i = kLenNumLowSymbols; i < kLenNumLowSymbols * 2; i++)
      p->prices2[i] = b;
    a += GET_PRICEa_1(enc->low[kLenNumLowSymbols]);
  }
  */

  // p->counter = numSymbols;
  // p->counter = 64;

  {
    uint i = p.tableSize;

    if (i > kLenNumLowSymbols * 2)
    {
      const(CLzmaProb)* probs = enc.high.ptr;
      uint *prices = p.prices[0].ptr + kLenNumLowSymbols * 2;
      i -= kLenNumLowSymbols * 2 - 1;
      i >>= 1;
      b += mixin(GET_PRICEa_1!"enc.low[kLenNumLowSymbols]");
      do
      {
        /*
        p->prices2[i] = a +
        // RcTree_GetPrice(enc->high, kLenNumHighBits, i - kLenNumLowSymbols * 2, ProbPrices);
        LitEnc_GetPrice(probs, i - kLenNumLowSymbols * 2, ProbPrices);
        */
        // uint price = a + RcTree_GetPrice(probs, kLenNumHighBits - 1, sym, ProbPrices);
        uint sym = --i + (1 << (kLenNumHighBits - 1));
        uint price = b;
        do
        {
          uint bit = sym & 1;
          sym >>= 1;
          price += mixin(GET_PRICEa!("probs[sym]", "bit"));
        }
        while (sym >= 2);

        {
          uint prob = probs[cast(usize)i + (1 << (kLenNumHighBits - 1))];
          prices[cast(usize)i * 2    ] = price + mixin(GET_PRICEa_0!"prob");
          prices[cast(usize)i * 2 + 1] = price + mixin(GET_PRICEa_1!"prob");
        }
      }
      while (i);

      {
        import core.stdc.string : memcpy;
        uint posState;
        usize num = (p.tableSize - kLenNumLowSymbols * 2) * (p.prices[0][0]).sizeof;
        for (posState = 1; posState < numPosStates; posState++)
          memcpy(p.prices[posState].ptr + kLenNumLowSymbols * 2, p.prices[0].ptr + kLenNumLowSymbols * 2, num);
      }
    }
  }
}

/*
  #ifdef SHOW_STAT
  g_STAT_OFFSET += num;
  printf("\n MovePos %u", num);
  #endif
*/

enum MOVE_POS1(string p, string num) = `{
    `~p~`.additionalOffset += (`~num~`);
    `~p~`.matchFinder.Skip(`~p~`.matchFinderObj, cast(uint)(`~num~`)); }
`;


private uint ReadMatchDistances(CLzmaEnc *p, uint *numPairsRes)
{
  uint numPairs;

  p.additionalOffset++;
  p.numAvail = p.matchFinder.GetNumAvailableBytes(p.matchFinderObj);
  {
    const uint *d = p.matchFinder.GetMatches(p.matchFinderObj, p.matches.ptr);
    // if (!d) { p->mf_Failure = True; *numPairsRes = 0;  return 0; }
    numPairs = cast(uint)(d - p.matches.ptr);
  }
  *numPairsRes = numPairs;

  /*
  #ifdef SHOW_STAT
  printf("\n i = %u numPairs = %u    ", g_STAT_OFFSET, numPairs / 2);
  g_STAT_OFFSET++;
  {
    uint i;
    for (i = 0; i < numPairs; i += 2)
      printf("%2u %6u   | ", p.matches[i], p.matches[i + 1]);
  }
  #endif
  */

  if (numPairs == 0)
    return 0;
  {
    const uint len = p.matches[cast(usize)numPairs - 2];
    if (len != p.numFastBytes)
      return len;
    {
      uint numAvail = p.numAvail;
      if (numAvail > LZMA_MATCH_LEN_MAX)
        numAvail = LZMA_MATCH_LEN_MAX;
      {
        const(ubyte)* p1 = p.matchFinder.GetPointerToCurrentPos(p.matchFinderObj) - 1;
        const(ubyte)* p2 = p1 + len;
        const ptrdiff_t dif = cast(ptrdiff_t)-1 - cast(ptrdiff_t)p.matches[cast(usize)numPairs - 1];
        const(ubyte)* lim = p1 + numAvail;
        for (; p2 != lim && *p2 == p2[dif]; p2++)
        {}
        return cast(uint)(p2 - p1);
      }
    }
  }
}

enum MARK_LIT = (cast(uint)cast(int)-1);

enum MakeAs_Lit(string p) = `{ (`~p~`).dist = MARK_LIT; (`~p~`).extra = 0; }`;
enum MakeAs_ShortRep(string p) = `{ (`~p~`).dist = 0; (`~p~`).extra = 0; }`;
enum IsShortRep(string p) = `((`~p~`).dist == 0)`;


enum GetPrice_ShortRep(string p, string state, string posState) =
  `( `~GET_PRICE_0!(p~`.isRepG0[`~state~`]`)~` + `~GET_PRICE_0!(p~`.isRep0Long[`~state~`][`~posState~`]`)~`)`;

enum GetPrice_Rep_0(string p, string state, string posState) = `(
    `~GET_PRICE_1!(p~`.isMatch[`~state~`][`~posState~`]`)~`
  + `~GET_PRICE_1!(p~`.isRep0Long[`~state~`][`~posState~`]`)~`)
  + `~GET_PRICE_1!(p~`.isRep[`~state~`]`)~`
  + `~GET_PRICE_0!(p~`.isRepG0[`~state~`]`);

//MY_FORCE_INLINE
private uint GetPrice_PureRep(const(CLzmaEnc)* p, uint repIndex, usize state, usize posState)
{
  uint price;
  uint prob = p.isRepG0[state];
  if (repIndex == 0)
  {
    price = mixin(GET_PRICE_0!"prob");
    price += mixin(GET_PRICE_1!"p.isRep0Long[state][posState]");
  }
  else
  {
    price = mixin(GET_PRICE_1!"prob");
    prob = p.isRepG1[state];
    if (repIndex == 1)
      price += mixin(GET_PRICE_0!"prob");
    else
    {
      price += mixin(GET_PRICE_1!"prob");
      price += mixin(GET_PRICE!("p.isRepG2[state]", "repIndex - 2"));
    }
  }
  return price;
}


private uint Backward(CLzmaEnc *p, uint cur)
{
  uint wr = cur + 1;
  p.optEnd = wr;

  for (;;)
  {
    uint dist = p.opt[cur].dist;
    uint len = cast(uint)p.opt[cur].len;
    uint extra = cast(uint)p.opt[cur].extra;
    cur -= len;

    if (extra)
    {
      wr--;
      p.opt[wr].len = cast(uint)len;
      cur -= extra;
      len = extra;
      if (extra == 1)
      {
        p.opt[wr].dist = dist;
        dist = MARK_LIT;
      }
      else
      {
        p.opt[wr].dist = 0;
        len--;
        wr--;
        p.opt[wr].dist = MARK_LIT;
        p.opt[wr].len = 1;
      }
    }

    if (cur == 0)
    {
      p.backRes = dist;
      p.optCur = wr;
      return len;
    }

    wr--;
    p.opt[wr].dist = dist;
    p.opt[wr].len = cast(uint)len;
  }
}



enum LIT_PROBS(string pos, string prevByte) =
  `(p.litProbs + cast(uint)3 * (((((`~pos~`) << 8) + (`~prevByte~`)) & p.lpMask) << p.lc))`;


private uint GetOptimum(CLzmaEnc *p, uint position)
{
  uint last, cur;
  uint[LZMA_NUM_REPS] reps;
  uint[LZMA_NUM_REPS] repLens;
  uint *matches;

  {
    uint numAvail;
    uint numPairs, mainLen, repMaxIndex, i, posState;
    uint matchPrice, repMatchPrice;
    const(ubyte)* data;
    ubyte curByte, matchByte;

    p.optCur = p.optEnd = 0;

    if (p.additionalOffset == 0)
      mainLen = ReadMatchDistances(p, &numPairs);
    else
    {
      mainLen = p.longestMatchLen;
      numPairs = p.numPairs;
    }

    numAvail = p.numAvail;
    if (numAvail < 2)
    {
      p.backRes = MARK_LIT;
      return 1;
    }
    if (numAvail > LZMA_MATCH_LEN_MAX)
      numAvail = LZMA_MATCH_LEN_MAX;

    data = p.matchFinder.GetPointerToCurrentPos(p.matchFinderObj) - 1;
    repMaxIndex = 0;

    for (i = 0; i < LZMA_NUM_REPS; i++)
    {
      uint len;
      const(ubyte)* data2;
      reps[i] = p.reps[i];
      data2 = data - reps[i];
      if (data[0] != data2[0] || data[1] != data2[1])
      {
        repLens[i] = 0;
        continue;
      }
      for (len = 2; len < numAvail && data[len] == data2[len]; len++)
      {}
      repLens[i] = len;
      if (len > repLens[repMaxIndex])
        repMaxIndex = i;
      if (len == LZMA_MATCH_LEN_MAX) // 21.03 : optimization
        break;
    }

    if (repLens[repMaxIndex] >= p.numFastBytes)
    {
      uint len;
      p.backRes = cast(uint)repMaxIndex;
      len = repLens[repMaxIndex];
      mixin(MOVE_POS1!("p", "len - 1"));
      return len;
    }

    matches = p.matches.ptr;
    //!#define MATCHES  matches
    // #define MATCHES  p->matches

    if (mainLen >= p.numFastBytes)
    {
      p.backRes = p.matches[cast(usize)numPairs - 1] + LZMA_NUM_REPS;
      mixin(MOVE_POS1!("p", "mainLen - 1"));
      return mainLen;
    }

    curByte = *data;
    matchByte = *(data - reps[0]);

    last = repLens[repMaxIndex];
    if (last <= mainLen)
      last = mainLen;

    if (last < 2 && curByte != matchByte)
    {
      p.backRes = MARK_LIT;
      return 1;
    }

    p.opt[0].state = cast(CState)p.state;

    posState = (position & p.pbMask);

    {
      const(CLzmaProb)* probs = mixin(LIT_PROBS!("position", "*(data - 1)"));
      p.opt[1].price = mixin(GET_PRICE_0!"p.isMatch[p.state][posState]") +
        (!mixin(IsLitState!"p.state") ?
          LitEnc_Matched_GetPrice(probs, curByte, matchByte, p.ProbPrices.ptr) :
          LitEnc_GetPrice(probs, curByte, p.ProbPrices.ptr));
    }

    mixin(MakeAs_Lit!"&p.opt[1]");

    matchPrice = mixin(GET_PRICE_1!"p.isMatch[p.state][posState]");
    repMatchPrice = matchPrice + mixin(GET_PRICE_1!"p.isRep[p.state]");

    // 18.06
    if (matchByte == curByte && repLens[0] == 0)
    {
      uint shortRepPrice = repMatchPrice + mixin(GetPrice_ShortRep!("p", "p.state", "posState"));
      if (shortRepPrice < p.opt[1].price)
      {
        p.opt[1].price = shortRepPrice;
        mixin(MakeAs_ShortRep!"&p.opt[1]");
      }
      if (last < 2)
      {
        p.backRes = p.opt[1].dist;
        return 1;
      }
    }

    p.opt[1].len = 1;

    p.opt[0].reps[0] = reps[0];
    p.opt[0].reps[1] = reps[1];
    p.opt[0].reps[2] = reps[2];
    p.opt[0].reps[3] = reps[3];

    // ---------- REP ----------

    for (i = 0; i < LZMA_NUM_REPS; i++)
    {
      uint repLen = repLens[i];
      uint price;
      if (repLen < 2)
        continue;
      price = repMatchPrice + GetPrice_PureRep(p, i, p.state, posState);
      do
      {
        uint price2 = price + mixin(GET_PRICE_LEN!("&p.repLenEnc", "posState", "repLen"));
        COptimal *opt = &p.opt[repLen];
        if (price2 < opt.price)
        {
          opt.price = price2;
          opt.len = cast(uint)repLen;
          opt.dist = cast(uint)i;
          opt.extra = 0;
        }
      }
      while (--repLen >= 2);
    }


    // ---------- MATCH ----------
    {
      uint len = repLens[0] + 1;
      if (len <= mainLen)
      {
        uint offs = 0;
        uint normalMatchPrice = matchPrice + mixin(GET_PRICE_0!"p.isRep[p.state]");

        if (len < 2)
          len = 2;
        else
          while (len > p.matches[offs])
            offs += 2;

        for (; ; len++)
        {
          COptimal *opt;
          uint dist = p.matches[cast(usize)offs + 1];
          uint price = normalMatchPrice + mixin(GET_PRICE_LEN!("&p.lenEnc", "posState", "len"));
          uint lenToPosState = mixin(GetLenToPosState!"len");

          if (dist < kNumFullDistances)
            price += p.distancesPrices[lenToPosState][dist & (kNumFullDistances - 1)];
          else
          {
            uint slot;
            mixin(GetPosSlot2!("dist", "slot"));
            price += p.alignPrices[dist & kAlignMask];
            price += p.posSlotPrices[lenToPosState][slot];
          }

          opt = &p.opt[len];

          if (price < opt.price)
          {
            opt.price = price;
            opt.len = cast(uint)len;
            opt.dist = dist + LZMA_NUM_REPS;
            opt.extra = 0;
          }

          if (len == p.matches[offs])
          {
            offs += 2;
            if (offs == numPairs)
              break;
          }
        }
      }
    }


    cur = 0;

    /+
    #ifdef SHOW_STAT2
    /* if (position >= 0) */
    {
      uint i;
      printf("\n pos = %4X", position);
      for (i = cur; i <= last; i++)
      printf("\nprice[%4X] = %u", position - cur + i, p.opt[i].price);
    }
    #endif
    +/
  }



  // ---------- Optimal Parsing ----------

  for (;;)
  {
    uint numAvail;
    uint numAvailFull;
    uint newLen, numPairs, prev, state, posState, startLen;
    uint litPrice, matchPrice, repMatchPrice;
    BoolInt nextIsLit;
    ubyte curByte, matchByte;
    const(ubyte)* data;
    COptimal *curOpt;
    COptimal *nextOpt;

    if (++cur == last)
      break;

    // 18.06
    if (cur >= kNumOpts - 64)
    {
      uint j, best;
      uint price = p.opt[cur].price;
      best = cur;
      for (j = cur + 1; j <= last; j++)
      {
        uint price2 = p.opt[j].price;
        if (price >= price2)
        {
          price = price2;
          best = j;
        }
      }
      {
        uint delta = best - cur;
        if (delta != 0)
        {
          mixin(MOVE_POS1!("p", "delta"));
        }
      }
      cur = best;
      break;
    }

    newLen = ReadMatchDistances(p, &numPairs);

    if (newLen >= p.numFastBytes)
    {
      p.numPairs = numPairs;
      p.longestMatchLen = newLen;
      break;
    }

    curOpt = &p.opt[cur];

    position++;

    // we need that check here, if skip_items in p->opt are possible
    /*
    if (curOpt->price >= kInfinityPrice)
      continue;
    */

    prev = cur - curOpt.len;

    if (curOpt.len == 1)
    {
      state = cast(uint)p.opt[prev].state;
      if (mixin(IsShortRep!"curOpt"))
        state = kShortRepNextStates[state];
      else
        state = kLiteralNextStates[state];
    }
    else
    {
      const(COptimal)* prevOpt;
      uint b0;
      uint dist = curOpt.dist;

      if (curOpt.extra)
      {
        prev -= cast(uint)curOpt.extra;
        state = kState_RepAfterLit;
        if (curOpt.extra == 1)
          state = (dist < LZMA_NUM_REPS ? kState_RepAfterLit : kState_MatchAfterLit);
      }
      else
      {
        state = cast(uint)p.opt[prev].state;
        if (dist < LZMA_NUM_REPS)
          state = kRepNextStates[state];
        else
          state = kMatchNextStates[state];
      }

      prevOpt = &p.opt[prev];
      b0 = prevOpt.reps[0];

      if (dist < LZMA_NUM_REPS)
      {
        if (dist == 0)
        {
          reps[0] = b0;
          reps[1] = prevOpt.reps[1];
          reps[2] = prevOpt.reps[2];
          reps[3] = prevOpt.reps[3];
        }
        else
        {
          reps[1] = b0;
          b0 = prevOpt.reps[1];
          if (dist == 1)
          {
            reps[0] = b0;
            reps[2] = prevOpt.reps[2];
            reps[3] = prevOpt.reps[3];
          }
          else
          {
            reps[2] = b0;
            reps[0] = prevOpt.reps[dist];
            reps[3] = prevOpt.reps[dist ^ 1];
          }
        }
      }
      else
      {
        reps[0] = (dist - LZMA_NUM_REPS + 1);
        reps[1] = b0;
        reps[2] = prevOpt.reps[1];
        reps[3] = prevOpt.reps[2];
      }
    }

    curOpt.state = cast(CState)state;
    curOpt.reps[0] = reps[0];
    curOpt.reps[1] = reps[1];
    curOpt.reps[2] = reps[2];
    curOpt.reps[3] = reps[3];

    data = p.matchFinder.GetPointerToCurrentPos(p.matchFinderObj) - 1;
    curByte = *data;
    matchByte = *(data - reps[0]);

    posState = (position & p.pbMask);

    /*
    The order of Price checks:
       <  LIT
       <= SHORT_REP
       <  LIT : REP_0
       <  REP    [ : LIT : REP_0 ]
       <  MATCH  [ : LIT : REP_0 ]
    */

    {
      uint curPrice = curOpt.price;
      uint prob = p.isMatch[state][posState];
      matchPrice = curPrice + mixin(GET_PRICE_1!"prob");
      litPrice = curPrice + mixin(GET_PRICE_0!"prob");
    }

    nextOpt = &p.opt[cast(usize)cur + 1];
    nextIsLit = 0/*False*/;

    // here we can allow skip_items in p->opt, if we don't check (nextOpt->price < kInfinityPrice)
    // 18.new.06
    if ((nextOpt.price < kInfinityPrice
        // && !mixin(IsLitState!"state")
        && matchByte == curByte)
        || litPrice > nextOpt.price
        )
      litPrice = 0;
    else
    {
      const(CLzmaProb)* probs = mixin(LIT_PROBS!("position", "*(data - 1)"));
      litPrice += (!mixin(IsLitState!"state") ?
          LitEnc_Matched_GetPrice(probs, curByte, matchByte, p.ProbPrices.ptr) :
          LitEnc_GetPrice(probs, curByte, p.ProbPrices.ptr));

      if (litPrice < nextOpt.price)
      {
        nextOpt.price = litPrice;
        nextOpt.len = 1;
        mixin(MakeAs_Lit!"nextOpt");
        nextIsLit = 1/*True*/;
      }
    }

    repMatchPrice = matchPrice + mixin(GET_PRICE_1!"p.isRep[state]");

    numAvailFull = p.numAvail;
    {
      uint temp = kNumOpts - 1 - cur;
      if (numAvailFull > temp)
        numAvailFull = cast(uint)temp;
    }

    // 18.06
    // ---------- SHORT_REP ----------
    if (mixin(IsLitState!"state")) // 18.new
    if (matchByte == curByte)
    if (repMatchPrice < nextOpt.price) // 18.new
    // if (numAvailFull < 2 || data[1] != *(data - reps[0] + 1))
    if (
        // nextOpt->price >= kInfinityPrice ||
        nextOpt.len < 2   // we can check nextOpt->len, if skip items are not allowed in p->opt
        || (nextOpt.dist != 0
            // && nextOpt->extra <= 1 // 17.old
            )
        )
    {
      uint shortRepPrice = repMatchPrice + mixin(GetPrice_ShortRep!("p", "state", "posState"));
      // if (shortRepPrice <= nextOpt->price) // 17.old
      if (shortRepPrice < nextOpt.price)  // 18.new
      {
        nextOpt.price = shortRepPrice;
        nextOpt.len = 1;
        mixin(MakeAs_ShortRep!"nextOpt");
        nextIsLit = 0/*False*/;
      }
    }

    if (numAvailFull < 2)
      continue;
    numAvail = (numAvailFull <= p.numFastBytes ? numAvailFull : p.numFastBytes);

    // numAvail <= p->numFastBytes

    // ---------- LIT : REP_0 ----------

    if (!nextIsLit
        && litPrice != 0 // 18.new
        && matchByte != curByte
        && numAvailFull > 2)
    {
      const ubyte *data2 = data - reps[0];
      if (data[1] == data2[1] && data[2] == data2[2])
      {
        uint len;
        uint limit = p.numFastBytes + 1;
        if (limit > numAvailFull)
          limit = numAvailFull;
        for (len = 3; len < limit && data[len] == data2[len]; len++)
        {}

        {
          uint state2 = kLiteralNextStates[state];
          uint posState2 = (position + 1) & p.pbMask;
          uint price = litPrice + mixin(GetPrice_Rep_0!("p", "state2", "posState2"));
          {
            uint offset = cur + len;

            if (last < offset)
              last = offset;

            // do
            {
              uint price2;
              COptimal *opt;
              len--;
              // price2 = price + GetPrice_Len_Rep_0(p, len, state2, posState2);
              price2 = price + mixin(GET_PRICE_LEN!("&p.repLenEnc", "posState2", "len"));

              opt = &p.opt[offset];
              // offset--;
              if (price2 < opt.price)
              {
                opt.price = price2;
                opt.len = cast(uint)len;
                opt.dist = 0;
                opt.extra = 1;
              }
            }
            // while (len >= 3);
          }
        }
      }
    }

    startLen = 2; /* speed optimization */

    {
      // ---------- REP ----------
      uint repIndex = 0; // 17.old
      // uint repIndex = mixin(IsLitState!"state") ? 0 : 1; // 18.notused
      for (; repIndex < LZMA_NUM_REPS; repIndex++)
      {
        uint len;
        uint price;
        const ubyte *data2 = data - reps[repIndex];
        if (data[0] != data2[0] || data[1] != data2[1])
          continue;

        for (len = 2; len < numAvail && data[len] == data2[len]; len++)
        {}

        // if (len < startLen) continue; // 18.new: speed optimization

        {
          uint offset = cur + len;
          if (last < offset)
            last = offset;
        }
        {
          uint len2 = len;
          price = repMatchPrice + GetPrice_PureRep(p, repIndex, state, posState);
          do
          {
            uint price2 = price + mixin(GET_PRICE_LEN!("&p.repLenEnc", "posState", "len2"));
            COptimal *opt = &p.opt[cur + len2];
            if (price2 < opt.price)
            {
              opt.price = price2;
              opt.len = cast(uint)len2;
              opt.dist = cast(uint)repIndex;
              opt.extra = 0;
            }
          }
          while (--len2 >= 2);
        }

        if (repIndex == 0) startLen = len + 1;  // 17.old
        // startLen = len + 1; // 18.new

        /* if (_maxMode) */
        {
          // ---------- REP : LIT : REP_0 ----------
          // numFastBytes + 1 + numFastBytes

          uint len2 = len + 1;
          uint limit = len2 + p.numFastBytes;
          if (limit > numAvailFull)
            limit = numAvailFull;

          len2 += 2;
          if (len2 <= limit)
          if (data[len2 - 2] == data2[len2 - 2])
          if (data[len2 - 1] == data2[len2 - 1])
          {
            uint state2 = kRepNextStates[state];
            uint posState2 = (position + len) & p.pbMask;
            price += mixin(GET_PRICE_LEN!("&p.repLenEnc", "posState", "len"))
                + mixin(GET_PRICE_0!"p.isMatch[state2][posState2]")
                + LitEnc_Matched_GetPrice(mixin(LIT_PROBS!("position + len", "data[cast(usize)len - 1]")),
                    data[len], data2[len], p.ProbPrices.ptr);

            // state2 = kLiteralNextStates[state2];
            state2 = kState_LitAfterRep;
            posState2 = (posState2 + 1) & p.pbMask;


            price += mixin(GetPrice_Rep_0!("p", "state2", "posState2"));

          for (; len2 < limit && data[len2] == data2[len2]; len2++)
          {}

          len2 -= len;
          // if (len2 >= 3)
          {
            {
              uint offset = cur + len + len2;

              if (last < offset)
                last = offset;
              // do
              {
                uint price2;
                COptimal *opt;
                len2--;
                // price2 = price + GetPrice_Len_Rep_0(p, len2, state2, posState2);
                price2 = price + mixin(GET_PRICE_LEN!("&p.repLenEnc", "posState2", "len2"));

                opt = &p.opt[offset];
                // offset--;
                if (price2 < opt.price)
                {
                  opt.price = price2;
                  opt.len = cast(uint)len2;
                  opt.extra = cast(CExtra)(len + 1);
                  opt.dist = cast(uint)repIndex;
                }
              }
              // while (len2 >= 3);
            }
          }
          }
        }
      }
    }


    // ---------- MATCH ----------
    /* for (uint len = 2; len <= newLen; len++) */
    if (newLen > numAvail)
    {
      newLen = numAvail;
      for (numPairs = 0; newLen > p.matches[numPairs]; numPairs += 2) {}
      p.matches[numPairs] = cast(uint)newLen;
      numPairs += 2;
    }

    // startLen = 2; /* speed optimization */

    if (newLen >= startLen)
    {
      uint normalMatchPrice = matchPrice + mixin(GET_PRICE_0!"p.isRep[state]");
      uint dist;
      uint offs, posSlot, len;

      {
        uint offset = cur + newLen;
        if (last < offset)
          last = offset;
      }

      offs = 0;
      while (startLen > p.matches[offs])
        offs += 2;
      dist = p.matches[cast(usize)offs + 1];

      // if (dist >= kNumFullDistances)
      mixin(GetPosSlot2!("dist", "posSlot"));

      for (len = /*2*/ startLen; ; len++)
      {
        uint price = normalMatchPrice + mixin(GET_PRICE_LEN!("&p.lenEnc", "posState", "len"));
        {
          COptimal *opt;
          uint lenNorm = len - 2;
          lenNorm = mixin(GetLenToPosState2!"lenNorm");
          if (dist < kNumFullDistances)
            price += p.distancesPrices[lenNorm][dist & (kNumFullDistances - 1)];
          else
            price += p.posSlotPrices[lenNorm][posSlot] + p.alignPrices[dist & kAlignMask];

          opt = &p.opt[cur + len];
          if (price < opt.price)
          {
            opt.price = price;
            opt.len = cast(uint)len;
            opt.dist = dist + LZMA_NUM_REPS;
            opt.extra = 0;
          }
        }

        if (len == p.matches[offs])
        {
          // if (p->_maxMode) {
          // MATCH : LIT : REP_0

          const ubyte *data2 = data - dist - 1;
          uint len2 = len + 1;
          uint limit = len2 + p.numFastBytes;
          if (limit > numAvailFull)
            limit = numAvailFull;

          len2 += 2;
          if (len2 <= limit)
          if (data[len2 - 2] == data2[len2 - 2])
          if (data[len2 - 1] == data2[len2 - 1])
          {
          for (; len2 < limit && data[len2] == data2[len2]; len2++)
          {}

          len2 -= len;

          // if (len2 >= 3)
          {
            uint state2 = kMatchNextStates[state];
            uint posState2 = (position + len) & p.pbMask;
            uint offset;
            price += mixin(GET_PRICE_0!"p.isMatch[state2][posState2]");
            price += LitEnc_Matched_GetPrice(mixin(LIT_PROBS!("position + len", "data[cast(usize)len - 1]")),
                    data[len], data2[len], p.ProbPrices.ptr);

            // state2 = kLiteralNextStates[state2];
            state2 = kState_LitAfterMatch;

            posState2 = (posState2 + 1) & p.pbMask;
            price += mixin(GetPrice_Rep_0!("p", "state2", "posState2"));

            offset = cur + len + len2;

            if (last < offset)
              last = offset;
            // do
            {
              uint price2;
              COptimal *opt;
              len2--;
              // price2 = price + GetPrice_Len_Rep_0(p, len2, state2, posState2);
              price2 = price + mixin(GET_PRICE_LEN!("&p.repLenEnc", "posState2", "len2"));
              opt = &p.opt[offset];
              // offset--;
              if (price2 < opt.price)
              {
                opt.price = price2;
                opt.len = cast(uint)len2;
                opt.extra = cast(CExtra)(len + 1);
                opt.dist = dist + LZMA_NUM_REPS;
              }
            }
            // while (len2 >= 3);
          }

          }

          offs += 2;
          if (offs == numPairs)
            break;
          dist = p.matches[cast(usize)offs + 1];
          // if (dist >= kNumFullDistances) {
            mixin(GetPosSlot2!("dist", "posSlot"));
          // }
        }
      }
    }
  }

  do
    p.opt[last].price = kInfinityPrice;
  while (--last);

  return Backward(p, cur);
}



enum ChangePair(string smallDist, string bigDist) = `(((`~bigDist~`) >> 7) > (`~smallDist~`))`;



private uint GetOptimumFast(CLzmaEnc *p)
{
  uint numAvail, mainDist;
  uint mainLen, numPairs, repIndex, repLen, i;
  const(ubyte)* data;

  if (p.additionalOffset == 0)
    mainLen = ReadMatchDistances(p, &numPairs);
  else
  {
    mainLen = p.longestMatchLen;
    numPairs = p.numPairs;
  }

  numAvail = p.numAvail;
  p.backRes = MARK_LIT;
  if (numAvail < 2)
    return 1;
  // if (mainLen < 2 && p->state == 0) return 1; // 18.06.notused
  if (numAvail > LZMA_MATCH_LEN_MAX)
    numAvail = LZMA_MATCH_LEN_MAX;
  data = p.matchFinder.GetPointerToCurrentPos(p.matchFinderObj) - 1;
  repLen = repIndex = 0;

  for (i = 0; i < LZMA_NUM_REPS; i++)
  {
    uint len;
    const ubyte *data2 = data - p.reps[i];
    if (data[0] != data2[0] || data[1] != data2[1])
      continue;
    for (len = 2; len < numAvail && data[len] == data2[len]; len++)
    {}
    if (len >= p.numFastBytes)
    {
      p.backRes = cast(uint)i;
      mixin(MOVE_POS1!("p", "len - 1"));
      return len;
    }
    if (len > repLen)
    {
      repIndex = i;
      repLen = len;
    }
  }

  if (mainLen >= p.numFastBytes)
  {
    p.backRes = p.matches[cast(usize)numPairs - 1] + LZMA_NUM_REPS;
    mixin(MOVE_POS1!("p", "mainLen - 1"));
    return mainLen;
  }

  mainDist = 0; /* for GCC */

  if (mainLen >= 2)
  {
    mainDist = p.matches[cast(usize)numPairs - 1];
    while (numPairs > 2)
    {
      uint dist2;
      if (mainLen != p.matches[cast(usize)numPairs - 4] + 1)
        break;
      dist2 = p.matches[cast(usize)numPairs - 3];
      if (!mixin(ChangePair!("dist2", "mainDist")))
        break;
      numPairs -= 2;
      mainLen--;
      mainDist = dist2;
    }
    if (mainLen == 2 && mainDist >= 0x80)
      mainLen = 1;
  }

  if (repLen >= 2)
    if (    repLen + 1 >= mainLen
        || (repLen + 2 >= mainLen && mainDist >= (1 << 9))
        || (repLen + 3 >= mainLen && mainDist >= (1 << 15)))
  {
    p.backRes = cast(uint)repIndex;
    mixin(MOVE_POS1!("p", "repLen - 1"));
    return repLen;
  }

  if (mainLen < 2 || numAvail <= 2)
    return 1;

  {
    uint len1 = ReadMatchDistances(p, &p.numPairs);
    p.longestMatchLen = len1;

    if (len1 >= 2)
    {
      uint newDist = p.matches[cast(usize)p.numPairs - 1];
      if (   (len1 >= mainLen && newDist < mainDist)
          || (len1 == mainLen + 1 && !mixin(ChangePair!("mainDist", "newDist")))
          || (len1 >  mainLen + 1)
          || (len1 + 1 >= mainLen && mainLen >= 3 && mixin(ChangePair!("newDist", "mainDist"))))
        return 1;
    }
  }

  data = p.matchFinder.GetPointerToCurrentPos(p.matchFinderObj) - 1;

  for (i = 0; i < LZMA_NUM_REPS; i++)
  {
    uint len, limit;
    const ubyte *data2 = data - p.reps[i];
    if (data[0] != data2[0] || data[1] != data2[1])
      continue;
    limit = mainLen - 1;
    for (len = 2;; len++)
    {
      if (len >= limit)
        return 1;
      if (data[len] != data2[len])
        break;
    }
  }

  p.backRes = mainDist + LZMA_NUM_REPS;
  if (mainLen != 2)
  {
    mixin(MOVE_POS1!("p", "mainLen - 2"));
  }
  return mainLen;
}




private void WriteEndMarker(CLzmaEnc *p, uint posState)
{
  uint range;
  range = p.rc.range;
  {
    uint ttt, newBound;
    CLzmaProb *prob = &p.isMatch[p.state][posState];
    mixin(RC_BIT_PRE!("&p.rc", "prob"));
    mixin(RC_BIT_1!("&p.rc", "prob"));
    prob = &p.isRep[p.state];
    mixin(RC_BIT_PRE!("&p.rc", "prob"));
    mixin(RC_BIT_0!("&p.rc", "prob"));
  }
  p.state = kMatchNextStates[p.state];

  p.rc.range = range;
  LenEnc_Encode(&p.lenProbs, &p.rc, 0, posState);
  range = p.rc.range;

  {
    // RcTree_Encode_PosSlot(&p->rc, p->posSlotEncoder[0], (1 << kNumPosSlotBits) - 1);
    CLzmaProb *probs = p.posSlotEncoder[0].ptr;
    uint m = 1;
    do
    {
      uint ttt, newBound;
      mixin(RC_BIT_PRE!("p", "probs + m"));
      mixin(RC_BIT_1!("&p.rc", "probs + m"));
      m = (m << 1) + 1;
    }
    while (m < (1 << kNumPosSlotBits));
  }
  {
    // RangeEnc_EncodeDirectBits(&p->rc, ((uint)1 << (30 - kNumAlignBits)) - 1, 30 - kNumAlignBits);    uint range = p->range;
    uint numBits = 30 - kNumAlignBits;
    do
    {
      range >>= 1;
      p.rc.low += range;
      mixin(RC_NORM!"&p.rc");
    }
    while (--numBits);
  }

  {
    // RcTree_ReverseEncode(&p->rc, p->posAlignEncoder, kNumAlignBits, kAlignMask);
    CLzmaProb *probs = p.posAlignEncoder.ptr;
    uint m = 1;
    do
    {
      uint ttt, newBound;
      mixin(RC_BIT_PRE!("p", "probs + m"));
      mixin(RC_BIT_1!("&p.rc", "probs + m"));
      m = (m << 1) + 1;
    }
    while (m < kAlignTableSize);
  }
  p.rc.range = range;
}


private SRes CheckErrors(CLzmaEnc *p)
{
  if (p.result != SZ_OK)
    return p.result;
  if (p.rc.res != SZ_OK)
    p.result = SZ_ERROR_WRITE;

  if ((p.matchFinderBase).result != SZ_OK)
    p.result = SZ_ERROR_READ;

  if (p.result != SZ_OK)
    p.finished = 1/*True*/;
  return p.result;
}


//MY_NO_INLINE
private SRes Flush(CLzmaEnc *p, uint nowPos)
{
  /* ReleaseMFStream(); */
  p.finished = 1/*True*/;
  if (p.writeEndMark)
    WriteEndMarker(p, nowPos & p.pbMask);
  RangeEnc_FlushData(&p.rc);
  RangeEnc_FlushStream(&p.rc);
  return CheckErrors(p);
}


//MY_NO_INLINE
private void FillAlignPrices(CLzmaEnc *p)
{
  uint i;
  const CProbPrice *ProbPrices = p.ProbPrices.ptr;
  const CLzmaProb *probs = p.posAlignEncoder.ptr;
  // p->alignPriceCount = 0;
  for (i = 0; i < kAlignTableSize / 2; i++)
  {
    uint price = 0;
    uint sym = i;
    uint m = 1;
    uint bit;
    uint prob;
    bit = sym & 1; sym >>= 1; price += mixin(GET_PRICEa!("probs[m]", "bit")); m = (m << 1) + bit;
    bit = sym & 1; sym >>= 1; price += mixin(GET_PRICEa!("probs[m]", "bit")); m = (m << 1) + bit;
    bit = sym & 1; sym >>= 1; price += mixin(GET_PRICEa!("probs[m]", "bit")); m = (m << 1) + bit;
    prob = probs[m];
    p.alignPrices[i    ] = price + mixin(GET_PRICEa_0!"prob");
    p.alignPrices[i + 8] = price + mixin(GET_PRICEa_1!"prob");
    // p->alignPrices[i] = RcTree_ReverseGetPrice(p->posAlignEncoder, kNumAlignBits, i, p->ProbPrices);
  }
}


//MY_NO_INLINE
private void FillDistancesPrices(CLzmaEnc *p)
{
  // int y; for (y = 0; y < 100; y++) {

  uint[kNumFullDistances] tempPrices;
  uint i, lps;

  const CProbPrice *ProbPrices = p.ProbPrices.ptr;
  p.matchPriceCount = 0;

  for (i = kStartPosModelIndex / 2; i < kNumFullDistances / 2; i++)
  {
    uint posSlot = mixin(GetPosSlot1!"i");
    uint footerBits = (posSlot >> 1) - 1;
    uint base = ((2 | (posSlot & 1)) << footerBits);
    const(CLzmaProb)* probs = p.posEncoders.ptr + cast(usize)base * 2;
    // tempPrices[i] = RcTree_ReverseGetPrice(p->posEncoders + base, footerBits, i - base, p->ProbPrices);
    uint price = 0;
    uint m = 1;
    uint sym = i;
    uint offset = cast(uint)1 << footerBits;
    base += i;

    if (footerBits)
    do
    {
      uint bit = sym & 1;
      sym >>= 1;
      price += mixin(GET_PRICEa!("probs[m]", "bit"));
      m = (m << 1) + bit;
    }
    while (--footerBits);

    {
      uint prob = probs[m];
      tempPrices[base         ] = price + mixin(GET_PRICEa_0!"prob");
      tempPrices[base + offset] = price + mixin(GET_PRICEa_1!"prob");
    }
  }

  for (lps = 0; lps < kNumLenToPosStates; lps++)
  {
    uint slot;
    uint distTableSize2 = (p.distTableSize + 1) >> 1;
    uint *posSlotPrices = p.posSlotPrices[lps].ptr;
    const CLzmaProb *probs = p.posSlotEncoder[lps].ptr;

    for (slot = 0; slot < distTableSize2; slot++)
    {
      // posSlotPrices[slot] = RcTree_GetPrice(encoder, kNumPosSlotBits, slot, p->ProbPrices);
      uint price;
      uint bit;
      uint sym = slot + (1 << (kNumPosSlotBits - 1));
      uint prob;
      bit = sym & 1; sym >>= 1; price  = mixin(GET_PRICEa!("probs[sym]", "bit"));
      bit = sym & 1; sym >>= 1; price += mixin(GET_PRICEa!("probs[sym]", "bit"));
      bit = sym & 1; sym >>= 1; price += mixin(GET_PRICEa!("probs[sym]", "bit"));
      bit = sym & 1; sym >>= 1; price += mixin(GET_PRICEa!("probs[sym]", "bit"));
      bit = sym & 1; sym >>= 1; price += mixin(GET_PRICEa!("probs[sym]", "bit"));
      prob = probs[cast(usize)slot + (1 << (kNumPosSlotBits - 1))];
      posSlotPrices[cast(usize)slot * 2    ] = price + mixin(GET_PRICEa_0!"prob");
      posSlotPrices[cast(usize)slot * 2 + 1] = price + mixin(GET_PRICEa_1!"prob");
    }

    {
      uint delta = (cast(uint)((kEndPosModelIndex / 2 - 1) - kNumAlignBits) << kNumBitPriceShiftBits);
      for (slot = kEndPosModelIndex / 2; slot < distTableSize2; slot++)
      {
        posSlotPrices[cast(usize)slot * 2    ] += delta;
        posSlotPrices[cast(usize)slot * 2 + 1] += delta;
        delta += (cast(uint)1 << kNumBitPriceShiftBits);
      }
    }

    {
      uint *dp = p.distancesPrices[lps].ptr;

      dp[0] = posSlotPrices[0];
      dp[1] = posSlotPrices[1];
      dp[2] = posSlotPrices[2];
      dp[3] = posSlotPrices[3];

      for (i = 4; i < kNumFullDistances; i += 2)
      {
        uint slotPrice = posSlotPrices[mixin(GetPosSlot1!"i")];
        dp[i    ] = slotPrice + tempPrices[i];
        dp[i + 1] = slotPrice + tempPrices[i + 1];
      }
    }
  }
  // }
}



private void LzmaEnc_Construct(CLzmaEnc *p)
{
  RangeEnc_Construct(&p.rc);
  MatchFinder_Construct(&(p.matchFinderBase));

  {
    CLzmaEncProps props;
    LzmaEncProps_Init(&props);
    LzmaEnc_SetProps(p, &props);
  }

  //#ifndef LZMA_LOG_BSR
  LzmaEnc_FastPosInit(p.g_FastPos.ptr);
  //#endif

  LzmaEnc_InitPriceTables(p.ProbPrices.ptr);
  p.litProbs = null;
  p.saveState.litProbs = null;
}


//==========================================================================
//
//  LzmaEnc_Create
//
//==========================================================================
public CLzmaEncHandle LzmaEnc_Create (ISzAllocPtr alloc) {
  void *p = ISzAlloc_Alloc(alloc, CLzmaEnc.sizeof);
  if (p) LzmaEnc_Construct(cast(CLzmaEnc *)p);
  return p;
}


private void LzmaEnc_FreeLits(CLzmaEnc *p, ISzAllocPtr alloc)
{
  ISzAlloc_Free(alloc, p.litProbs);
  ISzAlloc_Free(alloc, p.saveState.litProbs);
  p.litProbs = null;
  p.saveState.litProbs = null;
}

private void LzmaEnc_Destruct(CLzmaEnc *p, ISzAllocPtr alloc, ISzAllocPtr allocBig)
{
  MatchFinder_Free(&(p.matchFinderBase), allocBig);
  LzmaEnc_FreeLits(p, alloc);
  RangeEnc_Free(&p.rc, alloc);
}


//==========================================================================
//
//  LzmaEnc_Destroy
//
//==========================================================================
public void LzmaEnc_Destroy (CLzmaEncHandle p, ISzAllocPtr alloc, ISzAllocPtr allocBig) {
  if (p !is null) {
    LzmaEnc_Destruct(cast(CLzmaEnc *)p, alloc, allocBig);
    ISzAlloc_Free(alloc, p);
  }
}


//MY_NO_INLINE
private SRes LzmaEnc_CodeOneBlock(CLzmaEnc *p, uint maxPackSize, uint maxUnpackSize)
{
  uint nowPos32, startPos32;
  if (p.needInit)
  {
    p.matchFinder.Init(p.matchFinderObj);
    p.needInit = 0;
  }

  if (p.finished)
    return p.result;
  int rinres = CheckErrors(p);
  if (rinres != 0) return rinres;

  nowPos32 = cast(uint)p.nowPos64;
  startPos32 = nowPos32;

  if (p.nowPos64 == 0)
  {
    uint numPairs;
    ubyte curByte;
    if (p.matchFinder.GetNumAvailableBytes(p.matchFinderObj) == 0)
      return Flush(p, nowPos32);
    ReadMatchDistances(p, &numPairs);
    RangeEnc_EncodeBit_0(&p.rc, &p.isMatch[kState_Start][0]);
    // p->state = kLiteralNextStates[p->state];
    curByte = *(p.matchFinder.GetPointerToCurrentPos(p.matchFinderObj) - p.additionalOffset);
    LitEnc_Encode(&p.rc, p.litProbs, curByte);
    p.additionalOffset--;
    nowPos32++;
  }

  if (p.matchFinder.GetNumAvailableBytes(p.matchFinderObj) != 0)

  for (;;)
  {
    uint dist;
    uint len, posState;
    uint range, ttt, newBound;
    CLzmaProb *probs;

    if (p.fastMode)
      len = GetOptimumFast(p);
    else
    {
      uint oci = p.optCur;
      if (p.optEnd == oci)
        len = GetOptimum(p, nowPos32);
      else
      {
        const COptimal *opt = &p.opt[oci];
        len = opt.len;
        p.backRes = opt.dist;
        p.optCur = oci + 1;
      }
    }

    posState = cast(uint)nowPos32 & p.pbMask;
    range = p.rc.range;
    probs = &p.isMatch[p.state][posState];

    mixin(RC_BIT_PRE!("&p.rc", "probs"));

    dist = p.backRes;

    /*
    #ifdef SHOW_STAT2
    printf("\n pos = %6X, len = %3u  pos = %6u", nowPos32, len, dist);
    #endif
    */

    if (dist == MARK_LIT)
    {
      ubyte curByte;
      const(ubyte)* data;
      uint state;

      mixin(RC_BIT_0!("&p.rc", "probs"));
      p.rc.range = range;
      data = p.matchFinder.GetPointerToCurrentPos(p.matchFinderObj) - p.additionalOffset;
      probs = mixin(LIT_PROBS!("nowPos32", "*(data - 1)"));
      curByte = *data;
      state = p.state;
      p.state = kLiteralNextStates[state];
      if (mixin(IsLitState!"state"))
        LitEnc_Encode(&p.rc, probs, curByte);
      else
        LitEnc_EncodeMatched(&p.rc, probs, curByte, *(data - p.reps[0]));
    }
    else
    {
      mixin(RC_BIT_1!("&p.rc", "probs"));
      probs = &p.isRep[p.state];
      mixin(RC_BIT_PRE!("&p.rc", "probs"));

      if (dist < LZMA_NUM_REPS)
      {
        mixin(RC_BIT_1!("&p.rc", "probs"));
        probs = &p.isRepG0[p.state];
        mixin(RC_BIT_PRE!("&p.rc", "probs"));
        if (dist == 0)
        {
          mixin(RC_BIT_0!("&p.rc", "probs"));
          probs = &p.isRep0Long[p.state][posState];
          mixin(RC_BIT_PRE!("&p.rc", "probs"));
          if (len != 1)
          {
            mixin(RC_BIT_1_BASE!("&p.rc", "probs"));
          }
          else
          {
            mixin(RC_BIT_0_BASE!("&p.rc", "probs"));
            p.state = kShortRepNextStates[p.state];
          }
        }
        else
        {
          mixin(RC_BIT_1!("&p.rc", "probs"));
          probs = &p.isRepG1[p.state];
          mixin(RC_BIT_PRE!("&p.rc", "probs"));
          if (dist == 1)
          {
            mixin(RC_BIT_0_BASE!("&p.rc", "probs"));
            dist = p.reps[1];
          }
          else
          {
            mixin(RC_BIT_1!("&p.rc", "probs"));
            probs = &p.isRepG2[p.state];
            mixin(RC_BIT_PRE!("&p.rc", "probs"));
            if (dist == 2)
            {
              mixin(RC_BIT_0_BASE!("&p.rc", "probs"));
              dist = p.reps[2];
            }
            else
            {
              mixin(RC_BIT_1_BASE!("&p.rc", "probs"));
              dist = p.reps[3];
              p.reps[3] = p.reps[2];
            }
            p.reps[2] = p.reps[1];
          }
          p.reps[1] = p.reps[0];
          p.reps[0] = dist;
        }

        mixin(RC_NORM!"&p.rc");

        p.rc.range = range;

        if (len != 1)
        {
          LenEnc_Encode(&p.repLenProbs, &p.rc, len - LZMA_MATCH_LEN_MIN, posState);
          --p.repLenEncCounter;
          p.state = kRepNextStates[p.state];
        }
      }
      else
      {
        uint posSlot;
        mixin(RC_BIT_0!("&p.rc", "probs"));
        p.rc.range = range;
        p.state = kMatchNextStates[p.state];

        LenEnc_Encode(&p.lenProbs, &p.rc, len - LZMA_MATCH_LEN_MIN, posState);
        // --p->lenEnc.counter;

        dist -= LZMA_NUM_REPS;
        p.reps[3] = p.reps[2];
        p.reps[2] = p.reps[1];
        p.reps[1] = p.reps[0];
        p.reps[0] = dist + 1;

        p.matchPriceCount++;
        mixin(GetPosSlot!("dist", "posSlot"));
        // RcTree_Encode_PosSlot(&p->rc, p->posSlotEncoder[mixin(GetLenToPosState!"len")], posSlot);
        {
          uint sym = cast(uint)posSlot + (1 << kNumPosSlotBits);
          range = p.rc.range;
          probs = p.posSlotEncoder[mixin(GetLenToPosState!"len")].ptr;
          do
          {
            CLzmaProb *prob = probs + (sym >> kNumPosSlotBits);
            uint bit = (sym >> (kNumPosSlotBits - 1)) & 1;
            sym <<= 1;
            mixin(RC_BIT!("&p.rc", "prob", "bit"));
          }
          while (sym < (1 << kNumPosSlotBits * 2));
          p.rc.range = range;
        }

        if (dist >= kStartPosModelIndex)
        {
          uint footerBits = ((posSlot >> 1) - 1);

          if (dist < kNumFullDistances)
          {
            uint base = ((2 | (posSlot & 1)) << footerBits);
            RcTree_ReverseEncode(&p.rc, p.posEncoders.ptr + base, footerBits, cast(uint)(dist /* - base */));
          }
          else
          {
            uint pos2 = (dist | 0xF) << (32 - footerBits);
            range = p.rc.range;
            // RangeEnc_EncodeDirectBits(&p->rc, posReduced >> kNumAlignBits, footerBits - kNumAlignBits);
            /*
            do
            {
              range >>= 1;
              p->rc.low += range & (0 - ((dist >> --footerBits) & 1));
              RC_NORM(&p->rc)
            }
            while (footerBits > kNumAlignBits);
            */
            do
            {
              range >>= 1;
              p.rc.low += range & (0 - (pos2 >> 31));
              pos2 += pos2;
              mixin(RC_NORM!"&p.rc");
            }
            while (pos2 != 0xF0000000);


            // RcTree_ReverseEncode(&p->rc, p->posAlignEncoder, kNumAlignBits, posReduced & kAlignMask);

            {
              uint m = 1;
              uint bit;
              bit = dist & 1; dist >>= 1; mixin(RC_BIT!("&p.rc", "p.posAlignEncoder.ptr + m", "bit")); m = (m << 1) + bit;
              bit = dist & 1; dist >>= 1; mixin(RC_BIT!("&p.rc", "p.posAlignEncoder.ptr + m", "bit")); m = (m << 1) + bit;
              bit = dist & 1; dist >>= 1; mixin(RC_BIT!("&p.rc", "p.posAlignEncoder.ptr + m", "bit")); m = (m << 1) + bit;
              bit = dist & 1;             mixin(RC_BIT!("&p.rc", "p.posAlignEncoder.ptr + m", "bit"));
              p.rc.range = range;
              // p->alignPriceCount++;
            }
          }
        }
      }
    }

    nowPos32 += cast(uint)len;
    p.additionalOffset -= len;

    if (p.additionalOffset == 0)
    {
      uint processed;

      if (!p.fastMode)
      {
        /*
        if (p->alignPriceCount >= 16) // kAlignTableSize
          FillAlignPrices(p);
        if (p->matchPriceCount >= 128)
          FillDistancesPrices(p);
        if (p->lenEnc.counter <= 0)
          LenPriceEnc_UpdateTables(&p->lenEnc, 1 << p->pb, &p->lenProbs, p->ProbPrices);
        */
        if (p.matchPriceCount >= 64)
        {
          FillAlignPrices(p);
          // { int y; for (y = 0; y < 100; y++) {
          FillDistancesPrices(p);
          // }}
          LenPriceEnc_UpdateTables(&p.lenEnc, cast(uint)1 << p.pb, &p.lenProbs, p.ProbPrices.ptr);
        }
        if (p.repLenEncCounter <= 0)
        {
          p.repLenEncCounter = REP_LEN_COUNT;
          LenPriceEnc_UpdateTables(&p.repLenEnc, cast(uint)1 << p.pb, &p.repLenProbs, p.ProbPrices.ptr);
        }
      }

      if (p.matchFinder.GetNumAvailableBytes(p.matchFinderObj) == 0)
        break;
      processed = nowPos32 - startPos32;

      if (maxPackSize)
      {
        if (processed + kNumOpts + 300 >= maxUnpackSize
            || mixin(RangeEnc_GetProcessed_sizet!"&p.rc") + kPackReserve >= maxPackSize)
          break;
      }
      else if (processed >= (1 << 17))
      {
        p.nowPos64 += nowPos32 - startPos32;
        return CheckErrors(p);
      }
    }
  }

  p.nowPos64 += nowPos32 - startPos32;
  return Flush(p, nowPos32);
}



enum kBigHashDicLimit = (cast(uint)1 << 24);

private SRes LzmaEnc_Alloc(CLzmaEnc *p, uint keepWindowSize, ISzAllocPtr alloc, ISzAllocPtr allocBig)
{
  uint beforeSize = kNumOpts;
  uint dictSize;

  if (!RangeEnc_Alloc(&p.rc, alloc))
    return SZ_ERROR_MEM;

  {
    uint lclp = p.lc + p.lp;
    if (!p.litProbs || !p.saveState.litProbs || p.lclp != lclp)
    {
      LzmaEnc_FreeLits(p, alloc);
      p.litProbs = cast(CLzmaProb *)ISzAlloc_Alloc(alloc, (cast(uint)0x300 << lclp) * CLzmaProb.sizeof);
      p.saveState.litProbs = cast(CLzmaProb *)ISzAlloc_Alloc(alloc, (cast(uint)0x300 << lclp) * CLzmaProb.sizeof);
      if (!p.litProbs || !p.saveState.litProbs)
      {
        LzmaEnc_FreeLits(p, alloc);
        return SZ_ERROR_MEM;
      }
      p.lclp = lclp;
    }
  }

  (p.matchFinderBase).bigHash = cast(ubyte)(p.dictSize > kBigHashDicLimit ? 1 : 0);


  dictSize = p.dictSize;
  if (dictSize == (cast(uint)2 << 30) ||
      dictSize == (cast(uint)3 << 30))
  {
    /* 21.03 : here we reduce the dictionary for 2 reasons:
       1) we don't want 32-bit back_distance matches in decoder for 2 GB dictionary.
       2) we want to elimate useless last MatchFinder_Normalize3() for corner cases,
          where data size is aligned for 1 GB: 5/6/8 GB.
          That reducing must be >= 1 for such corner cases. */
    dictSize -= 1;
  }

  if (beforeSize + dictSize < keepWindowSize)
    beforeSize = keepWindowSize - dictSize;

  /* in worst case we can look ahead for
        max(LZMA_MATCH_LEN_MAX, numFastBytes + 1 + numFastBytes) bytes.
     we send larger value for (keepAfter) to MantchFinder_Create():
        (numFastBytes + LZMA_MATCH_LEN_MAX + 1)
  */

  if (!MatchFinder_Create(&(p.matchFinderBase), dictSize, beforeSize,
      p.numFastBytes, LZMA_MATCH_LEN_MAX + 1 /* 21.03 */
      , allocBig))
    return SZ_ERROR_MEM;
  p.matchFinderObj = &(p.matchFinderBase);
  MatchFinder_CreateVTable(&(p.matchFinderBase), &p.matchFinder);

  return SZ_OK;
}

private void LzmaEnc_Init(CLzmaEnc *p)
{
  uint i;
  p.state = 0;
  p.reps[0] =
  p.reps[1] =
  p.reps[2] =
  p.reps[3] = 1;

  RangeEnc_Init(&p.rc);

  for (i = 0; i < (1 << kNumAlignBits); i++)
    p.posAlignEncoder[i] = kProbInitValue;

  for (i = 0; i < kNumStates; i++)
  {
    uint j;
    for (j = 0; j < LZMA_NUM_PB_STATES_MAX; j++)
    {
      p.isMatch[i][j] = kProbInitValue;
      p.isRep0Long[i][j] = kProbInitValue;
    }
    p.isRep[i] = kProbInitValue;
    p.isRepG0[i] = kProbInitValue;
    p.isRepG1[i] = kProbInitValue;
    p.isRepG2[i] = kProbInitValue;
  }

  {
    for (i = 0; i < kNumLenToPosStates; i++)
    {
      CLzmaProb *probs = p.posSlotEncoder[i].ptr;
      uint j;
      for (j = 0; j < (1 << kNumPosSlotBits); j++)
        probs[j] = kProbInitValue;
    }
  }
  {
    for (i = 0; i < kNumFullDistances; i++)
      p.posEncoders[i] = kProbInitValue;
  }

  {
    uint num = cast(uint)0x300 << (p.lp + p.lc);
    uint k;
    CLzmaProb *probs = p.litProbs;
    for (k = 0; k < num; k++)
      probs[k] = kProbInitValue;
  }


  LenEnc_Init(&p.lenProbs);
  LenEnc_Init(&p.repLenProbs);

  p.optEnd = 0;
  p.optCur = 0;

  {
    for (i = 0; i < kNumOpts; i++)
      p.opt[i].price = kInfinityPrice;
  }

  p.additionalOffset = 0;

  p.pbMask = (cast(uint)1 << p.pb) - 1;
  p.lpMask = (cast(uint)0x100 << p.lp) - (cast(uint)0x100 >> p.lc);

  // p->mf_Failure = False;
}


private void LzmaEnc_InitPrices(CLzmaEnc *p)
{
  if (!p.fastMode)
  {
    FillDistancesPrices(p);
    FillAlignPrices(p);
  }

  p.lenEnc.tableSize =
  p.repLenEnc.tableSize =
      p.numFastBytes + 1 - LZMA_MATCH_LEN_MIN;

  p.repLenEncCounter = REP_LEN_COUNT;

  LenPriceEnc_UpdateTables(&p.lenEnc, cast(uint)1 << p.pb, &p.lenProbs, p.ProbPrices.ptr);
  LenPriceEnc_UpdateTables(&p.repLenEnc, cast(uint)1 << p.pb, &p.repLenProbs, p.ProbPrices.ptr);
}

private SRes LzmaEnc_AllocAndInit(CLzmaEnc *p, uint keepWindowSize, ISzAllocPtr alloc, ISzAllocPtr allocBig)
{
  uint i;
  for (i = kEndPosModelIndex / 2; i < kDicLogSizeMax; i++)
    if (p.dictSize <= (cast(uint)1 << i))
      break;
  p.distTableSize = i * 2;

  p.finished = 0/*False*/;
  p.result = SZ_OK;
  int rinres = LzmaEnc_Alloc(p, keepWindowSize, alloc, allocBig);
  if (rinres != 0) return rinres;
  LzmaEnc_Init(p);
  LzmaEnc_InitPrices(p);
  p.nowPos64 = 0;
  return SZ_OK;
}

private SRes LzmaEnc_Prepare(CLzmaEncHandle pp, ISeqOutStream *outStream, ISeqInStream *inStream,
    ISzAllocPtr alloc, ISzAllocPtr allocBig)
{
  CLzmaEnc *p = cast(CLzmaEnc *)pp;
  (p.matchFinderBase).stream = inStream;
  p.needInit = 1;
  p.rc.outStream = outStream;
  return LzmaEnc_AllocAndInit(p, 0, alloc, allocBig);
}

private void LzmaEnc_SetInputBuf(CLzmaEnc *p, const ubyte *src, usize srcLen)
{
  (p.matchFinderBase).directInput = 1;
  (p.matchFinderBase).bufferBase = cast(ubyte *)src;
  (p.matchFinderBase).directInputRem = srcLen;
}

private SRes LzmaEnc_MemPrepare(CLzmaEncHandle pp, const ubyte *src, usize srcLen,
    uint keepWindowSize, ISzAllocPtr alloc, ISzAllocPtr allocBig)
{
  CLzmaEnc *p = cast(CLzmaEnc *)pp;
  LzmaEnc_SetInputBuf(p, src, srcLen);
  p.needInit = 1;

  LzmaEnc_SetDataSize(pp, srcLen);
  return LzmaEnc_AllocAndInit(p, keepWindowSize, alloc, allocBig);
}

private void LzmaEnc_Finish(CLzmaEncHandle pp)
{
  //(void)pp;
}


enum SanityMagic = 0xb0f0debeU;

struct CLzmaEnc_SeqOutStreamBuf {
  ISeqOutStream vt;
  version(LZMA_SANITY_MAGIC) {
    usize magic;
  }
  ubyte *data;
  usize rem;
  BoolInt overflow;
}


/*
#define MY_offsetof(type, m) ((size_t)&(((type *)0)->m))
#define MY_container_of(ptr, type, m) ((type *)(void *)((char *)(void *)(1 ? (ptr) : &((type *)0)->m) - MY_offsetof(type, m)))
#define CONTAINER_FROM_VTBL(ptr, type, m) MY_container_of(ptr, type, m)
*/

private usize SeqOutStreamBuf_Write(ISeqOutStream* pp, const(void)* data, usize size) nothrow
{
  //CLzmaEnc_SeqOutStreamBuf *p = CONTAINER_FROM_VTBL(pp, CLzmaEnc_SeqOutStreamBuf, vt);
  CLzmaEnc_SeqOutStreamBuf* p = cast(CLzmaEnc_SeqOutStreamBuf*)(cast(ubyte*)pp-CLzmaEnc_SeqOutStreamBuf.vt.offsetof);
  version(LZMA_SANITY_MAGIC) {
    assert(p.magic == SanityMagic);
  }
  if (p.overflow) return 0; // ignore all writes
  if (p.rem < size) {
    size = p.rem;
    p.overflow = 1/*True*/;
    if (size == 0) return 0;
  }
  import core.stdc.string : memcpy;
  memcpy(p.data, data, size);
  p.rem -= size;
  p.data += size;
  return size;
}


//MY_NO_INLINE
private SRes LzmaEnc_Encode2(CLzmaEnc *p, ICompressProgress *progress)
{
  SRes res = SZ_OK;

  for (;;)
  {
    res = LzmaEnc_CodeOneBlock(p, 0, 0);
    if (res != SZ_OK || p.finished)
      break;
    if (progress)
    {
      //res = ICompressProgress_Progress(progress, p.nowPos64, mixin(RangeEnc_GetProcessed!"&p.rc"));
      res = progress.Progress(progress, p.nowPos64, mixin(RangeEnc_GetProcessed!"&p.rc"));
      if (res != SZ_OK)
      {
        res = SZ_ERROR_PROGRESS;
        break;
      }
    }
  }

  LzmaEnc_Finish(p);

  /*
  if (res == SZ_OK && !Inline_MatchFinder_IsFinishedOK(&(p.matchFinderBase)))
    res = SZ_ERROR_FAIL;
  }
  */

  return res;
}


//==========================================================================
//
//  LzmaEnc_Encode
//
//==========================================================================
public SRes LzmaEnc_Encode (CLzmaEncHandle pp, ISeqOutStream *outStream, ISeqInStream *inStream,
                            ICompressProgress *progress, ISzAllocPtr alloc, ISzAllocPtr allocBig)
{
  int rinres = LzmaEnc_Prepare(pp, outStream, inStream, alloc, allocBig);
  if (rinres != 0) return rinres;
  return LzmaEnc_Encode2(cast(CLzmaEnc *)pp, progress);
}


//==========================================================================
//
//  LzmaEnc_WriteProperties
//
//==========================================================================
public SRes LzmaEnc_WriteProperties (CLzmaEncHandle pp, ubyte* props, uint* size) @nogc {
  if (*size < LZMA_PROPS_SIZE) return SZ_ERROR_PARAM;
  *size = LZMA_PROPS_SIZE;

  const(CLzmaEnc)* p = cast(const(CLzmaEnc)*)pp;
  immutable uint dictSize = p.dictSize;
  uint v;
  props[0] = cast(ubyte)((p.pb * 5 + p.lp) * 9 + p.lc);

  // we write aligned dictionary value to properties for lzma decoder
  if (dictSize >= (cast(uint)1 << 21)) {
    const uint kDictMask = (cast(uint)1 << 20) - 1;
    v = (dictSize + kDictMask) & ~kDictMask;
    if (v < dictSize) v = dictSize;
  } else {
    uint i = 11 * 2;
    do {
      v = cast(uint)(2 + (i & 1)) << (i >> 1);
      i++;
    } while (v < dictSize);
  }

  SetUi32(props + 1, v);
  return SZ_OK;
}


//==========================================================================
//
//  LzmaEnc_IsWriteEndMark
//
//==========================================================================
public uint LzmaEnc_IsWriteEndMark (CLzmaEncHandle pp) @nogc {
  return cast(uint)(cast(CLzmaEnc *)pp).writeEndMark;
}


//==========================================================================
//
//  LzmaEnc_MemEncode
//
//==========================================================================
public SRes LzmaEnc_MemEncode (CLzmaEncHandle pp, ubyte* dest, usize* destLen, const(ubyte)* src, usize srcLen,
                               int writeEndMark, ICompressProgress *progress, ISzAllocPtr alloc, ISzAllocPtr allocBig)
{
  SRes res;
  CLzmaEnc *p = cast(CLzmaEnc *)pp;

  CLzmaEnc_SeqOutStreamBuf outStream;
  version(LZMA_SANITY_MAGIC) {
    outStream.magic = SanityMagic;
  }

  //outStream.vt.Write = &SeqOutStreamBuf_Write;
  outStream.vt.Write = delegate usize (ISeqOutStream* pp, const(void)* data, usize size) nothrow {
    return SeqOutStreamBuf_Write(pp, data, size);
  };

  outStream.data = dest;
  outStream.rem = *destLen;
  outStream.overflow = 0/*False*/;

  p.writeEndMark = writeEndMark;
  p.rc.outStream = &outStream.vt;

  res = LzmaEnc_MemPrepare(pp, src, srcLen, 0, alloc, allocBig);

  if (res == SZ_OK)
  {
    res = LzmaEnc_Encode2(p, progress);
    if (res == SZ_OK && p.nowPos64 != srcLen)
      res = SZ_ERROR_FAIL;
  }

  *destLen -= outStream.rem;
  if (outStream.overflow)
    return SZ_ERROR_OUTPUT_EOF;
  return res;
}


//==========================================================================
//
//  LzmaEncode
//
//==========================================================================
public SRes LzmaEncode (ubyte* dest, usize* destLen, const(ubyte)* src, usize srcLen,
                        const(CLzmaEncProps)* props, ubyte* propsEncoded, uint* propsSize,
                        int writeEndMark, ICompressProgress *progress, ISzAllocPtr alloc, ISzAllocPtr allocBig)
{
  CLzmaEnc *p = cast(CLzmaEnc *)LzmaEnc_Create(alloc);
  SRes res;
  if (!p)
    return SZ_ERROR_MEM;

  res = LzmaEnc_SetProps(p, props);
  if (res == SZ_OK)
  {
    res = LzmaEnc_WriteProperties(p, propsEncoded, propsSize);
    if (res == SZ_OK)
      res = LzmaEnc_MemEncode(p, dest, destLen, src, srcLen,
          writeEndMark, progress, alloc, allocBig);
  }

  LzmaEnc_Destroy(p, alloc, allocBig);
  return res;
}
